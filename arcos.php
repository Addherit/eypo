<?php
	include "db/Utils.php";
	session_start();
	$_SESSION['usuario'];
	$empleadoVentas = $_SESSION['usuario'];
	$folioGet = "";	

	$sql = "SELECT Nombre, SegundoNombre, Apellidos, CodigoPosicion FROM EYPO . dbo . IV_EY_PV_Usuarios WHERE[User] = '$empleadoVentas'";

	$consulta = sqlsrv_query($conn, $sql);
	$Row = sqlsrv_fetch_array($consulta);
	$CodigoPosicion = $Row['CodigoPosicion'];
	$nombreCompleto = $Row['Nombre']." ".$Row['SegundoNombre']." ".$Row['Apellidos'];
	$hoy = date('Y-m-d');

	if(!empty($_GET['folio'])){
		$folioGet = $_GET['folio'];
	}
	$sql = "SELECT TOP 1 FolioIndependiente FROM dbEypo.dbo.arcos WHERE empresa = 'eypo' ORDER BY FolioIndependiente DESC";
	$consultasql = sqlsrv_query($conn, $sql);
	$Row = sqlsrv_fetch_array($consultasql);
	$numRes = sqlsrv_num_rows($consultasql);
	$folio = $Row['FolioIndependiente'];
	$folio++;
	if ($numRes) {
		$folio = 0;
	}
				


?>
<!DOCTYPE html>
<html lang="en">
<?php include "header.php"?>
<body>
	<?php include "nav.php"?>
	<?php include "modales.php"; ?>
	<div class="container formato-font-design" id="contenedorDePagina">
		<br>
		<div class="mensaje"><div class="col-12"></div></div>	
		<div class="row" style="margin-bottom: 5px;">
			<div class="col-md-12" style="background-color: #005580; text-align: center;">
				<img src="img/tituloEypo.png" alt="EYPO" style="max-width: 20%">
			</div>
			<input type="hidden" id="empresa" value="eypo" >
		</div>
		<div class="row" style="color: white; background-color: #336599; margin-bottom: 15px">			
			<div class="col-8">
				<b style="font-size: 1.3rem; line-height: 2.1">FORMATO DE ARMADO/DESARMADO</b>
			</div>
			<div class="col-4 text-right">
				<?php include "btnEncaArcos.php"; ?>
			</div>	
		</div>		
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<label class="col-4 col-form-label">Fecha Solicitud: </label>
					<div class="col-6">
						<input id="dateInicial" type="date" value="<?php echo $hoy ?>"	>				        
					</div>
				</div>
				<div class="row">
					<label class="col-4 col-form-label">Fecha Entrega Requerida: </label>
					<div class="col-6">
						<input id="dateFinal" type="date">
					</div>
				</div>
				<div class="row">
					<label class="col-4 col-form-label">Nombre Solicitante:</label>
					<div class="col-6">
						<input type="text" value="<?php echo $nombreCompleto ?>" id="nombreSolicitante">
					</div>
				</div>
				<div class="row">
					<label class="col-4 col-form-label">Nombre Proyecto:</label>
					<div class="col-6">
						<input type="text" value="" id="nombreProyecto">
					</div>
				</div>
				<div class="row">
					<label class="col-4 col-form-label">Orden Venta SAP:</label>
					<div class="col-6">
						<input type="text" id="numeroDeOrden">						
					</div>
				</div>
			</div>			
			<div class="col-md-6">
				<div class="row">
					<label for="folio_ord_fab" class="col-4 col-lg-3 offset-lg-3 col-form-label">Folio:</label>
					<div class="col-6 col-md-8 col-lg-6">
						<input type="text" class="text-right" id="folio_ord_fab" value="<?php echo "$folio"?>" disabled>
					</div>
				</div>
				<div class="row">
					<label class="col-4 col-lg-3 offset-lg-3 col-form-label">Estatus:</label>
					<div class="col-6 col-md-8 col-lg-6">
						<input id="estatusOrdenesFabricacion" type="text" disabled>				        
					</div>									
				</div>
				<div class="row">
					<label class="col-4 col-lg-3 offset-lg-3 col-form-label">Código del cliente:</label>
					<div class="col-6 col-md-8 col-lg-6">
						<input type="text" id="codigoCliente">						
						<a id="btnCliente" href="#" data-toggle="modal" data-target="#myModal" class="ocultarAlCte">
							<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
						</a>
					</div>
				</div>
				<div class="row">
					<label class="col-4 col-lg-3 offset-lg-3 col-form-label">Nombre Cliente SAP: </label>
					<div class="col-6 col-md-8 col-lg-6">
						<input type="text" value="" id="nombreCliente">
						<a href="#" data-toggle="modal" data-target="#myModal" class="ocultarAlCte">
							<i class="fas fa-search" style="color: #57b4ea" title="Búsqueda Nombre"></i>
						</a>
					</div>
				</div>
				<div class="row">
					<label class="col-4 col-lg-3 offset-lg-3 col-form-label">Cant a Fabricar (letra): </label>
					<div class="col-6 col-md-8 col-lg-6">
						<input type="text" value="" id="cantidadFabricar">
					</div>
				</div>
				<div class="row">
					<label class="col-4 col-lg-3 offset-lg-3 col-form-label">Entregas Parciales Aceptadas Especifique: </label>
					<div class="col-6 col-md-8 col-lg-6">
						<select name="entregasParciales" id="entregasParciales">
							<option value="" disabled selected>Seleccione</option>
							<option value="si">Si</option>
							<option value="no">No</option>
						</select>
					</div>
				</div>
			</div>

		</div> 
		<br>
		<div class="row">
			<div class="col-md-12 text-center">
				<span style="background-color: #44ade9; color: white; font-weight: bold;">Armado</span>
			</div>
		</div>
		<div class="row datosEnc">
			<div class="col-sm-12">
				<div class="row">
					<label class="col-sm-2 col-form-label" >Producto a Fabricar (Detallado):</label>
					<div class="col-sm-8">
						<input type="text" id="productosFabricar" style="width: 20%">
						
						<input type="text" value="" id="productosFabricarNombre" style="width: 65%">
						<a id="btnArticuloArmado" href="#" data-toggle="modal" data-target="#myModalArticulos" class="ocultarAlCte">
							<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
						</a>
						<a href="#" id="quitar_armado" style="color: red">
							<i class="fas fa-times-circle"></i> Quitar articulo
						</a>
					</div>
				</div>			
				<div class="row">
					<label class="col-sm-2 col-form-label">Subproductos: </label>
					<div class="col-sm-9">
						<table class="table table-sm table-bordered table-striped text-center" id="tblSubproductoArmado">
							<thead>
								<th>Código</th>
								<th>Nombre del producto</th>
								<th>Piezas</th>
								<th>Unidad</th>
								<th style="display:none">Armado</th>
							</thead>
							<tbody>
								<tr style="height: 28px">
									<td class="codigoArt" contenteditable="true"></td>
									<td class="nombreArt" contenteditable="true"></td>
									<td class="piezas" contenteditable="true"></td>
									<td class="unidad" contenteditable="true"></td>
									<td class="armadoDesarmado" style="display:none">A</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<br>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<span style="background-color: green; color: white; font-weight: bold;">Desarmado</span>
			</div>
		</div>
		<div class="row datosEnc">
			<div class="col-md-12">
				<div class="row">
					<label class="col-sm-2 col-form-label">Producto a Desarmar (Detallado):</label>
					<div class="col-sm-8">
						<input type="text" id="productosDesarmar" style="width: 20%">
						
						<input type="text" value="" id="productosDesarmarNombre" style="width: 65%">
						<a id="btnArticuloDesarmado" href="#"  data-toggle="modal" data-target="#myModalArticulos" class="ocultarAlCte">
							<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
						</a>
						<a href="#" id="quitar_desarmado" style="color: red">
							<i class="fas fa-times-circle"></i> Quitar articulo
						</a>
					</div>
				</div>
				<div class="row">
			      	<label class="col-sm-2 col-form-label">Subproductos: </label>
			      	<div class="col-sm-9">
			        	<table class="table table-sm table-bordered table-striped text-center" id="tblSubproductoDesarmado">
			        		<thead>
			        			<th>Código</th>
			        			<th>Nombre del producto</th>
								<th>Piezas</th>
								<th>Unidad</th>
								<th style="display:none">Desarmado</th>
			        		</thead>
			        		<tbody>
			        			<tr style="height: 28px">
			        				<td class="codigoArt" contenteditable="true"></td>
			        				<td class="nombreArt" contenteditable="true"></td>
									<td class="piezas" contenteditable="true"></td>
									<td class="unidad" contenteditable="true"></td>
									<td class="armadoDesarmado" style="display:none">D</td>
			        			</tr>
			        		</tbody>
			        	</table>
			      	</div>
			    </div>
			</div>
		</div>
			        
		<div class="row datosEnc">
			<div class="col-md-6">			
				<label class="col-md-3 col-form-label">Observaciones:</label>
				<div class="col-md-9">
					<textarea id="observaciones" cols="80" rows="4" style="background-color: #ffff002e"></textarea>
				</div>			   
				<br>
			</div>

			<div class="col-md-6 soloVendedores">				
				<label class="col-sm-3 col-form-label" >Logistica: </label>
				<div class="col-sm-9">
					<select id="logistica" style="height: 22px; width: 74%">
						<option value="" selected disabled>Selecciona una opción</option>
						<option value="clienteRecoge">1. Cliente recoge </option>
						<option value="flete">2. Flete</option>
						<option value="eypoRecoge">3. EYPO recoge</option>
						<option value="plantaEmbarca">4. Planta embarca</option>
					</select>
				</div><br>				
				<div class="col-md-12">
					<span>Comentarios extras:</span><br>
					<textarea id="comentariosLogistica" cols="50" rows="2" style="background-color:#ffff002e"></textarea>
				</div><br>					
				<div class="col-sm-6">		
					<button class="btn btn-block btn-sm" style="background-color: orange;" id="btnEstatusCerrar">CERRAR</button>
				</div>				
			</div>	

			<div class="col-md-6 soloCoordinadorPlanta">								
				<label class="col-sm-12 col-form-label col-form-label-sm"><b>Entrega Parcial:</b></label>
				<div class="col-md-12">
					<table class="table table-sm table-bordered table-stripe text-center" id="tblEntregaParcial">
						<thead>
							<tr class="text-center">
							<th scope="col">#</th>
							<th scope="col">Piezas</th>
							<th scope="col">Fecha</th>
							<th scope="col"> <a href="#" id="plusNewEntregaParcial" style="color: white"><i class="fas fa-plus-circle"></i></a></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">1</th>
								<td contenteditable="true" class="numPiezas"></td>
								<td contenteditable="true" class="fechaNumPiezas"><?php echo date('Y-m-d')?></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<div class="col-md-12">
					<div class="row">
						<label class="col-md-4 col-form-label">Observaciones de planta: </label>
						<div class="col-sm-8">
							<textarea id="observacionesPlanta" cols="61" rows="4" style="background-color:#ffff002e"></textarea>
						</div>
					</div>
				</div><br>				
				<div class="col-md-12">
					
						<input class="" type="checkbox" id="EntregaTotal" style="width: 10px">
						<label class="" for="defaultCheck1"><b>Entrega Total</b></label>
						<input type="date"  id="dateEntregaTotal">
					
				</div>
			</div>	


		</div>
		<div class="row">
			<div class="col-md-6">
				<button class="btn btn-sm" style="background-color: orange" id="btnGuardar"> Guardar</button>
				<button class="btn btn-sm" style="background-color: orange" id="btnActualizar"> Actualizar</button>
				<button class="btn btn-sm" style="background-color: orange" id="btnAutorizar"> Autorizar</button>
				
				<button class="btn btn-sm" style="background-color: orange" id="btnProcesoFabricacion"> Proceso fabricación</button>		
				<button class="btn btn-sm" style="background-color: orange" id="btnSolicitudEnPlanta"> Solicitud en planta </button>		
							
				<button class="btn btn-sm" style="background-color: orange" id="btnCancelar"> Cancelar</button>				
			</div>
			<div class="col-md-6">
				<button class="btn btn-sm" style="background-color: orange" id="btnEntregaParcial"> Entrega parcial</button>
				<button class="btn btn-sm" style="background-color: orange" id="btnEntregaTotal"> Entrega total</button>								
			</div>
		</div><br><br>
		<br>	
		<?php include "footer.php"; ?>
	</body>

	<script>		
		var folio = '<?php echo "$folioGet"; ?>'; 
		var empresa = 'EYPO';			
		var codigoCoordinador = <?php echo $CodigoPosicion ?>;						
		buscarFolio(folio, empresa, codigoCoordinador);	
		if (!Boolean($("#estatusOrdenesFabricacion").val())) {			
			var estatus = "sinestatus";
			caracteristicasEstatus(estatus)
			manejo_de_permisos_usuarios(codigoCoordinador)
		} 
																		
		function manejo_de_permisos_usuarios(codigoCoordinador) {

			switch (codigoCoordinador) {
				case 36: //coordinador grupo eypo solo puede autorizar									
					$("#btnGuardar").hide();	
					$(".soloVendedores").hide();
					$("#btnSolicitudEnPlanta").hide()
					$("#btnProcesoFabricacion").hide()	
					$("#btnEntregaParcial").hide()
					$("#btnEntregaTotal").hide()
					$("#btnEstatusCerrar").hide()	
					$("#btnAutorizar").show(); 
													
					break;

				case 37: //empleado ventas	
					// $("#btnActualizar").show();				
					$("#btnAutorizar").hide();  
					$("#btnSolicitudEnPlanta").hide()
					$("#btnProcesoFabricacion").hide()	
					$("#btnEntregaParcial").hide()
					$("#btnEntregaTotal").hide()
					$("#btnEstatusCerrar").show()					
					break;		
				case 48: // usario ensamble
					$("#btnGuardar").hide();
					$("#btnActualizar").hide() 
					$("#btnAutorizar").hide()
					$(".soloVendedores").hide();
					$("#btnSolicitudEnPlanta").show()
					$("#btnProcesoFabricacion").show()	
					$("#btnEntregaParcial").show()
					$("#btnEntregaTotal").show()																			
					break;		
			}							
		}				

		$("#btnbuscarOferta").on('click', function () {								
			consulta_gral_ordenFab();
		});

		function consulta_gral_ordenFab() {
			$.ajax({					
				url:'arcosConsultas/consulta_segun_empresa_gral.php',
				type:'post',
				data:{ empresa: 'EYPO'	}
			}).done (function (response){
				$("#tblarcosGeneral tbody").empty();
				$("#tblarcosGeneral tbody").append(response)
				$("#tblarcosGeneral tbody tr").on('click', function(){        
					var folio = $(this).find('td').eq(1).text();															
					$("#modalArcosConsultaGeneral").modal('hide');
					window.location.href = 'arcos.php?folio='+folio;
				});
			});			
		}

		function mostrarDatosEnInputs(resp){

			$("#folio_ord_fab").val(resp['folio']);
			$("#estatusOrdenesFabricacion").val(resp['estatus']);
			$("#dateInicial").val(resp['fechaSolicitud']);
			$("#dateFinal").val(resp['fechaEntrega']);
			$("#nombreSolicitante").val(resp['nombreSolicitante']);
			$("#nombreProyecto").val(resp['nombreProyecto']);
			$("#numeroDeOrden").val(resp['numeroOrden']);

			$("#codigoCliente").val(resp['codigoCliente']);
			$("#nombreCliente").val(resp['nombreCliente']);
			$("#cantidadFabricar").val(resp['cantidadFabricar']);
			$("#entregasParciales").val(resp['entregasParciales']);

			$("#productosFabricar").val(resp['productoaFabricar']);
			$("#productosFabricarNombre").val(resp['productosFabricarNombre']);
			$("#tblSubproductoArmado tbody").empty();				
			for (x= 0; x<resp.detalleArmado.length; x++ ){
				$("#tblSubproductoArmado tbody").append(resp.detalleArmado[x]);
			} 

			$("#productosDesarmar").val(resp['productosDesarmar']);
			$("#productosDesarmarNombre").val(resp['productosDesarmarNombre']);
			$("#tblSubproductoDesarmado tbody").empty();				
			for (x= 0; x<resp.detalleDesarme.length; x++ ){
				$("#tblSubproductoDesarmado tbody").append(resp.detalleDesarme[x]);
			} 

			$("#observaciones").val(resp['observaciones']);								

			if (resp['entregaTotal']) {
                $("#EntregaTotal").prop('checked', true)
                $("#dateEntregaTotal").val(resp['FechaEntregaTotal'])
            }
            $("#observacionesPlanta").val(resp['observacionesPlanta'])
            $("#logistica").val(resp['logistica'])
            $("#comentariosLogistica").val(resp['comentarioLogistica']) 
			
		}

		function obtenerDatosDeInputs(){

			var arrayValores = [];
			arrayValores.push($('#dateInicial').val());
			arrayValores.push($('#dateFinal').val());
			arrayValores.push($('#nombreSolicitante').val());
			arrayValores.push($('#nombreProyecto').val());
			arrayValores.push($('#numeroDeOrden').val());

			arrayValores.push($('#codigoCliente').val());
			arrayValores.push($('#nombreCliente').val());
			arrayValores.push($('#cantidadFabricar').val());
			arrayValores.push($('#entregasParciales').val());

			arrayValores.push($('#productosFabricar').val());
			arrayValores.push($('#productosFabricarNombre').val());

			arrayValores.push($('#productosDesarmar').val());
			arrayValores.push($('#productosDesarmarNombre').val());

			arrayValores.push($('#observaciones').val());
			arrayValores.push($('#empresa').val());
			arrayValores.push($('#folio_ord_fab').val());

			return arrayValores;
		}

		function obtenerDetalleFabricacion(){

			var valoresDet = [];

			var array_codigoArt = [];
			var array_nombreArt = [];
			var array_piezas = [];			
			var array_desarmeArme = [];


			$('#tblSubproductoArmado .codigoArt').each(function(){
				array_codigoArt.push($(this).text());
			});

			$('#tblSubproductoArmado .nombreArt').each(function(){
				array_nombreArt.push($(this).text());
			});

			$('#tblSubproductoArmado .piezas').each(function(){
				array_piezas.push($(this).text());
			});

			$('#tblSubproductoArmado .armadoDesarmado').each(function(){
				array_desarmeArme.push($(this).text());
			});

			valoresDet.push(array_codigoArt, array_nombreArt, array_piezas, array_desarmeArme);
			return valoresDet;

		}

		function obtenerDetalleDesarmado(){

			var valoresDet = [];

			var array_codigoArt = [];
			var array_nombreArt = [];
			var array_piezas = [];			
			var array_desarmeArme = [];


			$('#tblSubproductoDesarmado .codigoArt').each(function(){
				array_codigoArt.push($(this).text());
			});

			$('#tblSubproductoDesarmado .nombreArt').each(function(){
				array_nombreArt.push($(this).text());
			});

			$('#tblSubproductoDesarmado .piezas').each(function(){
				array_piezas.push($(this).text());
			});

			$('#tblSubproductoDesarmado .armadoDesarmado').each(function(){
				array_desarmeArme.push($(this).text());
			});

			valoresDet.push(array_codigoArt, array_nombreArt, array_piezas, array_desarmeArme);
			return valoresDet;

		}

		function buscarFolio(folio, empresa, codigoCoordinador){
			$.ajax({
				url:'arcosConsultas/arcosConsultaGeneral.php',
				type:'post',
				dataType:'json',
				data: {folio: folio, empresa: empresa},
			}).done (function (resp) {		
				var estatus = resp['estatus'];										
				mostrarDatosEnInputs(resp);					
				caracteristicasEstatus(estatus)		
				manejo_de_permisos_usuarios(codigoCoordinador)
				

				$.ajax({
					type: "POST",
					url: "arcosConsultas/arcosConsultaGeneral2.php",
					data: {folio: folio, empresa: 'eypo' },				
				}).done (function (response) {
					if (response) {
						$("#tblEntregaParcial tbody").empty();
						$("#tblEntregaParcial tbody").append(response); 
					}					
				})			
			})
		}

		$("#btnGuardar").on('click', function(){

			var valores = obtenerDatosDeInputs();
										
			if ($("#productosDesarmar").val()) {					
				var valDet_Desarme = obtenerDetalleDesarmado();					
			}

			if ($("#productosFabricar").val()) {				
				var valDet = obtenerDetalleFabricacion();						
			}																
			
			
			$.ajax({
				type:'post',
				url:"arcosConsultas/arcosInsert.php",
				data:{ valores: valores, valDet: valDet, valDet_Desarme: valDet_Desarme }, 
				success: function(resp){
															
					window.scrollTo(0,0);	
					$(".mensaje").append(resp);
					setTimeout('document.location.reload()',1500);
				}
			})							
		});

		function mostrar_mensaje(response, mensaje) {
			console.log(response, mensaje);
			
			var element_mensaje = $(".mensaje :first-child")
			element_mensaje.empty()
			switch (response) {
				case  'ok':                                
					$('html, body').animate({scrollTop:0}, 'slow');    
					element_mensaje.append(mensaje).removeClass('background-color-mensaje-error').addClass('background-color-mensaje-success')            
					setTimeout(() => { window.location.href = 'arcos.php' }, 2500); 
					break;
				case  'err':                                
					$('html, body').animate({scrollTop:0}, 'slow');    
					element_mensaje.append(mensaje).removeClass('background-color-mensaje-success').addClass('background-color-mensaje-error')                    
					break;
			}    
		}

		$("#btnActualizar").on('click', function () {
			var valores = obtenerDatosDeInputs();
			var valDet_Desarme = "";
			var valDet = "";
			if ($("#productosDesarmar").val().length > 0) {					
				valDet_Desarme = obtenerDetalleDesarmado()											
			}	      

			if ($("#productosFabricar").val().length > 0) {				
				valDet = obtenerDetalleFabricacion()
			}	
 
			$.ajax({                    
				url:"arcosConsultas/actualizarOrdFabEypo.php",
				type:'POST',
				data:{ valores: valores, valDet: valDet, valDet_Desarme: valDet_Desarme },
				dataType: 'JSON'
			}).done((resp) => {
				mostrar_mensaje(resp.resp, resp.mensaje) 		
			})			
		});

		$("#btnCancelar").on('click', function(){
			setTimeout('location.href = "arcos.php";',2000);
		})

		$("#codigoCliente").change(function(){
			var norden = $("#codigoCliente").val();
			mostrarCliente(norden);
		});

		function mostrarCliente($id) {
			var c= $id;
			$.ajax({
				type:'post',
				url:'arcosConsultas/arcosSend.php',
				data: {cte:c},
				success: function(data){
					$("#nombreCliente").val(data).attr('title', data);
				}
			});
		}

		function mostrarMaterialesHijos (codigo) {
			$.ajax({
				url:'arcosConsultas/agregarMaterialesHijos.php',
				type:'post',
				dataType:'json',
				data:{code: codigo , tipo: 'A'},			
			}).done (function (response) {
				$("#tblSubproductoArmado tbody").empty();						
				$("#tblSubproductoArmado tbody").append(response['array_Componentes']);		
			})
		}

		function mostrarMaterialesHijosDesarme(codigo){
			$.ajax({
				url: 'arcosConsultas/agregarMaterialesHijos.php',
				type: 'post',
				dataType: 'json',
				data:{ code: codigo, tipo: 'D'},				
			}).done (function (response) {
				$("#tblSubproductoDesarmado tbody").empty();	
				$("#tblSubproductoDesarmado tbody").append(response['array_Componentes']);	
			})
		}

		$("#buscadorCliente").keyup(function(){
			var valorEscrito = $("#buscadorCliente").val();
			if (valorEscrito) {
				$.ajax({
					url: 'socios/buscadorConsultaClientesYN.php',
					type: 'post',
					data: {valor: valorEscrito},
					success: function(resp){
						
						$("#tblcliente tbody").empty();
						$("#tblcliente tbody").append(resp);
						
						$("#tblcliente tr").on('click',function(){
							
							var codigo = $(this).find('td').eq(1).text();
							var name = $(this).find('td').eq(2).text();	
							$("#nombreCliente").val(name);
							$("#codigoCliente").val(codigo);														 
							agregarPersonaContacto (codigo, name);
							$("#tblcliente tbody").empty();
							$("#buscadorCliente").val("");
							$("#myModal").hide();

							$.ajax({
								url:'rellenarSelectFormaPago.php',
								type:'POST',
								data:{codigo:codigo},
							}).done(function (response) {
								$("#formaPagoSelect").append(response);								
							});										 
																								
						});
					},
				});
			} else { 
				$("#tblcliente tbody").empty(); 
			}
		});	

		function agregarPersonaContacto ($code, $name) {

			var code = $code;
			var name = $name;
			$("#codcliente").val(code);
			$("#NombreC").val(name).prop('title', name);				
			$('#myModal').modal('hide');
			$.ajax({
				url: 'consultaPersonaContacto.php',
				type: 'post',
				data: {code: code},
				success: function(response){
					$("#listcontactos").append(response);
				}
			});
		}

		$("#btnArticuloArmado").on('click', function () {
			$("#buscadorArticulo").keypress(function(e){
				var code = (e.keyCode ? e.keyCode : e.which);
				if (code == 13) {
					var valorEscrito = $("#buscadorArticulo").val();
					if (valorEscrito) {
						$.ajax({
							url: 'ofvConsultas/consulta_articulo.php',
							type: 'post',
							data: {valor: valorEscrito},
							dataType:'JSON',
						}).done((response) => {
							if (response.length) {
							response.forEach((element,index) => {				
								tr_articulo += `<tr onclick="seleccionar_articulo_click_desde_table(this)" class="cursor"><td>${index++}</td><td class="codigo_articulo">${element.CodigoArticulo}</td><td>${element.NombreArticulo}</td><td>${element.EnStock}</td><td>${element.Comprometido}</td><td>${element.Solicitado}</td><td class="codigo_almacen">${element.CodigoAlmacen}</td><td>${element.NombreAlmacen}</td></tr>`	
							});
							} else {
								var tr_articulo = '<tr><td colspan="8" class="text-center">No hay registros para mostrar</td></tr>';
							}					
							$("#tblarticulo tbody").empty().append(tr_articulo);
							$("#tblarticulo tbody tr").on('click',function() {
								var codigo =  $(this).find('td').eq(1).text();					
									var nombre =  $(this).find('td').eq(2).text();												

									$("#productosFabricar").val(codigo);
									$("#productosFabricarNombre").val(nombre);	
									mostrarMaterialesHijos(codigo);									

									$("#buscadorArticulo").val("");
									$("#tblarticulo tbody").empty();
									$("#myModalArticulos").modal('hide');							
							})				
						})						
					} else {
						$("#tblarticulo tbody").empty();
						$("#buscadorArticulo").val("");
					}
				}
			});								
		})			

		$("#btnArticuloDesarmado").on('click', function () {

			$("#buscadorArticulo").keypress(function(e){
				var code = (e.keyCode ? e.keyCode : e.which);
				if (code == 13) {
					var valorEscrito = $("#buscadorArticulo").val();
					if (valorEscrito) {
						$.ajax({
							url: 'ofvConsultas/consulta_articulo.php',
							type: 'post',
							data: {valor: valorEscrito},
							dataType:'JSON',
						}).done ((response) => {

							if (response.length) {
								response.forEach((element,index) => {				
									tr_articulo += `<tr onclick="seleccionar_articulo_click_desde_table(this)" class="cursor"><td>${index++}</td><td class="codigo_articulo">${element.CodigoArticulo}</td><td>${element.NombreArticulo}</td><td>${element.EnStock}</td><td>${element.Comprometido}</td><td>${element.Solicitado}</td><td class="codigo_almacen">${element.CodigoAlmacen}</td><td>${element.NombreAlmacen}</td></tr>`	
								});
							} else {
								var tr_articulo = '<tr><td colspan="8" class="text-center">No hay registros para mostrar</td></tr>';
							}							
							$("#tblarticulo tbody").empty().append(tr_articulo);
							$("#tblarticulo tbody tr").on('click',function() {
								var codigo =  $(this).find('td').eq(1).text();					
								var nombre =  $(this).find('td').eq(2).text();												

								$("#productosDesarmar").val(codigo);
								$("#productosDesarmarNombre").val(nombre);	

								mostrarMaterialesHijosDesarme(codigo);																															

								$("#buscadorArticulo").val("");
								$("#tblarticulo tbody").empty();
								$("#myModalArticulos").modal('hide');								
							})							
						})
					} else {
						$("#tblarticulo tbody").empty();
						$("#buscadorArticulo").val("");
					}
				}
			});				

		})
	
		$("#buscadorArcos").keyup(function () {

			var valorEscrito = $("#buscadorArcos").val();
			if (valorEscrito) {
				$.ajax({
					url: 'arcosConsultas/buscarOrdenFabricacion.php',
					type: 'post',
					data: {valor: valorEscrito, empresa: 'EYPO'},				
				}).done (function (resp) {
					$("#tblarcosGeneral tbody").empty();
					$("#tblarcosGeneral tbody").append(resp);					
					$("#tblarcosGeneral tr").on('click',function() {
						var folio = $(this).find('td').eq(1).text();																									
						$("#modalArcosConsultaGeneral").modal('hide');
						window.location.href = 'arcos.php?folio='+folio;		 																									
					});						
				});
			} else { 
				consulta_gral_ordenFab();
			}				
		});

		$("#buscadorArcosFecha").change(function () {

			var valorEscrito = $("#buscadorArcosFecha").val();
			
			if (valorEscrito) {
				$.ajax({
					url: 'arcosConsultas/buscarOrdenFabricacionFecha.php', 
					type: 'post',
					data: {valor: valorEscrito, empresa: 'EYPO'},				
				}).done (function (resp) {
					$("#tblarcosGeneral tbody").empty();
					$("#tblarcosGeneral tbody").append(resp);					
					$("#tblarcosGeneral tr").on('click',function() {
						var folio = $(this).find('td').eq(1).text();																									
						$("#modalArcosConsultaGeneral").modal('hide');
						window.location.href = 'arcos.php?folio='+folio;		 																									
					});						
				});
			} else { 
				consulta_gral_ordenFab();
			}				
		});		

		function caracteristicasEstatus(estatus) {			
			
            switch (estatus) {
				case "SOLICITADO":				
					$("#btnGuardar").prop('disabled', true)      
					$("#btnEntregaParcial").prop('disabled', true) 
					$("#btnEntregaTotal").prop('disabled', true)                                                          
                    $("#EntregaTotal").prop('disabled', true)
					$("#dateEntregaTotal").attr('disabled', true);               
                    break;
                case "AUTORIZADA":
					$("#btnGuardar").prop('disabled', true)
					$("#btnAutorizar").prop('disabled', true)     
					$("#btnEntregaTotal").prop('disabled', true)                																								                    
					break;
				case "SOLICITUD EN PLANTA":					
					$("#btnGuardar").prop('disabled', true)  
					$("#btnAutorizar").prop('disabled', true)   
					$("#btnSolicitudEnPlanta").prop('disabled', true)	
					$("#btnEntregaTotal").prop('disabled', true) 				
                    break;
                case "PROCESO DE FABRICACION":					
					$("#btnGuardar").prop('disabled', true)
					$("#btnAutorizar").prop('disabled', true)  
					$("#btnSolicitudEnPlanta").prop('disabled', true)  
					$("#btnProcesoFabricacion").prop('disabled', true)  
					// $("#btnEntregaTotal").prop('disabled', true) 
					if ($("#entregasParciales").val() == 'no') {
						$("#btnEntregaParcial").attr('disabled', true)
						$("#btnEntregaTotal").attr('disabled', false)
					} else {
						$("#btnEntregaParcial").attr('disabled', false)
						$("#btnEntregaTotal").attr('disabled', true)
					}	 	
																				
                    break;
                
				case "ENTREGA PARCIAL":
					$("#btnGuardar").prop('disabled', true)
					$("#btnAutorizar").prop('disabled', true)  
					$("#btnSolicitudEnPlanta").prop('disabled', true)  
					$("#btnProcesoFabricacion").prop('disabled', true)
					$("#btnEntregaTotal").prop('disabled', false)  
					
														                                  									
                    break;
				case "ENTREGA TOTAL":
				
					$("#btnGuardar").prop('disabled', true)
					$("#btnActualizar").prop('disabled', true)  
					$("#btnAutorizar").prop('disabled', true) 
					$("#btnSolicitudEnPlanta").prop('disabled', true)  
					$("#btnProcesoFabricacion").prop('disabled', true) 

					$("#btnEntregaParcial").prop('disabled', true) 
					$("#btnEntregaTotal").prop('disabled', true)                                                          
                    $("#EntregaTotal").prop('disabled', true)
					$("#dateEntregaTotal").attr('disabled', true); 
					                   
                    $('.numPiezas').each(function() {
                    	$(this).prop('contenteditable', false)
					})					
					$('.FechanumPiezas').each(function() {
                    	$(this).prop('contenteditable', false)
					})		
					$("#observacionesPlanta").prop('disabled', true);
					
					$('#folio_ord_fab').attr('disabled', true)
					$('#dateInicial').attr('disabled', true)
					$('#dateFinal').attr('disabled', true)
					$('#nombreSolicitante').attr('disabled', true)
					$('#nombreProyecto').attr('disabled', true)
					$('#numeroDeOrden').attr('disabled', true)
					$('#nombreCliente').attr('disabled', true)
					$('#cantidadFabricar').attr('disabled', true)
					$('#entregasParciales').attr('disabled', true)
					$('#codigoProducto').prop('readonly', true)
					$('#descripcionProducto').prop('readonly', true)
					$('#productosFabricarNombre').attr('disabled', true)
				
					$('#terminadoFinal').attr('disabled', true)
					$('#observaciones').attr('disabled', true)
					$('#empresa').attr('disabled', true)
					$('#entregasParciales').attr('disabled', true)
					$('#EntregaParcial').attr('disabled', true)
					$('#EntregaTotal').attr('disabled', true)

					$('#SePM').attr('disabled', true)
					$('#SeFt').attr('disabled', true)
					$('#SeDib').attr('disabled', true)
					$('#SeDia').attr('disabled', true)
					$('#SeOtr').attr('disabled', true)                        

					$("#dateEntregaTotal").attr('disabled', true);
					$("#dateEntregaParcial").attr('disabled', true);
                    break;
                case "CERRADO":
					$("#btnGuardar").prop('disabled', true)
					$("#btnActualizar").prop('disabled', true) 
					$("#btnAutorizar").prop('disabled', true)  
					$("#btnSolicitudEnPlanta").prop('disabled', true)  
					$("#btnProcesoFabricacion").prop('disabled', true) 

					$("#btnEntregaParcial").prop('disabled', true) 
					$("#btnEntregaTotal").prop('disabled', true)                                      
                    $("#EntregaTotal").prop('checked', true)
                    $("#EntregaTotal").prop('disabled', true)
					$("#dateEntregaTotal").attr('disabled', true); 
					                   
                    $('.numPiezas').each(function() {
                    	$(this).prop('contenteditable', false)
					})					
					$('.FechanumPiezas').each(function() {
                    	$(this).prop('contenteditable', false)
					})		
					$("#observacionesPlanta").prop('disabled', true);
					
					$('#folio_ord_fab').attr('disabled', true)
					$('#dateInicial').attr('disabled', true)
					$('#dateFinal').attr('disabled', true)
					$('#nombreSolicitante').attr('disabled', true)
					$('#nombreProyecto').attr('disabled', true)
					$('#numeroDeOrden').attr('disabled', true)
					$('#nombreCliente').attr('disabled', true)
					$('#cantidadFabricar').attr('disabled', true)
					$('#entregasParciales').attr('disabled', true)
					$('#codigoProducto').prop('readonly', true)
					$('#descripcionProducto').prop('readonly', true)
					$('#productosFabricarNombre').attr('disabled', true)
				
					$('#terminadoFinal').attr('disabled', true)
					$('#observaciones').attr('disabled', true)
					$('#empresa').attr('disabled', true)
					$('#entregasParciales').attr('disabled', true)
					$('#EntregaParcial').attr('disabled', true)
					$('#EntregaTotal').attr('disabled', true)

					$('#SePM').attr('disabled', true)
					$('#SeFt').attr('disabled', true)
					$('#SeDib').attr('disabled', true)
					$('#SeDia').attr('disabled', true)
					$('#SeOtr').attr('disabled', true)                        

					$("#dateEntregaTotal").attr('disabled', true);
					$("#dateEntregaParcial").attr('disabled', true);
                    $("#btnEstatusCerrar").attr('disabled', true)
					break;	
					case "sinestatus":
				
						$("#btnAutorizar").hide()
						$("#btnSolicitudEnPlanta").hide()  
						$("#btnProcesoFabricacion").hide() 
						$("#btnEntregaParcial").hide() 
						$("#btnEntregaTotal").hide()                                                          
						
						
						break;			    
            }
        }

		$("#btnAutorizar").on('click', function () {
			var estatusActual = $("#estatusOrdenesFabricacion").val();
			var folio = $("#folio_ord_fab").val();
			if (estatusActual === 'SOLICITADO') {
				
				$.ajax({
					url:'arcosConsultas/cambio_de_estatus.php',
					type:'POST',
					data:{folio: folio, empresa: 'eypo', estatus: 'AUTORIZADA'},
					dataType: 'JSON',
				}).done((response) => {
					console.log(response);					
					mostrar_mensaje(response.resp, response.mnj)
				})
			} else {
				alert("Orden de fabricación debe de estár en estatus SOLICITADO para poder AUTORIZARLA");
			}
		})

		$("#btnSolicitudEnPlanta").on('click', function() {
			var estatusActual = $("#estatusOrdenesFabricacion").val();
			var folio = $("#folio_ord_fab").val()	
			if (estatusActual === 'AUTORIZADA') {		
				$.ajax({
					url:'arcosConsultas/cambio_de_estatus.php',
					type:'POST',
					data:{ folio:folio, empresa: 'eypo', estatus: 'SOLICITUD EN PLANTA'},
					dataType: 'JSON',
				}).done((response) => {					
					mostrar_mensaje(response.resp, response.mnj)
				})
			} else {
				alert("Orden de fabricación debe de estár en estatus AUTORIZADA para ponerla en SOLICITUD EN PLANTA");
			}
		});
		
		$("#btnProcesoFabricacion").on('click', function() {
			var estatusActual = $("#estatusOrdenesFabricacion").val();
			var folio = $("#folio_ord_fab").val()
			if (estatusActual === 'SOLICITUD EN PLANTA') {
				$.ajax({
					url:'arcosConsultas/cambio_de_estatus.php',
					type:'POST',
					data:{ folio:folio, empresa: 'eypo', estatus: 'PROCESO DE FABRICACION' },
					dataType: 'JSON',
				}).done((response) => {               
					mostrar_mensaje(response.resp, response.mnj)
				})
			} else {
				alert("Orden de fabricación debe de estár en estatus SOLICITUD EN PLANTA para ponerla en PROCESO DE FABRICACIÓN");
			}
        });

		$("#btnEntregaParcial").on('click', function() {     
            var estatusActual = $("#estatusOrdenesFabricacion").val();       
            
            if ( estatusActual != 'PROCESO DE FABRICACION' && estatusActual != 'ENTREGA PARCIAL') {
                    alert("Favor de cambiar primero a estatus PROCESO DE FABRICACIÓN antes de pasar a ENTREGA PARCIAL")
            } else {
				var array_piezas = [];
				var array_fechas = [];
				var folio = $("#folio_ord_fab").val()

				$(".numPiezas").each(function() {
					array_piezas.push($(this).text())
				})
				$(".fechaNumPiezas").each(function() {
					array_fechas.push($(this).text())
				})
		
				var observacionesPlanta = $("#observacionesPlanta").val();

				if (observacionesPlanta && array_fechas &&  array_piezas) {
					$.ajax({
						type: "post",
						url: "arcosConsultas/insertArcosEntregaParcial.php",
						data: {
							folio:folio,
							piezas:array_piezas,
							fechas:array_fechas,
							obParcial: observacionesPlanta,
							empresa: 'eypo',
						},                        
					}).done ((estatus) => {
						if (estatus == '0'){ 
							mostrar_mensaje( 'err','Algo Falló a la hora de guardar los valores de ENTREGA PARCIAL')
						} else {
							mostrar_mensaje( 'ok', estatus)
						}
						
					})
				} else {
					alert('Favor de completar la tabla ENTREGA PARCIAL y llenar campo de OBSERVACIONES  DE PLANTA')
				}  				             
            }
		});

		$("#btnEntregaTotal").on('click', function() {
            var folio = $("#folio_ord_fab").val()
            var fechaEntregaTotal = $("#dateEntregaTotal").val();
            if($("#EntregaTotal").is(':checked')) {
				if(Boolean($("#dateEntregaTotal").val())){
					var checado = 1;

					$.ajax({
						url:'arcosConsultas/cambio_de_estatus.php',
						type:'POST',
						data:{
							folio : folio,
							checado : checado,
							fechaEntregaTotal:fechaEntregaTotal,
							empresa: 'eypo',
							estatus: 'ENTREGA TOTAL',
						},
						dataType: 'JSON',
					}).done((response) => {                    
						mostrar_mensaje(response.resp, response.mnj)
					})
				} else {					
					mostrar_mensaje('err', 'Favor de poner una fecha de ENTREGA TOTAL')
				}
                
            } else {				
				mostrar_mensaje('err', 'Favor de marcar la casilla de ENTREGA TOTAL')
            }
		});		

		$("#plusNewEntregaParcial").on('click', function(){
            $("#tblEntregaParcial tr:last").clone().appendTo("#tblEntregaParcial");
            var numrow = parseInt($("#tblEntregaParcial tr:last").find('th').eq(0).text())
            numrow ++;
            $("#tblEntregaParcial tr:last").find('th').eq(0).text(numrow)
            $("#tblEntregaParcial tr:last").find('td').eq(0).empty()
            $("#tblEntregaParcial tr:last").find('td').eq(0).prop('contenteditable', true)
            $("#tblEntregaParcial tr:last").find('td').eq(1).empty()
            $("#tblEntregaParcial tr:last").find('td').eq(1).append(<?php echo date('Y-m-d')?>)
            $("#tblEntregaParcial tr:last").find('td').eq(1).prop('contenteditable', true)
		})
		
		$("#btnEstatusCerrar").on('click', function () {
            var logistica = $("#logistica").val();
            var comentarios_logistica = $("#comentariosLogistica").val()      
            var folio = $("#folio_ord_fab").val()     
            var estatus_actual = $("#estatusOrdenesFabricacion").val();            

            if (estatus_actual === 'ENTREGA TOTAL') {

                if (logistica && comentarios_logistica ) {
                    $.ajax({
                        url: 'arcosConsultas/cambio_de_estatus.php',
                        type:'POST',
						data: {
							estatus: "CERRADO", 
							logistica: logistica, 
							comExt: comentarios_logistica, 
							folio: folio, 
							empresa: 'eypo' }
                    }).done(function (params) {

						alert(params)						
						location.reload();  
						                  
                    })
				} else {
					alert("Favor de completar los campos de LOGISTICA Y COMENTARIOS EXTRAS para poder cambiar el estatus a CERRADO. ")
				}

            } else {
                alert("La orden debe estar en estatus ENTREGA TOTAL para poder CERRARLA");
            }                                    
		}) 

		$("#quitar_armado").on('click', function () {
			$("#productosFabricar").val('')
			$("#productosFabricarNombre").val('')
			$("#tblSubproductoArmado tbody").empty()
			var nueva_linea = '<tr style="height: 28px"><td class="codigoArt" contenteditable="true"></td><td class="nombreArt" contenteditable="true"></td><td class="piezas" contenteditable="true"></td><td class="unidad" contenteditable="true"></td><td class="armadoDesarmado" style="display:none">A</td></tr>'
			$("#tblSubproductoArmado tbody").append(nueva_linea)
		})

		$("#quitar_desarmado").on('click', function () {
			$("#productosDesarmar").val('')
			$("#productosDesarmarNombre").val('')
			$("#tblSubproductoDesarmado tbody").empty()
			var nueva_linea = '<tr style="height: 28px"><td class="codigoArt" contenteditable="true"></td><td class="nombreArt" contenteditable="true"></td><td class="piezas" contenteditable="true"></td><td class="unidad" contenteditable="true"></td><td class="armadoDesarmado" style="display:none">A</td></tr>'
			$("#tblSubproductoDesarmado tbody").append(nueva_linea)

		})

		
function ArcosPdf(){
	var folio =  $("#folio_ord_fab").val();			
    var empresa =  $("#empresa").val();			
 //   console.log(folio);
 //   console.log(empresa);
 //   console.log('http://187.188.40.90:85/CrystalReportViewer/ReporteArmado.aspx?empresa='+empresa+'&folio='+folio);
	$.ajax({
		type: 'POST',
		url: 'http://187.188.40.90:85/CrystalReportViewer/ReporteArmado.aspx?empresa='+empresa+'&folio='+folio,
	}).done((params) => {
    });
	var winFeature ='location=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes';
    wait(2200);
	window.open('visor/tres/RA '+folio+'-'+empresa+'.pdf','_blank','menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=no,resizable=no,dependent,width=800,height=620,left=0,top=0');
}

function wait(ms) {
	var start = new Date().getTime();
	var end = start;
	while(end < start + ms) {
		end = new Date().getTime();
	}
}
										
	</script>
</html>
