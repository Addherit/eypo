-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ERNESTO>

-- Create date: <17-04-2020>
-- Description:	<trae toda la informacion de las ordenes de venta por dia para hacer un corte diario>
-- =============================================
alter PROCEDURE [dbo].[Cortediario] 
	-- Add the parameters for the stored procedure here
	@fechabusqueda varchar(100),@namec varchar(100)  
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @id varchar(255),@fecha varchar(255),@cliente varchar(255),@nombre varchar(255)
    DECLARE @fpago varchar(255),@importe varchar(255),@iva varchar(255),@total varchar(255),@detalles varchar(255)
-- declare the table with the final form of the data      
DECLARE @cortes TABLE (
    numero INT IDENTITY(1,1) PRIMARY KEY,
	id varchar(255), 
	fecha varchar(255),
	cliente varchar(255),
	nombre varchar(255),
	fpago varchar(255),
	importe varchar(255),
	iva varchar(255),
	total varchar(255)
);


Declare curinsert CURSOR LOCAL 
For 
			
	select m.Id,CONVERT(varchar,m.CreatedAt,105) as date,m.Users,m.Detalles2
	from money_transaction m 
	inner join solution s on s.Id = m.Solution
	where
	s.Uid like '%'+@namec+'%' 
	and m.Type in ('sale_order_payment', 'cancelled_sale_order_payment', 'sale_order_payment_cancelled_for_inactivity','incomplete_sale_order_payment')
	and CONVERT(varchar,m.CreatedAt,23) = @fechabusqueda
	order by m.createdAt desc
	
--apertura del cursor para inserted 
	OPEN curinsert
    --busqueda del primer inserted
	Fetch next from curinsert into  @id,@fecha,@nombre,@detalles
	While @@FETCH_STATUS = 0 
	Begin 
		--NC:, TotalNC:, OV:56407, CantOV:1.00, Cliente:CLI-000001
		--NC:, TotalNC:, OV:56418, CantOV:262.00, Cliente:CLI-030321
		SELECT @cliente = case when left(value, 1) <> ' ' then value else substring(value, 2, LEN(value)) end from (SELECT row_number () over (order by (select 0)) rn,* FROM SplitString(@detalles,',')) AS Der WHERE rn = 5
		
		SELECT @fpago = case when left(value, 1) <> ' ' then value else substring(value, 2, LEN(value)) end from (SELECT row_number () over (order by (select 0)) rn,* FROM SplitString(@detalles,',')) AS Der WHERE rn = 1
		SELECT @fpago = case when left(value, 1) <> ' ' then value else substring(value, 2, LEN(value)) end from (SELECT row_number () over (order by (select 0)) rn,* FROM SplitString(@fpago,':')) AS Der WHERE rn = 2
		IF @fpago is not null and @fpago <> 'NC:'
		BEGIN
			SELECT @importe = case when left(value, 1) <> ' ' then value else substring(value, 2, LEN(value)) end from (SELECT row_number () over (order by (select 0)) rn,* FROM SplitString(@detalles,',')) AS Der WHERE rn = 2
			set @fpago = 'Nota de Pago'
		END
		SELECT @fpago = case when left(value, 1) <> ' ' then value else substring(value, 2, LEN(value)) end from (SELECT row_number () over (order by (select 0)) rn,* FROM SplitString(@detalles,',')) AS Der WHERE rn = 6
		IF @fpago is not null and @fpago <> 'NC:'
		BEGIN
			SELECT @importe = case when left(value, 1) <> ' ' then value else substring(value, 2, LEN(value)) end from (SELECT row_number () over (order by (select 0)) rn,* FROM SplitString(@detalles,',')) AS Der WHERE rn = 4
			set @fpago = 'Tarjeta de Credito'
		END
		--efectivo
		IF @fpago is null or @fpago =  'NC:'
		BEGIN
			SELECT @importe = case when left(value, 1) <> ' ' then value else substring(value, 2, LEN(value)) end from (SELECT row_number () over (order by (select 0)) rn,* FROM SplitString(@detalles,',')) AS Der WHERE rn = 4
			set @fpago = 'Pago en Efectivo'
		END
		
		
		--limpieza dedatos
		SELECT @cliente = case when left(value, 1) <> ' ' then value else substring(value, 2, LEN(value)) end from (SELECT row_number () over (order by (select 0)) rn,* FROM SplitString(@cliente,':')) AS Der WHERE rn = 2
		SELECT @importe = case when left(value, 1) <> ' ' then value else substring(value, 2, LEN(value)) end from (SELECT row_number () over (order by (select 0)) rn,* FROM SplitString(@importe,':')) AS Der WHERE rn = 2
		
		set @iva = CAST(@importe as float) * .16
		set @total = @importe
		set @importe = CAST(@importe as float) / 1.16
		
		-- execute Cortediario '2020-04-07','EYPO01'
		insert into @cortes(id,fecha,nombre,cliente,importe,iva,total,fpago) values(@id,@fecha,@nombre,@cliente,@importe,@iva,@total,@fpago)
	--Busqueda del siguiente inserted
	Fetch next from curinsert into @id,@fecha,@nombre,@detalles
  End--cierre de cursor de inserted
 Close curinsert
Deallocate curinsert


select * from @cortes
	
END
GO
