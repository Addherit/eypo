function cargar_funciones_principales() {
	obtener_folio_max_min_vista_SAP()
	cargar_tipo_moneda()
	cargar_uso_principal()
	cargar_proyectos_sn()
	cargar_ventas_adicional()
	cargar_promotor()
	cargar_coordinador_de_venta()
	contador_cargador()
} //lista 

function obtener_folio_max_min_vista_SAP() {
	var maximini
	$.ajax({
		url:'ofvConsultas/consultar_folio_max_min_vista_SAP.php',
		dataType:'JSON',
		async: false	
	}).done((response) => {	
		
		console.log(response);
		
		var max_folio = parseInt(response[0].maxi)		
		$("#cdoc").empty()
		$("#cdoc").val(max_folio + 1)
		maximini = response
	})
	 return maximini
} //lista

function cargar_tipo_moneda() {
	$.ajax({
		url:'ofvConsultas/consultar_tipo_moneda.php',
		dataType:'JSON'
	}).done((response) => {
		$("#tmoneda").empty()
		var options = '<option value="" disabled selected>Seleccionar moneda</option>'
		response.forEach(element => {
			options += `<option value="${element.CurrCode}">${element.CurrName}</option>`
		});
		$("#tmoneda").append(options)
	})
} //lista

function cargar_uso_principal() {
	$.ajax({
		url:'ofvConsultas/consultar_uso_principal.php',
		dataType:'JSON'
	}).done((response) => {		
		$("#usoPrincipal").empty()				 
		var options = '<option value="" disabled selected>Seleccionar Uso</option>'
		response.forEach(element => {
			options += `<option value="${element.CodigoUsoCfdi}">${element.DescUsoCfdi}</option>`
		});
		$("#usoPrincipal").append(options)
	})
} //lista

function cargar_persona_de_contacto(code) {																				
	$.ajax({
		url: 'ofvConsultas/consulta_persona_contacto.php',
		type: 'POST',
		data: {code: code},
		dataType:'JSON',
	}).done((response) => {						
		var option = '<option value="" disabled selected>Selecciona persona</option>'
		response.forEach(element => {
			option += `<option value="${element.CodigoContacto}">${element.Nombre}${" "}${element.SegundoNombre}${" "}${element.Apellido}</option>`
		});	
		$("#listcontactos").empty().append(option)							
	})
} //lista

function cargar_proyectos_sn() {
	$.ajax({
		url:'ofvConsultas/consultar_proyecto_sn.php',
		dataType:'JSON'
	}).done((response) => {		
		$("#proyectoSN").empty()
		var options = '<option value="" disabled selected>Seleccionar proyecto</option>'
		response.forEach(element => {
			options += `<option value="${element.PrjCode}">${element.PrjName}</option>`
		});
		$("#proyectoSN").append(options)
	})
} //lista

function cargar_ventas_adicional() {
	$.ajax({
		url:'ofvConsultas/consultar_ventas_adicional.php',
		dataType:'JSON'
	}).done((response) => {		
		$("#ventasAdic").empty()
		var options = '<option value="" disabled selected>Seleccionar Ventas</option>'
		response.forEach(element => {
			options += `<option value="${element.CodigoEmpleadoVC}">${element.NombreEmpleadoVC}</option>`
		});
		$("#ventasAdic").append(options)
	})
} //lista

function cargar_promotor() {
	$.ajax({
		url:'ofvConsultas/consultar_ventas_adicional.php',
		dataType:'JSON'
	}).done((response) => {

		$("#promotor").empty()
		var options = '<option value="" disabled selected>Seleccionar promotor</option>'
		response.forEach(element => {
			options += `<option value="${element.CodigoEmpleadoVC}">${element.NombreEmpleadoVC}</option>`
		});
		$("#promotor").append(options)
	})
} //lista

function cargar_coordinador_de_venta() {
	$.ajax({
		url:'ofvConsultas/consultar_ventas_adicional.php',
		dataType:'JSON'
	}).done((response) => {		
		$("#cordVenta").empty()
		var options = '<option value="" disabled selected>Seleccionar coordinador</option>'
		response.forEach(element => {
			options += `<option value="${element.CodigoEmpleadoVC}">${element.NombreEmpleadoVC}</option>`
		});
		$("#cordVenta").append(options)
	})
} //lista

function cambio_de_moneda() {	
	$("#monedaVisor").empty()
	$("#monedaVisor").val($("#tmoneda").val())							
} //lista

function cambio_de_precio_por_partida(precio_unitario) {
	console.log(precio_unitario);
	
	var precio_tr = parseFloat(precio_unitario.textContent).toFixed(2)
	var cantidad = parseInt(precio_unitario.previousElementSibling.textContent)
	var total_tr = precio_tr * cantidad
	
	precio_unitario.nextElementSibling.innerHTML = new Intl.NumberFormat().format(total_tr)
	calcular_subtotal();	
} //lista

function numero_a_letra() {
	switch ($('#monedaVisor').val()) {
		case 'USD':
			var currency = new Object();
			currency.plural = "DOLARES";
			currency.singular = "DOLAR";
			currency.centPlural = "CENTAVOS DE DOLAR";
			currency.centSingular = "CENTAVO DE DOLAR";
			break;
		case 'EUR':
			var currency = new Object();
			currency.plural = "EUROS";
			currency.singular = "EURO";
			currency.centPlural = "CENTAVOS DE EURO";
			currency.centSingular = "CENTAVO DE EURO";
			break;
		case 'MXP':
			var currency = new Object();
			currency.plural = "PESOS";
			currency.singular = "PESO";
			currency.centPlural = "CENTAVOS DE PESO";
			currency.centSingular = "CENTAVO DE PESO";
			break;
		case 'CAN':
			var currency = new Object();
			currency.plural = "DOLARES CANADIENCIES";
			currency.singular = "DOLAR CANADIENCIE";
			currency.centPlural = "CENTAVOS DE DOLAR CANADIENCIE";
			currency.centSingular = "CENTAVO DE CANADIENCIE";
			break;
	}
	$("#numLetra").empty();
	var total_documento_sin_comas = replaceAll($("#totalDelDocumento").val(), ',', "")
	var numAlet = numeroALetras(total_documento_sin_comas, currency);
	$("#numLetra").val(numAlet);	
} //lista
					
function btn_busqueda_general() {
	$("#modalBuscarOfertas").modal();	
} //lista

function typea_y_busca_cliente() {
	var valorEscrito = $("#buscadorCliente").val();	
	if (valorEscrito != "") {
		$.ajax({
			url: 'ofvConsultas/consulta_cliente.php',
			type: 'post',
			data: {valor: valorEscrito},
			dataType: 'JSON',
		}).done((resp) => {
			$("#tblcliente tbody").empty();	
			
			if (resp.length) {
				resp.forEach((element, index) => {
					index ++
					var tr_clientes = `<tr onclick="selecciona_cliente_click_desde_table(this)" class="cursor"><td>${index}</td><td class="codigoSN">${element.CodigoSN}</td><td class="nombre">${element.Nombre}</td><td class="rfc">${element.RFC}</td></tr>`
					$("#tblcliente tbody").append(tr_clientes);
				});
			} else {
				var no_hay_registro_td = '<tr><td colspan="4" class="text-center">No hay registros para mostrar</td></tr>' 
					$("#tblcliente tbody").append(no_hay_registro_td);
			}
			
		})															
	} else { 
		$("#tblcliente tbody").empty();
		var no_hay_registro_td = '<tr><td colspan="4" class="text-center">No hay registros para mostrar</td></tr>' 
		$("#tblcliente tbody").append(no_hay_registro_td)
	}
} //lista

function selecciona_cliente_click_desde_table(tr_cliente) {
	tr_cliente.childNodes.forEach(element => {
		var class_name = element.className
		switch (class_name) {
			case 'codigoSN':
				if ($('#BackOrderVentas').hasClass('show')) {		
					console.log(element);
								
					$("#nombreCliente_query").val(element.textContent)
					$("#codigoCliente_query").val(element.textContent);
				} else if ($('#OfertaVentaCliente').hasClass('show')) {
					$("#clienteProveedor").val(element.textContent)
				} else if ($('#OfertaClienteF').hasClass('show')) {
					$("#codigoClienteF").val(element.textContent)
				}else {				
					$("#codcliente").empty()				
					$("#codcliente").val(element.textContent);
					cargar_persona_de_contacto(element.textContent)	
					agregarFormaDePagoSelect(element.textContent)				
				}
				break;
			case 'nombre':
				if ($('#BackOrderVentas').hasClass('show')) {					
					$("#nombreCliente_query").val(element.textContent)					
				} else {
					$("#NombreC").empty()				
					$("#NombreC").val(element.textContent).prop('title', element.textContent);
				}
			break;
			case 'rfc':		
				$("#rfc").empty()		
				$("#rfc").val(element.textContent);
			break;
		}
	})
	$("#buscadorCliente").val("");
	$('#myModal').modal('hide');
} //lista

function ejecutar_buscador_de_articulo(e) {
	var code = (e.keyCode ? e.keyCode : e.which);
	if (code == 13) {
		var valorEscrito = $("#buscadorArticulo").val();
		if (valorEscrito != "") {
			$.ajax({
				url: 'ofvConsultas/consulta_articulo.php',
				type: 'post',
				data: {valor: valorEscrito},
				dataType:'JSON'
			}).done((response) => {					
				$("#tblarticulo tbody").empty();
				var tr_articulo = "";
				if (response.length) {
					response.forEach((element,index) => {					
						tr_articulo += `<tr onclick="seleccionar_articulo_click_desde_table(this)" class="cursor"><td>${index++}</td><td class="codigo_articulo">${element.CodigoArticulo}</td><td>${element.NombreArticulo}</td><td>${new Intl.NumberFormat().format(parseFloat(element.EnStock).toFixed(2))}</td><td>${new Intl.NumberFormat().format(parseFloat(element.Comprometido).toFixed(2))}</td><td>${new Intl.NumberFormat().format(parseFloat(element.Solicitado).toFixed(2))}</td><td class="codigo_almacen">${element.CodigoAlmacen}</td><td>${element.NombreAlmacen}</td></tr>`	
					});
				} else {
					var tr_articulo = '<tr><td colspan="8" class="text-center">No hay registros para mostrar</td></tr>';
				}
				$("#tblarticulo tbody").append(tr_articulo);
			})
		} else {			
			$("#tblarticulo tbody").empty();
			var no_hay_registro_td = '<tr><td colspan="10" class="text-center">No hay registros para mostrar</td></tr>' 
			$("#tblarticulo tbody").append(no_hay_registro_td)
		}
	}
} //lista

function seleccionar_articulo_click_desde_table(tr_articulo) {
	var codigo_articulo = "";
	var codigo_almacen = "";
	tr_articulo.childNodes.forEach(element => {
		switch (element.className) {
			case 'codigo_articulo':
				codigo_articulo = element.textContent;
			break;
			
			case 'codigo_almacen':
				codigo_almacen = element.textContent;
			break;
		}
	})													
	agregar_articulo_a_tabla(codigo_articulo, codigo_almacen);
	$("#buscadorArticulo").val("");
	$("#myModalArticulos").modal('hide');	
} //lista

function agregar_articulo_a_tabla(code, almacen){
												
	$.ajax({
		url: 'ofvConsultas/consulta_articulo_detalle.php',   
		type: 'post',
		data:{code: code, almacen: almacen},
		dataType: 'JSON',
	}).done((res) => {				
		var tr_articulo_detalle = "";
		var num_consecutivo = 0;
		$("#detalleoferta tbody").each((index, element) => {
			num_consecutivo = element.children.length --
		})
		res.forEach((element, index) => {			
			tr_articulo_detalle += `<tr class="remove_tr">
				<td class="u_precio"><a href="#" id="ultimoPrecio"><i class="fas fa-dollar-sign"></i>U precio</a></td>
				<td class="eliminar"><a href="#" id="eliminarFila"><i class="fas fa-trash-alt"></i>Eliminar</a></td>
				<td class="no">${num_consecutivo}</td>
				<td class="narticulo">${element.CodigoArticulo}</td>
				<td class="darticulo">${element.NombreArticulo}</td>
				<td class="cantidad" contenteditable="true" onKeyup="calcular_total_por_tr(this)">1</td>
				<td class="precio_tr" onkeyup="cambio_de_precio_por_partida(this)" contenteditable="true">${new Intl.NumberFormat().format(parseFloat(element.Precio).toFixed(2))}</td>
				<td class="total_tr">${new Intl.NumberFormat().format(parseFloat(element.Precio).toFixed(2))}</td>
				<td class="almacen">${element.CodigoAlmacen}</td>
				<td class="pendiente"></td>
				<td class="codigoPlanificacionSAP"></td>
				<td class="unidadMedidaSAP"></td>
				<td class="comentario1" contenteditable="true"></td>
				<td class="comentario2" contenteditable="true"></td>
				<td class="stock">${new Intl.NumberFormat().format(parseFloat(element.EnStock).toFixed(0))}</td>
				<td class="comprometido">${new Intl.NumberFormat().format(parseFloat(element.Comprometido).toFixed(2))}</td>
				<td class="solicitado">${new Intl.NumberFormat().format(parseFloat(element.Solicitado).toFixed(2))}</td>
				<td class="TreeType"></td>
				<td class="LineNum"></td>
				<td class="Hijo"></td>
				</tr>`
		});
		$(tr_articulo_detalle).insertBefore($("#detalleoferta tbody .last_tr"))							
		calcular_subtotal()	
	})				
} //lista

function calcular_total_por_tr(cantidad) {
	
	var tds = cantidad.parentNode.childNodes;
	if (cantidad.textContent.length && cantidad.textContent != NaN) {
		var cantidad_tr = parseInt(cantidad.textContent)
		var precio_tr = ""	
		
		tds.forEach(element => {
			if (element.className === 'precio_tr') { precio_tr = parseFloat(element.textContent).toFixed(2) }
		})
		tds.forEach(element =>{
			if (element.className === 'total_tr') { element.innerHTML = new Intl.NumberFormat().format(parseFloat(precio_tr * cantidad_tr).toFixed(2)) }
		})

	} else {
		tds.forEach(element =>{
			if (element.className === 'total_tr') { element.innerHTML = 0.00 }
		})
	}
	calcular_subtotal();
} //lista

function replaceAll( text, busca, reemplaza ) {
	while (text.toString().indexOf(busca) != -1)
		text = text.toString().replace(busca,reemplaza);
	return text;
} //lista

function calcular_porcentaje_descuento(descuento) {	
	var porcentaje_descuento = descuento.value

	if (porcentaje_descuento == 0 || porcentaje_descuento == null) {
		document.querySelector("#descNum").value = 0
		document.querySelector("#descAplicado").value = (0.00).toFixed(2)
	} else {
		var subtotal = document.querySelector("#totalAntesDescuento").value
		var cantidad_descuento = parseFloat(subtotal) * (parseInt(porcentaje_descuento) / 100)
		document.querySelector("#descAplicado").value = parseFloat(cantidad_descuento).toFixed(2)
		calcular_total_documento(subtotal, document.querySelector("#impuestoTotal").value)
	}
} //lista

function calcular_redondeo() {

}

function calcular_subtotal() {
	var totalDeuda = 0;
	$("#detalleoferta .total_tr").each((index, element) => {		
		var numero_string_sin_coma = replaceAll( element.innerHTML, ',', "" )
		var numero_float = parseFloat(numero_string_sin_coma)
		totalDeuda = totalDeuda + numero_float			
	})
	$("#totalAntesDescuento").val(new Intl.NumberFormat().format( totalDeuda));
	calcular_total_impuestos(totalDeuda)
	calcular_porcentaje_descuento(document.querySelector("#descNum"))
} //lista

function calcular_total_impuestos(subtotal) {
	var impuesto = subtotal * .16;
	var impuesto_float = parseFloat(impuesto).toFixed(2)
	$("#impuestoTotal").val(new Intl.NumberFormat().format(impuesto_float));
	calcular_total_documento(subtotal, impuesto)
} //lista

function calcular_total_documento(subtotal, impuesto) {
	var subtotal = parseFloat(subtotal)
	var impuesto = parseFloat(impuesto)
	var descuento = parseFloat(document.querySelector("#descAplicado").value)							
	var totalDocumento = (subtotal + impuesto) - descuento
	
	var TotalDocumento_float = parseFloat(totalDocumento).toFixed(2)
	$("#totalDelDocumento").val(new Intl.NumberFormat().format(TotalDocumento_float))
	numero_a_letra()
} //lista

function pintar_tabla_seguimiento() {
	var folio = $("#cdoc").val()
	$.ajax({
		url:'ofvConsultas/consultar_seguimientos.php',
		type: 'POST',
		data: {folio: folio},
		dataType: 'JSON'
	}).done((datos) => {				
		$("#tbl_seguimiento tbody").empty()
		$("#tbl_seguimiento tbody").append(datos)
	})
}

function mostrar_datos_CAB(element) {
	
	$("#codcliente").val(element.CodigoSN).prop("disabled", true);
	$("#NombreC").val(element.Nombre).prop("disabled", true);
	$("#listcontactos").prop("disabled", true).addClass('select-disabled-style');
	$("#oCompra").empty().val(`${element.Referencia}`).prop("disabled", true)
	$("#tmoneda").empty().append(`<option selected>${element.Moneda}</option>`).prop("disabled", true).addClass('select-disabled-style');	
	$(`#usoPrincipal option[value="${element.UsoCFDi}"]`).attr("selected",true).prop("disabled", true);
	$(`#metodoPago option[value="${element.MetodoPago}"]`).attr("selected",true).prop("disabled", true);
	$(`#formaPagoSelect option[value="${element.IdFormaPago}"]`).attr("selected",true).prop("disabled", true);

	$("#cdoc").val(element.FolioSAP).prop("disabled", true);
	$(".status").val(element.Estatus).prop("disabled", true);				
	$("#fconta").val(moment(element.FechaContabilizacion.date).format('YYYY-MM-DD')).prop("disabled", true).addClass('select-disabled-style');
	// $("#fentrega").val(response['FEntrega']).prop("disabled", true);
	$("#fdoc").val(moment(element.FechaDocumento.date).format('YYYY-MM-DD')).prop("disabled", true).addClass('select-disabled-style');
	$("#fvencimiento").val(moment(element.FechaVencimiento.date).format('YYYY-MM-DD')).prop("disabled", true).addClass('select-disabled-style');

	$("#numLetra").val(element.NumeroLetra).prop("disabled", true);
	$("#tipoFactura").val(element.TipoFactura);
	$("#lab").val(element.LAB);
	$("#condicionPago").val(element.DescCondicionesPago);
	$("#rfc").val(element.RFC);

	$('#empleado').val(element.Usuario).prop("disabled", true);
	$('#NombrePropietario').val(element.NombreUsuario).prop("disabled", true);
	$("#proyectoSN").empty().append(`<option selected>${element.Proyecto}</option>`).prop("disabled", true).addClass('select-disabled-style');	
	$("#ventasAdic").empty().append(`<option selected>${element.VentasAdic}</option>`).prop("disabled", true).addClass('select-disabled-style');
	$("#promotor").empty().append(`<option selected>${element.Promotor}</option>`).prop("disabled", true).addClass('select-disabled-style');
	$("#cordVenta").empty().append(`<option selected>${element.CoordinadorVentas}</option>`).prop("disabled", true).addClass('select-disabled-style');
	$(`#comentarios`).val(element.Comentarios);

	$('#totalAntesDescuento').val(element.SubTotalDocumentoFormat).prop("disabled", true);
	$('#descNum').val(element.PorcentajeDescuentoFormat).prop("disabled", true);
	$('#desAplicado').val(element.ImporteDescuento).prop("disabled", true);
	$('#redondeo').val(element.RedondeoFormat).prop("disabled", true);								
	$('#impuestoTotal').val(element.SumaImpuestosFormat).prop("disabled", true);				
	$('#totalDelDocumento').val(element.TotalDocumentoFormat).prop("disabled", true);
	$('#monedaVisor').val(element.Moneda).prop("disabled", true)
}

function mostrar_datos_DET(detalle) {

	var tr_detalle = "";
	detalle.forEach((element, index) => {	
		var cantidad_necesaria
		element.CantidadNecesaria === null ? cantidad_necesaria = 0 : cantidad_necesaria = parseFloat(element.CantidadNecesaria).toFixed(0)
		tr_detalle += `<tr class="remove_tr">
			<td class="u_precio" style="color: green"><a href="#" id="ultimoPrecio"><i class="fas fa-dollar-sign"></i>U precio</a></td>
			<td class="eliminar" style="color: red"><a href="#" id="eliminarFila"><i class="fas fa-trash-alt"></i>Eliminar</a></td>
			<td class="no">${index+1}</td>
			<td>${element.CodigoArticulo}</td>
			<td>${element.NombreArticulo}</td>
			<td class="cantidad" contenteditable="true" onKeyup="calcular_total_por_tr(this)">${parseInt(element.QuantityFormat)}</td>
			<td class="precio_tr" onkeyup="cambio_de_precio_por_partida(this)" contenteditable="true">${(element.PrecioUnitarioFormat)}</td>
			<td class="total_tr">${element.TotalLineaFormat}</td>
			<td>${element.Almacen}</td>
			<td>${cantidad_necesaria}</td>
			<td>${element.CodigoPlanificacionSat}</td>
			<td>${element.UnidadMedidaSAT}</td>
			<td contenteditable>${element.ComentarioPartida1}</td>
			<td contenteditable>${element.ComentarioPartida2}</td>
			<td>${element.EnStockFormat}</td>
			<td>${element.ComprometidoFormat}</td>
			<td>${element.SolicitadoFormat}</td>
			<td>${element.TreeType}</td>
			<td>${element.LineNum}</td>
			<td>${element.Hijo}</td>
			</tr>`
	})
	$("#detalleoferta tbody .remove_tr").remove()
	$(tr_detalle).insertBefore($("#detalleoferta tbody .last_tr"))	
}
  
function consultar_ofv(folio) {
	validar_folio_tblproyectos_masterControl(folio)		
	$.ajax({
		type: 'POST',
		url: 'ofvConsultas/consultaOfertaDeVenta.php',
		data: { cdoc: folio },
		dataType: "json",
	}).done((response) => {		
		response.forEach((element, index) => { //se ejecuta 2 veces para recorrer la CAB y el DET de la OFV consultada			
			if (index === 0 ) { mostrar_datos_CAB(element[0]) }
			if (index === 1 ) { mostrar_datos_DET(element) }
		});								

		$('#btnCrear').prop("disabled", true)
		$('#btnActualizar').show();
		$("#copiarA").removeAttr("disabled");	
		$('#status').val() == 'CERRADO' ? $("#btnActualizar").prop("disabled", true) : $("#btnActualizar").prop("disabled", false)
	});			
} //lista
	
function consultar_ofv_por_typeo(input_typeo) {
	var valorEscrito = input_typeo.value;
	if (valorEscrito) {
		$.ajax({
			url: 'ofvConsultas/buscadorGeneralOFV.php',
			type: 'post',
			data: {valor: valorEscrito},
			dataType: 'JSON'
		}).done((response) => {
			var tr_ofvs = '';
			response.forEach(element => {
				tr_ofvs += `<tr class="cursor" onclick="seleccionar_tr_ofv(this)">
				<td class="cdoc">${element.FolioSAP}</td>
				<td>${element.CodigoSN}</td>
				<td>${element.Nombre}</td>
				<td>${element.FechaContabilizacion.date}</td>
				<td>${element.Estatus}</td>
				</tr>`
			});
			$('#tblBuscarOfertas tbody').empty();
			$('#tblBuscarOfertas tbody').append(tr_ofvs);
		});
	}else {
		$('#tblBuscarOfertas tbody').empty();
	}
} //lista

function consultar_ofv_por_fecha(input_fecha) { 
	var valorFecha = input_fecha.value
	$.ajax({
		url: 'ofvConsultas/buscadorGeneralOFVFecha.php',
		type: 'POST',
		data: { valor: valorFecha },
		dataType:'JSON',
	}).done((response) => {		
		var tr_ofvs = '';
		response.forEach(element => {
			tr_ofvs += `<tr class="cursor" onclick="seleccionar_tr_ofv(this)">
			<td class="cdoc">${element.FolioSAP}</td>
			<td>${element.CodigoSN}</td>
			<td>${element.Nombre}</td>
			<td>${element.FechaContabilizacion.date}</td>
			<td>${element.Estatus}</td>
			</tr>`
		});
		$('#tblBuscarOfertas tbody').empty();
		$('#tblBuscarOfertas tbody').append(tr_ofvs);			
	});
} //lista

function seleccionar_tr_ofv(tr_ofv) {	
	tr_ofv.childNodes.forEach(element => {
		if(element.className === 'cdoc') { consultar_ofv(element.textContent) }
	});	
	$("#modalBuscarOfertas").modal('hide');
} //lista

function consulta_1_atras() {
	var cdoc = $("#cdoc").val();
	cdoc = cdoc -1;
	consultar_ofv(cdoc);	
} //lista

function consulta_1_adelante() {
	var cdoc = $("#cdoc").val();
	cdoc = parseInt(cdoc)+1;
	consultar_ofv(cdoc);	
} //lista

function consultar_primer_registro() {
	var maximini = obtener_folio_max_min_vista_SAP()
	var mini = maximini[0].mini
	consultar_ofv(mini)	
} //lista

function consultar_ultimo_registro() {
	var maximini = obtener_folio_max_min_vista_SAP()
	var maxi = maximini[0].maxi
	consultar_ofv(maxi)	
} //lista	

function mandar_ofv_pedido_al_cliente() {
	$.ajax({
		type:'post',
		url: "ordenDeVentaInsertEnc.php",
		data:{
			cdoc: $('#cdoc').val(),						
			usoCfdi : $("#usoPrincipal").val(),
			metodoPago : $("#metodoPago").val(),
			formaPago : $("#formaPagoSelect").val(),
			numLetra : $("#numLetra").val(),
			tipoFactura : $("#tipoFactura").val(),
			lab : $("#lab").val(),
			condicionPago : $("#condicionPago").val(),
			comentarios : $("#comentarios").val(),
			rfc: $("#rfc").val(),
		},
	}).done ((resp) => {				
		if (resp > 0 ) {
			pintar_mensaje(401, "Espere un momento, creación de orden de venta en proceso")			
		} else {
			pintar_mensaje(2, "Listo, documento en proceso de autorización")			
		}
	})
}

$(document).on('click', '#eliminarFila', function (event) {
	event.preventDefault();
	$(this).closest('tr').remove();
	calcular_subtotal();		
	numero_consecutivo_tabla_detalle_OFV();
}) //lista

function numero_consecutivo_tabla_detalle_OFV() {
	var nFilas = $("#detalleoferta tbody tr").length;
	for (let index = 1; index < nFilas; index++) {
		$("#detalleoferta").find('tr').eq(index).find('td').eq(2).text(index);
	}
} //lista

$(document).on('click', '#ultimoPrecio', function (event) {
	event.preventDefault();
	var cliente = $("#codcliente").val();
	var articulo = $(this).closest('tr').find('td').eq(3).text()

	$.ajax({
		url:'ofvConsultas/ultimo_precio.php',
		type:'POST',
		data: { cliente: cliente, articulo: articulo },
	}).done (function (params) {
		$("#tbl_ultimo_precio tbody").empty();
		$("#tbl_ultimo_precio tbody").append(params);
		$("#modal_ultimo_precio").modal('show');
	})
})

$(document).on('click', '#eliminarFilaSeguimiento', function (event) {	
	event.preventDefault();
	var currentRow=$(this).closest("tr"); 
	var valorEscrito=currentRow.find("td:eq(1)").text();
	$(this).closest('tr').remove();		
	$.ajax({
		type: "post",
		url: "ofvConsultas/eliminaseguimiento.php",
		data: {valor: valorEscrito},		
	}).done ((response) => {
	})											  
});

function agregarFormaDePagoSelect(codigo_cliente){	
	$.ajax({
		url:'rellenarSelectFormaPago.php',
		type:'POST',
		data:{ codigo : codigo_cliente },
	}).done(function (response) {
		$("#formaPagoSelect").empty().append('<option selected disabled>Seleccionar</option>').append(response)				                                                                                                                                                           
	})
}

function validar_folio_tblproyectos_masterControl(folio) {
	$.ajax({
		url:'ofvConsultas/validar_folio_tblproyectos_masterControl.php',
		type:'post',
		data:{ folio: folio }
	}).done (function (response) {
		if (!response) {														
			$("#btnPdf_protegida").hide();
		}
	}) 
}

/////////////////////////////////////////////////////////////////////////////////////////

//SIN REVISAR Y CREO QUE FUNCIONAN

function getValoresCab() {

	var arrayValores = []
	arrayValores = {
		"ClienteCod": $('#codcliente').val(), 
		"NombreCliente": $('#NombreC').val(), 
		"PersonaContacto": $('#NombreC').val(),
		"OrdenCompra": $('#oCompra').val(), 
		"TipoMoneda" : $('#tmoneda').val(), 
		"UsoPrincipal" : $('#usoPrincipal').val(),
		"MetodoPago" : $('#metodoPago').val(), 
		"FormaPago" : $('#formaPagoSelect').val(),

		"FolioDoc" : $('#cdoc').val(), 
		"Status" : $('#status').val(), 
		"FechaConta" : $('#fconta').val(), 
		"FechaEntrega" : $('#fentrega').val(), 
		"FechaDoc" : $('#fdoc').val(), 
		"FechaVencimiento" : $('#fvencimiento').val(),

		"NumeroLetra" : $('#numLetra').val(), 
		"TipoFactura" : $('#tipoFactura').val(), 
		"LAB" : $('#lab').val(),
		"CondicionPago" : $('#condicionPago').val(), 
		"RFC" : $('#rfc').val(),

		"CodEmpleado" : $('#empleado').val(),
		"NombrePropietario" : $('#NombrePropietario').val(),
		"ProyectoSN" : $('#proyectoSN').val(),
		"VentasAdic" : $('#ventasAdic').val(),
		"Promotor" : $('#promotor').val(),
		"CordVenta" : $("#cordVenta").val(),
		"Comentarios" : $('#comentarios').val(),
		
		"Subtotal" : replaceAll($('#totalAntesDescuento').val(), ',', ""),
		"DescPorcentaje" : $('#descNum').val(),
		"DescuentoCant" : replaceAll($('#descAplicado').val(), ',', ""),
		"Redondeo" : replaceAll($('#redondeo').val(), ',', ""),
		"ImpuestoTotal" : replaceAll($('#impuestoTotal').val(), ',', ""),
		"TotalDocumento" : replaceAll($('#totalDelDocumento').val(), ',', ""),
		"MonedaVisor" : $('#monedaVisor').val(),

	}
	return arrayValores;
}

function aplicarVistaCliente(){
	$(".ocultarAlCte").hide()
	$("#codcliente").prop("disabled", true);
	$("#NombreC").prop("disabled", true);
	$("#empleado").prop("disabled", true);
	$("#NombrePropietario").prop("disabled", true);
	$("#detalleoferta tbody tr td:eq(4)").prop("contenteditable", false);
	$("#detalleoferta tbody tr td:eq(10)").prop("contenteditable", false);
	$("#detalleoferta tbody tr td:eq(11)").prop("contenteditable", false);
}

function mostrar_mensajes_ofv() {

	$("#tblMensajes tbody").empty();
	$.ajax({
		url:'modalMensajeConsulta.php',
		success: function (params) {
			$("#tblMensajes tbody").append(params);

			$("#tblMensajes tbody tr").on('click', function(){
					var FolioSAP = $(this).find('td').eq(0).text();
					var ClaseDocumento = $(this).find('td').eq(1).text();
					var estatus = $(this).find('td').eq(4).text();
					var ordenVenta = $(this).find('td').eq(5).text();

					$("#tblMensajes2").find('td').eq(0).text("1");
					$("#tblMensajes2").find('td').eq(1).text(ordenVenta);
					$("#tblMensajes2").find('td').eq(2).text(FolioSAP);
					$("#tblMensajes2").find('td').eq(3).text(ClaseDocumento);
					$("#tblMensajes2").find('td').eq(4).text(estatus);

				$("#tblMensajes2 tbody tr").on('click', function(){
					var ordenVenta = $(this).find('td').eq(1).text();
					var FolioSAP = $(this).find('td').eq(2).text();
					var estatus = $(this).find('td').eq(4).text();

					switch (estatus) {
						case 'CREADA':
							window.location.href = 'ordenDeVenta.php?FolioSAP='+FolioSAP+'&ofv='+ordenVenta;
							break;
						case 'PENDIENTE CREAR REAL':
							alert("Documento en proceso de creación en firme");
							break;
						case 'ERROR CREAR REAL':
							alert("Error en proceso de creación en firme");
							break;
						case 'BORRADOR CREADO':
							window.location.href = 'ordenDeVentaBorrador.php?FolioSAP='+FolioSAP;
							break;
						case 'PENDIENTE CREAR BORRADOR':
							alert("Documento en proceso de creación de borrador");
							break;
						case 'ERROR CREAR BORRADOR':
							alert("Error en proceso de creación de borrador");
							break;
					}
				});
			});
		}
	})
}

////////////////////////////////////////////////////////////////////////////////

function getValoresDet() {
	arrayValores = [];
	$("#detalleoferta tbody tr").each((index, element) => {		
		if(element.className === "remove_tr") {
			var single_tr = []			
			element.childNodes.forEach((element, index) => {
				if (element.nodeName != "#text" && element.className != 'no' && element.className != 'eliminar' && element.className != 'u_precio') {					
					if(element.nodeName === 'precio_tr' || element.nodeName === 'total_tr') {
						var cantidades_sin_comas = replaceAll(element.textContent, ',', "")
						single_tr.push(cantidades_sin_comas)
					} else {
						single_tr.push(element.textContent)
					}
				}				
			})
			arrayValores.push(single_tr);
		}
	})
	return arrayValores;
}

function crear_ofv() {
	var valoresCab = getValoresCab();	
	var valoresDet = getValoresDet();	
		
	if (Object.keys(valoresDet[0]).length <= 0) {
		pintar_mensaje(401, "Favor de introducir articulos en la tabla antes de continuar");
	} else {		
		$.ajax({
			type:'POST',
			url: "ofvConsultas/ofertaDeVentaInsertEnc.php",
			data:{ valores : valoresCab, detalle : valoresDet },
			dataType: 'JSON',
		}).done((response) => {
			pintar_mensaje(response.resp, response.mnj)							
		})
	}
} //lista
	
function seleccionar_condicion_de_pago(tr_condicion_pago) {	
	$("#condicionPago").val(tr_condicion_pago.children[0].textContent);
	$("#modalCondicionPago").modal('hide');
}

function seleccionar_tipo_factura(tr_tipo_factura) {
	$("#tipoFactura").val(tr_tipo_factura.children[0].textContent);
	$("#modalTipoFactura").modal('hide');
}

function pintar_mensaje(valor, mensaje) {
	var position_mensaje = $(".mensaje .col-12")
	position_mensaje.empty()
	switch (valor) {
		case 0:
			position_mensaje.addClass('mensaje-style-error').append('<i class="fas fa-times"></i>'+' '+mensaje)
			break;
		case 2:
			position_mensaje.addClass('mensaje-style-success').append('<i class="fas fa-check-circle"></i>'+' '+mensaje)
			setTimeout('document.location.reload()',3000);
			break;
		default:
			position_mensaje.addClass('mensaje-style-warning').append('<i class="fas fa-exclamation-triangle"></i>'+' '+mensaje)
			break;
	}
	$('html, body').animate({scrollTop:0}, 'slow'); 
} //lista

//---------------------------------------------------------------------CONSULTAS PARA QUERYS 

function query_back_orders() {
	var fechaIni = $("#fechaIni").val();
	var fechaFin = $("#fechaFin").val();
	var nombreCliente = $("#nombreCliente_query").val();
	var codigoCliente = $("#codigoCliente_query").val();
	window.location.href = 'btnBackOrderOk.php?fechaIni='+fechaIni+'&fechaFin='+fechaFin+'&nombreCliente='+nombreCliente+'&codigoCliente='+codigoCliente;			
} //lista

function query_comisiones() {
	var fechaS = $("#fechaSuperior").val();
	var fechaM = $("#fechaMenor").val();
	window.location.href = 'comisionesVentas.php?fechaS='+fechaS+'&fechaM='+fechaM;	
} //lista

function entrda_articulos() {
	var fechaContabilizacionSuperior = $("#fechaContabilizacionSuperior").val();
	var fechaContabilizacionMenor = $("#fechaContabilizacionMenor").val();			
	window.location.href = 'btnEntregaArticulos.php?fechaContabilizacionSuperior='+fechaContabilizacionSuperior+'&fechaContabilizacionMenor='+fechaContabilizacionMenor;		
} //lista

function query_oferta_de_venta_especial() {
	var clienteProveedor = $("#clienteProveedor").val();
	window.location.href = 'ofertaVentaCliente2doNivel.php?clienteProveedor='+clienteProveedor;
} //Lista 

function query_oferta_de_veenta_especial_fecha() {
	var codigoCliente = $("#codigoClienteF").val();
	var fecha1 = $("#fecha1").val();
	var fecha2 = $("#fecha2").val();
	window.location.href = 'btnOFVClienteFecha.php?codigoCliente='+codigoCliente+'&fecha1='+fecha1+'&fecha2='+fecha2;
} //lista

function query_oferta_de_venta_fecha() {
	var fecha1 = $("#campoFecha1").val();
	var fecha2 = $("#campoFecha2").val();								
	window.location.href = 'btnOFVFecha.php?fecha1='+fecha1+'&fecha2='+fecha2;
} //lista

function query_pedidos_del_dia() {
	var fechaconta = $("#fechaconta").val();
	var statusDocumento = $("#statusDocumento").val();
	window.location.href = 'pedidos_del_dia.php?fechaconta='+fechaconta+'&statusDocumento='+statusDocumento;
} //lista

function query_mapa_de_relaciones() {
	var fechaInicio = $("#fechaInicio").val();	
	var fechaFin = $("#fechaFinito").val();
	location.href ='mapaDeRelaciones.php?fi='+fechaInicio+'&ff='+fechaFin;
} //lista

//------------------------------------------------------------------------END CONSULTAS PARA QUERYS 

function agregarPersonaContactoSelect() {
	var codigo = '<?php echo $_SESSION["cliente"]; ?>';
	var name = $("#tblcliente").find('td').eq(2).text();   	
	var rfc = $("#tblcliente").find('td').eq(3).text();   	
	$("#codcliente").val(codigo);
	$("#NombreC").val(name);
	$('#myModal').modal('hide');
	$("#rfc").val(rfc);
	$.ajax({
		url: 'consultaPersonaContacto.php',
		type: 'POST',
		data: {code: codigo},
	}).done((response) => {
		$("#listcontactos").append(response);		
	});
}

function cargarCliente() {
	var valorEscrito = '<?php echo $_SESSION["cliente"] ?>';					 
	$.ajax({
		url: 'buscadorConsultaCliente.php', 
		type: 'POST',
		data: {valor: valorEscrito},
	}).done((resp) => {
		$("#tblcliente tbody").empty();
		$("#tblcliente tbody").append(resp);																																																
		agregarPersonaContactoSelect();
		agregarFormaDePagoSelect();
	});
}

/////////////////////////////////////////////////////////////////////////////////////////
// FALTA DARLE UNA RENOVADITA AL CODIGO
$("#btnPdf").on("click", function (params) {
	var folio =  $("#cdoc").val();			
	$.ajax({
		type: 'POST',
		url: 'http://187.188.40.90:85/CrystalReportViewer/ReporteOFV.aspx?nom='+folio,
	}).done((params) => {
		// if (params === '2') {												
		// 	window.open(' visor/dos/IV Oferta de venta.pdf'); //This will open Google in a new window.
		// } else {
		// 	alert("No se puede mostrar la oferta por el momento");
		// } 
		
	})
	var winFeature ='location=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes';
	wait(2200);
	window.open('visor/dos/OF '+folio+'.pdf','_blank','menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=no,resizable=no,dependent,width=800,height=620,left=0,top=0');
})

function wait(ms) {
	var start = new Date().getTime();
	var end = start;
	while(end < start + ms) {
		end = new Date().getTime();
	}
}
////////////////////////////////////////////////////////////////////////////////////////

$("#btnPdf_protegida").on("click", function (params) {
	var folio =  $("#cdoc").val();			

	$.ajax({
		url: 'visor/visor_crystal_protegido.php',
		type: 'POST',
		data: { folio: folio }
	}).done((params) => {
		if (params === '2') {												
			window.open(' visor/dos/IV Oferta de venta.pdf')
		} else {			
			pintar_mensaje(0, "No se puede mostrar la oferta por el momento")
		}
		
	})
})

	function actualizar_ofv() {
	var valoresDet = getValoresDet()
	$.ajax({
		url: 'ofvConsultas/actualizarCab.php',
		type: 'POST',
		dataType: 'JSON',
		data: {
			usoPrincipal: $('#usoPrincipal').val(),
			metodoPago: $('#metodoPago').val(),
			formaPago: $('#formaPagoSelect').val(), 
			
			cdoc: $('#cdoc').val(),
			
			numLetra: $('#numLetra').val(),
			tipoFactura: $('#tipoFactura').val(),
			lab: $('#lab').val(),
			condicionPago: $('#condicionPago').val(),
			rfc: $("#rfc").val(),
			comentarios: $("#comentarios").val(),

			subtotal: $("#totalAntesDescuento").val(),
			descuentoPorcentaje: $("#descNum").val(),
			descuentoCantidad: $("#descAplicado").val(),
			redondeo: $("#redondeo").val(), 
			impuesto: $("#impuestoTotal").val(),
			totalDocumento: $("#totalDelDocumento").val(),
			
			detalle: valoresDet,
		}
	}).done((response) => {				
		pintar_mensaje(response.resp , response.mnj)	
	})	
}

function agregar_visita() {
	var comentarios = $("#txt_area_comentarios_visita").val()
	var tipo = $("#slt-comentarios-visita").val()
	var usuario = $("#empleado").val()
	var folio = $("#cdoc").val()

	$.ajax({
		url: 'ofvConsultas/agregar_seguimiento.php',
		type: 'Post',
		data: {folio: folio, comentarios: comentarios, tipo: tipo, usuario: usuario, tipoSeguimiento: "Visita"}
	}).done((response) => {
		alert("se guardó con éxito")
		pintar_tabla_seguimiento(folio)
	})
}

function agregar_llamada() {
	var comentarios = $("#txt_area_comentarios_llamada").val()
	var tipo = $("#slt-comentarios-llamada").val()
	var usuario = $("#empleado").val()
	var folio = $("#cdoc").val()

	$.ajax({
		url: 'ofvConsultas/agregar_seguimiento.php',
		type: 'Post',
		data: {folio: folio, comentarios: comentarios, tipo: tipo, usuario: usuario, tipoSeguimiento: "Llamada"}
	}).done((response) => {				
		alert("se guardó con éxito")
		pintar_tabla_seguimiento(folio)
	})
}



// if($("#hide-cliente").val() != '0'){
// 	var esCliente = $("#hide-cliente").val();
// 	cargarCliente();
// 	aplicarVistaCliente();
// }					
			



function contador_cargador() {
	var segundos_reales = moment().format('ss');
	var contador_reversa = 60 - parseInt(segundos_reales)	
	$("#timer").empty()
	$("#timer").append(contador_reversa)
	$("#last-timer").empty()
	$("#last-timer").append(moment().format('LT'))
	setTimeout(() => {
		contador_cargador()
	}, 1000);
} //lista