<!DOCTYPE html>
<html>
<?php include "header.php" ?>
	<body>
		<?php include "nav.php" ?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<input type="hidden" value="<?php echo $_GET["fechaIni"] ?>" id="fechaIni">
					<input type="hidden" value="<?php echo $_GET["fechaFin"] ?>" id="fechaFin">
					<input type="hidden" value="<?php echo $_GET["nombreCliente"] ?>" id="nombreCliente">
					<input type="hidden" value="<?php echo $_GET["codigoCliente"] ?>" id="codigoCliente">
					<div class="col-md-12">
						<br>
						<section class="table-responsive">
							<table class="table table-striped table-sm table-bordered table-editable text-center" id="tblbackOrderTable">
								<thead>
									<tr>
									<th>#</th>
									<th>Código Artículo</th>
									<th>Nombre Artículo</th>
									<th>Almacén</th>
									<th>Cant. Sol</th>
									<th>Cant. Ab</th>
									<th>Exist. Alm. Dest</th>
									<th>U FechaResepción</th>
									<th>U Comentarios</th>
									<th>No. Cliente</th>
									<th>Nombre Cliente</th>
									<th>Orden de venta</th>
									<th>OC SAP</th>
									<th>Codigo Proveedor/th>
									<th>Nombre Proveedor</th>
									<th>Referencia</th>
									<th>Fecha Documento</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</section>
					</div>
				</div>
				<div class="col-5 offset-7">
					<a href="ofertaDeVenta.php">
						<button class="btn btn-primary btn-block">Regresar a OFV</button>
					</a>
				</div>
			</div>
		</div>
		<?php include "footer.php" ?>		
		<script src="js/btn_back_order_ok.js"></script>
	</body>                     
</html>