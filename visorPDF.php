<?php 
$folio = $_GET['folio'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Eypo</title>
    <style>
        .pdfobject-container { height: 39rem; border: 1rem solid rgba(0,0,0,.1); }
    </style>
</head>
<body>
    <?php include "header.php" ?>
    <div class="container">
        <br>
        <div id="viewpdf"></div>
    </div>
    <?php include "footer.php" ?>    

    <script>
        var folio = <?php echo $folio ?>;                    
        console.log(folio);
        
        var viewer  =  $("#viewpdf")    
        var algo = PDFObject.embed("file:///C://FAE_PRODUCTIVO//Factura//"+folio+".pdf", viewer);
        console.log(algo);
        
    </script>
</body>
</html>