<!DOCTYPE html>
<html>
	<?php include "header.php" ?>
	<body>
	<?php include "nav.php" ?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">	
					<input type="hidden" value="<?php echo $_GET["clienteProveedor"]?>" id="cliente_proveedor_modal">		
					<br>	
					<section class="table-responsive"> 					
						<table class="table table-striped table-sm table-bordered table-editable text-center" id="tblOfvCliente">
							<thead>
								<tr>
									<th>#</th>
									<th>DocStatus</th>
									<th>dOCNUM</th>
									<th>docdate</th>
									<th>CadCode</th>
									<th>ItemCode</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</section>					
				</div>
				<div class="col-5 offset-7">
					<a href="ofertaDeVenta.php">
						<button class="btn btn-primary btn-block">Regresar a OFV</button>
					</a>
				</div>
			</div>
			<?php include "footer.php" ?>	
			<script src="js/ofv_cliente2do_nivel.js"></script>	
		</div>
	</body>                     
	
</html>