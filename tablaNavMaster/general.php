<br>
						
					  	<br>
<div class="row">
					  		<div class="col-md-12">
							<div class="row" align="center">
						<label for="" class="col-sm-3 col-form-label">Codigos Protegidos:</label>
						
					</div>
								Da clic sobre el renglon para buscar Códigos Protegidos
					  			<table class="table table-bordered table-editable table-hover table-striped" width="100%" id="detalleprotegidos">
					        		<thead>
					        			<tr class="encabezado" >
											<th><i class="fas fa-ban"></i></th>
											<th>#</th>
											
					        				<th>Marca</th>
					        				<th>Genero</th>
					        				<th>CodigoReal</th>
					        				<th>CodigoProtegido</th>
					        				<th>Descripcion</th>
					        				<th>Tipo</th>
											<th>Potencia</th>
					        				<th>Cantidad</th>
					        				<th style="display:none;">CodigoBase</th>
											
										</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>
											<td class="cont"></td>
											<td data-toggle="modal" data-target="#myModalCodigos" class="marca"></td>
								          	<td data-toggle="modal" data-target="#myModalCodigos" class="modelo"></td>
											<td data-toggle="modal" data-target="#myModalCodigos" class="codigoreal"></td>
											<td data-toggle="modal" data-target="#myModalCodigos" class="codigoprotegido"></td>
											<td data-toggle="modal" data-target="#myModalCodigos" class="descripcion"></td>
											<td class="tipo"></td>
											<td class="potencia"></td>
								            <td contenteditable="true" class="cantidad"></td>
								            <td class="codigobase" style="display:none;"></td>
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>
<br>

<br>
						
					  	<br>
<div class="row">
					  		<div class="col-md-12">
							<div class="row" align="center">
						<label for="" class="col-sm-3 col-form-label">Codigos Editables:</label>
						
					</div>
								Da doble clic sobre el renglon para buscar articulos en SAP
					  			<table class="table table-bordered table-editable table-hover table-striped" width="100%" id="detallenoprotegidos">
					        		<thead>
					        			<tr class="encabezado" >
											<th><i class="fas fa-ban"></i></th>
											<th>#</th>
											<th>Marca</th>
					        				<th>Género</th>
					        				<th>CodigoReal</th>
					        				<th>CodigoProtegido</th>
					        				<th>Descripcion</th>
					        				<th>Tipo</th>
											<th>Potencia</th>
					        				<th>Cantidad</th>
					        				<th style="display:none;">CodigoBase</th>
											
										</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>
											<td class="cont"></td>
								          	
								          	<td contenteditable="true" class="marcan" onclick="marcaeditable();"></td>
											<td contenteditable="true" class="modelon" onclick="marcaeditable();"></td>
											<td contenteditable="true" class="codigorealn" onclick="marcaeditable();"></td>
											<td contenteditable="true" class="codigoprotegidon"onclick="marcaeditable();"></td>
											<td contenteditable="true" class="descripcionn"onclick="marcaeditable();"></td>
											<td contenteditable="true" class="tipon" onclick="marcaeditable();"></td>
											<td contenteditable="true" class="potencian" onclick="marcaeditable();"></td>
								            <td contenteditable="true" class="cantidadn"onclick="marcaeditable();"></td>
								            <td class="codigobasen" style="display:none;"></td>
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>
<br>
				