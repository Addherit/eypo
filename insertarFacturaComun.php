<?php 
    include "conexion.php";


    $arrayDET = $_POST['arrayDetalle'];
    $arrayCAB = $_POST['arrayCabecera'];
    $hoy = date("Y-m-d H:i:s"); 
 

    $val = "";

    for($x=0; $x < count($arrayCAB); $x++) {
            
        $val .="'".$arrayCAB[$x]."',"; 
    }
    $val = trim($val, ',');
    

    $sql = "INSERT INTO dbEypo.dbo.facturasCab 
    (Cliente, Nombre, PersonaDeContacto, TipoMoneda, FormaPago, NDoc, CDoc, Estado, FechaContabilizacion, FechaDocumento, 
    FechaVencimiento, UsoPrincipal, MetodoPago, EmpleadoVentas, ProyectoSN, VentasAdic, Promotor, PromotorDeVenta, 
    Comentarios, TotalAntesDescuento, Descuento, Redondeo, Impuesto, Total, ImporteAplicado, SoloVencido, TipoDeFactura) 
    VALUES ($val)";        
    $consulta = sqlsrv_query($conn, $sql);

    if( $consulta === false ) {
        echo "Algo falló al insertar los datos de cabecera.";
    } else {

        for($x=0; $x<count($arrayDET[0]); $x++){
        $string = "";
        for($y=0; $y<10; $y++){                
            $string .= "'".$arrayDET[$y][$x]."',";               
        }

        $string .= "'".$arrayCAB[6]."'";
        $sql = "INSERT INTO dbEypo.dbo.FacturasDet (CodigoArticulo, NombreArticulo, Cantidad, PrecioPorUnidad, Descuento, 
        IndImpuesto, SujetoARetencionDeImpuesto, Total, UnidadSAT, Almacen, FolioSAP)
        VALUES ($string)";
        $consulta2 = sqlsrv_query($conn, $sql);                      
        }

        if( $consulta2 === false ) {
        echo "Algo falló al insertar los datos de detalle.";
        } else { 
        echo "exito";   
        } 
    }

?>