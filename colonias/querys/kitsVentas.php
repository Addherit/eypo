<!DOCTYPE html>
<html lang="en"> 
<?php include "../header.html"?>		
	<body>		
        <?php include "../nav.php" ?>
		<?php include "../../modalQuerys.php" ?>
		<?php include "../../modales.php" ?>
        <div class="container formato-font-design" id="contenedorDePagina">
            <div class="row">
                <div class="col-12">	
                    <br>	
                    <section class="table-responsive"> 				
                        <table id="tbl_kit_venta" class="table table-striped table-sm table-bordered table-editable text-center">
                            <thead>
                                <tr>
                                    <th>No°</th>
                                    <th>Code</th>
                                    <th>IntemName</th>
                                    <th>Cant</th>
                                    <th>Tipo</th>                      
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>	
                    </section>			
                </div>
                <div class="col-5 offset-7">
                <a href="ofertaDeVenta.php">
                    <button class="btn btn-primary btn-block">Regresar a OFV</button>
                </a>
                </div>
            </div>
        </div>          
		<?php include "../footer.html" ?>		
        <script src="js/kits_ventas.js"></script>
	</body>
</html>