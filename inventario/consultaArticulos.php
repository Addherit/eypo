<?php
  include "../conexion.php";
  $codigoSN = $_POST['code'];
  $codigoAlmacen = $_POST['calmacen'];


  $sql = "SELECT lm.CodigoArticuloComponente, art.NombreArticulo, lm.Quantity FROM EYPO.dbo.IV_EY_PV_Articulos art
  LEFT JOIN EYPO.dbo.IV_EY_PV_ListasMateriales lm ON art.CodigoArticulo = lm.CodigoArticuloComponente
  WHERE CodigoArticuloPadre = '$codigoSN'";
$consulta = sqlsrv_query($conn, $sql);
$inventarioComponentes = [];
$count = 0;
WHILE ($row = sqlsrv_fetch_array($consulta)){
  $count ++;

  $linea = "
  <tr>    
    <td>".$count."</td>
    <td>".$row['CodigoArticuloComponente']."</td>
    <td>".utf8_encode($row['NombreArticulo'])."</td>
    <td>".number_format($row['Quantity'],0, '.', ',')."</td>
  </tr>
  ";
  array_push($inventarioComponentes, $linea);

}

  $sql ="SELECT TOP 1 * FROM EYPO.dbo.IV_EY_PV_Articulos art 
  LEFT JOIN EYPO.dbo.IV_EY_PV_ListasPrecios lp ON art.CodigoArticulo = lp.CodigoArticulo 
  LEFT JOIN EYPO.dbo.IV_EY_PV_Stock stock ON art.CodigoArticulo = stock.CodigoArticulo
  LEFT JOIN EYPO.dbo.IV_EY_PV_Almacenes al ON stock.CodigoAlmacen = al.CodigoAlmacen 
 
  LEFT JOIN EYPO.dbo.IV_EY_PV_GruposUnidadesMedida gum ON  art.CodigoGrupoUnidadMedida = gum.CodigoGrupoUnidadMedida 
  LEFT JOIN EYPO.dbo.IV_EY_PV_Fabricantes fab ON art.CodigoFabricante = fab.CodigoFabricante
  WHERE art.CodigoArticulo = '$codigoSN' AND al.CodigoAlmacen = '$codigoAlmacen'";









  $consulta = sqlsrv_query($conn, $sql);
  $Row = sqlsrv_fetch_array($consulta);
  $codigoArticuloPadre = $Row['CodigoArticulo'];
 
  $inventarioGeneral = [   
    "gruposArticulos" => $Row['DescGrupoArticulo'],
    "descripcionUnidadMedida" => $Row['Descripcion'],
    "codigoListaPrecios" => $Row['CodigoListaPrecios'],

    "sujetoImpuesto" => $Row['AplicaImpuesto'],
    "noAplicaGrupoDesc" => $Row['NoAplicaGrupDesc'],

    "fabricante" => $Row['NombreFabricante'], 
    "idAdicional" => $Row['IdAdicional'],
    "gestionPor" => $Row['GestionPor'],  //nyserie
    // "pedimento" => $Row[''],


    "codigoPredeterminado" => $Row['CodigoProveedor'],
    "NumCatFabricante" => $Row['NumCatFabricante'],
    "InvUM" => $Row['InvUM'],
    "ComprasUM" => $Row['ComprasUM'],
    // "unidadMedidaEmp" => $Row[''],
    "CantidadPaq_Com" => number_format($Row['CantidadPaq_Com'],0,'.',' '),
    "clasificacionSATCodigo" => $Row['ClasificacionSatCodigo'],



    "codigoAlmacen" => $Row['CodigoAlmacen'],
    "nombreAlmacen" => utf8_encode($Row['NombreAlmacen']),
    "bloqueo" => $Row['Bloqueo'],
    "stock" => number_format($Row['EnStock'],0, '.', ' '),
    "primeraUbicacion" => $Row['PrimeraUbicacion'],
    "comprometido" => number_format($Row['Comprometido'], 2, '.', ' '),
    "ubicacionPorDef" => $Row['UbicacionPorDef'],
    "pedido" => number_format($Row['Solicitado'], 2, '.', ' '),
    "ejecUbiEstandar" => $Row['EjecUbiEstandar'],
    "disponible" => number_format($Row['Disponible'],0, '.', ' '),

      

    "array_Componentes" => $inventarioComponentes,

    "SQL" => $sql


    

];






   echo json_encode($inventarioGeneral);




?>
