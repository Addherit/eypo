<?php 
    include "../../../db/Utils.php";
    $folioSAP = $_POST['folio'];
    $condicion = $_POST['con'];   
    $ultimoRegistro = obtener_ultima_folio($conn);
    $primerRegistro = obtener_primer_folio($conn);

    if ($folioSAP == 'primero') {
        $primerRegistro = obtener_primer_folio($conn);
        $folioSAP = $primerRegistro['FolioInterno'];
    }

    if ($folioSAP == 'ultimo') {
        $ultimoRegistro = obtener_ultima_folio($conn);
        $folioSAP = $ultimoRegistro["FolioInterno"];
    }
    

    function obtener_primer_folio($conn) {
        
        $sql="SELECT TOP 1 enc.*, soc.Nombre, soc.PersonaContacto FROM EYPO.dbo.IV_EY_PV_EntregasVentaCab enc
        LEFT JOIN EYPO.dbo.IV_EY_PV_SociosNegocios soc ON enc.CodigoSN = soc.CodigoSN WHERE enc.SerieNombre = 'COLONIAS' ORDER BY FolioSAP ASC";
            $consulta = sqlsrv_query($conn, $sql); 
            $row = sqlsrv_fetch_array($consulta, SQLSRV_FETCH_ASSOC);            
            return $row;
    }

    function obtener_ultima_folio($conn) {
        
        $sql="SELECT TOP 1 enc.*, soc.Nombre, soc.PersonaContacto FROM EYPO.dbo.IV_EY_PV_EntregasVentaCab enc
        LEFT JOIN EYPO.dbo.IV_EY_PV_SociosNegocios soc ON enc.CodigoSN = soc.CodigoSN WHERE enc.SerieNombre = 'COLONIAS' ORDER BY FolioSAP DESC";
            $consulta = sqlsrv_query($conn, $sql); 
            $row = sqlsrv_fetch_array($consulta, SQLSRV_FETCH_ASSOC);            
            return $row;
    }
    
    if($folioSAP > $ultimoRegistro["FolioInterno"] OR $folioSAP < $primerRegistro["FolioInterno"]){
        $Array2 = [
            'respuesta' => 1
        ];
        echo json_encode($Array2);
    } else {

        do{

            $sql="SELECT enc.*, soc.Nombre, soc.PersonaContacto FROM EYPO.dbo.IV_EY_PV_EntregasVentaCab enc
            LEFT JOIN EYPO.dbo.IV_EY_PV_SociosNegocios soc ON enc.CodigoSN = soc.CodigoSN                  
            WHERE FolioSAP = '$folioSAP' AND enc.SerieNombre = 'COLONIAS'";
            $consulta = sqlsrv_query($conn, $sql); 
            $row = sqlsrv_fetch_array($consulta);
            $folioInternoCAB = $row["FolioInterno"];


            switch ($condicion) {
                case 'adelante':
                    $folioSAP++;
                    break;
                
                case 'atras': 
                    $folioSAP--;
                    break; 
            }
        }while(!$row);

        $sql2 ="SELECT * FROM EYPO.dbo.IV_EY_PV_EntregasVentaDet 
        WHERE FolioInterno = '$folioInternoCAB'";
        $consulta2 = sqlsrv_query($conn, $sql2);
        $array_det=[]; 
        $cont=0; 
        WHILE ($RowDet = sqlsrv_fetch_array($consulta2)){
            $cont++;          
            $linea = '<tr>                
                <td>' . $cont . '</td>
                <td class="codAticulo">'.$RowDet['CodigoArticulo'].'</td>
                <td class="narticulo">'.$RowDet['NombreArticulo'].'</td>   
                <td class="cantidad" contenteditable="true">'.number_format($RowDet['Quantity'],0,'.','').'</td>   
                <td class="precioUnitario">'.number_format($RowDet['PrecioUnitario'],2,'.',',').'</td>   
                <td class="porcentajeDescuento">'.number_format($RowDet['PorcentajeDescuento'],2,'.','').'</td>   
                <td class="impuesto">'.number_format($RowDet['Impuestos'],2,'.',',').'</td>   
                <td class="codigoImpuesto">'.$RowDet['CodigoImpuesto'].'</td>                                                        
                <td class="total">'.number_format($RowDet['TotalLinea'],2,'.',',').'</td>                  
                <td class="unidadSAT">'.$RowDet['UnidadMedidaSAT'].'</td> 
                <td class="almacen">'.$RowDet['Almacen'].'</td>                                                      
            ';
                    
            array_push($array_det, $linea);
        }

        $arrayContent = [

            'respuesta' => 2,
            'cliente' => $row['CodigoSN'],
            'nombreCliente' => $row['Nombre'],
            'personaContacto' => $row['PersonaContacto'],
            'tipoMoneda' => $row['Moneda'], 
            
            'ndoc' => $row['SerieNombre'], 
            'cdoc' => $row['FolioSAP'], 
            'estado' => $row['Estatus'],
            'fconta' => $row['FechaContabilizacion']->format('Y-m-d'),
            'fdoc' => $row['FechaDocumento']->format('Y-m-d'),
            'fven' => $row['FechaVencimiento']->format('Y-m-d'),
            'usoPrincipal' => $row['UsoCFDi'],
            'metodoPago' => $row['MetodoPago'],
            'formaPago' => $row['DescFormaPago'],

            'empleado' => $row['NombreEmpleadoVC'],
            'propietario' => $row['NombrePropietario']." ".$row['SegundoNombrePropietario']." ".$row['ApellidoPropietario'],
            'proyectoSN'  => $row['Proyecto'],
            'promotor' => $row['PromotorVentas'],
            'comentarios' => $row['Comentarios'],
            'ventasAdic' => $row['VentasAdic'],

            'totalAntesDescuento' => number_format($row['SubTotalDocumento'],2,'.',','),                        
            'descuento' => number_format($row['ImporteDescuento'],2,'.',','),    
            'redondeo' => number_format($row['Redondeo'],2,'.',''),    
            'impuestoTotal' => number_format($row['SumaImpuestos'], 2, '.', ','),
            'totalDelDocumento' => number_format($row['TotalDocumento'], 2, '.', ','),                   
            'array' => $array_det,

        ];

        echo json_encode($arrayContent);
    }

?>