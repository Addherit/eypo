<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="capture" style="padding: 10px; background: #f5da55">
        <h4 style="color: #000; ">Hello world!</h4>
    </div>
</body>
<script src="node_modules/html2canvas/dist/html2canvas.min.js"></script>
<script>
    html2canvas(document.querySelector("#capture")).then(canvas => {
        var base64image = canvas.toDataURL("image/png");
        window.open(base64image , "_blank");        
    });

//     html2canvas(document.body).then(function(canvas) {
//     // Export the canvas to its data URI representation
//     var base64image = canvas.toDataURL("image/png");

//     // Open the image in a new window
//     window.open(base64image , "_blank");
// });
</script>

</html>