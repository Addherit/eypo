document.addEventListener('DOMContentLoaded', () => {
    document.querySelector("#empleado").value = localStorage.getItem("departamento")
    document.querySelector("#propietario").value = localStorage.getItem("nombre_completo")
    document.querySelector("#mensajes").hidden = true 
    document.querySelector("#consultaQuery").hidden = true
    document.querySelector("#btnPdf").hidden = true
    document.querySelector("#btnxlm").hidden = true
    cargar_folio_consecutivo()
})

function cargar_folio_consecutivo() {
    const formData = new FormData()
    formData.append('folio', 'ultimo')
    formData.append('con', 'atras')

    fetch('php/rellenarInputsEntrega.php', {method: 'POST', body: formData})
    .then(data => data.json())
    .then(data => {
        document.querySelector("#cdoc").value = (data.cdoc) + 1        
    })
}


document.querySelector("#btnBuscarGRAL").addEventListener('click', () => {
    $("#modalEntrega").modal('show')
});

function consultaClickFactura(folio, condicion) {
    const formData = new FormData()
    formData.append('folio', folio)
    formData.append('con', condicion)

    fetch('php/rellenarInputsEntrega.php', {method: 'POST', body: formData})
    .then(data => data.json())
    .then(data => {                						
        switch(data['respuesta']){
            case 1:									
                alert("No hay mas resultados.");								 
            break;
            case 2: 									
                mostrarValoresDeBusquedaEnInputs(data)	
            break;
        }
    })
}

function mostrarValoresDeBusquedaEnInputs(data) {    
    
    $("#cliente").val(data.cliente)
    $("#nCliente").val(data.nombreCliente)
    $("#personaContacto").val(data.personaContacto)
    $("#tipoMoneda").val(data.tipoMoneda)

    $("#nDoc").val(data.ndoc)
    $("#cdoc").val(data.cdoc)
    $("#estado").val(data.estado)
    $("#fconta").val(data.fconta)
    $("#fdoc").val(data.fdoc)
    $("#fven").val(data.fven)
    $("#usoPrincipal").val(data.usoPrincipal)
    $("#metodoPago").val(data.metodoPago)
    $("#formaPago").val(data.formaPago)

    $("#empleado").val(data.empleado)
    $("#propietario").val(data.propietario)
    $("#proyectoSN").val(data.proyectoSN)
    $("#promotor").val(data.promotor)
    $("#ventasAdic").val(data.ventasAdic)
    $("#comentarios").val(data.comentarios)

    $("#totalAntesDescuento").val(data.totalAntesDescuento)
    $("#descAplicado").val(data.descuento)
    $("#redondeo").val(data.redondeo)
    $("#impuestoTotal").val(data.impuestoTotal)
    $("#totalDelDocumento").val(data.totalDelDocumento)

    $("#tblEntregaVista tbody").empty()
    for (x= 0; x<data.array.length; x++ ) {
        $("#tblEntregaVista tbody").append(data.array[x])
    } 
}

document.querySelector("#bucardorEntrega").addEventListener('keyup', () => {    						 
    const formData = new FormData()
    formData.append('valorEscrito', document.querySelector('#bucardorEntrega').value)
    fetch('php/consultaGRALEntrega.php', {method: 'POST', body: formData})
    .then(data => data.text())
    .then(data => {
        document.querySelector("#tblEntrega tbody").innerHTML = data      
    })
})

function click_select_entrega(fila) { 
    $("#modalEntrega").modal('hide');  											
    consultaClickFactura(fila.children[1].textContent, 'nada')          
}

//---------------------------------------------------------------------CONSULTAS PARA QUERYS 

    function query_back_orders() {
        var fechaIni = $("#fechaIni").val();
        var fechaFin = $("#fechaFin").val();
        var nombreCliente = $("#nombreCliente_query").val();
        var codigoCliente = $("#codigoCliente_query").val();
        window.location.href = 'btnBackOrderOk.php?fechaIni='+fechaIni+'&fechaFin='+fechaFin+'&nombreCliente='+nombreCliente+'&codigoCliente='+codigoCliente;			
    } //lista

    function query_comisiones() {
        var fechaS = $("#fechaSuperior").val();
        var fechaM = $("#fechaMenor").val();
        window.location.href = 'comisionesVentas.php?fechaS='+fechaS+'&fechaM='+fechaM;	
    } //lista

    function entrda_articulos() {
        var fechaContabilizacionSuperior = $("#fechaContabilizacionSuperior").val();
        var fechaContabilizacionMenor = $("#fechaContabilizacionMenor").val();			
        window.location.href = 'btnEntregaArticulos.php?fechaContabilizacionSuperior='+fechaContabilizacionSuperior+'&fechaContabilizacionMenor='+fechaContabilizacionMenor;		
    } //lista

    function query_oferta_de_venta_especial() {
        var clienteProveedor = $("#clienteProveedor").val();
        window.location.href = 'ofertaVentaCliente2doNivel.php?clienteProveedor='+clienteProveedor;
    } //Lista 

    function query_oferta_de_veenta_especial_fecha() {
        var codigoCliente = $("#codigoClienteF").val();
        var fecha1 = $("#fecha1").val();
        var fecha2 = $("#fecha2").val();
        window.location.href = 'btnOFVClienteFecha.php?codigoCliente='+codigoCliente+'&fecha1='+fecha1+'&fecha2='+fecha2;
    } //lista

    function query_oferta_de_venta_fecha() {
        var fecha1 = $("#campoFecha1").val();
        var fecha2 = $("#campoFecha2").val();								
        window.location.href = 'btnOFVFecha.php?fecha1='+fecha1+'&fecha2='+fecha2;
    } //lista

    function query_pedidos_del_dia() {
        var fechaconta = $("#fechaconta").val();
        var statusDocumento = $("#statusDocumento").val();
        window.location.href = 'pedidos_del_dia.php?fechaconta='+fechaconta+'&statusDocumento='+statusDocumento;
    } //lista

    function query_mapa_de_relaciones() {
        var fechaInicio = $("#fechaInicio").val();	
        var fechaFin = $("#fechaFinito").val();
        location.href ='mapaDeRelaciones.php?fi='+fechaInicio+'&ff='+fechaFin;
    } //lista

//------------------------------------------------------------------------END CONSULTAS PARA QUERYS 

document.querySelector("#bucardorEntregaFecha").addEventListener('change', () => {
    const formData = new FormData()
    formData.append('valorEscrito', document.querySelector('#bucardorEntregaFecha').value)
    fetch('php/consultaGRALEntregaFecha.php', {method: 'POST', body: formData})
    .then(data => data.text())
    .then(data => {
        document.querySelector("#tblEntrega tbody").innerHTML = data  
    })
});

document.querySelector("#consultaPrimerRegistro").addEventListener('click', () => { consultaClickFactura('primero', 'adelante') })
document.querySelector("#consultaUltimoRegistro").addEventListener('click', () => { consultaClickFactura('ultimo', 'atras') })
document.querySelector("#consulta1Atras").addEventListener('click', () => { consultaClickFactura(document.querySelector("#cdoc").value - 1, 'atras') })
document.querySelector("#consulta1Adelante").addEventListener('click', () => { consultaClickFactura(document.querySelector("#cdoc").value ++, 'adelante') })
document.querySelector("#btnPdf").addEventListener('click', () => { window.location.href =`visorPDF.php?folio=${document.querySelector("#cdoc").value}` })