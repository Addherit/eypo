<?php
  include "../db/Utils.php";
  $codigoSN = $_POST['code'];
  $tipo = $_POST['tipo'];  
  $count = 0 ;


    $sql = "SELECT lm.CodigoArticuloComponente, art.NombreArticulo, lm.Quantity FROM EYPO.dbo.IV_EY_PV_Articulos art
    LEFT JOIN EYPO.dbo.IV_EY_PV_ListasMateriales lm ON art.CodigoArticulo = lm.CodigoArticuloComponente
    WHERE CodigoArticuloPadre = '$codigoSN'";
    $consulta = sqlsrv_query($conn, $sql);
    $inventarioComponentes = [];    
    WHILE ($row = sqlsrv_fetch_array($consulta)){
        $count ++;
        $linea = "
            <tr>                    
                <td class='codigoArt' >".$row['CodigoArticuloComponente']."</td>
                <td class='nombreArt'>".($row['NombreArticulo'])."</td>
                <td class='piezas'>".number_format($row['Quantity'],0, '.', ',')."</td>
                <td class='unidad'>PZA</td>                
                <td class='armadoDesarmado' style='display:none'>".$tipo."</td>                
            </tr>
        ";
        array_push($inventarioComponentes, $linea);
    }

    $inventarioGeneral = ["array_Componentes" => $inventarioComponentes];

    echo json_encode($inventarioGeneral);
?>
