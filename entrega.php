<?php
	include "db/Utils.php";	

	$sql = "SELECT min(FolioSAP) as primer, max(FolioSAP) as ultimo FROM EYPO.dbo.IV_EY_PV_EntregasVentaCab"; 
			
	$consulta = sqlsrv_query($conn, $sql);
	$r = sqlsrv_fetch_array($consulta);
	$primerRegistro = $r['primer'];
	$ultimoRegistro = $r['ultimo'];
	$consecutivo = $ultimoRegistro + 1;
?>
	<!DOCTYPE html>
		<html>
		<?php include "header.php"?>
			<body>
				<?php include "nav.php"?>	
				<?php include "modalQuerys.php" ?>			
				<?php include "modales.php"?> 
				<div class="container formato-font-design" id="contenedorDePagina">
					<br>
					<div class="row">
						<div class="col-lg-5 col-xl-6">
							<h1 style="color: #2fa4e7">Entrega</h1>
						</div>
						<div id="btnEnca" class="col-lg-7 col-xl-6" style="font-size: 2rem">
							<?php include "botonesDeControl.php" ?>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<label class="col-3 col-md-4 col-lg-3 col-form-label" >Cliente:</label>
								<div class="col-6">
									<input type="text" id="cliente">
								</div>
							</div>
							<div class="row">
								<label class="col-3 col-md-4 col-lg-3 col-form-label">Nombre:</label><br>
								<div class="col-6">
									<input type="text" id="nCliente">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Persona de contacto:</label>
								<div class="col-6">
									<input type="text" id="personaContacto">								
								</div>
							</div>					
							<div class="row">
								<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Tipo moneda:</label>
								<div class="col-6">
									<input type="text" id="tipoMoneda">								
								</div>
							</div>													
						</div>
						<div class="col-md-6">
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">N° Folio:</label>
								<div class="col-6">							
									<input type="text" id="cdoc" readonly="true" value="<?php echo $consecutivo ?>">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Estado:</label>
								<div class="col-6">
									<input type="text" id="estado" value="ABIERTO" readonly="true">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha contabilización:</label>
								<div class="col-6">
									<input type="date" value="<?php echo $hoy ?>" id="fconta" readonly="true">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha documento:</label>
								<div class="col-6">
									<input type="date" value="<?php echo $hoy ?>" id="fdoc" readonly="true">
								</div>
							</div>							
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Uso principal:</label>
								<div class="col-6 ">
									<input type="text" id="usoPrincipal" readonly="true">								
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Método de pago:</label>
								<div class="col-6">
									<input type="text" id="metodoPago" readonly="true">								
								</div>
							</div>
                            <div class="row">
								<label class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Forma de pago:</label>
								<div class="col-6">
									<input type="text" id="formaPago" readonly="true">								
								</div>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-md-12">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Contenido</a>
								</li>						
							</ul>
							<div class="tab-content" id="nav-tabContent">
								<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
								<br>		 
								<div class="row">
									<div class="col-md-12"> 
										<table class="table table-sm table-hover table-bordered table-striped text-center" id="tblEntregaVista">
											<thead>
												<tr>													
													<th>#</th>
													<th>Código articulo</th>
													<th style="width: 100%">Nombre artíuclo</th>					        				
													<th>Cantidad</th>
													<th>Precio por unidad</th>
													<th>Descuento %</th>
													<th>Indicador Impuesto</th>
													<th>Sujeto a retención de impuesto</th>										
													<th>Total</th>
													<th>Unidad SAT</th>	
                                                    <th>Almacén</th>																																		
												</tr>
											</thead>
											<tbody>
												<tr style="height: 27px">											
													<td class="cont"></td>
													<td class="narticulo"></td>
													<td class="darticulo"></td>
													<td class="cantidad"></td>
													<td class="precio"></td>
													<td class="descuento"></td>
													<td class="impuesto"></td>
													<td class="codigoImpuesto"></td> 													
													<td class="total"></td>
													<td class="unidadSAT"></td>
													<td class="almacen"></td>																																				
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-6">
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Empleado de ventas:</label>
											<div class="col-sm-6">
												<input type="text" id="empleado" readonly="true" value="<?php echo $empleadoVentas ?>">
											</div>
										</div>
                                        <div class="row">
											<label for="" class="col-sm-3 col-form-label">Propietario:</label>
											<div class="col-sm-6">
												<input type="text" id="propietario" value="<?php echo $nombreCompleto ?>">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Proyecto SN:</label>
											<div class="col-sm-6">
												<input type="text" id="proyectoSN" readonly="true">
											</div>
										</div>										
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Promotor:</label>
											<div class="col-sm-6">
												<input type="text" id="promotor" readonly="true">
											</div>
										</div>
                                        <div class="row">
											<label for="" class="col-sm-3 col-form-label">Ventas Adicional:</label>
											<div class="col-sm-6">
												<input type="text" id="ventasAdic" readonly="true">
											</div>
										</div>								
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Comentarios:</label>
											<div class="col-8">
												<textarea name="" id="comentarios" class="form-control" rows="3" readonly="true"></textarea>
											</div>
										</div>
									</div>
									<div class="col-md-6">
									
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Subtotal:</label>
											<div class="col-6">
												<input type="text" id="totalAntesDescuento" value="0.00" class="text-right" readonly="true">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Descuento:</label>
											<div class="col-6">
												<input type="text" id="descNum" value="0" class="text-center" style="width: 16%;">
												<span whidth="6%">%</span>
												<input type="text" value="0.00" class="text-right" id="descAplicado" style="width: 68%" readonly="true">
											</div>
										</div>									
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Redondeo:</label>
											<div class="col-6">
												<input type="text" id="redondeo" value="0.00" class="text-right" readonly="true">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Impuesto:</label>
											<div class="col-6">
												<input type="text" value="0.00" class="text-right" id="impuestoTotal" readonly="true">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Total:</label>
											<div class="col-6">
												<input type="text" id="totalDelDocumento" value="0.00" class="text-right" readonly="true">
											</div>
										</div>										
									</div>								
								</div>		 				
							</div>
						</div>
						<div class="row" id= btnFoot style="margin-bottom: 30px">
							<div class="col-md-6">	<br>													
								<a href=""><button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>					
							</div>			
						</div>	
					</div>						
				</div> 
				<?php include "footer.php"; ?>
			</body>
			<script>
				$("#btnBuscarGRAL").on('click', function() {
					$("#modalEntrega").modal('show')
				});

				function consultaClickFactura(folio, condicion){
									
					var ultimoRegistro = <?php echo "$ultimoRegistro"?>;
					var primerRegistro = <?php echo "$primerRegistro"?>;									
					$.ajax({
						url:'entregas/rellenarInputsEntrega.php',
						type:'POST', 
						dataType: 'json', 
						data:{
							folio : folio, 
							ur : ultimoRegistro, 
							pr : primerRegistro, 
							con : condicion,													
						}, 
						success: function(response){												
							var resp = response['respuesta'];							
							switch(resp){
								case 1:									
									alert("No hay mas resultados.");								 
								break;
								case 2: 									
									mostrarValoresDeBusquedaEnInputs(response)	
								break;
							}
						}
					});
					
				}

				function mostrarValoresDeBusquedaEnInputs(data){

					$("#cliente").val(data['cliente']);
					$("#nCliente").val(data['nombreCliente']);
					$("#personaContacto").val(data['personaContacto']);							
					$("#tipoMoneda").val(data['tipoMoneda']);					

					$("#nDoc").val(data['ndoc']);
					$("#cdoc").val(data['cdoc']);				
					$("#estado").val(data['estado']);		
					$("#fconta").val(data['fconta']);				
					$("#fdoc").val(data['fdoc']);
					$("#fven").val(data['fven']);
					$("#usoPrincipal").val(data['usoPrincipal']);
					$("#metodoPago").val(data['metodoPago']);
					$("#formaPago").val(data['formaPago']);

					$("#empleado").val(data['empleado']);
					$("#propietario").val(data['propietario']);	
					$("#proyectoSN").val(data['proyectoSN']);					
					$("#promotor").val(data['promotor']);
					$("#ventasAdic").val(data['ventasAdic'])
					$("#comentarios").val(data['comentarios']);

					$("#totalAntesDescuento").val(data['totalAntesDescuento']);					
					$("#descAplicado").val(data['descuento']);				
					$("#redondeo").val(data['redondeo']);
					$("#impuestoTotal").val(data['impuestoTotal']);
					$("#totalDelDocumento").val(data['totalDelDocumento']);

					$("#tblEntregaVista tbody").empty();				
					for (x= 0; x<data.array.length; x++ ){
						$("#tblEntregaVista tbody").append(data.array[x]);
					} 
				}

				$("#bucardorEntrega").keyup(function(){
					var valorEscrito = $('#bucardorEntrega').val();							 

					$.ajax({
						url:'entregas/consultaGRALEntrega.php',
						type:'POST',
						data:{valorEscrito: valorEscrito},
						success: function(response){
							$("#tblEntrega tbody").empty()
							$("#tblEntrega tbody").append(response)

							$("#tblEntrega tbody tr").on('click',function(){
								var cdoc = $(this).find('td').eq(1).text();		
								var condicion = 'nada';											
								consultaClickFactura(cdoc, condicion);								
								$("#bucardorEntrega").modal('hide');
							});
						}					
					})
				});

				//---------------------------------------------------------------------CONSULTAS PARA QUERYS 

					function query_back_orders() {
						var fechaIni = $("#fechaIni").val();
						var fechaFin = $("#fechaFin").val();
						var nombreCliente = $("#nombreCliente_query").val();
						var codigoCliente = $("#codigoCliente_query").val();
						window.location.href = 'btnBackOrderOk.php?fechaIni='+fechaIni+'&fechaFin='+fechaFin+'&nombreCliente='+nombreCliente+'&codigoCliente='+codigoCliente;			
					} //lista

					function query_comisiones() {
						var fechaS = $("#fechaSuperior").val();
						var fechaM = $("#fechaMenor").val();
						window.location.href = 'comisionesVentas.php?fechaS='+fechaS+'&fechaM='+fechaM;	
					} //lista

					function entrda_articulos() {
						var fechaContabilizacionSuperior = $("#fechaContabilizacionSuperior").val();
						var fechaContabilizacionMenor = $("#fechaContabilizacionMenor").val();			
						window.location.href = 'btnEntregaArticulos.php?fechaContabilizacionSuperior='+fechaContabilizacionSuperior+'&fechaContabilizacionMenor='+fechaContabilizacionMenor;		
					} //lista

					function query_oferta_de_venta_especial() {
						var clienteProveedor = $("#clienteProveedor").val();
						window.location.href = 'ofertaVentaCliente2doNivel.php?clienteProveedor='+clienteProveedor;
					} //Lista 

					function query_oferta_de_veenta_especial_fecha() {
						var codigoCliente = $("#codigoClienteF").val();
						var fecha1 = $("#fecha1").val();
						var fecha2 = $("#fecha2").val();
						window.location.href = 'btnOFVClienteFecha.php?codigoCliente='+codigoCliente+'&fecha1='+fecha1+'&fecha2='+fecha2;
					} //lista

					function query_oferta_de_venta_fecha() {
						var fecha1 = $("#campoFecha1").val();
						var fecha2 = $("#campoFecha2").val();								
						window.location.href = 'btnOFVFecha.php?fecha1='+fecha1+'&fecha2='+fecha2;
					} //lista

					function query_pedidos_del_dia() {
						var fechaconta = $("#fechaconta").val();
						var statusDocumento = $("#statusDocumento").val();
						window.location.href = 'pedidos_del_dia.php?fechaconta='+fechaconta+'&statusDocumento='+statusDocumento;
					} //lista

					function query_mapa_de_relaciones() {
						var fechaInicio = $("#fechaInicio").val();	
						var fechaFin = $("#fechaFinito").val();
						location.href ='mapaDeRelaciones.php?fi='+fechaInicio+'&ff='+fechaFin;
					} //lista

//------------------------------------------------------------------------END CONSULTAS PARA QUERYS 

				$("#bucardorEntregaFecha").change(function(){
					var valorEscrito = $('#bucardorEntregaFecha').val();							 

					$.ajax({
						url:'entregas/consultaGRALEntregaFecha.php',
						type:'POST',
						data:{valorEscrito: valorEscrito},
						success: function(response){
							$("#tblEntrega tbody").empty()
							$("#tblEntrega tbody").append(response)

							$("#tblEntrega tbody tr").on('click',function(){
								var cdoc = $(this).find('td').eq(1).text();		
								var condicion = 'nada';											
								consultaClickFactura(cdoc, condicion);								
								$("#bucardorEntrega").modal('hide');
							});
						}					
					})
				});

				$("#consultaPrimerRegistro").on('click', function(){								
					var folioSAP = <?php echo "$primerRegistro"?>;	
					var condicion = 'nada';
					consultaClickFactura(folioSAP, condicion);

				});

				$("#consultaUltimoRegistro").on('click', function(){
					var folioSAP = <?php echo "$ultimoRegistro"?>;
					var condicion = 'nada';
					consultaClickFactura(folioSAP, condicion);
				});

				$("#consulta1Atras").on('click', function(){
					var folioSAP = ($("#cdoc").val()) - 1;
					var condicion = 'atras';	
					consultaClickFactura(folioSAP, condicion);

				});
				$("#consulta1Adelante").on('click', function(){
					var folioSAP = $("#cdoc").val();	
					folioSAP ++;				
					var condicion = 'adelante';
					consultaClickFactura(folioSAP, condicion);
					
				});				

				$("#btnPdf").on('click', function(){
					var folio = $("#cdoc").val();															
					location.href ="visorPDF.php?folio="+folio;
				});
					


			</script>
		</html>
