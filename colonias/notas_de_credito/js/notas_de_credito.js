document.addEventListener('DOMContentLoaded', () => {
    geet_min_max_folio()
    document.querySelector("#mensajes").hidden = true
    document.querySelector("#consultaQuery").hidden = true
    document.querySelector("#empleado").value = localStorage.getItem("departamento")
    document.querySelector("#propietario").value = localStorage.getItem("nombre_completo")
  
})

document.querySelector("#btnBuscarGRAL").addEventListener('click', () => { $("#modalNotaDeCredito").modal('show') })
document.querySelector("#btnPdf").addEventListener('click', () => { crear_pdf_o_xml('.pdf') })
document.querySelector("#btnxlm").addEventListener('click', () => { crear_pdf_o_xml('.xml') })
document.querySelector("#consultaPrimerRegistro").addEventListener('click', () => { consultaClickFactura(document.querySelector("#primer-registro").value, 'nada') })
document.querySelector("#consultaUltimoRegistro").addEventListener('click', () => { consultaClickFactura(document.querySelector("#ultimo-registro").value, 'nada') })
document.querySelector("#consulta1Atras").addEventListener('click', () => { consultaClickFactura(parseInt(document.querySelector("#cdoc").value) - 1, 'atras') })
document.querySelector("#consulta1Adelante").addEventListener('click', () => { consultaClickFactura(parseInt(document.querySelector("#cdoc").value) + 1, 'adelante') })

function consultaClickFactura(folio, condicion) {

    const formData = new FormData()
    formData.append('folio', folio)
    formData.append('con', condicion)

    fetch('php/rellenarInputs.php', {method: 'POST', body: formData})
    .then(data => data.json())
    .then(data => { 
        mostrarValoresDeBusquedaEnInputs(data)
    })
}

function mostrarValoresDeBusquedaEnInputs(data){

    $("#cliente").val(data.cliente)
    $("#nombreDelCliente").val(data.nombreCliente)
    $("#personaContacto").val(data.personaContacto)
    $("#referencia").val(data.referencia)
    $("#tipoMoneda").val(data.tipoMoneda)
        
    $("#cdoc").val(data.cdoc)
    $("#estado").val(data.estado)
    $("#fconta").val(data.fconta)
    $("#fdoc").val(data.fdoc)
    $("#fven").val(data.fven)
    $("#metodoPago").val(data.metodoPago)
    $("#formaPago").val(data.formaPago)
    $("#usoPrincipal").val(data.usoPrincipal)
                

    $("#empleado").val(data.empleado)
    $("#propietario").val(data.propietario)
    $("#comentarios").val(data.comentarios)

    $("#totalAntesDescuento").val(data.totalAntesDescuento)
    $("#descNum").val(data.porcentajeDescuento)
    $("#descuento").val(data.descuento)
    $("#anticipoTotal").val(data.anticipoTotal)
    $("#redondeo").val(data.redondeo)
    $("#impuestoTotal").val(data.impuestoTotal)
    $("#retencion").val(data.retencion)
    $("#totalDocumento").val(data.totalDelDocumento)
    $('#monedaVisor').val($("#tipoMoneda").val()).prop("readonly", true);

    $("#importeAplicado").val(data.importeAplicado)
    $("#saldoPendiente").val(data.saldoPendiente)
    $("#rutaArchivos").val(data.rutaArchivos)
                                    

    $("#tblNotasDeCredito tbody").empty();				
    for (x= 0; x<data.array.length; x++ ) {
        $("#tblNotasDeCredito tbody").append(data.array[x]);
    } 
}

document.querySelector("#buscadorNotaDeCredito").addEventListener('keyup', () => {
    const formData = new FormData()
    formData.append("valorEscrito", document.querySelector('#buscadorNotaDeCredito').value)
    
    fetch('php/consultaNotasDeCredito.php', {method: 'POST', body: formData})
    .then(data => data.text())
    .then(data => {
        document.querySelector("#tblNotaDeCredito tbody").innerHTML = data
    }) 							    
})

function inspeccionar_nota_desde_modal(fila) {    									
    consultaClickFactura(fila.children[1].textContent, 'nada');								
    $("#modalNotaDeCredito").modal('hide');
}

document.querySelector("#buscadorNotaDeCreditoFecha").addEventListener('change', () => {
    const formData = new FormData()
    formData.append("valorEscrito", document.querySelector('#buscadorNotaDeCreditoFecha').value)
    fetch('php/consultaNotasDeCreditoFecha.php', {method: 'POST', body: formData})
    .then(data => data.text())
    .then(data => {
        document.querySelector("#tblNotaDeCredito tbody").innerHTML = data
    })
});

function geet_min_max_folio() {

    fetch('php/consultar_min_max_notas_de_credito.php')
    .then(data => data.json())
    .then(data => {
        document.querySelector("#primer-registro").value = data.primero
        document.querySelector("#ultimo-registro").value = data.ultimo
        document.querySelector("#cdoc").value = parseInt(data.ultimo) + 1
    })
}

function crear_pdf_o_xml(extension) {    
    const formData = new FormData()
    formData.append('origen', `${document.querySelector("#rutaArchivos").value}${extesion}`)
    formData.append('origen', origen.replace(/\\/g, "\\\\"))
    formData.append('ext', extension)
    formData.append('nombre', Math.floor((Math.random() * 99999) + 1))

    fetch('php/copiarArchivo.php', {method: 'POST', body: formData})
    .then(data => data.json())
    .then(data => {
        data.resp == '1' ? window.location = `temporales/${data.new_name}${data.extension}` : alert(data.mensaje);
    })    
}