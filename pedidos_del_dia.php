<!DOCTYPE html>
<html>
	<?php include "header.php" ?>
	<body>
	<?php include "nav.php" ?>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<br>
					<input type="hidden" value="<?php echo $_GET["fechaconta"] ?>" id="fechaconta">
					<input type="hidden" value="<?php echo $_GET["statusDocumento"] ?>" id="statusDocumento">			
					<section class="table-responsive"> 	
						<table class="table table-bordered table-striped table-hover table-sm" id="tbl_pedidos_dl_dia">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Nom Doc</th>
									<th scope="col">Hora</th></th>
									<th scope="col">Op/Cl</th>
									<th scope="col">Codigo</th>
                                    <th scope="col">CardName</th>
                                    <th scope="col">Mon</th>
                                    <th scope="col">T.Fac</th>
                                    <th scope="col">DocTotal</th>
                                    <th scope="col">AsVts</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Fecha Entrega</th>
                                    <th scope="col">Ref</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</section>
				</div>
				<div class="col-5 offset-7">
					<a href="ofertaDeVenta.php">
						<button class="btn btn-primary btn-block">Regresar a OFV</button>
					</a>
				</div>
			</div>
		</div>
		<?php include "footer.php" ?>	
		<script src="js/pedidosDia.js"></script>	
	</body>                     
	
</html>