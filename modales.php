<!-- Modal para cliente -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Lista de Socios de Negocios</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscadorCliente" placeholder="Buscar" title="Buscador cliente" onkeyup="typea_y_busca_cliente()">
          </div>
        </div>
        <hr>
        <div class="table-responsive" style="max-height: 500px">
          <table class="table table-hover table-striped table-sm" id="tblcliente"style="font-size: .8rem;">
            <thead>
              <tr>  
                <th>N°</th>
                <th>Codigo</th>
                <th>Nombre del cliente</th>
                <th>RFC</th>
              </tr>
            </thead>
          <tbody class="white-space: normal;">
          </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para artículos -->
<div class="modal fade" id="myModalArticulos">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title">Lista de Artículos</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <input type="text" id="buscadorArticulo" placeholder="Buscar" title="Buscador Articulo" onkeypress="ejecutar_buscador_de_articulo(event)"><br>
              <span style="font-size: .8rem">Para buscar presiona <b>ENTER</b> en tu computadora</span>
            </div>
          </div>
          <div id="divalerta" class="alert alert-warning" style="display: none;">
            <strong>Alerta!</strong> No has seleccionado ningún Cliente.
          </div>
          <div class="table-responsive" style="max-height: 500px">
          <table class="table table-striped table-hover table-sm" id="tblarticulo" style="font-size: .8rem;" >
                  <thead>
                    <tr>
                        <th>N°</th>
                        <th>Código</th>
                        <th>Articulo</th>
                        <th>Stock</th>
                        <th>Comp</th>
                        <th>Pedido</th>
                        <th>Cod alm</th>
                        <th>Almacen</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para ofertas -->
<div class="modal fade" id="modalBuscarOfertas">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Buscar Oferta de venta</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscadorOfertas" placeholder="Buscar" title="Buscador General" onkeyup="consultar_ofv_por_typeo(this)">
            <input type="date" id="buscadorOfertasFecha" onchange="consultar_ofv_por_fecha(this)"><br>
            <span style="font-size: .8rem; color grey">Busca por <b>Folio</b>, <b>Nombre</b> y para <b>Fecha</b>, utiliza el campo de fecha</span>
          </div>
        </div>
        <hr>
        <div class="table-responsive" style="max-height: 500px">
          <table class="table table-striped table-hover table-sm" id="tblBuscarOfertas" style="font-size: .8rem">
            <thead>
              <tr>
                <th>Oferta</th>
                <th>Código</th>
                <th>Nombre del cliente</th>
                <th>Fecha creación</th>
                <th>Estatus</th>
              </tr>
            </thead>
          <tbody>
          </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para ordenes -->
<div class="modal fade" id="modalBuscarOrdenes">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Buscar Orden de venta</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscadorOrdenes" placeholder="Buscar" title="Buscador General" onkeyup="typear_buscar_orv(this)">
            <input type="date" id="buscadorOrdenesFecha" onchange="consultar_orv_por_fecha(this)"><br>
            <span style="font-size: .8rem; color grey">Busca por <b>Folio</b>, <b>Nombre</b> y para <b>Fecha</b>, utiliza el campo de fecha</span>
          </div>
        </div>
        <hr>
        <div class="table-responsive" style="max-height: 500px">
          <table class="table table-striped table-hover dtHorizontalVerticalExample table-sm" id="tblBuscarOrdenes" style="font-size: .8rem">
            <thead>
              <tr>
                <th>N°</th>
                <th>Oferta</th>
                <th>Nombre del cliente</th>
                <th>Fecha creación</th>
                <th>Estatus</th>
              </tr>
            </thead>
          <tbody>
          </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para ordenes de solicitud de compra-->
<div class="modal fade" id="modalBuscarOrdenesParaSolicitud">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Buscar Orden de venta</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscadorOrdenesParaSoli" placeholder="Buscar" title="Buscador General">
          </div>
        </div>
        <hr>
        <div class="table-responsive" style="max-height: 500px">
        <table class="table table-striped table-hover table-sm" id="tblBuscarOrdenesSolicitudes" style="font-size: .8rem">
          <thead>
            <tr>
              <th >N°</th>
              <th>Folio Oferta</th>
              <th>Codigo Cliente</th>
              <th>Nombre Cliente</th>
              <th>Fecha creación</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para solicitudes de compra -->
<div class="modal fade" id="modalBuscarSolicitudDeCompra">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Buscar Solicitud de Compra</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscarSolicitudDeCompra" placeholder="Buscar" title="Buscador General">
            <input type="date" id="buscarSolicitudDeCompraFecha"><br>
            <span style="font-size: .8rem; color grey">Busca por <b>Folio</b>, <b>Nombre</b> y para <b>Fecha</b>, utiliza el campo de fecha</span>
          </div>
        </div>
        <hr>
        <div class="table-responsive" style="max-height: 500px">
          <table class="table table-striped table-hover table-sm" id="tblBuscarSolicitudDeCompra" style="font-size: .8rem">
            <thead>
              <tr>
                <th>N°</th>
                <th>Folio</th>
                <th>Solicitante</th>
                <th>Nombre Solicitante</th>
                <th>Fecha creación</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para buscarSocios -->
<div class="modal fade" id="modalBuscarSocios" tabindex="-1" role="dialog" aria-labelledby="modalBuscarSocios" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Buscar socios de Negocios</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscadorSocios" placeholder="Buscar">
          </div>
        </div>
        <hr>
        <div id="divalerta" class="alert alert-warning" style="display: none;">
          <strong>Alerta!</strong> No has seleccionado ningún Cliente.
        </div>

          <table class="table-sm table table-hover table-bordered tblblock" id="tblSocios" style="font-size: .8rem">
            <thead>
              <tr style="text-align: center">
                <th>#</th>
                <th>CódigoSN</th>
                <th>Nombre</th>
                <th>activo</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    </div>
  </div>
</div>
<!-- Modal para Direcciones-->
<div class="modal fade" id="myModalDireccion">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Lista de Direcciones</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscadorDireccion" placeholder="Buscar">
          </div>
        </div>
        <hr>
        <div id="divalerta" class="alert alert-warning" style="display: none;">
          <strong>Alerta!</strong> No has seleccionado ningún Cliente.
        </div>

        <table class="table-sm table table-hover table-bordered tblblock" id="tblDireccion" style="font-size: .9rem">
          <thead>
            <tr style="text-align: center">
              <th>#</th>
              <th>Codigo</th>
              <th>Id Direccion</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal para personas de contacto-->
<div class="modal fade" id="modalPersonasContacto">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title">Lista de Personas de contacto</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <input type="text" id="buscadorPersonaContacto" placeholder="Buscar">
            </div>
          </div>
          <hr>
          <div class="table-responsive" style="max-height: 500px">
          <table class="table table-hover table-bordered table-sm" id="tblPersonaContacto" style="font-size: .8rem">
            <thead>
              <tr style="text-align: center">
              <th>N°</th>
                <th>Código SN</th>
                <th>Código contacto</th>
                <th>Nombre</th>
                <th>Posición</th>
                <th>Correo Electrónico</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para query -->
<div class="modal fade" id="modalQuery">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Lista de Query</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <ul id="listaDeQuerys" >
          <a href="#"> <li id="BackOrder" data-dismiss="modal" data-target="#BackOrderVentas" data-toggle="modal"><i class="fas fa-play-circle" style="color: orange"></i> Back Order Ventas nuevo</li></a>          
          <a href="#"> <li id="Comisiones" data-dismiss="modal" data-target="#ComisionesVentas" data-toggle="modal"><i class="fas fa-play-circle" style="color: orange"></i> Comisiones</li></a>
          <a href="#"> <li id="EntradasArt" data-dismiss="modal" data-target="#EntradasVentas" data-toggle="modal"><i class="fas fa-play-circle" style="color: orange"></i> Entradas Articulos</li></a>
          <a href="../querys/kitsVentas.php"><li><i class="fas fa-play-circle" style="color: orange"></i> Kits vigentes en sistema (reporte original en Admon)</li></a>
          <a href="#"> <li id="OFVCliente" data-dismiss="modal" data-target="#OfertaVentaCliente" data-toggle="modal"><i class="fas fa-play-circle" style="color: orange"></i> Oferta de venta linea especial cliente</li></a>
          <a href="#"> <li id="OFVClienteyFecha" data-dismiss="modal" data-target="#OfertaClienteF" data-toggle="modal"><i class="fas fa-play-circle" style="color: orange"></i> Oferta de venta linea especial cliente y fecha</li></a>
          <a href="#"> <li id="OFVFecha" data-dismiss="modal" data-target="#ofvLineaFecha" data-toggle="modal"><i class="fas fa-play-circle" style="color: orange"></i> Oferta de venta linea especial fecha</li></a>
          <a href="#"> <li id="pedidosDelDia" data-dismiss="modal" data-target="#Pedidos" data-toggle="modal"><i class="fas fa-play-circle" style="color: orange"></i> Pedidos del día</li></a>
          <a href="#"> <li id="mapaDeRelaciones" data-dismiss="modal" data-target="#mapaDeRelaciones" data-toggle="modal"><i class="fas fa-play-circle" style="color: orange"></i> Mapa de relaciones</li></a>
        </ul>
      </div>
    </div>
  </div>
</div>

<!-- Modal para mensajes-->
<div class="modal fade" id="modalMensaje">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Lista de Mensajes</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <ul class="nav nav-tabs">
              <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#bandejaDeEntrada" role="tab" aria-controls="home" aria-selected="true">Bandeja de entrada</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="home-tab" data-toggle="tab" href="#bandejaDeSalida" role="tab" aria-controls="home" aria-selected="true">Bandeja de salida</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="home-tab" data-toggle="tab" href="#bandejaDeSalida" role="tab" aria-controls="home" aria-selected="true">Mensajes enviados</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="bandejaDeEntrada" role="tabpanel" aria-labelledby="home-tab">
                <div style="height: 400px; overflow-y: hidden;">
                  <table class="table table-sm table-hover table-striped tblblock" id="tblMensajes">
                    <thead class="text-center">
                      <th width="100px">N° Oferta</th>
                      <th width="400px">Estatus</th>
                      <th width="130px">Fecha</th>
                      <th width="100px">Hora</th>
                      <th style="display: none"></th>
                      <th style="display: none"></th>
                      <th><i class="far fa-file"></i> </th>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
                <br>
                <table class="table table-sm table-hover table-striped" id="tblMensajes2">
                  <thead>
                    <th width="70px">#</th>
                    <th width="140px">N° de Orden</th>
                    <th width="210px">N° de Oferta</th>
                    <th width="350px">Estatus del documento</th>
                    <th style="display: none"></th>
                  </thead>
                  <tbody>
                    <tr>
                      <td width="70px"></td>
                      <td width="140px"></td>
                      <td width="210px"></td>
                      <td width="350px"></td>
                      <td style="display: none"></td>
                    </tr>
                    <tr>
                      <td width="70px"></td>
                      <td width="140px"></td>
                      <td width="210px"></td>
                      <td width="350px"></td>
                      <td style="display: none"></td>
                    </tr>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para arcos fabricar -->
<div class="modal fade" id="modalTablaFabricar">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title">Lista de artículos para fabricar</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscadorArcosFabricar" placeholder="Buscar">
            </div>
        </div>
        <br>
        <div class="table-responsive-sm">
          <table class="table table-hover table-sm" id="tblarticulo">
              <thead>
                  <tr>
                    <th style="text-align: center;">CódigoSN</th>
                    <th style="text-align: center;">NombreSN</th>
                  </tr>
              </thead>
              <tbody>
                <?php
                $consultasql = sqlsrv_query($conn, "SELECT Code, ItemName FROM ITT1 INNER JOIN OITM ON ITT1.Code = OITM.ItemCode");
                while ($Row = sqlsrv_fetch_array($consultasql)) { ?>
                <tr>
                  <td><?php echo $Row['Code']; ?></td>
              <td><?php echo $Row['ItemName']; ?></td>
                </tr>
              <?php
            } ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para arcos desarme -->
<div class="modal fade" id="modalTablaDesarmar">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Lista de Artículos</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscadorArcosDesarme" placeholder="Buscar">
          </div>
        </div>
        <br>
        <div class="table-responsive-sm">
          <table class="table table-hover table-sm" id="tblArcosDesarme">
            <thead>
                <tr>
                  <th style="text-align: center;">CódigoSN</th>
                  <th style="text-align: center;">NombreSN</th>
                </tr>
            </thead>
            <tbody>
              <?php
              $consultasql = sqlsrv_query($conn, "SELECT Code, ItemName FROM ITT1 INNER JOIN OITM ON ITT1.Code = OITM.ItemCode");
              while ($Row = sqlsrv_fetch_array($consultasql)) { ?>
                  <tr>
                    <td><?php echo $Row['Code']; ?></td>
                    <td><?php echo $Row['ItemName']; ?></td>
                  </tr>
                <?php
              } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para arcos consultagenera -->
<div class="modal fade" id="modalArcosConsultaGeneral">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title">Lista de Ordenes</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscadorArcos" onkeyup="typear_buscar_orf_arcos()" placeholder="Buscar">
            <input type="date" id="buscadorArcosFecha" onchange="buscar_orf_arcos_por_fecha()"><br>
            <span style="font-size: .8rem; color grey">Busca por <b>Folio</b>, <b>Nombre</b>, <b>estatus</b>, <b>empresa</b> y para <b>Fecha</b>, utiliza el campo de fecha</span>
            </div>
        </div>
        <br>
        <div class="table-responsive">
          <table  class="table table-striped table-hover text-center" id="tblarcosGeneral" style="font-size: .8rem">
              <thead>
                  <tr>
                    <th >N°</th>
                    <th >Folio</th>
                    <th >Nombre del Solicitante</th>
                    <th >Empresa</th>
                    <th >Prod fabricar</th>
                    <th >Prod Desarmar</th>
                    <th >Estatus</th>
                    <th >Fecha de solicitud</th>
                  </tr>
              </thead>
              <tbody >
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal condicones de pago  -->
<div class="modal fade" id="modalCondicionPago" tabindex="-1" role="dialog" aria-labelledby="modalCondicionPago" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Condiciones de pago</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-sm table-striped table-hover" id="tblCondicionPago" style=" font-size: .8rem">
          <thead>
            <tr>
              <th width="100%">Descripción</th>
            </tr>
          </thead>
          <tbody>
            <tr onclick="seleccionar_condicion_de_pago(this)">
              <td>Contado</td>
            </tr>
            <tr onclick="seleccionar_condicion_de_pago(this)">
              <td>30 días </td>
            </tr>
            <tr onclick="seleccionar_condicion_de_pago(this)">
              <td>Pago inmediato</td>
            </tr>
            <tr onclick="seleccionar_condicion_de_pago(this)">
              <td>45 días neto</td>
            </tr>
            <tr onclick="seleccionar_condicion_de_pago(this)">
              <td>Parcialidades</td>
            </tr>
            <tr onclick="seleccionar_condicion_de_pago(this)">
              <td>Anticipo 50% resto vs aviso de embarque</td>
            </tr>
            <tr onclick="seleccionar_condicion_de_pago(this)">
              <td>Anticipo 50% resto vs entrega</td>
            </tr>
            <tr onclick="seleccionar_condicion_de_pago(this)">
              <td>Anticipo 50% resto a credito</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal TIPO DE FACTURA -->
<div id="modalTipoFactura" class="modal fade" role="dialog">
  <div class="modal-dialog">    
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tipo de factura</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <table class="table table-sm table-striped table-hover" id="tblTipoFactura" style=" font-size: .8rem">
          <thead>
            <tr>
              <th>Código</th>
              <th>Descripción</th>
            </tr>
          </thead>
          <tbody>
            <?php
            include "conexion.php";
            $sql = "SELECT * FROM EYPO.dbo.IV_EY_PV_CamposUsuario WHERE CampoSAP LIKE 'Tipo_Factura'";
            $consulta = sqlsrv_query($conn, $sql);
            while ($Row = sqlsrv_fetch_array($consulta)) {
              echo $linea = '<tr onclick="seleccionar_tipo_factura(this)" class="cursor"><td>' . $Row['Valor'] . '</td>
              <td>' . $Row['Descripcion'] . '</td></tr>';
            } ?>
          </tbody>
          </table>
      </div>
    </div>

  </div>
</div>

<!-- Modal para factura comun -->
<div class="modal fade" id="modalFacturaComun">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Facturas</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="bucardorFacturaComun" placeholder="Buscar" title="Buscador cliente">
            <input type="date" id="bucardorFacturaComunFecha"><br>
            <span style="font-size: .8rem; color grey">Busca por <b>Folio</b>, <b>Nombre</b> y para <b>Fecha</b>, utiliza el campo de fecha</span>
          </div>
        </div>
          <hr>
          <div class="table-responsive" style="max-height: 500px">
          <table class="table table-hover table-striped table-sm"  id="tblFacturaComun" style="font-size: .8rem">
            <thead>
              <tr>
                <th>N°</th>
                <th>Folio</th>
                <th>Nombre cliente</th>
                <th>Fecha</th>
                <th>Estatus</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para Entrega -->
<div class="modal fade" id="modalEntrega">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Entregas</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="bucardorEntrega" placeholder="Buscar" title="Buscador cliente">
            <input type="date" id="bucardorEntregaFecha"><br>
            <span style="font-size: .8rem; color grey">Busca por <b>Folio</b>, <b>Nombre</b> y para <b>Fecha</b>, utiliza el campo de fecha</span>
          </div>
        </div>
          <hr>
          <div class="table-responsive" style="max-height: 500px">
          <table class="table table-striped tblblock table-hover table-sm" id="tblEntrega" style="font-size: .8rem">
            <thead>
              <tr>
                <th>N°</th>
                <th>Folio</th>
                <th>Nombre Cliente</th>
                <th>Fecha</th>
                <th>Estatus</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para nota de creditos -->
<div class="modal fade" id="modalNotaDeCredito">
  <div class="modal-dialog  modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Notas de Crédito</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscadorNotaDeCredito" placeholder="Buscar" title="Buscador cliente">
            <input type="date" id="buscadorNotaDeCreditoFecha" placeholder="Buscar" title="Buscador cliente">
          </div>
        </div>
        <hr>
        <div calss="table-responsive" style="max-height: 500px; overflow:auto">
          <table class="table table-striped table-hover table-sm"  id="tblNotaDeCredito" style="font-size: .8rem">
            <thead>
              <tr>
                <th>N°</th>
                <th>Folio</th>
                <th>Cliente</th>
                <th>Fecha</th>
                <th>Usuario</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para cajero -->
<div class="modal fade" id="myModalCajero">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Lista de Cajeros</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscadorCajero" placeholder="Buscar" title="Buscador cajero">
          </div>
        </div>
        <hr>
          <table class="table table-responsive table-hover table-striped tblblock"  id="tblcajero"style="font-size: .7rem">
            <thead>
              <tr>
                <th width="50px" class="fth-header">N°</th>
                <th width="60px" class="fth-header">Uid</th>
                <th width="120px" class="fth-header">Name</th>
			        	<th width="350px" class="fth-header">Address</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal para ultimo precio -->
<div class="modal fade" id="modal_ultimo_precio">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Lista ultimos precios</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="table-responsive" style="max-height: 500px">
          <table class="table table-hover table-striped table-sm" id="tbl_ultimo_precio"style="font-size: .8rem;">
            <thead>
              <tr>
                <th>N°</th>
                <th>Documento</th>
                <th>Fecha Contabilización</th>
                <th>Código Cliente</th>
                <th>Código Articulo</th>
                <th>Cantidad</th>
                <th>Precio antes descuento</th>
                <th>Porcentaje Descuento</th>
                <th>Precio Final</th>
                <th>Unidad medida</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal para anteproyectos -->
<div class="modal fade" id="modalBuscarProyectos">
<div class="modal-dialog  modal-lg">
  <div class="modal-content">
    <div class="modal-header"
      <h5 class="modal-title">Buscar Proyecto</h5>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
      <div class="row">
        <div class="col-md-12">
          <input type="text" id="buscadorProyectos" placeholder="Buscar" title="Buscador General">
    <label>Fecha Inicio:</label>
            <input type="date" onchange="cargarProyectos();" id="FechaInicio" value="<?php echo $FechaInicio; ?>">
      <label>Fecha Fin:</label>
          <input type="date" onchange="cargarProyectos();" id="FechaFin" value="<?php echo $today; ?>">
        </div>
      </div>
      <hr>
      <table class="table table-striped tblblock" id="tblBuscarProyectos" style="font-size: .8rem">
        <thead>
          <tr>
            <th width="70px">N° Proyecto</th>
      <th width="130px">Prioridad</th>
            <th width="130px">Codigo Cliente</th>
            <th width="310px">Nombre Cliente</th>
      <th width="310px">Tipo de Propuesta</th>
            <th width="150px">Fecha creación</th>

            <th width="100px">Estatus</th>
          </tr>
        </thead>
      <tbody>
      </tbody>
      </table>
    </div>
  </div>
</div>
</div>
<!-- Modal para CodigosProtegidos -->
<div class="modal fade" id="myModalCodigos">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Lista de Codigos Protegidos</h5>
        <button type="button" id="closeArti" class="closeArt" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <input type="text" id="buscadorCodigo" placeholder="Buscar" title="Buscador Codigo">
      <label class="col-sm-8 col-form-label">Presiona Enter al finalizar para hacer la búsqueda</label>
          </div>
        </div>

          <table class="table table-hover table-striped tblblock" id="tblcodigo" style="font-size: .7rem;" >
              <thead>
                <tr>
                    <th width="50px">#</th>
          <th width="20px">Id</th>
                    <th width="100px">Marca</th>
                    <th width="100px">Modelo</th>
                    <th width="100px">CodigoReal</th>
                    <th width="100px">CodigoProtegido</th>
                    <th width="150px">Descripcion</th>
                    <th width="50px">Tipo</th>
          <th width="50px">Potencia</th>
          <th width="50px">Optica</th>
                  </tr>
              </thead>
              <tbody>

              </tbody>
          </table>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalReferencias">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" align="center">PRECIOS DE REFERENCIA</h5>
        <button type="button" id="closeArti" class="closeArt" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row" style="font-size: .7rem">
          <div class="col-md-12">
      <label class="col-sm-1 col-form-label">Codigo Real:</label>
            <input type="text" class="" name="codigorealr" id="codigorealr">
      <label class="col-sm-1 col-form-label">Tipo:</label>
      <input type="text" class="" name="tipor" id="tipor">
      <label class="col-sm-1 col-form-label">Potencia:</label>
      <input type="text" class="" name="potenciar" id="potenciar">
      
 			  <input type="text" class="" name="marcar" id="marcar" style="display:none">
 			  <input type="text" class="" name="cantidadr" id="cantidadr"  style="display:none">
 			  <button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnSolicitarr" title="Solicitar Precio">Solicitar Precio</button>
          </div>
        </div>

          <table class="table table-hover table-striped tblblock" id="tblreferencias" style="font-size: .7rem;" >
              <thead>
                <tr>
                    <th width="10px">#</th>
          <th width="100px">Fecha</th>
                    <th width="100px">Proyecto</th>
                    <th width="100px">Costo</th>
                    <th width="100px">Moneda Costo</th>
                    <th width="100px">Utilidad</th>
                    <th width="100px">Coeficiente</th>
                    <th width="100px">TE</th>
          <th width="100px">Precio de Venta</th>
          <th width="100px">Moneda Venta</th>

                  </tr>
              </thead>
              <tbody>

              </tbody>
          </table>
        </div>
    </div>
  </div>
</div>
<div class="modal fade" id="myModalPerdido">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" align="center">COMENTARIOS PROYECTO PERDIDO</h5>
        <button type="button" id="closeArti" class="closeArt" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row" style="font-size: .7rem">
          <div class="col-md-12">
      <label class="col-sm-1 col-form-label">Observaciones:</label>
            <div class="col-sm-12">
      <textarea name="observacionesp" id = "observacionesp" cols="40" rows="5" style="width: 70%"></textarea>
      </div>
      <button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnPerder" title="Cerrar como perdido">Confirmar Proyecto Perdido</button>
          </div>
        </div>


        </div>
    </div>
  </div>
</div>
