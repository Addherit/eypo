<?php
include "conexioncajero.php";
session_start();

$month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;

$new_date = date('Y-m-d', strtotime($_POST['FechaR']));
$usuario  = $_SESSION["usuarioN"];
require('vendor/php-excel-reader/excel_reader2.php');
require('vendor/SpreadsheetReader.php');
   if(isset($_FILES['file'])){
	   unset($_SESSION["usuarioN"]);
		unset($_SESSION["logged_in"]);
      $errors= array();
      $file_name = $_FILES['file']['name'];
      $file_size = $_FILES['file']['size'];
      $file_tmp = $_FILES['file']['tmp_name'];
      $file_type = $_FILES['file']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['file']['name'])));

      $expensions= array("xlsx","xls","csv");//aqui se elige que extenciones puede aceptar el server para subir

      if(in_array($file_ext,$expensions)=== false){
         $errors[]="extension not allowed.";
      }


      if(empty($errors)==true) {
		  $new_name =rand() . '.' . $file_ext;
		   $destination = 'Upload/'.$new_name;
        if(move_uploaded_file($file_tmp,$destination))

		echo("<div class='alert alert-success' role='alert'>");
		echo("<strong>Success</strong> You <a href='#' class='alert-link'>successfully upload");
		echo(" this File</a>");
		echo("</div>");
      }else{
         print_r($errors);
      }



	$allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

  if(in_array($_FILES["file"]["type"],$allowedFileType)){


        $Reader = new SpreadsheetReader($destination);
		$sheetCount = count($Reader->sheets());



        for($i=0;$i<$sheetCount;$i++)
        {
            $Reader->ChangeSheet($i);

            foreach ($Reader as $Row)
            {

                $NOM = "";
                if(isset($Row[0])) {

                    $NOM = $Row[0];
                }

                $Cantidad = "";
                if(isset($Row[1])) {
                    $Cantidad = $Row[1];
                }
				        $Concepto = "";
                if(isset($Row[2])) {
                    $Concepto = utf8_decode($Row[2]);
                }
				        $codcaj = "";
                if(isset($Row[3])) {
                    $codcaj = utf8_decode($Row[3]);
                }


                    //$query = "INSERT INTO depositos_empleados Values('".$NOM."','".$Cantidad."','".$Concepto."',0,getdate())";
                    $query = "INSERT INTO depositos_empleados (NOM, Cantidad, Concepto, retirado, CreatedAt, FechaRetirar, ImporteInicial, UsuarioDeposito,Solution)  Values('".$NOM."','".$Cantidad."','".$Concepto."',0,getdate(), '".$new_date."','".$Cantidad."', '".$usuario."',$codcaj)";
//                    echo $query;
                    $result = sqlsrv_query($conn, $query);

                    if (! empty($result)) {
                        $type = "success";
                        $message = "Excel Data Imported into the Database";
                    } else {
                        $type = "error";
                        $message = "Problem in Importing Excel Data";
                    }

             }

         }

		}

  }



?>
<html>
<style>
input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
</style>

<br>


					  	<br>

						<form id="Autorizacion" action = "" method = "POST" enctype = "multipart/form-data" >

								 <a href="login2.php" class="btn btn-info" role="button">Subir Depositos</a>



						</form>
						<form id="Depositos" action = "" method = "POST" enctype = "multipart/form-data" style ="display:none">
              <label for="file-upload" class="custom-file-upload">
              <input type = "file" onchange='cambiar()' name = "file" id="file-upload" class="file-upload"/>
              Selecciona el archivo
                </label>
                <input type = "submit" value="Enviar" class="btn"/>


              <div id="info"></div>
              <div class="col-sm-4">
            <label>Fecha a Retirar:</label>

              <input type="date" id="FechaR" name="FechaR"  value="<?php echo $today; ?>">
            </div>

						</form>
          <br>
              <p align="right">
            	<button onclick="exportToExcel('detallenuevo')">Exportar a Excel</button>
              </p>
<div class="row">

					  		<div class="col-md-12">

					  			<table class="table-bordered table-editable table-hover table-striped table-responsive table" width="100%" id="detallenuevo" >
					        		<thead>
					        			<tr class="encabezado" >
											<th><i class="fas fa-ban"></i></th>
                      <th>Numero</th>
											<th>Id</th>
											<th>NOM</th>
					        		<th>Nombre</th>
											<th>Importe Disponible</th>
					        		<th>Concepto</th>
					        		<th>Fecha Creación</th>
											<th>Fecha a Retirar</th>
											<th>Importe Inicial</th>
											<th>Cajero</th>
					        			</tr>
					        		</thead>
					        		<tbody>
							        	<tr>
											<td>	<a href="#" style="color: red" id="eliminarFila"  data-toggle="modal" data-target="#myModalCajero"><i class="fas fa-trash-alt"></i></a></td>
                      <td class="Numero"></td>
  											<td class="Id"></td>
  								      <td class="NOM"></td>
  											<td class="Nombre"></td>
  								      <td class="Cantidad"></td>
  											<td class="Concepto"></td>
  											<td class="CreatedAt"></td>
  											<td class="FechaRetirar"></td>
  											<td class="ImporteInicial"></td>
  											<td class="Cajero"></td>
										</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>

<br>
