<?php
include "../db/Utils.php";

$folio = $_POST['folio'];
$empresa = $_POST['empresa'];

$sqldet = "SELECT * FROM dbEypo.dbo.arcosDet WHERE folio = '$folio'";
$consultDet = sqlsrv_query($conn, $sqldet);
$detalleArmadoConsulta = [];
$detalleDesarmeConsulta = [];
WHILE($row = sqlsrv_fetch_array($consultDet)){ 	
	if($row['ArmadoDesarmado'] == 'A')
		$armadoDesarmado = 'A';
	if($row['ArmadoDesarmado'] == 'D')
		$armadoDesarmado = 'D';
	$linea = '
            <tr style="height: 28px">   
                <td class="codigoArt">'.($row['CodigoArt']).'</td>
                <td class="nombreArt">'.($row['NombreArt']).'</td>
				<td class="piezas">'.$row['piezas'].'</td>
				<td class="unidad">PZA</td>
				<td class="armadoDesarmado"  style="display:none">'.$armadoDesarmado.'</td>
            </tr>
		'; 
		if($row['ArmadoDesarmado'] == 'A')
		array_push($detalleArmadoConsulta, $linea);

		if($row['ArmadoDesarmado'] == 'D')
		array_push($detalleDesarmeConsulta, $linea);
}


$sql = "SELECT * FROM dbEypo.dbo.arcos WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'";
$consulta = sqlsrv_query($conn, $sql);
$Row = sqlsrv_fetch_array($consulta);


if($Row['FechaEntregaTotal']){
	$fechaTotal = $Row['FechaEntregaTotal']->format('Y-m-d');
} else {
	$fechaTotal = $Row['FechaEntregaTotal'];
}

$consultaunoatras = [
	"folio" => $Row['FolioIndependiente'],
	
	"estatus" => $Row['estatus'],
	"fechaSolicitud" => $Row['fechaSolicitud']->format('Y-m-d'),
	"fechaEntrega" => $Row['fechaEntrega']->format('Y-m-d'),
	"nombreSolicitante" => ($Row['nombreSolicitante']),
	"nombreProyecto" => ($Row['nombreProyecto']),
	"numeroOrden" => $Row['numeroOrden'],

	"codigoCliente" => $Row['codigoCliente'],
	"nombreCliente" => ($Row['nombreCliente']),
	"cantidadFabricar" => $Row['cantidadFabricar'],
	"entregasParciales" => $Row['entregasParciales'],


	"productoaFabricar" => ($Row['productoaFabricar']),
	"material" => ($Row['material']),
	"terminadoFinal" => ($Row['terminadoFinal']),
	"observaciones" => ($Row['observaciones']),
	"empresa" => ($Row['empresa']),
	"codigoProducto" => ($Row['codigoProducto']),
	"descripcionProducto" => ($Row['descripcionProducto']),
	"productosFabricarNombre" => ($Row['productosFabricarNombre']),
	"productosDesarmar" => ($Row['productoaDesarmar']),
	"productosDesarmarNombre" => ($Row['productosDesarmarNombre']),
	"SePM" => $Row['SePM'],
	"SeFt" => $Row['SeFt'],
	"SeDib" => $Row['SeDib'],
	"SeDia" => $Row['SeDia'],
	"SeOtr" => $Row['SeOtr'],
	"observacionesPlanta" => ($Row['observacionesPlanta']),
	"EntregaParcial" => $Row['EntregaParcial'],
	"entregaTotal" => $Row['EntregaTotal'],
	"FechaEntregaTotal" => $fechaTotal,
	"detalleArmado" => $detalleArmadoConsulta,
	"detalleDesarme" => $detalleDesarmeConsulta,
	"logistica" => $Row['Logistica'],
	"comentarioLogistica" => ($Row['ComentarioLogistica']),
	


];

echo json_encode($consultaunoatras);
?>
