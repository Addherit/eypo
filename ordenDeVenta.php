<?php
	include "db/Utils.php";
	
	if (isset($_GET['FolioSAP'])) {
		$ofertaVenta = $_GET[ 'FolioSAP'];
	} else {
		$ofertaVenta = 0;
	}

	if (isset($_GET['ofv'])) {
		$ordenVenta = $_GET['ofv'];
	} else {
		$ordenVenta = 0;
	}

	$sql2 = "SELECT max(FolioSAP) as maxi, min(FolioSAP) as mini FROM EYPO.dbo.IV_EY_PV_OrdenesVentaCab WHERE SerieNombre = 'OVT-GRAL'";
	$consultasql = sqlsrv_query($conn, $sql2);
	$Row = sqlsrv_fetch_array($consultasql);	
	$folioPrimero = $Row['mini'];
	$folioUltimo = $Row['maxi'];
	$folioSiguiente = $folioUltimo + 1;								
?>

<!DOCTYPE html>
<html>
	<?php include "header.php"?>
	<body>
		<?php include "nav.php"?>
		<?php include "modales.php"?>
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-lg-5 col-xl-6">
					<h1 style="color: #2fa4e7">Orden de venta</h1>
				</div>
				<div id="btnEnca" class="col-lg-7 col-xl-6" style="font-size: 2rem">
					<?php include "botonesDeControl.php" ?>
				</div>
			</div>
			<hr>
			<div class="row mensaje"><div class="col-12"></div></div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
					<label class="col-3 col-md-4 col-lg-3 col-form-label">Cliente:</label>
						<div class="col-6">
							<input type="text" id="codcliente" disabled>
						</div>
					</div>
					<div class="row">
						<label class="col-3 col-md-4 col-lg-3 col-form-label">Nombre:</label><br>
						<div class="col-6">
							<input type="text" id="nombreCliente" disabled>
						</div>
					</div>
					<div class="row">
						<label class="col-3 col-md-4 col-lg-3 col-form-label">Persona de contacto:</label>
						<div class="col-6">
							<input type="text" id="personaContacto" disabled >
						</div>
					</div>
					<div class="row">
					<label class="col-3 col-md-4 col-lg-3 col-form-label">Oferta de compra:</label>
					<div class="col-6">
						<input type="text" id="ofertaDeCompra" disabled>
					</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Tipo moneda:</label>
						<div class="col-6">
							<input type="text" id="tipoMoneda" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Uso principal</label>
						<div class="col-6">
							<input type="text" id="usoPrincipal" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Método de pago</label>
						<div class="col-6">
							<input type="text" id="metodoPago" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Forma de pago</label>
						<div class="col-6">
							<input type="text" id="formaPago" disabled>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<label class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">N° Folio:</label>
						<div class="col-6">
							<input type="text" id="cdoc" class="text-right" value="<?php echo $folioSiguiente ?>" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Estado:</label>
						<div class="col-6">
							<input type="text" id="estatus" class="text-right" value="Abierto" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha contabilización:</label>
						<div class="col-6">
							<input type="date" id="fconta" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha entrega:</label>
						<div class="col-6">
							<input type="date" id="fentrega" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha documento:</label>
						<div class="col-6">
							<input type="date" id="fdoc" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha vencimiento:</label>
						<div class="col-6">
							<input type="date" id="fvencimiento" disabled>
						</div>
					</div>





					<div class="row">
						<div class="col-md-7 offset-md-4 text-right" style="margin-top: 10px" disabled>
							<button class="btn btn-secundary btn-sm" id="btnCamposDefinidos" title="Campos definidos por el usuario">Campos definidos por el usuario</button>
						</div>
					</div>
					<div class="row" id="camposDefinidos" style=" margin-top: 10px">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Número a letra:</label>
						<div class="col-6">
							<input type="text" id="numLetra" disabled>
						</div>				
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Tipo de factura:</label>
						<div class="col-sm-4">
							<input type="text" id="tipoFactura" disabled>
						</div>
						<label for="" class="col-sm-4 offset-md-4 col-form-label">LAB:</label>
						<div class="col-sm-4">
							<input type="text" id="lab" disabled>
						</div>
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Condición de pago:</label>
						<div class="col-sm-4">
							<input type="text" id="condicionPago" disabled>
						</div>

						<label for="" class="col-sm-4 offset-md-4 col-form-label">RFC:</label>
						<div class="col-sm-4">
							<input type="text" id="rfc_campos_definidos" disabled>
						</div>
					</div>



				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Contenido</a>
						</li>
					</ul>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
							<br>										
							<div class="row">
								<div class="col-md-12">
									<table class="table table-sm table-bordered table-striped text-center" id="detalleoferta" disable>
										<thead>
											<tr>
												<th>#</th>
												<th>Número de artíuclo</th>
												<th>Descripción de artíuclo</th>
												<th>Cantidad</th>
												<th>Precio por unidad</th>						        										        																		
												<th>Total Unitario</th>
												<th>Almacen</th>
												<th>Cantidad pendiente</th>
												<th>Codigo Clasificación SAT</th>
												<th> Unidad medida SAP</th>
												<th>Comentarios de partida 1</th>
												<th>Comentarios de partida 2</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="cont">1</td>
												<td class="narticulo"></td>
												<td class="darticulo"></td>
												<td class="cantidad"></td>
												<td class="precio"></td>																
												<td class="total"></td>
												<td class="almacen"></td>
												<td class="pendiente"></td>
												<td class="codigoPlanificacionSAP"></td>
												<td class="unidadMedidaSAP"></td>
												<td class="comentario1"></td>
												<td class="comentario2"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>			
						</div>
						<br>
		
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Empleado de ventas:</label>
									<div class="col-6">
										<input type="text" id="empleadoVentas" value="<?php echo "$empleadoVentas"; ?>" disabled>
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Propietario:</label>
									<div class="col-6">
										<input type="text" id="propietario" value="<?php echo "$nombreCompleto"; ?>"  disabled>
									</div>
								</div>
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Comentarios:</label>
									<div class="col-8">
										<textarea id="comentarios" rows="3" class="form-control" disabled></textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Subtotal:</label>
									<div class="col-6">
										<input type="text" id="totalAntesDescuento" disabled value="0.00" class="text-right">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Descuento:</label>
									<div class="col-6">
										<input type="text" id="descNum" value="0" class="text-center" style="width: 16%;" disabled>
										<span whidth="6%">%</span>
										<input type="text" value="0.00" class="text-right" id="descAplicado" style="width: 68%" disabled>
									</div>
								</div>
								<div class="row">
									<label class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Impuesto:</label>
									<div class="col-6">
										<input type="text"value="0.00" class="text-right" id="impuestoTotal" disabled>
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Total del documento:</label>
									<div class="col-6">
										<input type="text" id="totalDelDocumento" value="0.00" style="width: 69%" class="text-right"  disabled>								
										<input class="text-center" type="text" id="monedaVisor" style="width: 20%" class="text-right" disabled>
									</div>
								</div>
							</div>
					</div>			
					
					<div class="row" id= btnFoot>
						<div class="col-md-6">
							<a href="">	<button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>
						</div>
					</div><br>			
				</div>
				<?php include "footer.php"; ?>
			</body>
	<script>

		var ordenVenta = <?php echo $ordenVenta ?>;
		var ofertaVenta = <?php echo $ofertaVenta ?>;

		if (ordenVenta != 0 ) {
			consultaORV(ordenVenta);
		}	

		function btn_busqueda_general() {
			$("#modalBuscarOrdenes").modal();	
		}

		function mostrarElementosEnInputs(response){

			$("#codcliente").val(response['CodigoCliente']).prop("readonly", true);
			$("#nombreCliente").val(response['NombreCliente']).prop("readonly", true);
			$("#personaContacto").val(response['ListContactos']).prop("readonly", true);
			$("#ofertaDeCompra").val(response['OrdCompra']).prop("readonly", true);
			$("#tipoMoneda").val(response['TipoMoneda']).prop("readonly", true);
			$("#usoPrincipal").val(response['usoPrincipal']);
			$("#metodoPago").val(response['metodoPago']);
			$("#formaPago").val(response['formaPago']);

			$("#ndoc").val(response['NDoc']).prop("readonly", true);					
			$("#cdoc").val(response['DocNum']).prop("readonly", true);
			$("#estatus").val(response['Status']).prop("readonly", true);					
			$("#fconta").val(response['FConta']).prop("readonly", true);
			$("#fentrega").val(response['FEntrega']).prop("readonly", true);
			$("#fdoc").val(response['FDoc']).prop("readonly", true);
			$("#fvencimiento").val(response['Fvencimiento']).prop("readonly", true);

			$("#numLetra").val(response['numLetra']);
			$("#tipoFactura").val(response['TipoFactura']);
			$("#lab").val(response['Lab']);
			$("#condicionPago").val(response['CondicionPago']);

			$('#empleadoVentas').val(response['usuario']).prop("readonly", true);
			$('#propietario').val(response['nombreUsuario']).prop("readonly", true);
			$('#comentarios').val(response['comentarios']);					

			$('#totalAntesDescuento').val(response['subTotal']).prop("readonly", true);
			// $('#descNum').val(response['descNum']).prop("disabled", true);
			// $('#desAplicado').val(response['desAplicado']).prop("disabled", true);
			$('#impuestoTotal').val(response['impuesto']).prop("readonly", true);
			$('#totalDelDocumento').val(response['totalDelDocumento']).prop("readonly", true);	
			$('#monedaVisor').val($("#tipoMoneda").val()).prop("readonly", true)	


			$("#detalleoferta tbody").empty();				
			for (x= 0; x<response.detalle.length; x++ ){
				$("#detalleoferta tbody").append(response.detalle[x]);
			} 	

		}

		function consultaORV(folio) {
			$.ajax({
				type: 'post',
				url: 'orvConsultas/consultaOrdenDeVenta.php',
				data: { cdoc: folio },
				dataType: "json",
				success: function(response) {
					console.log(response);				
					mostrarElementosEnInputs(response);							
				},
			});
		}

		function typear_buscar_orv() {
			var valorEscrito = $('#buscadorOrdenes').val();

			if (valorEscrito) {
				$.ajax({
					url: 'orvConsultas/buscadorGeneralORV.php',
					type: 'post',
					data: {valor: valorEscrito},
					success: function(response){
						$('#tblBuscarOrdenes tbody').empty();
						$('#tblBuscarOrdenes tbody').append(response);

						$("#tblBuscarOrdenes tbody tr").on('click',function(){
							var codigo = $(this).find('td').eq(1).text();
							consultaORV(codigo);	
							$("#modalBuscarOrdenes").modal('hide');														
						});
					},
				});

			}else {
				$('#tblBuscarOfertas tbody').empty();				
			}
		}

		function consultar_orv_por_fecha() {
			var valorFecha = $('#buscadorOrdenesFecha').val();
			
			$.ajax({
				url: 'orvConsultas/buscadorGeneralORVFecha.php',
				type: 'post',
				data: {valor: valorFecha},
				success: function(response){
					$('#tblBuscarOrdenes tbody').empty();
					$('#tblBuscarOrdenes tbody').append(response);

					$("#tblBuscarOrdenes tbody tr").on('click',function(){
						var codigo = $(this).find('td').eq(1).text();
						consultaORV(codigo);	
						$("#modalBuscarOrdenes").modal('hide');														
					});
				},
			});
		}

		function consulta_1_atras() {
			var cdoc = parseInt($("#cdoc").val()) -1;
			consultaORV(cdoc);
		}

		function consulta_1_adelante() {
			var cdoc = parseInt($("#cdoc").val()) + 1;
			consultaORV(cdoc);
		}

		function consultar_primer_registro() {
			var cdoc = <?php echo "$folioPrimero" ?>;
			consultaORV(cdoc);			
		}

		function consultar_ultimo_registro() {
			var cdoc = <?php echo "$folioUltimo" ?>;
			consultaORV(cdoc);
		}

		$("#btnPdf").on("click", function (params) {
				var folio =  $("#cdoc").val();

				console.log(folio);

				$.ajax({
					url: 'http://187.188.40.90:85/CrystalReportViewer/ReporteORV.aspx?nom='+folio+'&objecto=17',
				
				}).done((params) => {					
				})
				var winFeature =
				'location=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes';
				wait(2200);
				window.open('visor/dos/OV '+folio+'.pdf','_blank','menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=no,resizable=no,dependent,width=800,height=620,left=0,top=0');
			})

			function wait(ms){
			var start = new Date().getTime();
			var end = start;
			while(end < start + ms) {
				end = new Date().getTime();
			}
		}

		function mostrar_mensajes_ofv() {

$("#tblMensajes tbody").empty();
$.ajax({
	url:'modalMensajeConsulta.php',
	success: function (params) {
		$("#tblMensajes tbody").append(params);

		$("#tblMensajes tbody tr").on('click', function(){
				var FolioSAP = $(this).find('td').eq(0).text();
				var ClaseDocumento = $(this).find('td').eq(1).text();
				var estatus = $(this).find('td').eq(4).text();
				var ordenVenta = $(this).find('td').eq(5).text();

				$("#tblMensajes2").find('td').eq(0).text("1");
				$("#tblMensajes2").find('td').eq(1).text(ordenVenta);
				$("#tblMensajes2").find('td').eq(2).text(FolioSAP);
				$("#tblMensajes2").find('td').eq(3).text(ClaseDocumento);
				$("#tblMensajes2").find('td').eq(4).text(estatus);

			$("#tblMensajes2 tbody tr").on('click', function(){
				var ordenVenta = $(this).find('td').eq(1).text();
				var FolioSAP = $(this).find('td').eq(2).text();
				var estatus = $(this).find('td').eq(4).text();

				switch (estatus) {
					case 'CREADA':
						window.location.href = 'ordenDeVenta.php?FolioSAP='+FolioSAP+'&ofv='+ordenVenta;
						break;
					case 'PENDIENTE CREAR REAL':
						alert("Documento en proceso de creación en firme");
						break;
					case 'ERROR CREAR REAL':
						alert("Error en proceso de creación en firme");
						break;
					case 'BORRADOR CREADO':
						window.location.href = 'ordenDeVentaBorrador.php?FolioSAP='+FolioSAP;
						break;
					case 'PENDIENTE CREAR BORRADOR':
						alert("Documento en proceso de creación de borrador");
						break;
					case 'ERROR CREAR BORRADOR':
						alert("Error en proceso de creación de borrador");
						break;
				}
			});
		});
	}
})
}
	</script>
</html>
