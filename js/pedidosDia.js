var fecha = $("#fechaconta").val()
var status = $("#statusDocumento").val()

$.ajax({
    url:"querys/pedidosDia.php",
    type:"POST",
    data:{ 
        fecha : fecha,
        statusDocumento : status,			
    },
    dataType: 'JSON',
}).done((pedidos) => {	
    
    $("#tbl_pedidos_dl_dia tbody").empty()
    var tr_order = "";

    if(pedidos.length > 0) {
        pedidos.forEach((element, index) => {
            index++
            tr_order += `<tr>
            <td>${index}</td>
            <td>${element["Num Doc"]}</td>
            <td>${element.Hora}</td>
            <td>${element["Op/Cl"]}</td>
            <td>${element.Codigo}</td>
            <td>${element.CardName}</td>
            <td>${element.Mon}</td>
            <td>${element["T.Fac"]}</td>
            <td>${element.DocTotal}</td>
            <td>${element.Fecha.date}</td>
            <td>${element["Fecha Entrega"].date}</td>
            <td>${element.AsVts}</td>
            <td>${element.Ref}</td>
            </tr>`
        })
    } else {
        tr_order += '<tr><td colspan="17">No existe registro alguno con los datos proporcionados</td></tr>' 
    }    
    $("#tbl_pedidos_dl_dia tbody").append(tr_order);        			
});		