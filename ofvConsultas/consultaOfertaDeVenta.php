<?php

  include "../db/Utils.php";

  $cdoc = $_POST['cdoc'];
  $cont = 0;
  $respuesta = [];
  $infoDetalle = []; 
  $sql = "SELECT det.*,
  convert(varchar, convert(money, det.Quantity), 1) AS QuantityFormat, 
  convert(varchar, convert(money, det.PrecioUnitario), 1) AS PrecioUnitarioFormat,
  convert(varchar, convert(money, det.TotalLinea), 1) AS TotalLineaFormat,
  convert(varchar, convert(money, stock.EnStock), 1) AS EnStockFormat,
  convert(varchar, convert(money, stock.Comprometido), 1) AS ComprometidoFormat,
  convert(varchar, convert(money, stock.Solicitado), 1) AS SolicitadoFormat
  FROM EYPO.dbo.IV_EY_PV_OfertasVentasDet det 
  LEFT JOIN EYPO.dbo.IV_EY_PV_Stock stock ON det.CodigoArticulo = stock.CodigoArticulo AND det.Almacen = stock.CodigoAlmacen
  WHERE FolioInterno = '$cdoc'";

  $consulta = sqlsrv_query($conn, $sql);
  while( $row = sqlsrv_fetch_array($consulta, SQLSRV_FETCH_ASSOC) ) {        
    $infoDetalle[] = $row;
  }   

    $sql = "SELECT vista.*, soc.Nombre, soc.RFC, tabla.NombreUsuario, 
    convert(varchar, convert(money, vista.SubTotalDocumento), 1) AS SubTotalDocumentoFormat,
    convert(varchar, convert(decimal(3,0), vista.PorcentajeDescuento), 1) AS PorcentajeDescuentoFormat,
    convert(varchar, convert(money, vista.SumaImpuestos), 1) AS SumaImpuestosFormat,
    convert(varchar, convert(money, vista.ImporteDescuento), 1) AS ImporteDescuentoFormat,
    convert(varchar, convert(money, vista.Redondeo), 1) AS RedondeoFormat,
    convert(varchar, convert(money, vista.TotalDocumento), 1) AS TotalDocumentoFormat
    FROM EYPO.dbo.IV_EY_PV_OfertasVentasCab vista
    LEFT JOIN EYPO.dbo.IV_EY_PV_SociosNegocios soc ON vista.CodigoSN = soc.CodigoSN
    LEFT JOIN dbEypo.dbo.ofertas tabla ON vista.FolioSAP = tabla.NuevoDocEntry
    WHERE vista.FolioSAP = '$cdoc'";

    $consulta = sqlsrv_query($conn, $sql);

    $response = [];
    while( $row = sqlsrv_fetch_array($consulta, SQLSRV_FETCH_ASSOC) ) {        
        $response[] = $row;
    }    
    $respuesta = [$response, $infoDetalle];
    echo json_encode( $respuesta);
    
  ?>
