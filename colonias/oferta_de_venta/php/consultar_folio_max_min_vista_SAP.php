<?php 
    include '../../../db/Utils.php';

    //esta consulta regresa el max y min de la vista de SAP
    $sql2 = "SELECT max(FolioSAP) as maxi, min(FolioSAP) as mini FROM EYPO.dbo.IV_EY_PV_OfertasVentasCab WHERE SerieNombre = 'COLONIAS'";
	$consulta = sqlsrv_query($conn, $sql2);
    $response = [];
    while( $row = sqlsrv_fetch_array($consulta, SQLSRV_FETCH_ASSOC) ) {        
        $response[] = $row;
    }
    echo json_encode( $response );
?>