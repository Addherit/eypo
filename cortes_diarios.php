<?php
include "conexioncajero.php";

session_start();
if (!isset($_SESSION["usuario"]))
{
    header("location: login.php");
}
$_SESSION['usuario'];
$empleadoVentas = $_SESSION['usuario'];
$hoy = date("Y-m-d");
$dia = date("d-m-Y");
$mesDespues = date( "Y-m-d", strtotime( "$hoy +1 month" ) );
$quincena = date( "Y-m-d", strtotime( "$hoy +15 days" ) );

$sql = "SELECT lastName, firstName, middleName FROM OHEM WHERE U_IV_EY_PV_User = '$empleadoVentas'";
$consulta = sqlsrv_query($conn2, $sql);
while ($Row = sqlsrv_fetch_array($consulta)) {
    $nombre = $Row['firstName'];
    $sNombre = $Row['middleName'];
    $apellido = $Row['lastName'];
}
$nombreCompleto = $nombre . " " . $sNombre . " " . $apellido;

$month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;
?>

<!DOCTYPE html>
<html>



<?php include "header.php"?>
<body>
    <?php include "nav.php"?>
    <?php include "modalQuerys.php"; ?>
    <?php include "modales.php"; ?>

    <div class="container" id="contenedorDePagina">
        <br>
        <div class="row">
            <div class="col-md-6">
                <h3 style="color: #2fa4e7">Consulta el Cajero</h3>
            </div>
            <div id="btnEnca" class="col-md-6" style="font-size: 2rem">

            </div>
        </div>
        <div class="row datosEnc" style="font-size: .7rem">
            <div class="col-md-6">
                <div class="row">
                    <label class="col-sm-3 col-form-label" >Id Cajero:</label>
                    <div class="col-sm-4">
                        <input type="text" class="" name="codcajero" id="codcajero" style="width: 70%">
                        <a id="btnCajero" href="#" data-toggle="modal" data-target="#myModalCajero">
                            <i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cajero"></i>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <label for="" class="col-sm-3 col-form-label">Nombre Cajero:</label><br>
                    <div class="col-sm-4">
                        <input type="text" class="" name="NombreC" id="NombreC" style="width: 70%">
                        <a href="#" data-toggle="modal" data-target="#myModalCajero" >
                            <i class="fas fa-search" style="color: #57b4ea" title="Búsqueda Nombre"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <br> 
        <br> 
        <br> 

<label for="fecha">Fecha:</label>
<input type="date" id="fecha" name="trip-start"value="<?php echo $today;?>">

<label for="fecha-final">Fecha final:</label>
<input type="date" id="fecha-final" name="trip-start"value="<?php echo $today;?>">

        <br> 
        <br> 
        <br> 
        <!--tabla de mierda -->
         <div class="row">
            <div class="col-sm">
                <button type="button" onclick="cortedia();" id ="cboton">Corte del Dia</button> 
				<!--<button onclick="exportTableToExcel('tablecortes')">Exportar a Excel</button>-->
            </div>
            <div class="col-7">
            </div>
            <div class="col">
                <input type="text" id="busquedaI" onkeyup="BusquedaTabla()" placeholder="Busca por Nombre">
            </div>
        </div>

        <table id="tablecortes"class="table table-striped table-bordered">
        <thead>
            <tr>

                <th>Numero</th>
                <th>Id</th>
                <th>Fecha</th>
                <th>Cliente</th>
                <th>Nombre de Cliente</th>
                <th>Forma de pago</th>
                <th>Importe</th>
                <th>Iva</th>
                <th>Total</th>
                <th>Action</th>
            </tr>
        </thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </table> 
    </div>



<!--start formato-->
<div id="formato" hidden="true" >

    <div class="container">
        <div class="row ">
            <img src="img/header_cortediario.jpg"> 
        </div>
        <div class="row">
            <div class="col-9">
            </div>
            <div class="col" id="fcorte">
                Corte del Dia: <?php echo $today;?>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <table id="table-nueva-reporte"class="table table-striped table-bordered">
                    <thead>
                        <tr>

                            <th>Numero</th>
                            <th>Id</th>
                            <th>Fecha</th>
                            <th>Cliente</th>
                            <th>Nombre de Cliente</th>
                            <th>Forma de pago</th>
                            <th>Importe</th>
                            <th>Iva</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </table> 
            </div>
        </div>
    </div>
 
    <div class="container">
        <div class="row">
            
            <div class="col-sm">
                Ventas totales del día en sistema    
                <table class="table table-striped table-bordered">
                    <tr>
                        <th style="height: 50px;">Efectivo.-</th>
                    </tr>
                    <tr>
                        <th style="height: 50px;">T.C.-</th>
                    </tr>
                    <tr>
                        <th style="height: 50px;">Cheques.-</th>
                    </tr>
                    <tr>
                        <th style="height: 50px;">N.C / Códigos QR.-</th>
                    </tr>
                </table> 
            </div>

            <div class="col-sm">
                Ingresos reales en cajero
                <table class="table table-striped table-bordered">
                    <tr>
                        <th>Efectivo.-</th>
                        <th id="efe"></th>
                    </tr>
                    <tr>
                        <th>T.C.-</th>
                        <th id="tc"></th>
                    </tr>
                    <tr>
                        <th>N.C / Códigos QR.-</th>
                        <th id="nc"></th>
                    </tr>
                </table> 
            </div>
        </div>

        <div class="row">
            <div class="col-sm " style="background-color:#336599;text-align: center;color:white">
                Enterado
            </div>
            <div class="col-sm"style="background-color:#336599;text-align: center;color:white">
                Recibido
            </div>
            <div class="w-100"></div>
        </div>
            <br>
            <br>
        <div class="row">
            <div class="col-sm"style="text-align: center;">
                Nombre completo, Fecha,Firma
            </div>
            <div class="col-sm" style="text-align: center;">
                Nombre completo, Fecha,Firma
            </div>
        </div>
            <br>
            <br>
        <div class="row">
            <div class="col-sm"style="text-align: center;">
                Av. Francia 1751-B   Col. Moderna   Guadalajara, Jal.  México.  
            </div>
            <div class="w-100"></div>
            <div class="col-sm"style="text-align: center;">
                (52) (33) 3812 3825 / ventas@eypo.com.mx / www.eypo.com.mx
            </div> 
            <div class="w-100"></div>
            <div class="col-sm"style="background-color:#336599;text-align: center;color:white;text-align: right;">
                F-EY-VEN-CD-02
            </div>
        </div>
    </div>

</div>
<!--end formato-->







<?php include "footer.php"; ?>
</body>

<script src="dist/html2pdf.bundle.min.js"></script>
<script src="html2canvas/node_modules/html2canvas/dist/html2canvas.min.js"></script>
<script>

$("#buscadorCajero").keyup(function(){
    var valorEscrito = $("#buscadorCajero").val();
    if (valorEscrito) {
        $.ajax({
            url: 'buscadorConsultaCajero.php',
            type: 'post',
            data: {valor: valorEscrito},
            success: function(resp){


                $("#tblcajero tbody").empty();
                $("#tblcajero tbody").append(resp);

                $("#tblcajero tr").on('click',function(){


                    if (!$('#BackOrderVentas').is(':visible')) {

                                var codigo = $(this).find('td').eq(1).text();
                                var name = $(this).find('td').eq(2).text();


                                agregarCajero(codigo, name);
                                $("#tblcajero tbody").empty();
                                $("#buscadorcajero").val("");

                                $("#nombreCajero").val(name);
                                $("#codigoCajero").val(codigo);




                    } else {
                        var codigo = $(this).find('td').eq(1).text();
                        var name = $(this).find('td').eq(2).text();
                        $("#nombreCajero").val(name);
                        $("#codigoCajero").val(codigo);
                        $("#myModalCajero").hide();


                        }
                    });
                },
            });
    } else {
        $("#tblcajero tbody").empty();
    }
});

$("#myModalCajero").on('click',function() {
    $("#buscadorCajero").val("");
});

$( "#fecha" ).change(function() {
    var code = document.getElementById("codcajero").value;
    var fechaI = document.getElementById("fecha").value;
    var fechaII = document.getElementById("fecha-final").value;
    var fcorte = "Corte del Dia: "+fechaI;

console.log(fechaI);
console.log(fechaII);
    if(fechaI != fechaII){
        document.getElementById("cboton").disabled = true;
    }else{
        document.getElementById("cboton").disabled = false;
    }

    if (code === "") {
    }else{
        document.getElementById("fcorte").innerText = fcorte;
        gettable(code,fechaI,fechaII);
    }
});
$( "#fecha-final" ).change(function() {
    var code = document.getElementById("codcajero").value;
    var fechaI = document.getElementById("fecha").value;
    var fechados = document.getElementById("fecha-final").value;
    var fcorte = "Corte del Dia: "+fechaI;

console.log(fechaI);
console.log(fechados);
    if(fechaI != fechados){
        document.getElementById("cboton").disabled = true;
    }else{
        document.getElementById("cboton").disabled = false;
    }

    if (code === "") {
    }else{
        document.getElementById("fcorte").innerText = fcorte;
        gettable(code,fechaI,fechados);
    }
});



function agregarCajero ($code, $name){
    var code = $code;
    var name = $name;
    var fechaI = document.getElementById("fecha").value;
    var fechaII = document.getElementById("fecha-final").value;

    $("#codcajero").val(code);
    $("#NombreC").val(name);

    $('#myModalCajero').modal('hide');

console.log(fechaI);
console.log(fechaII);
    if(fechaI != fechaII){
        document.getElementById("cboton").disabled = true;
    }else{
        document.getElementById("cboton").disabled = false;
    }

   gettable(code,fechaI,fechaII);
}

function gettable(code,fechaI,fechados){
    $("#tablecortes tbody").empty();
    var linea = "";
    $.ajax({
        url: 'consultacorte.php',
        type: 'post',
        data: {code: code,fecha: fechaI,fecha2:fechados},
        success: function(response){
            response.forEach(element => {
            linea += `<tr>
                        <td>${element.numero}</td>
                        <td>${element.id}</td>
                        <td>${element.fecha}</td>
                        <td>${element.cliente}</td>
                        <td>${element.nombre}</td>
                        <td>${element.fpago}</td>
                        <td>${element.importe}</td>
                        <td>${element.iva}</td>
                        <td>${element.total}</td>
                        <td><button type="button" onclick="corteind(this)">Pdf</button></td>
                    </tr>`;
        });
        document.querySelector("#tablecortes tbody").innerHTML = linea;
        console.log(response);
        }
    });
}

function cortedia(){
    obtener_tbody_tabla_cortesDiarios();
}

function corteind(fila){    
    console.log(fila.parentElement.parentElement)
    //lo que retorna es el boton, subimos dos niveles con parentElement para llegar al tr
    var fila_unica = fila.parentElement.parentElement.outerHTML
    asignar_tbody_a_nueva_table_reporte(fila_unica)

    // document.querySelector("#loquesea-cuadritos").innerHTML = ''

}

function BusquedaTabla() {
  // Declare variables
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("busquedaI");
  filter = input.value.toUpperCase();
  table = document.getElementById("tablecortes");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[4];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}

function obtener_tbody_tabla_cortesDiarios() {
    var tbody = document.querySelector("#tablecortes tbody").innerHTML
    asignar_tbody_a_nueva_table_reporte(tbody)
}

function asignar_tbody_a_nueva_table_reporte(tbody) {
    var nc = 0.0;
    var tc = 0.0;
    var pe = 0.0;
    document.querySelector("#table-nueva-reporte tbody").innerHTML = tbody

    var filas = document.querySelectorAll("#table-nueva-reporte tbody")[0].childNodes
    filas.forEach(element => {
        switch (element.children[5].innerText) {
        case "Nota de Credito'":
            nc += parseFloat(element.children[8].innerText);
            break;
        case "Tarjeta de Credito":
            tc += parseFloat(element.children[8].innerText);
            break;
        case "Pago en Efectivo":
            pe += parseFloat(element.children[8].innerText);
            break;
        }
        element.children[9].remove()
    });
    document.getElementById('efe').innerText=pe;
    document.getElementById('nc').innerText=nc;
    document.getElementById('tc').innerText=tc;
    
    hacer_screen_formato();
   //document.querySelector("#formato").hidden = true
}

function hacer_screen_formato() {
    
//    document.querySelector("#formato").hidden = false
    var element = document.getElementById('formato').innerHTML;
    var fechaI = document.getElementById("fecha").value;
    var nombre = "corte_"+fechaI+".pdf"
    var opt = {
    margin:       0,
    filename:     nombre,
    image:        { type: 'jpeg', quality: 0.98 },
    html2canvas:  { scale: 1 },
    pagebreak: { mode: 'avoid-all' }
    //  jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
    };

    // New Promise-based usage:
//    html2pdf().set(opt).from(element).save();
    var worker = html2pdf().set(opt).from(element).save();


/*
    html2canvas(document.querySelector("#formato")).then(canvas => {
        var base64image = canvas.toDataURL("image/png");
        window.open(base64image , "_blank");        
        //var pop= window.open(base64image,'popup','opciones');
		//pop.print();
    });*/
}


function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename?filename+'.xls':'movimientos.xls';

    // Create download link element
    downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

        // Setting the file name
        downloadLink.download = filename;

        //triggering the function
        downloadLink.click();
    }
}


</script>
</html>