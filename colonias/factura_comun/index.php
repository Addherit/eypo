<!DOCTYPE html>
<?php include "../header.html" ?>
<html lang="en">
	<body onload="cargar_funciones_principales()">
		<?php include "../nav.php" ?> 
		<?php include "../../modalQuerys.php" ?>
		<?php include "../../modales.php" ?>
		
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h1 style="color: #2fa4e7">Factura Común</h1>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">
					<?php include "../../botonesDeControl.php" ?>
				</div>
				<input type="hidden" id="primer-registro">
				<input type="hidden" id="ultimo-registro">
			</div>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<label class="col-3 col-md-4 col-lg-3 col-form-label" >Cliente:</label>
						<div class="col-6">
							<input type="text" id="cliente">
						</div>
					</div>
					<div class="row">
						<label class="col-3 col-md-4 col-lg-3 col-form-label">Nombre:</label><br>
						<div class="col-6">
							<input type="text" id="nCliente">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Persona de contacto:</label>
						<div class="col-6">
							<input type="text" id="personaContacto">								
						</div>
					</div>					
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Tipo moneda:</label>
						<div class="col-6">
							<input type="text" id="tipoMoneda">								
						</div>
					</div>						
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Orden de compra</label>
						<div class="col-6">
							<input type="text" id="ordenCompra">	
						</div>
					</div>
					<div class="row">								
						<div class="col-sm-4">
							<input type="text" id="rutaArchivos" style="display: none">	
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">N° Folio:</label>
						<div class="col-6">														
							<input type="text" class="text-right" id="cdoc" readonly="true" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Estado:</label>
						<div class="col-6">
							<input type="text" id="estado" value="Abierto" class="text-right" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha de contabilización:</label>
						<div class="col-6">
							<input type="date" value="<?php echo $hoy ?>" id="fconta" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha del documento:</label>
						<div class="col-6">
							<input type="date" value="<?php echo $hoy ?>" id="fdoc" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha de vencimiento:</label>
						<div class="col-6">
							<input type="date" value="<?php echo $quincena ?>" id="fven" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Uso principal:</label>
						<div class="col-6">
							<input type="text" id="usoPrincipal">								
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Método de pago:</label>
						<div class="col-6">
							<input type="text" id="metodoPago">								
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Forma de pago</label>
						<div class="col-6">
							<input type="text" id="formaPago">	
						</div>
					</div>
				</div>
			</div>					
			<div class="row ">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Contenido</a>
						</li>						
					</ul>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
						<br>
						<div class="row">
							<div class="col-md-12">
							<section class="table-responsive">
								<table class="table table-sm table-striped table-bordered text-center" id="tblFactuaComun">
									<thead>
										<tr class="encabezado" >
											<th><i class="fas fa-ban"></i></th>
											<th>#</th>
											<th>Código articulo</th>
											<th>Nombre artíuclo</th>					        				
											<th>Cantidad</th>
											<th>Precio por unidad</th>
											<th>Descuento</th>													
											<th>Ind. Impuesto</th>
											<th>Sujeto a retención de impuesto</th>										
											<th>Total</th>
											<th>Unidad SAT</th>	
											<th>Almacen</th>	
											<th>Comentarios de partida 1</th>
											<th>Comentarios de partida 2</th>	
											<th>Stock</th>
											<th>Comprometido</th>
											<th>Solicitado</th>																																														
										</tr>
									</thead>
									<tbody> 
										<tr>
											<th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>
											<td class="cont"></td>
											<td data-toggle="modal" data-target="#myModalArticulos" class="narticulo"></td>
											<td data-toggle="modal" data-target="#myModalArticulos" class="darticulo"></td>
											<td contenteditable="true" class="cantidad"></td>
											<td contenteditable="true" class="precio"></td>
											<td contenteditable="true" class="descuento"></td>
											<td class="impuesto"></td> 
											<td class="codigoImpuesto"></td>
											<td class="total"></td>
											<th class="unidadSAT"></th>	
											<td class="almacen"></td>	
											<td contenteditable="true" class="comentario1"></td>
											<td contenteditable="true" class="comentario2"></td>
											<td class="stock"></td>
											<td class="comprometido"></td>
											<td class="solicitado"></td>																																				
										</tr>
									</tbody>
								</table>
							</section>
							</div>
						</div>
						<br>
						<div class="row ">
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Empleado de ventas:</label>
									<div class="col-sm-6">
										<input type="text" id="empleado">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Proyecto SN:</label>
									<div class="col-sm-6">
										<input type="text" id="proyectoSN">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Ventas Adicional:</label>
									<div class="col-sm-6">
										<input type="text" id="ventasAdic">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Promotor:</label>
									<div class="col-sm-6">
										<input type="text" id="promotor">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Promotor de venta:</label>
									<div class="col-sm-6">
										<input type="text" id="promotorDeVenta">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Comentarios:</label>
									<div class="col-sm-4">
										<textarea name="" id="comentarios" cols="60" rows="4" style="background-color: #ffff002e;"></textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
							
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Subtotal:</label>
									<div class="col-6">
										<input type="text" id="totalAntesDescuento" value="0.00" class="text-right">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Descuento:</label>
									<div class="col-6">
										<input type="text" id="descNum" value="0" class="text-center" style="width: 16%;">
										<span whidth="6%">%</span>
										<input type="text" id="descAplicado" value="0.00" class="text-right" style="width: 68%">
									</div>
								</div>									
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Redondeo:</label>
									<div class="col-6">
										<input type="text" id="redondeo" value="0.00" class="text-right">
									</div>
								</div>
								<div class="row">
									<label class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Impuesto:</label>
									<div class="col-6">
										<input type="text" value="0.00" class="text-right" id="impuestoTotal">
									</div>
								</div>
								<div class="row">
									<label class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Total:</label>
									<div class="col-6">
											<input type="text" id="totalDelDocumento" value="0.00" style="width: 74%" class="text-right">
											<input type="text" id="monedaVisor" style="width: 15%" class="text-center">
									</div>
								</div>
								<div class="row">
									<label class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Inporte aplicado:</label>
									<div class="col-6">
											<input type="text" id="importeAplicado" value="0.00" class="text-right">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Solo vencido:</label>
									<div class="col-6">
										<input type="text" id="soloVencido" value="0.00" class="text-right">
									</div>
								</div>
							</div>								
						</div>		 				
					</div>
				</div>
				<div class="row" id= btnFoot style="margin-bottom: 30px">
					<div class="col-md-6"><br>					
						<a href="#"><button class="btn btn-sm" style="background-color: orange" title="Crear Devoluciones" id="btnCrear">Crear devolución</button></a>					
						<a href=""><button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>					
					</div>			
				</div>	
			</div>									
    	</div>		
	</body>
	<?php include "../footer.html"; ?>
	<script src="js/factura_comun.js"></script>
</html>
