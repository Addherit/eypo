<?php
  include "../db/Utils.php";
  $cdoc = $_POST['cdoc'];


  $cont = 0;
  $infoDetalle = [];
  $sql = "SELECT * FROM EYPO.dbo.IV_EY_PV_OrdenesVentaDet WHERE FolioInterno = '$cdoc'";
  $consulta = sqlsrv_query($conn, $sql);
  while ($Row = sqlsrv_fetch_array($consulta)) {
    $cont++;  
    $linea = '<tr>
                <td>'.$cont.'</td> 
                <td>'.$Row['CodigoArticulo'].'</td>
                <td>'.utf8_encode($Row['NombreArticulo']).'</td>
                <td>'.number_format($Row['Quantity'],0,'.',',').'</td>
                <td>'.number_format($Row['PrecioUnitario'],2,'.',',').'</td>
                <td>'.number_format($Row['TotalLinea'],2,'.',','). '</td>                                     
                <td>'.$Row['Almacen'].'</td>
                <td>'.number_format($Row['CantidadAbierta'],2,'.',',').'</td>
                <td>'.$Row['CodigoPlanificacionSat'].'</td>
                <td>'.$Row['UnidadMedida'].'</td>     
                <td class="comentario1" contenteditable="true">'.utf8_encode($Row['ComentarioPartida1']).'</td>
                <td class="comentario2" contenteditable="true">'.utf8_encode($Row['ComentarioPartida2']).'</td>                                                
              </tr>';

          array_push($infoDetalle, $linea);
  }     

      $sql2 = "SELECT TOP 1 *, orv.CodigoSN, soc.Nombre, orv.Referencia, usu.Nombre as NombrePropietario, usu.SegundoNombre, 
      usu.Apellidos, orv.Usuario, orv.Comentarios as comentariosOrdenVenta, orv.Moneda as monedaSeleccionada FROM EYPO.dbo.IV_EY_PV_OrdenesVentaCab orv
      LEFT JOIN EYPO.dbo.IV_EY_PV_SociosNegocios soc ON orv.CodigoSN = soc.CodigoSN      
      LEFT JOIN EYPO.dbo.IV_EY_PV_Usuarios usu ON orv.Usuario  = usu.[User]
    WHERE orv.FolioSAP = '$cdoc'";
    $consulta2 = sqlsrv_query($conn, $sql2);
    $Row = sqlsrv_fetch_array($consulta2);
    
      $consultaOrdenDeVenta = [
        "CodigoCliente" => utf8_encode($Row['CodigoSN']),
        "NombreCliente" => utf8_encode($Row['Nombre']),
        "personaContacto" => utf8_encode($Row['PersonaContacto']),
        "OrdCompra" => utf8_encode($Row['Referencia']),
        "TipoMoneda" => utf8_encode($Row['monedaSeleccionada']),
        "usoPrincipal" => utf8_encode($Row['UsoCFDi']),
        "metodoPago" => utf8_encode($Row['MetodoPago']),
        "formaPago" => utf8_encode($Row['IdFormaPago']),

        "NDoc" => utf8_encode($Row['SerieNombre']),
        "DocNum" => $Row['FolioSAP'], 
        "Status" => utf8_encode($Row['Estatus']),
        "FConta" => $Row['FechaContabilizacion']->format('Y-m-d'),
        "FEntrega" => $Row['FechaVencimiento']->format('Y-m-d'),  
        "FDoc" => $Row['FechaDocumento']->format('Y-m-d'),
        "Fvencimiento" => $Row['FechaVencimiento']->format('Y-m-d'),

        "numLetra" => utf8_encode($Row['NumeroLetra']), 
        "TipoFactura" => utf8_encode($Row['TipoFactura']),
        "Lab" => utf8_encode($Row['LAB']),
        "CondicionPago" => utf8_encode($Row['DescCondicionesPago']),


        "usuario" => utf8_encode($Row['Usuario']),
        "nombreUsuario" => utf8_encode($Row['NombrePropietario']." ".$Row['SegundoNombre']." ".$Row['Apellidos']),
        "comentarios" => utf8_encode($Row['comentariosOrdenVenta']),
        
        "subTotal" => number_format($Row['SubTotalDocumento'], 2, '.', ','),
        // "descNum" => number_format($Row['PorcentajeDescuento'], 0, '.', ''),
        // "desAplicado" => number_format($Row['ImporteDescuento'], 2, '.', ''),        
        "impuesto" => number_format($Row['SumaImpuestos'], 2, '.', ','),
        "totalDelDocumento" => number_format($Row['TotalDocumento'], 2, '.', ','),
        "detalle" => $infoDetalle,
      ];
      echo json_encode($consultaOrdenDeVenta);
  ?>
