	<?php
	
	include "conexion.php";
	session_start();
	$_SESSION['usuario'];
	
	If ($_POST['valor'] != '')
	{
	$_SESSION['valor'] = $_POST['valor'];
	}
	 $folio = $_SESSION['valor'];
		$month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;

$FechaInicio = date( "Y-m-d", strtotime( "$today -6 month" ) );
	?>

	<!DOCTYPE html>
	<html>
		<?php include "header.php"; ?>

		<body onload="consultaOFV(<?php echo $folio;?>);">		
		<?php include "nav.php"; ?>		
		<?php include "modales.php"; ?>
		
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Proyecto:</h3>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">
					<?php include "botonesDeControlMaster.php" ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<label class="col-sm-3 col-form-label" >Cliente:</label>
						<div class="col-sm-9">
							<input type="text" class="" name="codcliente" id="codcliente" style="width: 70%">
							<a id="btnCliente" href="#" data-toggle="modal" data-target="#myModal">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Nombre del Cliente:</label><br>
						<div class="col-sm-9">
							<input type="text" class="" name="NombreC" id="NombreC" style="width: 70%">
							<a href="#" data-toggle="modal" data-target="#myModal" >
								<i class="fas fa-search" style="color: #57b4ea" title="Búsqueda Nombre"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Persona de Contacto:</label>
						<div class="col-sm-9">
							<input type="text" class="" id="Contacto" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Ubicación del Proyecto:</label>
						<div class="col-sm-9">
							<input type="text" class="" id="Ubicacion" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Celular del Contacto:</label>
						<div class="col-sm-9">
							<input type="text" class="" id="Telefono" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Correo:</label>
						<div class="col-sm-9">
							<input type="email" class="" id="Correo" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Nombre del Proyecto:</label>
						<div class="col-sm-9">
							<input type="text" class="" id="NombreProyecto" style="width: 70%">
						</div>
					</div>
					
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Fecha de Posible Venta:</label>
						<div class="col-sm-9">
							<input type="date" class="" id="FechaVenta" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Fecha de Promesa de Entrega:</label>
						<div class="col-sm-9">
							<input type="date" class="" id="FechaEntrega" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Tipo de Propuesta:</label>
						<div class="col-sm-9">
							<select name="" id="tpropuesta" style="height: 23px; width: 70%">
								<option value="" disabled selected>Seleccione</option>
									<?php
								$sql = "SELECT * FROM MasterEypo.dbo.TiposPropuestas where Estatus = 'ALTA'";
								$consulta = sqlsrv_query($conn, $sql);
								while ($row = sqlsrv_fetch_array($consulta)) { ?>
											<option value="<?php echo ($row['TipoPropuesta']); ?>"><?php echo ($row['TipoPropuesta']); ?></option>
									<?php 
							} ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						    <label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Asesor de Ventas:</label>
						    <div class="col-sm-3">
									<select name="" id="AsesorV" style="width: 100%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT iv.NombreEmpleadoVC+ ' ' + isnull(firstName,'') + ' ' + isnull(middleName,'') + ' ' + isnull(lastName,'') as Nombre, iv.* FROM IV_EY_PV_EmpleadosVentasCompras iv
left outer join OHEM e on iv.CodigoEmpleadoVC   = e.salesPrson";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['NombreEmpleadoVC']); ?>"> <?php echo ($row['Nombre']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
							<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Administrador de Ventas:</label>
						    <div class="col-sm-3">
									<select name="" id="AdminV" style="width: 100%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT iv.NombreEmpleadoVC+ ' ' + isnull(firstName,'') + ' ' + isnull(middleName,'') + ' ' + isnull(lastName,'') as Nombre, iv.* FROM IV_EY_PV_EmpleadosVentasCompras iv
left outer join OHEM e on iv.CodigoEmpleadoVC   = e.salesPrson";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['NombreEmpleadoVC']); ?>"> <?php echo ($row['Nombre']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
					</div>
						
					
				</div>
				<div class="col-md-6">
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">N°:</label>
						<div class="col-sm-4">
							<select id="ndoc" style="height: 23px">
								<option value="COT-GRAL">PROYECTO</option>
								
							</select>
							
							<input type="text" class="" id="cdoc" name="cdoc" size="5" value="<?php echo "$folio"; ?>" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Prioridad:</label>
						 <div class="col-sm-4">
									<select name="" id="Prioridad">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT * FROM MasterEypo.dbo.Prioridades where Estatus = 'ALTA' order by prioridad";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['Prioridad']); ?>"> <?php echo ($row['Prioridad']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
												
					</div>
					<div class="row">
						<label for=""  class="col-sm-4 offset-md-4 col-form-label">Estatus:</label>
						<div class="col-sm-4">
							<input type="text" disabled="true" class="" id="Estatus">
						</div>
					</div>
					
					
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Ventas Directivas*:</label>
						<div class="col-sm-4">
							<input type="text" class="" id="vDirectivas" style="width: 70%">
						</div>
					</div>
					<div class="row">
					<label for="" class="col-sm-4 offset-md-4 col-form-label table-hover table-striped table-responsive table">Archivos:</label>
					</div>
					<table class="col-sm-4 offset-md-4" id="Adjuntos" style="border-spacing:5px; border-collapse: separate;">
						  <tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						  </tr>
						  <tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						  </tr>
						  </table>
					<div class="row">
					<div class="col-md-7 offset-md-4 text-Right" style="margin-top: 10px ">
					
							<!--<form id="Adjuntos" action = "" method = "POST" enctype = "multipart/form-data">-->
								<input type="text" class="" id="idproyecto" name="idproyecto" size="5" value="<?php echo "$folio"; ?>" readonly="true" style="display:none">
								<label for="file-upload" class="custom-file-upload">
								<input type = "file" onchange='cambiar()' name = "file" id="file-upload" class="file-upload"/>
								
								  </label>
								  <button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnAdjuntar" title="Adjuntar">Adjuntar Archivo</button>
								<!--  <input type = "submit" value="Adjuntar Archivo" class="btn"/>-->
								
								
								<div id="info"></div>
								 
								

								 
			
							<!--</form>-->
						</div>
							
					</div>
				<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Complejidad:</label>
						 <div class="col-sm-3">
									<select name="" id="Complejidad" style="width: 70%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT * FROM MasterEypo.dbo.Complejidades where Estatus = 'ALTA'  order by Complejidad";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['Complejidad']); ?>"> <?php echo ($row['Complejidad']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
												
					</div>
					
				
				</div>
			</div>
			<div class="row" style="font-size: .7rem">
			<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Techo Financiero:</label>
						<div class="col-sm-12">
							<textarea name="TechoFinanciero" id = "TechoFinanciero" cols="40" rows="5" style="width: 70%"></textarea>
						</div>
			</div>
			<div class="row" style="font-size: .7rem">
			
			<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Observaciones de Ventas:</label>
						<div class="col-sm-12">
							<textarea name="Observaciones" id = "Observaciones" cols="40" rows="5" style="width: 70%"></textarea>
						</div>
			</div>
			<div class="row" id="ObservacionesP" style="font-size: .7rem">
			<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Observaciones de Proyectos:</label>
						<div class="col-sm-12">
							<textarea disabled = "true" name="ObservacionesProyectos" id = "ObservacionesProyectos" cols="40" rows="5" style="width: 70%"></textarea>
						</div>
			</div>
			<br>
			<br>
			<hr>
				<div class="row datosProy" style="font-size: .7rem">
				<div class="col-md-6">
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Fecha Compromiso*:</label>
						<div class="col-sm-9">
							<input type="date" class="" id="FechaCompromiso" style="width: 70%">
						</div>
					</div>
					<div class="row">
						    <label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Proyectista:</label>
						    <div class="col-sm-3">
									<select name="" id="Proyectista" style="width: 70%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT iv.NombreEmpleadoVC+ ' ' + isnull(firstName,'') + ' ' + isnull(middleName,'') + ' ' + isnull(lastName,'') as Nombre, iv.* FROM IV_EY_PV_EmpleadosVentasCompras iv
left outer join OHEM e on iv.CodigoEmpleadoVC   = e.salesPrson where iv.NombreEmpleadoVC like 'P%'";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['NombreEmpleadoVC']); ?>"> <?php echo ($row['Nombre']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
							
							<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Coordinador:</label>
						    <div class="col-sm-3">
									<select name="" id="Coordinador" style="width: 70%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT iv.NombreEmpleadoVC+ ' ' + isnull(firstName,'') + ' ' + isnull(middleName,'') + ' ' + isnull(lastName,'') as Nombre, iv.* FROM IV_EY_PV_EmpleadosVentasCompras iv
left outer join OHEM e on iv.CodigoEmpleadoVC   = e.salesPrson where iv.NombreEmpleadoVC like 'P%'";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['NombreEmpleadoVC']); ?>"> <?php echo ($row['Nombre']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
							 <label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Proyectista Domótica:</label>
						    <div class="col-sm-3">
									<select name="" id="ProyectistaDom" style="width: 70%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT iv.NombreEmpleadoVC+ ' ' + isnull(firstName,'') + ' ' + isnull(middleName,'') + ' ' + isnull(lastName,'') as Nombre, iv.* FROM IV_EY_PV_EmpleadosVentasCompras iv
left outer join OHEM e on iv.CodigoEmpleadoVC   = e.salesPrson where iv.NombreEmpleadoVC like 'P%'";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['NombreEmpleadoVC']); ?>"> <?php echo ($row['Nombre']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
							
					</div>
					
					
					
						
					
				</div>
				<div class="col-md-6">
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Tipo de Proyecto*:</label>
						<div class="col-sm-4">
							<select name="" id="TipoProyecto" style="height: 23px; width: 50%">
								<option value="" disabled selected>Seleccione</option>
									<?php
								$sql = "SELECT * FROM MasterEypo.dbo.TiposProyectos where Estatus = 'ALTA'";
								$consulta = sqlsrv_query($conn, $sql);
								while ($row = sqlsrv_fetch_array($consulta)) { ?>
											<option value="<?php echo ($row['Tipo']); ?>"><?php echo ($row['Tipo']); ?></option>
									<?php 
							} ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label" >Siglas*:</label>
						<div class="col-sm-4">
							<input type="text" class="" id="Siglas"  maxlength="4" style="width: 70%; font-weight: bold;">
						</div>
					</div>
					<div class="row">
						
					</div>
					
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">No. de Proyecto*:</label>
						<div class="col-sm-4">
							<input type="text" class="" id="NoProyecto" style="width: 70%; font-weight: bold;">
						</div>
					</div>
					
							
				</div>
					
				
				</div>
			
			<br>
			
			
		<div class="row" id= btnFoot style="margin-bottom: 30px">
					<div class="col-md-12" align="center">
					<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnGuardar" title="TomarProyecto">GUARDAR</button>
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnCrear" title="TOMAR PROYECTO">TOMAR PROYECTO</button>
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnPerdido" title="CANCELAR" href="#" data-toggle="modal" data-target="#myModalPerdido">CANCELAR</button>	
					</div>
				
			</div>
		<?php include "footer.php"; ?>
	</body>
		<script>
		$("#btnPerder").on('click', function(){
				
				var Id = $('#cdoc').val();
				var observacionescostos = $('#observacionesp').val();
				var estatus = 'PERDIDO';
				
				
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterActualizaCostos.php",
					data:{
						
						Id: Id,
						observacionescostos: observacionescostos,
						estatus: estatus,
						
					},
					success: function(resp){
					    
							alert('El proyecto ha sido almacenado como PERDIDO');
						
						$("#MyModalPerdido").modal('hide');
					postForm('listaespera.php');
						
						
					}
					
				});
			});
			function consultanotificaciones(){
	
					//contador proyectos lista de espera
					if ("<?php echo $_SESSION['CodigoPosicion']?>" == '50'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'){
						var tipopropuesta = 'dom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'){
						var tipopropuesta = 'nodom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '52'){
						var tipopropuesta = 'crossn';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '53'){
						var tipopropuesta = 'crossd';
					}
					else
					{
						var tipopropuesta = 'normal';
					}
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'1', tipopropuesta:tipopropuesta},
					success: function(response){
						
					
                     }
				});
						//carga contador lista espera autorizacion coordinador proyectos
					if ("<?php echo $_SESSION['CodigoPosicion']?>" == '50'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'){
						var tipopropuesta = 'dom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'){
						var tipopropuesta = 'nodom';
					}
					else
					{
						var tipopropuesta = 'normal';
					}
				
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'2', tipopropuesta:tipopropuesta},
					success: function(response){
					
                     }
				});
				//Cargar contador autorizaciones ventas
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'6'},
					success: function(response){
						
                     }
				});
				//Cargar contador codigos por autorizar
					$.ajax({
					url: 'OFVConsultasMaster/buscadorcodigosporautorizar.php',
					type: 'post',
					success: function(response){
						
                     }
				});
				if ("<?php echo $_SESSION['CodigoPosicion']?>" == '50'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'){
						var tipopropuesta = 'dom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'){
						var tipopropuesta = 'nodom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '52'){
						var tipopropuesta = 'crossn';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '53'){
						var tipopropuesta = 'crossd';
					}
					else
					{
						var tipopropuesta = 'normal';
					}
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'3', tipopropuesta:tipopropuesta},
					success: function(response){
						
                     }
				});
			};
				
		    $("#Siglas").keyup(function(){
				var code = $('#Siglas').val();
				var TipoPropuesta = $('#tpropuesta').val();
							
										$.ajax({
											url: 'ofvConsultasMaster/buscacodigosiglas.php',
											type: 'post',
											data: {TipoPropuesta: TipoPropuesta},
											success: function(response){
												
												 var siglas = response;
												 
												 var revision = 'R0';
												
												 $("#NoProyecto").val(code+'-'+siglas+'-'+revision).prop("disabled", true);
												
											}
										});
			})
			$("#buscadorCliente").keyup(function(){
				var valorEscrito = $("#buscadorCliente").val();
				if (valorEscrito) {
					$.ajax({
						url: 'socios/buscadorConsultaClientesYN.php',
						type: 'post',
						data: {valor: valorEscrito},
						success: function(resp){

							
							$("#tblcliente tbody").empty();
							$("#tblcliente tbody").append(resp);

							$("#tblcliente tr").on('click',function(){


								if (!$('#BackOrderVentas').is(':visible')) { 
									if (!$('#OfertaVentaCliente').is(':visible')) { 
										if (!$('#OfertaClienteF').is(':visible')) { 
											var codigo = $(this).find('td').eq(1).text();
											var name = $(this).find('td').eq(2).text();							 
											agregar(codigo, name);
											$("#tblcliente tbody").empty();
											$("#buscadorCliente").val("");

											$.ajax({
												url:'rellenarSelectFormaPago.php',
												type:'POST',
												data:{codigo:codigo},

											}).done(function (response) {
												$("#formaPagoSelect").append(response);								
											});										 
										} else {
											var codigo = $(this).find('td').eq(1).text();
											$("#codigoClienteF").val(codigo);	
											$("#myModal").hide();
										}
									} else {
										var codigo = $(this).find('td').eq(1).text();
										$("#clienteProveedor").val(codigo);	
										$("#myModal").hide();
									}
								} else {
									var codigo = $(this).find('td').eq(1).text();
									var name = $(this).find('td').eq(2).text();	
									$("#nombreCliente").val(name);
									$("#codigoCliente").val(codigo);
									$("#myModal").hide();
								}																								
							});
						},
					});
				} else {
					$("#tblcliente tbody").empty();
				}
			});

			$("#myModal").on('click',function() {
				$("#buscadorCliente").val("");
			});

			

			$("#myModalArticulos").on('click',function() {
				$("#buscadorArticulo").val("");
				$("#tblarticulo tbody").empty();
			});
			
			
			function Siglas(){
				var code = $("#Siglas").val;
				var TipoPropuesta = $("#TipoPropuesta").val;
				
				$.ajax({
					url: 'ofvConsultasMaster/buscacodigosiglas.php',
					type: 'post',
					data: {TipoPropuesta: TipoPropuesta},
					success: function(response){
						
						 var siglas = response['CodigoSiglas'];
						 var revision = 'R0';
					
						 $("#NoProyecto").val(code+'-'+siglas+'-'+revision).prop("disabled", true);
						
					}
				});
			}

			
			function agregar ($code, $name){
				var code = $code;
				var name = $name;
				$("#codcliente").val(code);
				$("#NombreC").val(name);
				$('#myModal').modal('hide');
				$.ajax({
					url: 'consultaPersonaContacto.php',
					type: 'post',
					data: {code: code},
					success: function(response){
						$("#listcontactos").append(response);
					}
				});
			}

			

			
			

			
			
			
			function cargarAdjuntos (){
				
				var valor = $('#cdoc').val();
				
				$.ajax({
					url: 'OFVConsultasMaster/buscadorArchivosAdjuntos.php',
					type: 'post',
					data: {valor: valor},
					success: function(response){
						$("#Adjuntos tbody").empty();
						$("#Adjuntos tbody").append(response);
                     }
				});
				
				
					
			}
			 if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
		
			$("#btnAdjuntar").on('click', function(){
						var file_data = $('#file-upload').prop('files')[0];
						var idproyecto = $('#cdoc').val();
						var form_data = new FormData();                  
						form_data.append('file', file_data);
						form_data.append('idproyecto', idproyecto);
						
						$.ajax({
						url: 'OFVConsultasMaster/masteradjuntararchivo.php', 
						dataType: 'text', 
						cache: false,
						contentType: false,
						processData: false,
						data: form_data,                         
						type: 'post',
						success: function(php_script_response){
								$("#file-upload").val('');
								$.ajax({
									url: 'OFVConsultasMaster/buscadorArchivosAdjuntos.php',
									type: 'post',
									data: {valor: idproyecto},
									success: function(response){
										$("#Adjuntos tbody").empty();
										$("#Adjuntos tbody").append(response);
										 window.history.replaceState( null, null, window.location.href );
									 }
								});
							}
						 });
			});
			$(document).on('click', '#eliminarArchivo', function (event) {
				event.preventDefault();
				var id =  $(this).attr("alt");
				var idproyecto = $('#cdoc').val();
				$.ajax({
						url: 'OFVConsultasMaster/mastereliminaarchivo.php', 
						data: {valor: id},
						type: 'post',
						success: function(response){
								$("#file-upload").val('');
								$.ajax({
									url: 'OFVConsultasMaster/buscadorArchivosAdjuntos.php',
									type: 'post',
									data: {valor: idproyecto},
									success: function(response){
										alert(id);
										$("#Adjuntos tbody").empty();
										$("#Adjuntos tbody").append(response);
										 window.history.replaceState( null, null, window.location.href );
									 }
								});
							}
						 });
				
			});
			
			function postForm(path, params, method) {
				method = method || 'post';

				var form = document.createElement('form');
				form.setAttribute('method', method);
				form.setAttribute('action', path);

				for (var key in params) {
					if (params.hasOwnProperty(key)) {
						var hiddenField = document.createElement('input');
						hiddenField.setAttribute('type', 'hidden');
						hiddenField.setAttribute('name', key);
						hiddenField.setAttribute('value', params[key]);

						form.appendChild(hiddenField);
					}
				}

				document.body.appendChild(form);
				form.submit();
			}
			$("#btnGuardar").on('click', function(){
				
				var Id = $('#cdoc').val();
				var cliente = $('#codcliente').val();
				var nombre = $('#NombreC').val();
				var personaContacto = $('#Contacto').val();
				var telefono = $('#Telefono').val();
				var correo = $('#Correo').val();
				var tipopropuesta = $('#tpropuesta').val();
				var asesorventas = $('#AsesorV').val();
				var administradorventas = $('#AdminV').val();
				var prioridad = $('#Prioridad').val();
				var ventasdirectivas = $('#vDirectivas').val();	
				var observaciones =  $('#Observaciones').val();
				var NombreProyecto = $('#NombreProyecto').val();
				var Ubicacion = $('#Ubicacion').val();
				var FechaCompromiso = $('#FechaCompromiso').val();
				var Proyectista = $('#Proyectista').val();
				var ProyectistaDom = $('#ProyectistaDom').val();
				var Coordinador = $('#Coordinador').val();
				var TipoProyecto = $('#TipoProyecto').val();
				var Siglas = $('#Siglas').val();
				var NoProyecto = $('#NoProyecto').val();
				var Fechaventa = $('#FechaVenta').val();
				var Fechaentrega = $('#FechaEntrega').val();
				var Techo = $('#TechoFinanciero').val();
				var Observacionesproyectos = $('#ObservacionesProyectos').val();
			
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterasignaproyecto.php",
					data:{
						
						Id: Id,
						cliente: cliente,
						nombre: nombre,
						personaContacto: personaContacto,
						telefono: telefono,
						correo: correo,
						tipopropuesta: tipopropuesta,
						asesorventas: asesorventas,
						administradorventas: administradorventas,
						prioridad: prioridad,
						ventasdirectivas: ventasdirectivas,
						observaciones: observaciones,
						NombreProyecto: NombreProyecto,
						Ubicacion: Ubicacion,
						FechaCompromiso: FechaCompromiso,
						Proyectista: Proyectista,
						ProyectistaDom: ProyectistaDom,
						Coordinador: Coordinador,
						TipoProyecto: TipoProyecto,
						Siglas: Siglas,
						NoProyecto: NoProyecto,
						Estatus: 'ASIGNADO',
						Revision: '',
						Fechaventa: Fechaventa,
						Fechaentrega: Fechaentrega,
						Techo: Techo,
						Observacionesproyectos: Observacionesproyectos,
						Accion: 'GUARDAR',
						
					},
					success: function(resp){
						
						alert('Se guardo correctamente el proyecto');
							
						
					}
				});
				 postForm('listaespera.php')
			});
			$("#btnCrear").on('click', function(){
				var siglasc = $('#Siglas').val();
				
				if (siglasc.length ==4)
				{
				var Id = $('#cdoc').val();
				var cliente = $('#codcliente').val();
				var nombre = $('#NombreC').val();
				var personaContacto = $('#Contacto').val();
				var telefono = $('#Telefono').val();
				var correo = $('#Correo').val();
				var tipopropuesta = $('#tpropuesta').val();
				var asesorventas = $('#AsesorV').val();
				var administradorventas = $('#AdminV').val();
				var prioridad = $('#Prioridad').val();
				var ventasdirectivas = $('#vDirectivas').val();	
				var observaciones =  $('#Observaciones').val();
				var NombreProyecto = $('#NombreProyecto').val();
				var Ubicacion = $('#Ubicacion').val();
				var FechaCompromiso = $('#FechaCompromiso').val();
				var Proyectista = $('#Proyectista').val();
				var ProyectistaDom = $('#ProyectistaDom').val();
				var Coordinador = $('#Coordinador').val();
				var TipoProyecto = $('#TipoProyecto').val();
				var Siglas = $('#Siglas').val();
				var NoProyecto = $('#NoProyecto').val();
				var Fechaventa = $('#FechaVenta').val();
				var Fechaentrega = $('#FechaEntrega').val();
				var Techo = $('#TechoFinanciero').val();
				var Observacionesproyectos = $('#ObservacionesProyectos').val();
			
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterasignaproyecto.php",
					data:{
						
						Id: Id,
						cliente: cliente,
						nombre: nombre,
						personaContacto: personaContacto,
						telefono: telefono,
						correo: correo,
						tipopropuesta: tipopropuesta,
						asesorventas: asesorventas,
						administradorventas: administradorventas,
						prioridad: prioridad,
						ventasdirectivas: ventasdirectivas,
						observaciones: observaciones,
						NombreProyecto: NombreProyecto,
						Ubicacion: Ubicacion,
						FechaCompromiso: FechaCompromiso,
						Proyectista: Proyectista,
						ProyectistaDom: ProyectistaDom,
						Coordinador: Coordinador,
						TipoProyecto: TipoProyecto,
						Siglas: Siglas,
						NoProyecto: NoProyecto,
						Estatus: 'ASIGNADO',
						Revision: '',
						Fechaventa: Fechaventa,
						Fechaentrega: Fechaentrega,
						Techo: Techo,
						Observacionesproyectos: Observacionesproyectos,
						
					},
					success: function(resp){
						alert('Se asigno correctamente el proyecto');
					//	alert(resp);
						consultanotificaciones();
						postForm('listaespera.php')
						
					}
				});
				}
				else
				{
					alert("Debes colocar 4 caracteres en Siglas");
				}
				
			});

			

			

					function cargarProyectos(){
				var  fechaInicio = document.getElementById("FechaInicio").value;
				 var fechaFin = document.getElementById("FechaFin").value;
				 var valorescrito = $("#buscadorProyectos").val();
				 
				 if (valorescrito) {
				$.ajax({
					url: 'ofvConsultasMaster/buscadorGeneralAnteProyectos.php',
					type: 'post',
					data: {valor:valorescrito, fechainicio:fechaInicio, fechafin:fechaFin},
					success: function(response){
							
							$('#tblBuscarProyectos tbody').empty();
							$('#tblBuscarProyectos tbody').append(response);
							$("#tblBuscarProyectos tbody tr").on('click',function(){
								var codigo = $(this).find('td').eq(0).text();
								consultaOFV(codigo);
							
								$("#modalBuscarProyectos").modal('hide');
							});
                     }
					
				});
				 }else {
					$('#tblBuscarProyectos tbody').empty();
				}
				};
			$("#buscadorProyectos").keyup(function(){
				cargarProyectos();
			});
			$("#consulta1Atras").on('click', function(){
				var cdoc = $("#cdoc").val();
				cdoc = cdoc -1;
				consultaOFV(cdoc);
			});

			$("#consulta1Adelante").on('click', function(){
				var cdoc = $("#cdoc").val();
				cdoc = parseInt(cdoc) + 1;
				consultaOFV(cdoc);
			});

			function consultaOFV(folio) {
				var cdoc = folio;
												
								$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaAtrasAdelante.php', 
								dataType:'json',
								data:{ cdoc: cdoc },
								success: function(response){
									
									var estatus = response['Estatus'];
									
									if (estatus == 'ANTEPROYECTO')
									{
										
										$("#codcliente").val(response['Cliente']).prop("disabled", true);
										$("#NombreC").val(response['NombreCliente']).prop("disabled", true);
										$("#Contacto").val(response['Contacto']).prop("disabled", false);
									$("#Prioridad").val(response['Prioridad']);
									$("#Direccion").val(response['Direccion']).prop("disabled", false);
									$("#Telefono").val(response['Telefono']).prop("disabled", false);
									$("#Correo").val(response['Email']).prop("disabled", false);
									$("#AsesorV").val(response['AsesorVentas']).prop("disabled", false);
									$("#AdminV").val(response['AdminVentas']).prop("disabled", false);
									$("#cdoc").val(response['IdProyecto']).prop("disabled", true);
									$("#idproyecto").val(response['IdProyecto']).prop("disabled", true);
									$("#vDirectivas").val(response['VentasDirectivas']).prop("disabled", false);
									$("#tpropuesta").val(response['TipoPropuesta']).prop("disabled", false);
									$("#Observaciones").val(response['Observaciones']).prop("disabled", false);
									$("#NombreProyecto").val(response['NombreProyecto']).prop("disabled", false);
									$("#FechaVenta").val(response['FechaVenta']).prop("disabled", false);
									$("#FechaEntrega").val(response['FechaEntrega']).prop("disabled", false);
									$("#NombreProyecto").val(response['NombreProyecto']).prop("disabled", false);
									$("#Ubicacion").val(response['Ubicacion']).prop("disabled", false);
									$("#TechoFinanciero").val(response['TechoFinanciero']).prop("disabled", false);
									$("#ObservacionesProyectos").val(response['ObservacionesProyectos']).prop("disabled", false);
									$("#Estatus").val(response['Estatus']).prop("disabled", true);
										$("#NombreProyecto").val(response['NombreProyecto']);
										$("#Ubicacion").val(response['Ubicacion']);
										$("#FechaCompromiso").val(response['FechaCompromiso']);
										$("#Proyectista").val(response['Proyectista']);
										$("#ProyectistaDom").val(response['ProyectistaDom']);
										$("#Coordinador").val(response['Coordinador']);
										$("#Siglas").val(response['Siglas']);
										$("#TipoProyecto").val(response['TipoProyecto']);
										$("#FechaCompromiso").val(response['FechaCompromiso']);
										$("#NoProyecto").val(response['NoProyecto']).prop("disabled", true);
										$("#Complejidad").val(response['Complejidad']).prop("disabled", true);
										$("#btnCrear").prop("disabled", false);
										$("#btnCrear").show();
										if (("<?php echo $_SESSION['CodigoPosicion']?>" == '50'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51') && (response['TipoPropuesta'].indexOf('Dom') != -1)){
										$("#btnCrear").prop("disabled", false);
										$("#btnCrear").show();
										}else if (("<?php echo $_SESSION['CodigoPosicion']?>" == '51'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51') && (response['TipoPropuesta'].indexOf('Dom') == -1)){
										$("#btnCrear").prop("disabled", true);
										$("#btnCrear").hide();
										}
										if (("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49') && (response['TipoPropuesta'].indexOf('Dom') != -1)){
										$("#btnCrear").prop("disabled", true);
										$("#btnCrear").hide();
										}else if (("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49') && (response['TipoPropuesta'].indexOf('Dom') == -1)){
										$("#btnCrear").prop("disabled", false);
										$("#btnCrear").show();
										}
										if (("<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53') && (response['TipoPropuesta'].indexOf('Cross') != -1)){
										$("#btnCrear").prop("disabled", false);
										$("#btnCrear").show();
										}else if (("<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53') && (response['TipoPropuesta'].indexOf('Cross') == -1)){
										$("#btnCrear").prop("disabled", true);
										$("#btnCrear").hide();
										}
									}
									else
									{
										
									$("#codcliente").val(response['Cliente']).prop("disabled", true);
									$("#NombreC").val(response['NombreCliente']).prop("disabled", true);
									$("#Contacto").val(response['Contacto']).prop("disabled", true);
									$("#Prioridad").val(response['Prioridad']);
									$("#Direccion").val(response['Direccion']).prop("disabled", true);
									$("#Telefono").val(response['Telefono']).prop("disabled", true);
									$("#Correo").val(response['Email']).prop("disabled", true);
									$("#AsesorV").val(response['AsesorVentas']).prop("disabled", true);
									$("#AdminV").val(response['AdminVentas']).prop("disabled", true);
									$("#cdoc").val(response['IdProyecto']).prop("disabled", true);
									$("#idproyecto").val(response['IdProyecto']).prop("disabled", true);
									$("#vDirectivas").val(response['VentasDirectivas']).prop("disabled", true);
									$("#tpropuesta").val(response['TipoPropuesta']).prop("disabled", true);
									$("#Observaciones").val(response['Observaciones']).prop("disabled", true);
									$("#NombreProyecto").val(response['NombreProyecto']).prop("disabled", true);
									$("#FechaVenta").val(response['FechaVenta']).prop("disabled", true);
									$("#FechaEntrega").val(response['FechaEntrega']).prop("disabled", true);
									$("#NombreProyecto").val(response['NombreProyecto']).prop("disabled", true);
									$("#Ubicacion").val(response['Ubicacion']).prop("disabled", true);
									$("#TechoFinanciero").val(response['TechoFinanciero']).prop("disabled", true);
									$("#ObservacionesProyectos").val(response['ObservacionesProyectos']).prop("disabled", true);
									$("#Estatus").val(response['Estatus']).prop("disabled", true);
									$("#FechaCompromiso").val(response['FechaCompromiso']);
									$("#Proyectista").val(response['Proyectista']);
									$("#Coordinador").val(response['Coordinador']);
									$("#Siglas").val(response['Siglas']);
									$("#TipoProyecto").val(response['TipoProyecto']);
									$("#FechaCompromiso").val(response['FechaCompromiso']);
									$("#NoProyecto").val(response['NoProyecto']).prop("disabled", true);
									$("#Complejidad").val(response['Complejidad']).prop("disabled", true);
										$("#btnCrear").prop("disabled", true);
										$("#btnCrear").hide();
									}
										var revisionletra = response['Revision'];
									
								 if (response['Revision'] == ''){
								 
								 revisionletra = '0';
								 }
									else
									{
										var revisionentera = parseInt(response['Revision']);
										
										// $("#NoProyecto").val(response['NoProyecto']).prop("disabled", true);
										revisionletra = response['Revision'];
									}
									var valor = $('#cdoc').val();
										
										$.ajax({
											url: 'OFVConsultasMaster/buscadorArchivosAdjuntos.php',
											type: 'post',
											data: {valor: valor},
											success: function(response){
												$("#Adjuntos tbody").empty();
												$("#Adjuntos tbody").append(response);
											 }
										});
										
										var code = $('#Siglas').val();
										var TipoPropuesta = response['TipoPropuesta'];
										
										$.ajax({
											url: 'ofvConsultasMaster/buscacodigosiglas.php',
											type: 'post',
											data: {TipoPropuesta: TipoPropuesta},
											success: function(response){
												
												 var siglas = response;
												 var revision = 'R' + revisionletra;
												 // var revision = 'R0';
												
												 $("#NoProyecto").val(code+'-'+siglas+'-'+revision).prop("disabled", true);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
									

									$("#modalBuscarProyectos").modal('hide');
								},
								});
							
						

			}
		
			$("#consultaPrimerRegistro").on('click', function(){
				var usuario = "coordinador";
				var orden = "ASC";
				$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaPrimerUltimoRegistro.php',
								dataType:'json',
								data:{ orden: orden, usuario:usuario },
								success: function(response){
									
										consultaOFV(response['IdProyecto']);
								},
				});
				
				
			});
			$("#consultaUltimoRegistro").on('click', function consultaUltimoRegistro(){
				var usuario = "coordinador";
				var orden = "DESC"
				$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaPrimerUltimoRegistro.php', 
								dataType:'json',
								data:{ orden: orden, usuario:usuario },
								success: function(response){
										consultaOFV(response['IdProyecto']);
								},
				});
				consultaOFV(cdoc);
			});
			

			

			

			

			
			$("#btnPdf").on('click', function(){
				var cdoc = $("#cdoc").val();
				window.open('reportes/pdfOfv.php?folio='+cdoc, "nombre de la ventana", "width=1024, height=325");
			});

		

		
		
		
		
		
		
		
		</script>
	</html>
