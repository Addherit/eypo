<?php
include "../../../db/Utils.php";

$fechaContabilizacionSuperior = $_POST['fechaContabilizacionSuperior'];
$fechaContabilizacionMenor = $_POST['fechaContabilizacionMenor'];

$fecha1 = str_replace("-", "", $fechaContabilizacionSuperior);
$fecha2 = str_replace("-", "", $fechaContabilizacionMenor);
$num = 0;

$sql = "
        declare @fechaini as datetime
        declare @fechafin as datetime

        declare @param1 as datetime
        set @param1 = '$fecha1'

        declare @param2 as datetime
        set @param2 = '$fecha2'


        select @fechaini = t0.docdate, @fechafin = t1.docdate FROM OPDN T0, OPDN T1 WHERE t0.docdate = @param1 and t1.docdate = @param2

        SET @FechaIni = CONVERT(DATETIME, @param1, 112)
        SET @FechaFin = CONVERT(DATETIME, @param2, 112)

        if (@FechaFin is null or @FechaFin='' or @FechaFin=' ')
        begin
        SELECT T0.[DocNum],T0.[DocDate][Fecha],  T0.[CardName], T1.[ItemCode][Codigo Art.], T1.[Dscription][Articulo], T1.[U_Texto][Comentario Partida], T1.[Quantity][Cant.],T1.[BaseQty][Pedido], T1.[U_N_Pedido_Cliente][O.C. Cliente/Stock], T1.[U_Codigo_Cliente][Cliente]
        FROM [dbo].[OPDN]  T0
        LEFT JOIN [dbo].[PDN1]  T1 ON T0.DocEntry = T1.DocEntry
        WHERE T0.[DocDate] =@param1
        END
        else
        if (@FechaFin is not null or @FechaFin<>'' or @FechaFin<>' ')
        begin
        SELECT T0.[DocNum],T0.[DocDate][Fecha],  T0.[CardName], T1.[ItemCode][Codigo Art.], T1.[Dscription][Articulo], T1.[U_Texto][Comentario Partida], T1.[Quantity][Cant.],T1.[BaseQty][Pedido], T1.[U_N_Pedido_Cliente][O.C. Cliente/Stock], T1.[U_Codigo_Cliente][Cliente]
        FROM [dbo].[OPDN]  T0
        LEFT JOIN [dbo].[PDN1]  T1 ON T0.DocEntry = T1.DocEntry
        WHERE T0.[DocDate] >=@param1 and T0.[DocDate] <=@param2
        end
    ";
    $consulta = sqlsrv_query($conn, $sql);

    $response = [];
    while( $row = sqlsrv_fetch_array($consulta, SQLSRV_FETCH_ASSOC) ) {        
        $response[] = $row;
    }
    echo json_encode( $response );
?>
