<?php
    include "../../../db/Utils.php";

	$fechaSuperior = $_POST['fechaS'];
	$fechaMenor = $_POST['fechaM'];

	$fecha1 = str_replace("-", "", $fechaSuperior);
	$fecha2 = str_replace("-", "", $fechaMenor);
	$num = 0;

    $sql= "

       	declare @param1 as datetime
        set @param1 = '$fecha1'
        declare @param2 as datetime
        set @param2 = '$fecha2'

		Select Distinct
		TT.DocEntry DocEntryPago
		, T1.DocEntry DocEntryFac
			, T1.CardName
			,SUM( T2.LineTotal) 'Subtotal'
			,T1.VatSum 'IVA'
			,T1.DocTotal 'Total'
			,DATEDIFF(d,T1.DocDueDate,TT.DocDate) 'Dias Vencimiento'
			,T0.SumApplied-T0.vatApplied 'SubtotalPago'
			,T0.vatApplied 'IVA Pago'
			,T0.SumApplied 'Montodepago'
		,(T0.SumApplied /T1.DocTotal ) *100 '% del pago'
			,T1.DocTotal-T1.PaidToDate 'Saldo Pendiente'
			,T1.PaidToDate 'Pagado al dia'
		,(T1.PaidToDate/T1.DocTotal) * 100 '%pagadoaldia'
			,SUM(case When T3.QryGroup1 = 'Y' then T2.LineTotal else 0 end) as L1
			,SUM(case When T3.QryGroup2 = 'Y' then T2.LineTotal else 0 end) as L2
			,SUM(case When T3.QryGroup3 = 'Y' then T2.LineTotal else 0 end) as L3
			,SUM(case When T3.QryGroup4 = 'Y' then T2.LineTotal else 0 end) as L4
			,SUM(case When T3.QryGroup5 = 'Y' then T2.LineTotal else 0 end) as L5
			,SUM(case When T3.QryGroup6 = 'Y' then T2.LineTotal else 0 end) as L6
			,SUM(case When T3.QryGroup7 = 'Y' then T2.LineTotal else 0 end) as L7
			,SUM(case When T3.QryGroup8 = 'Y' then T2.LineTotal else 0 end) as L8
			,SUM(case When T3.QryGroup9 = 'Y' then T2.LineTotal else 0 end) as L9
			,SUM(case When T3.QryGroup10 = 'Y' then T2.LineTotal else 0 end) as L10
			--YO DEL FUTURO --El vendedor principal esta correcto por que el filtro de los vendedores se hace automatico en el JOIN con el SN y la OSLP. Así es como me trae un solo Empleado y esta bien por eso. NO LE MUEVAS.
			,T5.SlpName AS 'Vendedro Principal'
			,(SUM(Case When T3.QryGroup1 = 'Y' then t5.U_UN1 * T2.LineTotal
					When T3.QryGroup2 = 'Y' then t5.U_UN2 * T2.LineTotal
					When T3.QryGroup3 = 'Y' then t5.U_UN3 * T2.LineTotal
					When T3.QryGroup4 = 'Y' then t5.U_UN4 * T2.LineTotal
					When T3.QryGroup5 = 'Y' then t5.U_UN5 * T2.LineTotal
					When T3.QryGroup6 = 'Y' then t5.U_UN6 * T2.LineTotal
					When T3.QryGroup7 = 'Y' then t5.U_UN7 * T2.LineTotal
					When T3.QryGroup8 = 'Y' then t5.U_UN8 * T2.LineTotal
					When T3.QryGroup9 = 'Y' then t5.U_UN9 * T2.LineTotal
					When T3.QryGroup10 = 'Y' then t5.U_UN10 * T2.LineTotal
				else 0 end )*(T0.SumApplied /T1.DocTotal))/100'Comision Principal'


			,T1.U_EmpleadoAdi 'Vendedor Adicional'
			,(SUM(Case When T3.QryGroup1 = 'Y' then T6.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T6.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T6.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T6.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T6.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then t6.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T6.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T6.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T6.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T6.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Adicional'
			,T1.U_Promotor 'Promotor de ventas'
			,(SUM(Case When T3.QryGroup1 = 'Y' then T7.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T7.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T7.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T7.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T7.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T7.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T7.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T7.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T7.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T7.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Promotor'
				,T1.U_PromoVentas 'Coordinador de Ventas'
			,(SUM(Case When T3.QryGroup1 = 'Y' then T15.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T15.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T15.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T15.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T15.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T15.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T15.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T15.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T15.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T15.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Coordinador de Ventas'
			,T1.Project 'Proyecto'
			,T8.U_Proyectista 'Proyectista 1'
			,(SUM(Case When T3.QryGroup1 = 'Y' then T9.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T9.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T9.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T9.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T9.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T9.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T9.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T9.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T9.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T9.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Proytectista 1'
		,T8.U_Proy2 'Proyectista 2'
		,(SUM(Case When T3.QryGroup1 = 'Y' then T10.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T10.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T10.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T10.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T10.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T10.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T10.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T10.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T10.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T10.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Proytectista 2'
		,T8.U_Proy3 'Proyectista 3'
		,(SUM(Case When T3.QryGroup1 = 'Y' then T11.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T11.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T11.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T11.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T11.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T11.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T11.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T11.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T11.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T11.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Proytectista 3'
			,T8.U_Proy4 'Proyectista 4'
		,(SUM(Case When T3.QryGroup1 = 'Y' then T12.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T12.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T12.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T12.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T12.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T12.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T12.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T12.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T12.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T12.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Proytectista 4'
			,T8.U_Proy5 'Proyectista 5'
		,(SUM(Case When T3.QryGroup1 = 'Y' then T13.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T13.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T13.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T13.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T13.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T13.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T13.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T13.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T13.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T13.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Proytectista 5'
			,T8.U_Proy6 'Coordinador de proyecto'
		,(SUM(Case When T3.QryGroup1 = 'Y' then T14.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T14.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T14.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T14.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T11.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T14.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T14.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T14.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T14.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T14.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision coordinador de Proyecto'

		from
		ORCT TT
		inner join RCT2 T0 ON TT.DocEntry = T0.DocNum
		Inner join OINV T1 ON T0.DocEntry = T1.DocEntry
		inner join INV1 T2 ON T1.DocEntry = T2.DocEntry
		--vendedor principal
		INNER JOIN OITM T3 ON T2.ItemCode = T3.ItemCode
		inner join OCRD T4 ON T1.CardCode = T4.CardCode
		left join OSLP T5 on T4.SlpCode = T5.SlpCode
		--vendedor adicional
		left join OSLP T6 ON T1.U_EmpleadoAdi = T6.SlpName
		--Promotor
		left join OSLP T7 ON T1.U_Promotor = T7.SlpName
		--Promotor de ventas
		LEFT join OSLP T15 ON T1.U_PromoVentas = T15.SlpName
		--Proyecto1
		left join OPRJ T8 ON T1.Project = T8.PrjCode
		left join OSLP T9 ON T8.U_Proyectista = T9.SlpName
		--Proyecto2
		left join OSLP T10 ON T8.U_Proy2 = T10.SlpName
		--Proyecto3
		left join OSLP T11 ON T8.U_Proy3 = T11.SlpName
		--Proyecto4
		left join OSLP T12 ON T8.U_Proy4 = T12.SlpName
		--Proyecto5
		left join OSLP T13 ON T8.U_Proy5 = T13.SlpName
		--Proyecto6
		left join OSLP T14 ON T8.U_Proy6 = T14.SlpName

		Where

		TT.DocDate >= @param1
		AND
		TT.DocDate <= @param2
		AND
		DATEDIFF(d,T1.DocDueDate,TT.DocDate)<181
		AND T1.U_Cancelacion = '1'

		--TT.DocDate >= '20180601'
		--AND
		--TT.DocDate <= '20180626'
		--AND
		--DATEDIFF(d,T1.DocDueDate,TT.DocDate)<181


		Group by TT.DocEntry, T1.DocEntry , T1.CardName, T5.SlpName,T1.U_EmpleadoAdi,T1.U_Promotor,T1.Project
		,T8.U_Proyectista, T8.U_Proy2,T1.PaidToDate,T1.DocDueDate,TT.DocDate,T1.DocTotal,T1.VatSum ,T8.U_Proy3
		,T8.U_Proy4,T8.U_Proy5,T8.U_Proy6,T1.U_PromoVentas,T0.SumApplied,T0.SumApplied-T0.vatApplied,(T0.SumApplied /TT.DocTotal )*100
		,T0.vatApplied
		--order by DocEntryPago, DocEntryFac

		---- Facturas Anticipo
		UNION ALL

		Select Distinct
		TT.DocEntry DocEntryPago
		, T1.DocEntry DocEntryFac
			, T1.CardName
			,SUM( T2.LineTotal) 'Subtotal'
			,T1.VatSum 'IVA'
			,T1.DocTotal 'Total'
			,DATEDIFF(d,T1.DocDueDate,TT.DocDate) 'Dias Vencimiento'
			,T0.SumApplied-T0.vatApplied 'Subtotal Pago'
			,T0.vatApplied 'IVA Pago'
			,T0.SumApplied 'Montodepago'
		,(T0.SumApplied /T1.DocTotal ) *100 '% del pago'
			,T1.DocTotal-T1.PaidToDate 'Saldo Pendiente'
			,T1.PaidToDate 'Pagado al dia'
		,(T1.PaidToDate/T1.DocTotal) * 100 '%pagadoaldia'
			,SUM(case When T3.QryGroup1 = 'Y' then T2.LineTotal else 0 end) as L1
			,SUM(case When T3.QryGroup2 = 'Y' then T2.LineTotal else 0 end) as L2
			,SUM(case When T3.QryGroup3 = 'Y' then T2.LineTotal else 0 end) as L3
			,SUM(case When T3.QryGroup4 = 'Y' then T2.LineTotal else 0 end) as L4
			,SUM(case When T3.QryGroup5 = 'Y' then T2.LineTotal else 0 end) as L5
			,SUM(case When T3.QryGroup6 = 'Y' then T2.LineTotal else 0 end) as L6
			,SUM(case When T3.QryGroup7 = 'Y' then T2.LineTotal else 0 end) as L7
			,SUM(case When T3.QryGroup8 = 'Y' then T2.LineTotal else 0 end) as L8
			,SUM(case When T3.QryGroup9 = 'Y' then T2.LineTotal else 0 end) as L9
			,SUM(case When T3.QryGroup10 = 'Y' then T2.LineTotal else 0 end) as L10
			--YO DEL FUTURO --El vendedor principal esta correcto por que el filtro de los vendedores se hace automatico en el JOIN con el SN y la OSLP. Así es como me trae un solo Empleado y esta bien por eso. NO LE MUEVAS.
			,T5.SlpName AS 'Vendedro Principal'
			,(SUM(Case When T3.QryGroup1 = 'Y' then t5.U_UN1 * T2.LineTotal
					When T3.QryGroup2 = 'Y' then t5.U_UN2 * T2.LineTotal
					When T3.QryGroup3 = 'Y' then t5.U_UN3 * T2.LineTotal
					When T3.QryGroup4 = 'Y' then t5.U_UN4 * T2.LineTotal
					When T3.QryGroup5 = 'Y' then t5.U_UN5 * T2.LineTotal
					When T3.QryGroup6 = 'Y' then t5.U_UN6 * T2.LineTotal
					When T3.QryGroup7 = 'Y' then t5.U_UN7 * T2.LineTotal
					When T3.QryGroup8 = 'Y' then t5.U_UN8 * T2.LineTotal
					When T3.QryGroup9 = 'Y' then t5.U_UN9 * T2.LineTotal
					When T3.QryGroup10 = 'Y' then t5.U_UN10 * T2.LineTotal
				else 0 end )*(T0.SumApplied /T1.DocTotal))/100'Comision Principal'


			,T1.U_EmpleadoAdi 'Vendedor Adicional'
			,(SUM(Case When T3.QryGroup1 = 'Y' then T6.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T6.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T6.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T6.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T6.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then t6.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T6.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T6.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T6.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T6.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Adicional'
			,T1.U_Promotor 'Promotor de ventas'
			,(SUM(Case When T3.QryGroup1 = 'Y' then T7.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T7.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T7.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T7.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T7.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T7.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T7.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T7.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T7.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T7.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Promotor'
				,T1.U_PromoVentas 'Coordinador de Ventas'
			,(SUM(Case When T3.QryGroup1 = 'Y' then T15.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T15.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T15.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T15.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T15.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T15.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T15.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T15.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T15.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T15.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Coordinador de Ventas'
			,T1.Project 'Proyecto'
			,T8.U_Proyectista 'Proyectista 1'
			,(SUM(Case When T3.QryGroup1 = 'Y' then T9.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T9.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T9.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T9.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T9.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T9.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T9.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T9.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T9.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T9.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Proytectista 1'
		,T8.U_Proy2 'Proyectista 2'
		,(SUM(Case When T3.QryGroup1 = 'Y' then T10.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T10.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T10.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T10.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T10.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T10.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T10.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T10.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T10.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T10.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Proytectista 2'
		,T8.U_Proy3 'Proyectista 3'
		,(SUM(Case When T3.QryGroup1 = 'Y' then T11.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T11.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T11.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T11.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T11.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T11.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T11.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T11.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T11.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T11.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Proytectista 3'
			,T8.U_Proy4 'Proyectista 4'
		,(SUM(Case When T3.QryGroup1 = 'Y' then T12.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T12.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T12.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T12.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T12.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T12.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T12.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T12.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T12.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T12.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Proytectista 4'
			,T8.U_Proy5 'Proyectista 5'
		,(SUM(Case When T3.QryGroup1 = 'Y' then T13.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T13.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T13.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T13.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T13.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T13.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T13.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T13.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T13.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T13.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision Proytectista 5'
			,T8.U_Proy6 'Coordinador de proyecto'
		,(SUM(Case When T3.QryGroup1 = 'Y' then T14.U_UN1 * T2.LineTotal
				When T3.QryGroup2 = 'Y' then T14.U_UN2 * T2.LineTotal
				When T3.QryGroup3 = 'Y' then T14.U_UN3 * T2.LineTotal
				When T3.QryGroup4 = 'Y' then T14.U_UN4 * T2.LineTotal
				When T3.QryGroup5 = 'Y' then T11.U_UN5 * T2.LineTotal
				When T3.QryGroup6 = 'Y' then T14.U_UN6 * T2.LineTotal
				When T3.QryGroup7 = 'Y' then T14.U_UN7 * T2.LineTotal
				When T3.QryGroup8 = 'Y' then T14.U_UN8 * T2.LineTotal
				When T3.QryGroup9 = 'Y' then T14.U_UN9 * T2.LineTotal
				When T3.QryGroup10 = 'Y' then T14.U_UN10 * T2.LineTotal
			else 0 end)*(T0.SumApplied /T1.DocTotal))/100 'Comision coordinador de Proyecto'

		from
		ORCT TT
		inner join RCT2 T0 ON TT.DocEntry = T0.DocNum
		Inner join ODPI T1 ON T0.DocEntry = T1.DocEntry
		inner join DPI1 T2 ON T1.DocEntry = T2.DocEntry
		--vendedor principal
		INNER JOIN OITM T3 ON T2.ItemCode = T3.ItemCode
		inner join OCRD T4 ON T1.CardCode = T4.CardCode
		left join OSLP T5 on T4.SlpCode = T5.SlpCode
		--vendedor adicional
		left join OSLP T6 ON T1.U_EmpleadoAdi = T6.SlpName
		--Promotor
		left join OSLP T7 ON T1.U_Promotor = T7.SlpName
		--Promotor de ventas
		LEFT join OSLP T15 ON T1.U_PromoVentas = T15.SlpName
		--Proyecto1
		left join OPRJ T8 ON T1.Project = T8.PrjCode
		left join OSLP T9 ON T8.U_Proyectista = T9.SlpName
		--Proyecto2
		left join OSLP T10 ON T8.U_Proy2 = T10.SlpName
		--Proyecto3
		left join OSLP T11 ON T8.U_Proy3 = T11.SlpName
		--Proyecto4
		left join OSLP T12 ON T8.U_Proy4 = T12.SlpName
		--Proyecto5
		left join OSLP T13 ON T8.U_Proy5 = T13.SlpName
		--Proyecto6
		left join OSLP T14 ON T8.U_Proy6 = T14.SlpName

		Where

		TT.DocDate >= @param1
		AND
		TT.DocDate <=@param2
		AND
		DATEDIFF(d,T1.DocDueDate,TT.DocDate)<181
		AND T1.U_Cancelacion = '1'

		--TT.DocDate >= '20180601'
		--AND
		--TT.DocDate <= '20180626'
		--AND
		--DATEDIFF(d,T1.DocDueDate,TT.DocDate)<181


		Group by TT.DocEntry, T1.DocEntry , T1.CardName, T5.SlpName,T1.U_EmpleadoAdi,T1.U_Promotor,T1.Project
		,T8.U_Proyectista, T8.U_Proy2,T1.PaidToDate,T1.DocDueDate,TT.DocDate,T1.DocTotal,T1.VatSum ,T8.U_Proy3
		,T8.U_Proy4,T8.U_Proy5,T8.U_Proy6,T1.U_PromoVentas,T0.SumApplied,T0.SumApplied-T0.vatApplied,(T0.SumApplied /TT.DocTotal )*100
		,T0.vatApplied
		order by DocEntryPago, DocEntryFac

		---- Facturas Anticipo
    ";
    $consulta = sqlsrv_query($conn, $sql);
    $response = [];
	while( $row = sqlsrv_fetch_array($consulta, SQLSRV_FETCH_ASSOC) ) {        
		$response[] = $row;
	}
	echo json_encode( $response );
?>
