	<?php
	
	include "conexion.php";
	session_start();
	$_SESSION['usuario'];
	
	If ($_POST['valor'] != '')
	{
	$_SESSION['siglas'] = $_POST['valor'];
	}
	If ($_POST['id'] != '')
	{
	$_SESSION['id'] = $_POST['id'];
	}
	else
	{
		$_SESSION['id'] = 0;
	}
	If ($_POST['aut'] != '')
	{
	$_SESSION['aut'] = $_POST['aut'];
	}
	else
	{
		$_SESSION['aut'] = 0;
	}
	 $folio = $_SESSION['id'];
	 $siglas = $_SESSION['siglas'];
	 $aut = $_SESSION['aut'];
	
	?>

	<!DOCTYPE html>
	<html>
		<?php include "header.php"; ?>
		
		
		<body onload="consultaOFV(<?php echo $folio;?>);">
		
		<?php include "nav.php"; ?>
		
		<?php include "modales.php"; ?>
		
		<div class="container" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Codigo Nuevo:</h3>
				</div>
				
			</div>
			<div class="row">
							<style>
		@media only screen and (max-width: 800px) {
        /* Force table to not be like tables anymore */
        #no-more-tables table,
        #no-more-tables thead,
        #no-more-tables tbody,
        #no-more-tables th,
        #no-more-tables td,
        #no-more-tables tr {
        display: block;
        }
         
        /* Hide table headers (but not display: none;, for accessibility) */
        #no-more-tables thead tr {
        position: absolute;
        top: -9999px;
        left: -9999px;
        }
         
        #no-more-tables tr { border: 1px solid #ccc; }
          
        #no-more-tables td {
        /* Behave like a "row" */
        border: none;
        border-bottom: 1px solid #eee;
        position: relative;
        padding-left: 50%;
        white-space: normal;
        text-align:left;
        }
         
        #no-more-tables td:before {
        /* Now like a table header */
        position: absolute;
        /* Top/left values mimic padding */
        top: 6px;
        left: 6px;
        width: 45%;
        padding-right: 10px;
        white-space: nowrap;
        text-align:left;
        font-weight: bold;
        }
         
        /*
        Label the data
        */
        #no-more-tables td:before { content: attr(data-title); }
        }
		</style>
						<div class="col-md-12">
					  		 <div id="no-more-tables">
					  			<table class="table col-sm-12 table-bordered table-striped table-condensed cf" width="100%" id="Codigos"  style="font-size: .6rem" >
					        		<thead class="cf">
					        			<tr class="encabezado" >
											<th>Marca</th>
											<th>Modelo</th>
											<th>Codigo Real</th>
					        				<th>Codigo Protegido</th>
											<th>Descripcion Real</th>
					        				<th>Descripcion Protegida</th>
											<th>Tipo</th>
					        				<th>Potencia</th>
											<th>Optica</th>
										</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
													
											
											 <td data-title="Marca:" class = "marca">
											<select onchange="ArmaCodigoProtegido()" id="marca">        
													<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT * FROM MasterEypo.dbo.Marcas order by Id";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['CodigoMarca']); ?>"><?php echo ($row['CodigoMarca']); ?>-<?php echo ($row['Marca']); ?> </option>
												<?php } ?>

											</select>
											<td data-title="Modelo:"  class="Modelo">
											<div>
												<input type="text" class="" id="Modelo">
											</div>
											</td> 
								            <td data-title="Codigo Real:"  class="CodigoReal">
											<div>
												<input type="text" class="" id="CodigoReal"><a href="#" data-toggle="modal" data-target="#myModalArticulos" >
														<i class="fas fa-search" style="color: #57b4ea" title="Búsqueda Articulo"></i>
													</a>
											</div>
											</td>
											<td data-title="Codigo Protegido:"  class="CodigoProtegido">
											
											<div>
												
												<input type="text" class="" id="iniciocodigo" style="display:none">
												<input type="text" class="" id="CodigoProtegido" pattern="[0-9]{3}-X{4}-[a-z0-9._%+-]{3}-[a-z0-9._%+-]{3}">
											</div>
											</td>
								            <td data-title="Descripcion Real:"  class="DescripcionReal">
											<div>
												<textarea name="DescripcionReal" id = "DescripcionReal" cols="15" rows="3"></textarea>
											</div>
											
											</td>
											<td data-title="Descripcion Protegida:"  class="DescripcionProtegida">
											<div>
												<textarea name="DescripcionProtegida" id = "DescripcionProtegida" cols="15" rows="3"></textarea>
											</div>
											</td>
											<td data-title="Tipo:"  class="Tipo">
											<div>
												<input type="text" class="" id="Tipo">
											</div>
											</td>
											<td data-title="Potencia:"  class="Potencia">
											<div>
												<input type="text" class="" id="Potencia">
											</div>
											<td data-title="Optica:"  class="Optica">
											<div>
												<input type="text" class="" id="Optica">
											</div>
											</td>
										</tr>
				            		</tbody>
				        		</table>
					  		</div>
						</div>
			</div>
			
			<div class="row" style="font-size: .7rem">
				
						<div class="col-sm-6">
						<label for="" class="col-md-6 text-align:left">Observaciones:</label>
							<textarea name="Observaciones" id = "Observaciones" cols="40" rows="5" style="width: 70%"></textarea>
							<input type="text" class="" id="Estatus" style="display:none">
						</div>
						<label for="" class="col-sm-2 col-form-label table-hover table-striped table-responsive table">Archivos:</label>
					<table class="col-sm-2" id="Adjuntos" style="border-spacing:5px; border-collapse: separate;">
						  <tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						  </tr>
						  <tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						  </tr>
					</table>
					<div class="col-md-6 text-Right" style="margin-top: 10px ">
					<?php
					If ($folio == '0')
						{
						$consultasql = sqlsrv_query($conn, "SELECT TOP 1 Id  FROM MasterEypo.dbo.CodigosProtegidos ORDER BY Id DESC");
						// $consultasql2 = sqlsrv_query($conn, "SELECT TOP 1 FolioSAP  FROM EYPO.dbo.IV_EY_PV_OfertasVentasCab ORDER BY FolioSAP DESC");
						$Row = sqlsrv_fetch_array($consultasql);
						// $Row2 = sqlsrv_fetch_array($consultasql2);
						$folio = $Row['Id'];
						// $folioSAP = $Row2['FolioSAP'];
						$folio++;
						}
						// if($folio >= $folioSAP){
							// $folio++;
						// } else {
							// $folio = $folioSAP;
							// $folio++;
						// }
						?>
							<input type="text" class="" id="idCodigo" name="idCodigo" size="5" value="<?php echo "$folio"; ?>" readonly="true" style="display:none">
					</div>
					<div class="col-md-6 text-Right" style="margin-top: 10px ">
					
							<!--<form id="Adjuntos" action = "" method = "POST" enctype = "multipart/form-data">-->
								<input type="text" class="" id="idproyecto" name="idproyecto" size="5" value="<?php echo "$folio"; ?>" readonly="true" style="display:none">
								<label for="file-upload" class="custom-file-upload">
								<input type = "file" onchange='cambiar()' name = "file" id="file-upload" class="file-upload"/>
								
								  </label>
								  <br>
								  <!--<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnAdjuntar" title="Adjuntar">Adjuntar Archivo</button>-->
								<!--  <input type = "submit" value="Adjuntar Archivo" class="btn"/>-->
								
								
								<div id="info"></div>
								 
								

								 
			
							<!--</form>-->
					</div>
							
					
					
				
				</div>
			<br>
			
			
<div class="row" id= btnFoot style="margin-bottom: 30px">
				<div class="col-md-12" align="center">
					<?php If($aut==1){
					?>
					<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnAutorizar" title="Autorizar">AUTORIZAR CODIGO</button>	
					<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnEliminar" title="Eliminar">ELIMINAR CODIGO</button>	
					<?php
					}else{?>
					<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnCrear" title="Crear">SOLICITAR ALTA</button>
					<?php }?>
					</div>
				
			</div>
		<?php include "footer.php"; ?>
	</body>
		<script>
		$(document).on('keydown', function (e) {
  if (e.keyCode == 8 && $('#CodigoProtegido').is(":focus") && $('#CodigoProtegido').val().length < 10) {
      e.preventDefault();
  }
});
$(document).ready(function() {
  $("#CodigoProtegido").on("keyup", function() {
    var value = $(this).val();
	
	
    
	if ($('#CodigoProtegido').val().length == 12)
	{
		
		$(this).val(value + '-');
	}
	else
	{
		$(this).val($("#iniciocodigo").val() + value.substring(9));
	}
	
  });
});
		function consultanotificaciones(){
	
					//contador proyectos lista de espera
					if ("<?php echo $_SESSION['CodigoPosicion']?>" == '50'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'){
						var tipopropuesta = 'dom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'){
						var tipopropuesta = 'nodom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '52'){
						var tipopropuesta = 'crossn';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '53'){
						var tipopropuesta = 'crossd';
					}
					else
					{
						var tipopropuesta = 'normal';
					}
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'1', tipopropuesta:tipopropuesta},
					success: function(response){
						
					
                     }
				});
						//carga contador lista espera autorizacion coordinador proyectos
					if ("<?php echo $_SESSION['CodigoPosicion']?>" == '50'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'){
						var tipopropuesta = 'dom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'){
						var tipopropuesta = 'nodom';
					}
					else
					{
						var tipopropuesta = 'normal';
					}
				
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'2', tipopropuesta:tipopropuesta},
					success: function(response){
					
                     }
				});
				//Cargar contador autorizaciones ventas
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'6'},
					success: function(response){
						
                     }
				});
				//Cargar contador codigos por autorizar
					$.ajax({
					url: 'OFVConsultasMaster/buscadorcodigosporautorizar.php',
					type: 'post',
					success: function(response){
						//alert(response);
                     }
				});
				if ("<?php echo $_SESSION['CodigoPosicion']?>" == '50'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'){
						var tipopropuesta = 'dom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'){
						var tipopropuesta = 'nodom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '52'){
						var tipopropuesta = 'crossn';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '53'){
						var tipopropuesta = 'crossd';
					}
					else
					{
						var tipopropuesta = 'normal';
					}
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'3', tipopropuesta:tipopropuesta},
					success: function(response){
						
                     }
				});
			};
			function ArmaCodigoProtegido()
			{
				var Marca = $('#marca').val();
				var siglas = '<?php echo $siglas?>';
				$("#iniciocodigo").text(Marca+'-XXXX-');
				$("#iniciocodigo").val(Marca+'-XXXX-');
				$("#CodigoProtegido").val(Marca+'-XXXX-XXX-XXX');
				
			}
				
			$("#buscadorCliente").keyup(function(){
				var valorEscrito = $("#buscadorCliente").val();
				if (valorEscrito) {
					$.ajax({
						url: 'buscadorConsultaCliente.php',
						type: 'post',
						data: {valor: valorEscrito},
						success: function(resp){

							
							$("#tblcliente tbody").empty();
							$("#tblcliente tbody").append(resp);

							$("#tblcliente tr").on('click',function(){


								if (!$('#BackOrderVentas').is(':visible')) { 
									if (!$('#OfertaVentaCliente').is(':visible')) { 
										if (!$('#OfertaClienteF').is(':visible')) { 
											var codigo = $(this).find('td').eq(1).text();
											var name = $(this).find('td').eq(2).text();							 
											agregar(codigo, name);
											$("#tblcliente tbody").empty();
											$("#buscadorCliente").val("");

											$.ajax({
												url:'rellenarSelectFormaPago.php',
												type:'POST',
												data:{codigo:codigo},

											}).done(function (response) {
												$("#formaPagoSelect").append(response);								
											});										 
										} else {
											var codigo = $(this).find('td').eq(1).text();
											$("#codigoClienteF").val(codigo);	
											$("#myModal").hide();
										}
									} else {
										var codigo = $(this).find('td').eq(1).text();
										$("#clienteProveedor").val(codigo);	
										$("#myModal").hide();
									}
								} else {
									var codigo = $(this).find('td').eq(1).text();
									var name = $(this).find('td').eq(2).text();	
									$("#nombreCliente").val(name);
									$("#codigoCliente").val(codigo);
									$("#myModal").hide();
								}																								
							});
						},
					});
				} else {
					$("#tblcliente tbody").empty();
				}
			});

			$("#myModal").on('click',function() {
				$("#buscadorCliente").val("");
			});

		

			function agregar ($code, $name){
				var code = $code;
				var name = $name;
				$("#codcliente").val(code);
				$("#NombreC").val(name);
				$('#myModal').modal('hide');
				$.ajax({
					url: 'consultaPersonaContacto.php',
					type: 'post',
					data: {code: code},
					success: function(response){
						$("#listcontactos").append(response);
					}
				});
			}

					function cargarAdjuntos (){
				var valor = $('#idCodigo').val();
				$.ajax({
					url: 'OFVConsultasMaster/buscadorArchivosAdjuntosCodigos.php',
					type: 'post',
					data: {valor: valor},
					success: function(response){
						$("#Adjuntos tbody").empty();
						$("#Adjuntos tbody").append(response);
                     }
				});
				
				
					
			}
			 if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
			
			$("#btnAdjuntar").on('click', function(){
						var file_data = $('#file-upload').prop('files')[0];
						var idproyecto = $('#idCodigo').val();
						var form_data = new FormData();                  
						form_data.append('file', file_data);
						form_data.append('idproyecto', idproyecto);
						
						$.ajax({
						url: 'OFVConsultasMaster/masteradjuntararchivocodigos.php', // point to server-side PHP script 
						dataType: 'text',  // what to expect back from the PHP script, if anything
						cache: false,
						contentType: false,
						processData: false,
						data: form_data,                         
						type: 'post',
						success: function(php_script_response){
								$("#file-upload").val('');
								$.ajax({
									url: 'OFVConsultasMaster/buscadorArchivosAdjuntosCodigos.php',
									type: 'post',
									data: {valor: idproyecto},
									success: function(response){
										$("#Adjuntos tbody").empty();
										$("#Adjuntos tbody").append(response);
										 window.history.replaceState( null, null, window.location.href );
									 }
								});
							}
						 });
			});
			$(document).on('click', '#eliminarArchivo', function (event) {
				event.preventDefault();
				var id =  $(this).attr("alt");
				var idproyecto = $('#cdoc').val();
				$.ajax({
						url: 'OFVConsultasMaster/mastereliminaarchivocodigo.php', // point to server-side PHP script 
						data: {valor: id},
						type: 'post',
						success: function(response){
								$("#file-upload").val('');
								$.ajax({
									url: 'OFVConsultasMaster/buscadorArchivosAdjuntosCodigos.php',
									type: 'post',
									data: {valor: idproyecto},
									success: function(response){
										alert(id);
										$("#Adjuntos tbody").empty();
										$("#Adjuntos tbody").append(response);
										 window.history.replaceState( null, null, window.location.href );
									 }
								});
							}
						 });
				
			});
			$("#btnCrear").on('click', function(){
				if ($('#CodigoProtegido').val().length != 16)
				{
					alert("El codigo protegido debe respetar este formato " + $("#iniciocodigo").val() +'XXX-XXX');
				}
				else
				{
				var id = $('#idCodigo').val();
				var marca = $('#marca option:selected').text();
				var modelo = $('#Modelo').val();
				var codigoreal = $('#CodigoReal').val();
				var codigoprotegido = $('#CodigoProtegido').val();
				var descripcionreal = $('#DescripcionReal').val();
				var descripcionprotegida = $('#DescripcionProtegida').val();
				var tipo = $('#Tipo').val();
				var potencia = $('#Potencia').val();
				var optica = $('#Optica').val();
				var observaciones = $('#Observaciones').val();
				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";
				var siglas = "<?php echo $_SESSION['siglas']?>";
				var estatus = "PORAUTORIZAR"
				var file_data = $('#file-upload').prop('files')[0];
						var idproyecto = $('#idCodigo').val();
						var form_data = new FormData();                  
						form_data.append('file', file_data);
						form_data.append('idproyecto', idproyecto);
						
						$.ajax({
						url: 'OFVConsultasMaster/masteradjuntararchivocodigos.php', // point to server-side PHP script 
						dataType: 'text',  // what to expect back from the PHP script, if anything
						cache: false,
						contentType: false,
						processData: false,
						data: form_data,                         
						type: 'post',
						success: function(php_script_response){
								$("#file-upload").val('');
								$.ajax({
									url: 'OFVConsultasMaster/buscadorArchivosAdjuntosCodigos.php',
									type: 'post',
									data: {valor: idproyecto},
									success: function(response){
										$("#Adjuntos tbody").empty();
										$("#Adjuntos tbody").append(response);
										 window.history.replaceState( null, null, window.location.href );
									 }
								});
							}
						 });
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterinsertacodigo.php",
					data:{
						id: id,
						marca: marca,
						modelo: modelo,
						codigoreal: codigoreal,
						codigoprotegido: codigoprotegido,
						descripcionreal: descripcionreal,
						descripcionprotegida: descripcionprotegida,
						tipo: tipo,
						potencia: potencia,
						optica: optica,
						observaciones: observaciones,
						usuariocreacion: usuariocreacion,
						siglas: siglas,
						estatus: estatus,
					},
					success: function(resp){
						alert('El codigo ha sido enviado a autorización');
						consultanotificaciones();
						postForm('opcioncodigos.php');
						
					}
				});
				}
			});
			function postForm(path, params, method) {
				method = method || 'post';

				var form = document.createElement('form');
				form.setAttribute('method', method);
				form.setAttribute('action', path);

				for (var key in params) {
					if (params.hasOwnProperty(key)) {
						var hiddenField = document.createElement('input');
						hiddenField.setAttribute('type', 'hidden');
						hiddenField.setAttribute('name', key);
						hiddenField.setAttribute('value', params[key]);

						form.appendChild(hiddenField);
					}
				}

				document.body.appendChild(form);
				form.submit();
			}
			$("#btnAutorizar").on('click', function(){
				if ($('#CodigoProtegido').val().length != 16)
				{
					alert("El codigo protegido debe respetar este formato " + $("#iniciocodigo").val() +'XXX-XXX');
				}
				else
				{
				var id = $('#idCodigo').val();
				var marca = $('#marca option:selected').text();
				var modelo = $('#Modelo').val();
				var codigoreal = $('#CodigoReal').val();
				var codigoprotegido = $('#CodigoProtegido').val();
				var descripcionreal = $('#DescripcionReal').val();
				var descripcionprotegida = $('#DescripcionProtegida').val();
				var tipo = $('#Tipo').val();
				var potencia = $('#Potencia').val();
				var optica = $('#Optica').val();
				var observaciones = $('#Observaciones').val();
				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";
				var siglas = "<?php echo $_SESSION['siglas']?>";
				var estatus = "AUTORIZADO";
				var EstatusActual = $('#Estatus').val();
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterinsertacodigo.php",
					data:{
						id: id,
						marca: marca,
						modelo: modelo,
						codigoreal: codigoreal,
						codigoprotegido: codigoprotegido,
						descripcionreal: descripcionreal,
						descripcionprotegida: descripcionprotegida,
						tipo: tipo,
						potencia: potencia,
						optica: optica,
						observaciones: observaciones,
						usuariocreacion: usuariocreacion,
						siglas: siglas,
						estatus: estatus,
					},
					success: function(resp){
						if (EstatusActual == 'AUTORIZADO')
						{
						alert('El codigo ha sido actualizado');
						consultanotificaciones();
						postForm('codigosrevisar.php');
						}
						else
						{
						alert('El codigo ha sido autorizado');
						consultanotificaciones();
						postForm('codigosautorizados.php');
						}
					}
				});
				}
			});

			$("#btnEliminar").on('click', function(){
				
				var id = $('#idCodigo').val();
				var marca = $('#marca option:selected').text();
				var modelo = $('#Modelo').val();
				var codigoreal = $('#CodigoReal').val();
				var codigoprotegido = $('#CodigoProtegido').val();
				var descripcionreal = $('#DescripcionReal').val();
				var descripcionprotegida = $('#DescripcionProtegida').val();
				var tipo = $('#Tipo').val();
				var potencia = $('#Potencia').val();
				var optica = $('#Optica').val();
				var observaciones = $('#Observaciones').val();
				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";
				var siglas = "<?php echo $_SESSION['siglas']?>";
				var estatus = "BAJA";
				var EstatusActual = $('#Estatus').val();
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterinsertacodigo.php",
					data:{
						id: id,
						marca: marca,
						modelo: modelo,
						codigoreal: codigoreal,
						codigoprotegido: codigoprotegido,
						descripcionreal: descripcionreal,
						descripcionprotegida: descripcionprotegida,
						tipo: tipo,
						potencia: potencia,
						optica: optica,
						observaciones: observaciones,
						usuariocreacion: usuariocreacion,
						siglas: siglas,
						estatus: estatus,
					},
					success: function(resp){
						if (EstatusActual == 'AUTORIZADO')
						{
						alert('El codigo ha sido eliminado');
						consultanotificaciones();
						postForm('codigosrevisar.php');
						}
						else
						{
						alert('El codigo ha sido eliminado');
						consultanotificaciones();
						postForm('codigosautorizados.php');
						}
					}
				});
				
			});


			

			$("#buscadorProyectos").keyup(function(){
				var valorEscrito = $('#buscadorProyectos').val();

				if (valorEscrito) {
					$.ajax({
						url: 'ofvConsultasMaster/buscadorGeneralAnteProyectos.php',
						type: 'post',
						data: {valor: valorEscrito},
						success: function(response){
							$('#tblBuscarProyectos tbody').empty();
							$('#tblBuscarProyectos tbody').append(response);
							$("#tblBuscarProyectos tbody tr").on('click',function(){
								var codigo = $(this).find('td').eq(0).text();
								
								$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaAtrasAdelante.php', 
								dataType:'json',
								data:{ cdoc: codigo },
								success: function(response){
									$("#codcliente").val(response['Cliente']).prop("disabled", true);
									$("#NombreC").val(response['NombreCliente']).prop("disabled", true);
									$("#Contacto").val(response['Contacto']).prop("disabled", true);
									$("#Prioridad").val(response['Prioridad']).prop("disabled", true);
									$("#Direccion").val(response['Direccion']).prop("disabled", true);
									$("#Telefono").val(response['Telefono']);
									$("#Correo").val(response['Email']);
									$("#AsesorV").val(response['AsesorVentas']);
									$("#AdminV").val(response['AdminVentas']).prop("disabled", true);
									$("#cdoc").val(response['IdProyecto']).prop("disabled", true);
									$("#idproyecto").val(response['IdProyecto']).prop("disabled", true);
									$("#vDirectivas").val(response['VentasDirectivas']).prop("disabled", true);
									$("#tpropuesta").val(response['TipoPropuesta']).prop("disabled", true);
									$("#Observaciones").val(response['Observaciones']).prop("disabled", true);
									cargarAdjuntos();
									$("#modalBuscarProyectos").modal('hide');
								},
								});
							});
						},
					});

				}else {
					$('#tblBuscarProyectos tbody').empty();
				}
			});
			$("#consulta1Atras").on('click', function(){
				var cdoc = $("#cdoc").val();
				cdoc = cdoc -1;
				consultaOFV(cdoc);
			});

			$("#consulta1Adelante").on('click', function(){
				var cdoc = $("#cdoc").val();
				cdoc = parseInt(cdoc) + 1;
				consultaOFV(cdoc);
			});

			function consultaOFV(folio) {
				var cdoc = folio;
											
								$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaAtrasAdelanteCodigos.php', 
								dataType:'json',
								data:{ cdoc: cdoc },
								success: function(response){
									 
									$("#marca").val(response['Marca'].substring(0,3));
									$("#Modelo").val(response['Modelo']);
									$("#CodigoReal").val(response['CodigoReal']);
									$("#CodigoProtegido").val(response['CodigoProtegido']);
									$("#iniciocodigo").val(response['Marca'].substring(0,3)+'-XXXX-');
									
									$("#DescripcionReal").val(response['DescripcionReal']);
									$("#DescripcionProtegida").val(response['DescripcionProtegida']);
									$("#Tipo").val(response['Tipo']);
									$("#Potencia").val(response['Potencia']);
									$("#Optica").val(response['Optica']);
									$("#Observaciones").val(response['Observaciones']);
									$("#idCodigo").val(response['Id']);
									$("#Estatus").val(response['Estatus']);
									var EstatusActual = $("#Estatus").val();
									cargarAdjuntos();
									if (("<?php echo $_SESSION['CodigoPosicion']?>" == '49'||"<?php echo $_SESSION['CodigoPosicion']?>" == '50')){
										$("#btnAutorizar").prop("disabled", true);
										$('#btnAutorizar').hide();
										
									}
									
									if (EstatusActual=='AUTORIZADO')
									{
										//alert(EstatusActual);
										replaceButtonText('btnAutorizar', 'ACTUALIZAR');
									}
									// $.ajax({
										// type:'post',
										// url:'ofvConsultas/consultaAtrasAdelante2.php',
										// data: { cdoc: lesito},
										// success: function(res){
											// $("#detalleoferta tbody").empty();
											// $("#detalleoferta tbody").append(res);
										// }
									// });

									$("#modalBuscarProyectos").modal('hide');
								},
								});
							
						

			}
function replaceButtonText(buttonId, text)
{
  if (document.getElementById)
  {
    var button=document.getElementById(buttonId);
    if (button)
    {
      if (button.childNodes[0])
      {
        button.childNodes[0].nodeValue=text;
      }
      else if (button.value)
      {
        button.value=text;
      }
      else //if (button.innerHTML)
      {
        button.innerHTML=text;
      }
    }
  }
}
			$("#consultaPrimerRegistro").on('click', function(){
				
				var orden = "ASC"
				$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaPrimerUltimoRegistro.php',
								dataType:'json',
								data:{ orden: orden },
								success: function(response){
									
										consultaOFV(response['IdProyecto']);
								},
				});
				
				
			});
			$("#consultaUltimoRegistro").on('click', function consultaUltimoRegistro(){
				
				var orden = "DESC"
				$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaPrimerUltimoRegistro.php', 
								dataType:'json',
								data:{ orden: orden },
								success: function(response){
										consultaOFV(response['IdProyecto']);
								},
				});
				consultaOFV(cdoc);
			});
			

			

			

			
			
			$("#btnPdf").on('click', function(){
				var cdoc = $("#cdoc").val();
				window.open('reportes/pdfOfv.php?folio='+cdoc, "nombre de la ventana", "width=1024, height=325");
			});

			$("#myModalArticulos").on('click',function() {
				$("#buscadorArticulo").val("");
				$("#tblarticulo tbody").empty();
			});
			
			$("#buscadorArticulo").keypress(function(e){
				var code = (e.keyCode ? e.keyCode : e.which);
				if (code == 13) {
					
					var valorEscrito = $("#buscadorArticulo").val();
					if (valorEscrito) {
						$.ajax({
							url: 'ofvConsultas/consulta_articulo.php',
							type: 'post',
							data: {valor: valorEscrito},
							dataType: 'json'
						}).done((response) => {		
							
							$("#tblarticulo tbody").empty();
							var tr_articulo = "";
							if (response.length) {
								response.forEach((element,index) => {					
									tr_articulo += `<tr onclick="seleccionar_articulo_click_desde_table(this)" class="cursor"><td>${index++}</td><td class="codigo_articulo">${element.CodigoArticulo}</td><td>${element.NombreArticulo}</td><td>${new Intl.NumberFormat().format(parseFloat(element.EnStock).toFixed(2))}</td><td>${new Intl.NumberFormat().format(parseFloat(element.Comprometido).toFixed(2))}</td><td>${new Intl.NumberFormat().format(parseFloat(element.Solicitado).toFixed(2))}</td><td class="codigo_almacen">${element.CodigoAlmacen}</td><td>${element.NombreAlmacen}</td></tr>`	
								});
							} else {
								var tr_articulo = '<tr><td colspan="8" class="text-center">No hay registros para mostrar</td></tr>';
							}
							$("#tblarticulo tbody").append(tr_articulo);

							$("#tblarticulo tbody tr").on('click',function() {
								var codigo = $(this).find('td').eq(1).text();
								var articulo = $(this).find('td').eq(2).text();
								var pree = $(this).find('td').eq(3).text();
								var almacen = $(this).find('td').eq(7).text();
								$.ajax({
									url: 'ofvConsultas/consulta_articulo_detalle.php',
									type: 'post',
									dataType: 'text',
									data:{ code: codigo },
									success: function (response){
										$("#CodigoReal").val(codigo);
										$("#DescripcionReal").val(articulo).prop("disabled", true);;
										// $("#detalleoferta tbody tr").find("td:eq(11)").text(response);
										// $("#detalleoferta tbody tr:last td:eq(11)").empty();

									},
								});
								$("#buscadorArticulo").val("");
								$("#myModalArticulos").modal('hide');
								
							});							
						});
					} else {
						$("#tblarticulo tbody").empty();
						$("#buscadorArticulo").val("");
					}
				}
			});
		
			
		</script>
	</html>
