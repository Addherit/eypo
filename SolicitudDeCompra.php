<?php 

	include "db/Utils.php";
	$sql="SELECT NombreDepartamento, NombreSucursal  FROM EYPO.dbo.IV_EY_PV_Usuarios WHERE [User]= 'sistemas'";
	$consulta = sqlsrv_query ($conn, $sql);
	$row = sqlsrv_fetch_array($consulta);

	
	
	$sql="SELECT min(FolioSAP) as mini, max(FolioSAP) as maxi FROM EYPO.dbo.IV_EY_PV_SolicitudesComprasCab";
	$consulta = sqlsrv_query ($conn, $sql);
	$Row = sqlsrv_fetch_array($consulta);

	$primerRegistro = $Row['mini'];
	$ultimoRegistro = $Row['maxi'];
	$folioSAPActual = $Row['maxi'] + 1;


?>

<!DOCTYPE html>
	<html>
	<?php include "header.php"?>
	<body>
		<?php include "nav.php"?>
		<?php include "modales.php"?>
		
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h1 style="color: #2fa4e7">Solicitud de Compra</h1>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">
					<?php include "botonesDeControl.php" ?>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-sm-6">
					<div class="row">
						<label class="col-sm-3 col-form-label" >Solicitante:</label>
						<div class="col-sm-4">
							<select id="solUsuario" style="width: 76px;  height:23px;" readonly="true">
								<option value="usuario">Usuario</option>
							</select>	
							<input type="text" value="<?php echo $_SESSION['usuario'] ?>"id="solNombre" style="width: 80px;" readonly="true">	
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Nombre:</label><br>
						<div class="col-sm-4">
							<input type="text" value="<?php echo $_SESSION['NombreCompleto'] ?>" id="nomSolicitante" style= "width: 159px" readonly="true">						
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Sucursal:</label>
						<div class="col-sm-4">
							<input type="text"  id="sucursal" value="<?php echo $row['NombreSucursal'] ?>" style= "width: 159px" readonly="true">											
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Departamento:</label>
						<div class="col-sm-4">
							<input type="text" id="departamento" value="<?php echo $row['NombreDepartamento'] ?>" style="width: 159px;" readonly="true">
						</div>
					</div>	
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Orden de venta:</label>
						<div class="col-sm-4">
							<input type="text" id="ordenVenta" style="width: 159px;" readonly="true">
						</div>
					</div>				
				</div>
				<div class="col-sm-6">
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">N°:</label>
						<div class="col-sm-4">
							<select id="nDoc" style="width: 90px; height: 23px" disabled>
								<option value="SI">Primario</option>							
							</select>						
							<input type="text" value="<?php echo $folioSAPActual ?>" id="cdoc"  style="width: 66px" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Estado:</label>
						<div class="col-sm-4">
							<input type="text"  id="estado" value="Abierto" readonly="true" style="width: 159px">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Fecha de contabilización:</label>
						<div class="col-sm-4">
							<input type="date" value="<?php echo $hoy ?>" id="fconta" style="width: 159px" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Valido hasta:</label>
						<div class="col-sm-4">
							<input type="date" value="<?php echo $hoy ?>" id="validoHasta" style="width: 159px" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Fecha de documento:</label>
						<div class="col-sm-4">
							<input type="date" value="<?php echo $hoy ?>" id="fdoc" style="width: 159px"  readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Fecha necesaria:</label>
						<div class="col-sm-4">
							<input type="date" value="<?php echo $hoy ?>" id="fNecesaria" style="width: 159px"  readonly="true">
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Contenido</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="profile-tab" data-toggle="tab" href="#anexos" role="tab" aria-controls="Anexos" aria-selected="false">Lógica</a>
						</li>
						
					</ul>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
						<br>
						<div class="row">
						<div class="col-md-12" style="margin-bottom: 10px">
							<button class="btn btn-sm" style="background-color: orange" data-toggle="modal" data-target="#modalBuscarOrdenesParaSolicitud">Buscar Orden de venta</button>
						</div>
							<div class="col-md-12 table-responsive">
								<table class="table table-sm table-bordered table-striped table-hover text-center" id="tblOFC">
									<thead>
										<tr class="encabezado">
											<th><i class="fas fa-ban"></i></th>
											<th>#</th>
											<th>Código</th>										
											<th style="width: 100%">Nombre de artíuclo</th>
											<th>Proveedor</th>
											<th>Fecha necesaría</th>
											<th>Cantidad necesaría</th>
											<th>Comentario de partida 1</th>
											<th>Comentario de partida 2</th>
											<th>Stock</th>
											<th>Comprometido</th>
											<th>Solicitado</th>
										</tr>
									</thead>
									<tbody> 
										<tr>
											<th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>
											<td class="cont"></td>
											<td class="codigo"></td>
											<td class="narticulo"></td>
											<td class="proveedor"></td>
											<td contenteditable="true" class="fecha"></td>
											<td contenteditable="true" class="cantidad"></td>
											<td contenteditable="true" class="comentario1"></td>
											<td contenteditable="true" class="comentario2"></td>
											<td class="stock"></td>
											<td class="comprometido"></td>
											<td class="solicitado"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<br>
						<div class="row" style="font-size: .7rem">
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-sm-4 col-form-label" style="padding-right: 0px; padding-bottom:  0px;">Propietario:</label>
									<div class="col-sm-6">
										<input type="text" value="<?php echo $_SESSION['NombreCompleto'] ?>" style="width: 70%" id="propietario" readonly="true">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-sm-4 col-form-label" style="padding-right: 0px; padding-bottom:  0px;">Uso principal:</label>
									<div class="col-sm-6">
										<input type="text" style="width: 70%" id="usoPrincipal">
									</div>
								</div>								
								<div class="row">
									<label for="" class="col-sm-4 col-form-label" style="padding-right: 0px; padding-bottom:  0px;">Comentarios Iniciales:</label>
									<div class="col-sm-6">
										<textarea id="comentarios" cols="60" rows="4" style="background-color: #ffff002e"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="anexos" role="tabpanel" aria-labelledby="home-tab">
					</div>
				</div>
			</div>
		</div>
		<div class="row" id= btnFoot style="margin-bottom: 30px">
			<div class="col-md-6"><br>
				<button class="btn btn-sm" style="background-color: orange" id="btnCrear" title="Crear">Crear</button>
				<button class="btn btn-sm" style="background-color: orange;" id="btnActualizar" title="Actualizar">Actualizar</button>
				<a href="">	<button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>
			</div>
		</div>
		<?php include "footer.php"; ?>

		<script>

			$("#mensajes").hide();
			$("#consultaQuery").hide();
			$("#btnxls").hide();
			$("#btnActualizar").hide();

			function btn_busqueda_general() {
				$("#modalBuscarSolicitudDeCompra").modal();				
			}

			$("#buscarSolicitudDeCompra").keyup(function(){
				var valorEscrito = $("#buscarSolicitudDeCompra").val();

				if (valorEscrito) {
					$.ajax({
						url: 'SCConsultas/consultaGRALSolicitudDeCompras.php',
						type: 'post',
						data: {valor: valorEscrito},
						success: function(response){
							$('#tblBuscarSolicitudDeCompra tbody').empty();
							$('#tblBuscarSolicitudDeCompra tbody').append(response);

							$("#tblBuscarSolicitudDeCompra tbody tr").on('click', function(){
								var folioSAP = $(this).find('td').eq(1).text();								
								var condicion = 'nada';	
								buscarSolicitudDeCompra(folioSAP, condicion);
								$("#modalBuscarSolicitudDeCompra").modal('hide');
								$("#btnActualizar").show();
							});
						},
					});

				}else {
					$('#tblBuscarOfertas tbody').empty();
				}
				
			});

			$("#buscarSolicitudDeCompraFecha").change(function(){
				var valorEscrito = $("#buscarSolicitudDeCompraFecha").val();

				$.ajax({
					url: 'SCConsultas/consultaGRALSolicitudDeComprasFecha.php',
					type: 'post',
					data: {valor: valorEscrito},
					success: function(response){
						$('#tblBuscarSolicitudDeCompra tbody').empty();
						$('#tblBuscarSolicitudDeCompra tbody').append(response);

						$("#tblBuscarSolicitudDeCompra tbody tr").on('click', function(){
							var folioSAP = $(this).find('td').eq(1).text();								
							var condicion = 'nada';	
							buscarSolicitudDeCompra(folioSAP, condicion);
							$("#modalBuscarSolicitudDeCompra").modal('hide');
						});
					},
				});			
			});

			function getValues(){

				var arrayValores = new Array();
				
				arrayValores.push($("#solUsuario").val());
				arrayValores.push($("#solNombre").val());				
				arrayValores.push($("#nomSolicitante").val());
				arrayValores.push($("#sucursal").val());
				arrayValores.push($("#departamento").val());
				arrayValores.push($("#ordenVenta").val());
				arrayValores.push($("#nDoc").val());
				arrayValores.push($("#cdoc").val());
				arrayValores.push($("#estado").val());				
				arrayValores.push($("#fconta").val());
				arrayValores.push($("#validoHasta").val());
				arrayValores.push($("#fdoc").val());
				arrayValores.push($("#fNecesaria").val());
				arrayValores.push($("#usoPrincipal").val());
				arrayValores.push($("#comentarios").val());				
				return arrayValores;
			}

			function buscarSolicitudDeCompra(folioSAP, condicion) {

				var condicion = condicion;
				var ultimoRegistro = <?php echo "$ultimoRegistro"?>;
				var primerRegistro = <?php echo "$primerRegistro"?>;								
				
				$.ajax({
					url:'SCConsultas/rellenarInputs.php',
					type:'post',
					dataType:'json',
					data:{folio: folioSAP, con: condicion, ur: ultimoRegistro, pr: primerRegistro},
					success: function(response){										
											
						console.log(response);
						var resp = response['respuesta'];
						switch(resp){
							case 1:
								alert("No hay mas resultados.");								 
							break;
							case 2: 
								mostrarValoresDeBusquedaEnInputs(response)	
								$("#btnCrear").prop("disabled",true);
							break;
						}
					}
				});
				
			}

			function mostrarValoresDeBusquedaEnInputs(data){

				$("#solNombre").val(data['codSolicitante']);
				$("#nomSolicitante").val(data['nomSolicitante']);
				$("#sucursal").val(data['sucursal']);
				$("#departamento").val(data['departamento']);
				$("#ordenVenta").val(data['ordenVenta']);	 		

				$("#cdoc").val(data['cdoc']);
				// $("#nDoc").val(data['']);
				$("#estado").val(data['estado']);			
				$("#fconta").val(data['fconta']);
				$("#validoHasta").val(data['validoHasta']);
				$("#fdoc").val(data['fdoc']);
				$("#fNecesaria").val(data['fNecesaria']);

				$("#usoPrincipal").val(data['usoPrincipal']);
				$("#propietario").val(data['propietario']);
				$("#comentarios").val(data['comentarios']);

				$("#tblOFC tbody").empty();				
				for (x= 0; x<data.array_Detalle.length; x++ ){
					$("#tblOFC tbody").append(data.array_Detalle[x]);
				} 
				
			}

			function rellanarDetalleORV(folio){
				$.ajax({ 
					url:'SCConsultas/consultarORVDetParaSolicitudes.php',
					type:'post',
					data:{valor: folio},
					success: function(response){
						var fechaNecesaria = $("#fNecesaria").val();
											
						$("#tblOFC tbody").empty();
						$("#tblOFC tbody").append(response);
						$('.fecha').each(function(){
							$(this).text(fechaNecesaria);
						});												
					}
				});	
			}

			function getDetalle(){
				var array_codigo = new Array();
				var array_articulo = new Array();				
				var array_proveedor = new Array();
				var array_fechaNecesaria = new Array();
				var array_cantidadNecesaria = new Array();
				var array_General = new Array();

				var array_comentario1 = new Array();
				var array_comentario2 = new Array();
				var array_stock = new Array();
				var array_comprometido = new Array();
				var array_solicitado = new Array();				

				$('.codigo').each(function(){
					array_codigo.push($(this).text());
				});
				$('.narticulo').each(function(){
					array_articulo.push($(this).text().replace(/'/g, "''"));
				});
				$('.proveedor').each(function(){
					array_proveedor.push($(this).text());
				});
				$('.fecha').each(function(){
					array_fechaNecesaria.push($(this).text());
				});
				$('.cantidad').each(function(){
					array_cantidadNecesaria.push($(this).text());
				});
				$('.comentario1').each(function(){
					array_comentario1.push($(this).text());
				});
				$('.comentario2').each(function(){
					array_comentario2.push($(this).text());
				});
				$('.stock').each(function(){
					array_stock.push($(this).text());
				});
				$('.comprometido').each(function(){
					array_comprometido.push($(this).text());
				});
				$('.solicitado').each(function(){
					array_solicitado.push($(this).text());
				});

				array_General.push(array_codigo, array_articulo, array_proveedor, array_fechaNecesaria, 
				array_cantidadNecesaria,array_comentario1,array_comentario2,array_stock,array_stock,array_comprometido,array_solicitado)

				return array_General;	

			}
			
			$("#btnCrear").on('click', function() {
				var valores = getValues();
				var detalle = getDetalle();			


				// tres campos obligatorios
				var orden_de_venta = $("#ordenVenta").val();
				var comentarios_iniciales = $("#comentarios").val();				
				
				if (orden_de_venta && comentarios_iniciales) {						
					
					var err = 0;
					for (var valor of detalle[4]) {
						if (!valor || valor === '0')						
							err++;						
					}		

					if (err) {
						alert("CANTIDAD NECESARIA no puede ir en cero ni vacio");
					} else {
												
						$.ajax({
							url:'insertarSolicitudCompra.php', 
							type:'post',
							data:{array:valores, arrayDetalle: detalle},
							success: function(response){			 			
														
								if(response === "exito"){
									alert("guardado con éxito");
									setTimeout('document.location.reload()',1000);
								}else{
									alert(response);													
								}
							}
						})
					}						
				
				} else {
					alert("favor de completar los campos ORDEN DE VENTA, COMENTARIOS INICIALES")
				}	
			});

			
			$("#tblBuscarSolicitudDeCompra tbody tr").on('click', function(){
				var folioSAP = $(this).find(td).eq[1].text();
			});

			$("#buscadorOrdenesParaSoli").keyup(function(){
				var valorEscrito = $("#buscadorOrdenesParaSoli").val();
				if (valorEscrito) {
					$.ajax({ 
						url: 'SCConsultas/consultarOrdenesParaSolicitudes.php',
						type: 'post',
						data: {valor: valorEscrito},
						success: function(response){
							$('#tblBuscarOrdenesSolicitudes tbody').empty();
							$('#tblBuscarOrdenesSolicitudes tbody').append(response);
							$("#tblBuscarOrdenesSolicitudes tbody tr").on('click', function(){
								var folioSAP = $(this).find('td').eq(1).text();			
								$("#ordenVenta").val(folioSAP);
								rellanarDetalleORV(folioSAP);						
														
								$("#modalBuscarOrdenesParaSolicitud").modal('hide');
								
							});
						},
					});

				}else {
					$('#tblBuscarOrdenesSolicitudes tbody').empty();
				}
			});

			$(document).on('click', '#eliminarFila', function (event) {
				event.preventDefault();
				$(this).closest('tr').remove();
			})

			function consultar_primer_registro() {
				var folioSAP = <?php echo "$primerRegistro"?>;
				var condicion = 'nada';
				buscarSolicitudDeCompra(folioSAP, condicion);
			}

			function consultar_ultimo_registro() {
				var folioSAP = <?php echo "$ultimoRegistro"?>;
				var condicion = 'nada';
				buscarSolicitudDeCompra(folioSAP, condicion);
			}

			function consulta_1_atras() {
				var folioSAP = ($("#cdoc").val()) - 1;
				var condicion = 'atras';	
				buscarSolicitudDeCompra(folioSAP, condicion);
			}

			function consulta_1_adelante() {
				var folioSAP = $("#cdoc").val();	
				folioSAP ++;				
				var condicion = 'adelante';
				buscarSolicitudDeCompra(folioSAP, condicion);
			}

			$("#btnActualizar").on('click', function () {

				var comentarios_iniciales = $("#comentarios").val();
				var detalle = getDetalle();
				var folio = $("#cdoc").val();
				$.ajax({
					url:'SCConsultas/actualizar_solicitud_de_compra.php', 
					type:'post',
					data:{comentarios:comentarios_iniciales, detalle: detalle, folio: folio},					
				}).done (function (response) {
					console.log(response);		
					switch (response) {
						case '1':
							alert("No se puede actualizar esta SOLICITUD DE COMPRA porque fue creada desde SAP");
						break;
						case '11':
							alert("Algo falló en la actualización de la SOLICITUD DE COMPRA");
						break;

						case '2':
							alert("Actualizado con ÉXITO");
							setTimeout('document.location.reload()',1000);
						break;									
					}					
				})										
			})
		</script>
	</body>
</html>
