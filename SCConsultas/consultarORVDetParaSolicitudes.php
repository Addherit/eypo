<?php
  include "../conexion.php";
  $cdoc = $_POST['valor'];
  $cont = 0;

  $sql = "SELECT * FROM EYPO.dbo.IV_EY_PV_OrdenesVentaDet orvDet 
          LEFT JOIN EYPO.dbo.IV_EY_PV_Articulos art ON orvDet.CodigoArticulo = art.CodigoArticulo
          WHERE FolioInterno = '$cdoc'";
  $consulta = sqlsrv_query($conn, $sql);
  while ($Row = sqlsrv_fetch_array($consulta)) {
    $cont++;
    $nombre = $Row['NombreArticulo'];

    echo  '<tr>
            <td><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></td>
            <td class="cont">'.$cont.'</td>    
            <td class="codigo">'.$Row['CodigoArticulo'].'</td>       
            <td class="narticulo">'.utf8_encode($nombre).'</td>
            <td class="proveedor">'.$Row['CodigoProveedor'].'</td>
            <td class="fecha"></td>
            <td class="cantidad" contenteditable>'.number_format($Row['Quantity'],0,'.','').'</td>             
          </tr>';
    } 
?>
