<!DOCTYPE html>
<?php include "../header.html" ?>
<html lang="en">
	<body onload="cargar_funciones_principales()">
		<?php include "../nav.php" ?>
		<?php include "../../modalQuerys.php" ?>
		<?php include "../../modales.php" ?>
		
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-lg-5 col-xl-6">
					<h1 style="color: #2fa4e7">Orden de venta</h1>
				</div>
				<div id="btnEnca" class="col-lg-7 col-xl-6" style="font-size: 2rem">
					<?php include "../../botonesDeControl.php" ?>
				</div>
			</div>
			<hr>
			<div class="row mensaje"><div class="col-12"></div></div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
					<label class="col-3 col-md-4 col-lg-3 col-form-label">Cliente:</label>
						<div class="col-6">
							<input type="text" id="codcliente" disabled>
						</div>
					</div>
					<div class="row">
						<label class="col-3 col-md-4 col-lg-3 col-form-label">Nombre:</label><br>
						<div class="col-6">
							<input type="text" id="nombreCliente" disabled>
						</div>
					</div>
					<div class="row">
						<label class="col-3 col-md-4 col-lg-3 col-form-label">Persona de contacto:</label>
						<div class="col-6">
							<input type="text" id="personaContacto" disabled >
						</div>
					</div>
					<div class="row">
					<label class="col-3 col-md-4 col-lg-3 col-form-label">Oferta de compra:</label>
					<div class="col-6">
						<input type="text" id="ofertaDeCompra" disabled>
					</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Tipo moneda:</label>
						<div class="col-6">
							<input type="text" id="tipoMoneda" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Uso principal</label>
						<div class="col-6">
							<input type="text" id="usoPrincipal" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Método de pago</label>
						<div class="col-6">
							<input type="text" id="metodoPago" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Forma de pago</label>
						<div class="col-6">
							<input type="text" id="formaPago" disabled>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<label class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">N° Folio:</label>
						<div class="col-6">
							<input type="text" id="cdoc" class="text-right" disabled>
							<input type="hidden" id="folioInterno">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Estado:</label>
						<div class="col-6">
							<input type="text" id="estatus" class="text-right" value="Abierto" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha contabilización:</label>
						<div class="col-6">
							<input type="date" id="fconta" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha entrega:</label>
						<div class="col-6">
							<input type="date" id="fentrega" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha documento:</label>
						<div class="col-6">
							<input type="date" id="fdoc" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha vencimiento:</label>
						<div class="col-6">
							<input type="date" id="fvencimiento" disabled>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Contenido</a>
							<a class="nav-link" id="home-tab" data-toggle="tab" href="#campos-usuario" role="tab" aria-controls="contenido" aria-selected="true">Campos de usuario</a>
						</div>					
					</nav>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
							<br>										
							<div class="row">
								<div class="col-md-12">
									<section class="table-responsive">
										<table class="table table-sm table-bordered table-striped text-center" id="detalleoferta" disable>
											<thead>
												<tr>
													<th>#</th>
													<th>Número de artíuclo</th>
													<th>Descripción de artíuclo</th>
													<th>Cantidad</th>
													<th>Precio por unidad</th>						        										        																		
													<th>Total Unitario</th>
													<th>Almacen</th>
													<th>Cantidad pendiente</th>
													<th>Codigo Clasificación SAT</th>
													<th> Unidad medida SAP</th>
													<th>Comentarios de partida 1</th>
													<th>Comentarios de partida 2</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td class="cont">1</td>
													<td class="narticulo"></td>
													<td class="darticulo"></td>
													<td class="cantidad"></td>
													<td class="precio"></td>																
													<td class="total"></td>
													<td class="almacen"></td>
													<td class="pendiente"></td>
													<td class="codigoPlanificacionSAP"></td>
													<td class="unidadMedidaSAP"></td>
													<td class="comentario1"></td>
													<td class="comentario2"></td>
												</tr>
											</tbody>
										</table>
									<section>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-6">
									<div class="row">
										<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Empleado de ventas:</label>
										<div class="col-6">
											<input type="text" id="empleadoVentas" disabled>
										</div>
									</div>
									<div class="row">
										<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Propietario:</label>
										<div class="col-6">
											<input type="text" id="propietario" disabled>
										</div>
									</div>
									<div class="row">
										<label for="" class="col-sm-3 col-form-label">Comentarios:</label>
										<div class="col-8">
											<textarea id="comentarios" rows="3" class="form-control" disabled></textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Subtotal:</label>
										<div class="col-6">
											<input type="text" id="totalAntesDescuento" disabled value="0.00" class="text-right">
										</div>
									</div>
									<div class="row">
										<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Descuento:</label>
										<div class="col-6">
											<input type="text" id="descNum" value="0" class="text-center" style="width: 16%;" disabled>
											<span whidth="6%">%</span>
											<input type="text" value="0.00" class="text-right" id="descAplicado" style="width: 68%" disabled>
										</div>
									</div>
									<div class="row">
										<label class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Impuesto:</label>
										<div class="col-6">
											<input type="text"value="0.00" class="text-right" id="impuestoTotal" disabled>
										</div>
									</div>
									<div class="row">
										<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Total del documento:</label>
										<div class="col-6">
											<input type="text" id="totalDelDocumento" value="0.00" style="width: 69%" class="text-right"  disabled>								
											<input class="text-center" type="text" id="monedaVisor" style="width: 20%" class="text-right" disabled>
										</div>
									</div>
								</div>
							</div>				
						</div>
					
						<div class="tab-pane fade" id="campos-usuario" role=tabpanel aria-labelledby="campos-usuario-tab">
							<div class="row">
								<div class="col-8">
									<div class="row">
										<label for="numLetra" class="col-4 col-md-4 col-lg-3 col-form-label">Número a letra:</label>
										<div class="col-6">
											<input type="text" id="numLetra" name="numLetra" disabled>
										</div>
									</div>
									<div class="row">
										<label for="tipoFactura" class="col-4 col-md-4 col-lg-3 col-form-label">Tipo de factura:</label>
										<div class="col-6">
											<input type="text" name="tipoFactura" id="tipoFactura" disabled>
										</div>
									</div>
									<div class="row">
										<label for="lab" class="col-4 col-md-4 col-lg-3 col-form-label">LAB:</label>
										<div class="col-6">
											<input type="text"  name="lab" id="lab" disabled>
										</div>
									</div>
									<div class="row">
										<label for="condicionPago" class="col-4 col-md-4 col-lg-3 col-form-label">Condición de pago:</label>
										<div class="col-6">
											<input type="text"  name="condicionPago" id="condicionPago" disabled>
										</div>
									</div>
									<div class="row">
										<label for="" class="col-4 col-md-4 col-lg-3 col-form-label">RFC:</label>
										<div class="col-6">
											<input type="text" name="rfc" id="rfc" disabled>		
										</div>
									</div>	
									</div>
								</div>
							</div>	
						</div>
					</div>
					<div class="row" id= btnFoot>
						<div class="col-md-6">
							<a href="">	<button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>
						</div>
					</div>
					<br>			
				</div>						
			</div> 
		</div>					
	</body>
	<?php include "../footer.html"?>
    <script src="js/ORV.js"></script>
</html>
