	<?php
	
	include "conexion.php";
		session_start();

	$_SESSION['usuario'];

	  $month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;

$FechaInicio = date( "Y-m-d", strtotime( "$today -12 month" ) );
	
	
	?>

	<!DOCTYPE html>
	<html>
		<?php include "header.php"; ?>
				
		<body onload="consultaEstadisticas();">		
		<?php include "nav.php"; ?>	
		<?php include "modales.php"; ?>
		
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Configuración del Master de Proyectos:</h3>
				</div>
				
			</div>
			<div class="row datosEnc" style="font-size: .7rem">
				<div class="col-md-6">
					
			<br>
			
			

	
					<div class="row">
					  		<div class="col-md-12" id="contenedordetalleprotegidos">
							<div class="row" align="left">
						<label for="" class="col-sm-12 col-form-label">Metas Mensuales:</label>
						
						</div>
					<div class="row">
						<label class="col-sm-2 col-form-label" >Meta:</label>
						<div class="col-sm-2">
							<input type="text" class="" name="metanueva" id="metanueva" style="width: 100%">
							
						</div>
						<label class="col-sm-2 col-form-label" >Mes:</label>
						<div class="col-sm-2">
							<select name="" id="mes" style="width: 100%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<option value="1">Enero</option>
										<option value="2">Febrero</option>
										<option value="3">Marzo</option>
										<option value="4">Abril</option>
										<option value="5">Mayo</option>
										<option value="6">Junio</option>
										<option value="7">Julio</option>
										<option value="8">Agosto</option>
										<option value="9">Septiembre</option>
										<option value="10">Octubre</option>
										<option value="11">Noviembre</option>
										<option value="12">Diciembre</option>
												

									</select>
							
						</div>
						<label class="col-sm-2 col-form-label" >Año:</label>
						<div class="col-sm-2">
							<select name="" id="año" style="width: 100%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<option value="2019">2019</option>
										<option value="2020">2020</option>
										<option value="2021">2021</option>
										<option value="2022">2022</option>
										<option value="2023">2023</option>
										<option value="2024">2024</option>
										<option value="2025">2025</option>
										<option value="2026">2026</option>
										<option value="2027">2027</option>
										<option value="2028">2028</option>
										<option value="2029">2029</option>
										<option value="2030">2030</option>
												

									</select>
							
						</div>
					</div>
					<br>
					<div class="row">
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnMetas" title="Agregar Meta">Agregar Meta</button>
					</div>
					<br>
					  			<table class="table-bordered table-editable table-hover table-striped table" width="100%" id="detallemetas">
					        		<thead>
					        			<tr class="encabezado">
											<th><i class="fas fa-ban"></i></th>
											<th>Id</th>
											<th>Meta</th>
											<th>Mes</th>
											<th>Año</th>
											<th>FechaCreción</th>
					        				<th>UsuarioCreación</th>
				        				</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<th><a href="#" style="color: red" id="eliminarFilameta"><i class="fas fa-trash-alt"></i></a></th>
											<td contenteditable="false" class="id"></td>
											<td contenteditable="false" class="meta"></td>
											<td contenteditable="false" class="mes"></td>
											<td contenteditable="false" class="año"></td>
								          	<td contenteditable="false" class="fechacreacion"></td>
											<td contenteditable="false" class="usuariocreacion"></td>
																						
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>

						
					  	<br>
						
						<div class="row" align="left">
						    <label>Limite Autorizacion:</label>
						    <div class="col-sm-4">
						    	<input type="text" id="limiteautorizacion" name="" value=""style="width: 100%">
						    </div>
						</div>
						<div class="row">
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnLimiteAutorizacion" title="Agregar Meta">Actualizar Limite Autorizacion</button>
						</div>
						
						<br>
				<div class="row">
					  		<div class="col-md-12" id="contenedordetalleprotegidos">
							<div class="row" align="left">
						<label for="" class="col-sm-12 col-form-label">Tipos Seguimientos</label>
						
						</div>
					<div class="row">
						<label class="col-sm-4 col-form-label" >Tipo Seguimiento:</label>
						<div class="col-sm-4">
							<input type="text" class="" name="seguimientonuevo" id="seguimientonuevo" style="width: 100%">
							
						</div>
						
					</div>
					<br>
					<div class="row">
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnSeguimientos" title="Agregar Seguimiento">Agregar Seguimiento</button>
					</div>
					<br>
					  			<table class="table-bordered table-editable table-hover table-striped  table" width="100%" id="detalleseguimientos">
					        		<thead>
					        			<tr class="encabezado">
											<th><i class="fas fa-ban"></i></th>
											<th>Id</th>
											<th>TipoSeguimiento</th>
											<th>FechaCreción</th>
					        				<th>UsuarioCreación</th>
				        				</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<th><a href="#" style="color: red" id="eliminarFilaseguimiento"><i class="fas fa-trash-alt"></i></a></th>
											<td contenteditable="false" class="id"></td>
											<td contenteditable="false" class="tiposeguimiento"></td>
											<td contenteditable="false" class="fechacreacion"></td>
											<td contenteditable="false" class="usuariocreación"></td>
																						
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
				</div>
							
							
				<div class="row">
				<div class="col-md-12" id="contenedordetalleprotegidos">
							<div class="row" align="left">
						<label for="" class="col-sm-12 col-form-label">Tipos Propuestas</label>
						
						</div>
					<div class="row">
						<label class="col-sm-3 col-form-label" >Tipo Propuesta:</label>
						<div class="col-sm-3">
							<input type="text" class="" name="propuestanueva" id="propuestanueva" style="width: 100%">
							
						</div>
						<label class="col-sm-3 col-form-label" >Codigo Siglas:</label>
						<div class="col-sm-3">
							<input type="text" class="" name="codigonuevo" id="codigonuevo" style="width: 70%">
							
						</div>
						
					</div>
					<br>
					<div class="row">
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnPropuestas" title="Agregar Tipo Propuesta">Agregar Tipo Propuesta</button>
					</div>
					<br>
					  			<table class="table-bordered table-editable table-hover table-striped  table" width="100%" id="detallepropuestas">
					        		<thead>
					        			<tr class="encabezado">
											<th><i class="fas fa-ban"></i></th>
											<th>Id</th>
											<th>TipoPropuesta</th>
											<th>Codigo Siglas</th>
											<th>FechaCreción</th>
					        				<th>UsuarioCreación</th>
				        				</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<th><a href="#" style="color: red" id="eliminarFilapropuesta"><i class="fas fa-trash-alt"></i></a></th>
											<td contenteditable="false" class="id"></td>
											<td contenteditable="false" class="tipopropuesta"></td>
											<td contenteditable="false" class="codigosiglas"></td>
											<td contenteditable="false" class="fechacreacion"></td>
											<td contenteditable="false" class="usuariocreación"></td>
																						
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
				</div>
				<br>
				<br>			
				<div class="row">
					  		<div class="col-md-12" id="contenedordetalleprotegidos">
							<div class="row" align="left">
						<label for="" class="col-sm-12 col-form-label">Tipos Proyectos</label>
						
						</div>
					<div class="row">
						<label class="col-sm-4 col-form-label" >Tipo Proyecto:</label>
						<div class="col-sm-4">
							<input type="text" class="" name="proyectonuevo" id="proyectonuevo" style="width: 100%">
							
						</div>
						
					</div>
					<br>
					<div class="row">
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnProyectos" title="Agregar Tipo Proyecto">Agregar Tipo Proyecto</button>
					</div>
					<br>
					  			<table class="table-bordered table-editable table-hover table-striped  table" width="100%" id="detalleproyectos">
					        		<thead>
					        			<tr class="encabezado">
											<th><i class="fas fa-ban"></i></th>
											<th>Id</th>
											<th>TipoProyecto</th>
											<th>FechaCreción</th>
					        				<th>UsuarioCreación</th>
				        				</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<th><a href="#" style="color: red" id="eliminarFilaproyecto"><i class="fas fa-trash-alt"></i></a></th>
											<td contenteditable="false" class="id"></td>
											<td contenteditable="false" class="tipoproyecto"></td>
											<td contenteditable="false" class="fechacreacion"></td>
											<td contenteditable="false" class="usuariocreación"></td>
																						
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
				</div>
				<div class="row">
				<div class="col-md-12" id="contenedordetalleprotegidos">
							<div class="row" align="left">
						<label for="" class="col-sm-12 col-form-label">Marcas</label>
						
						</div>
					<div class="row">
						<label class="col-sm-3 col-form-label" >Marca:</label>
						<div class="col-sm-3">
							<input type="text" class="" name="marcanueva" id="marcanueva" style="width: 100%">
							
						</div>
						<label class="col-sm-3 col-form-label" >Codigo Marca:</label>
						<div class="col-sm-3">
							<input type="text" class="" name="codigonuevom" id="codigonuevom" style="width: 70%">
							
						</div>
						
					</div>
					<br>
					<div class="row">
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnMarcas" title="Agregar Marca">Agregar Marca</button>
					</div>
					<br>
					  			<table class="table-bordered table-editable table-hover table-striped  table" width="100%" id="detallemarcas">
					        		<thead>
					        			<tr class="encabezado">
											<th><i class="fas fa-ban"></i></th>
											<th>Id</th>
											<th>Marca</th>
											<th>Codigo Marca</th>
											<th>FechaCreción</th>
					        				<th>UsuarioCreación</th>
				        				</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<th><a href="#" style="color: red" id="eliminarFilapropuesta"><i class="fas fa-trash-alt"></i></a></th>
											<td contenteditable="false" class="id"></td>
											<td contenteditable="false" class="marca"></td>
											<td contenteditable="false" class="codigomarca"></td>
											<td contenteditable="false" class="fechacreacion"></td>
											<td contenteditable="false" class="usuariocreación"></td>
																						
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
				</div>
				<br>
				<br>	
				<div class="row">
					  		<div class="col-md-12" id="contenedordetalleprotegidos">
							<div class="row" align="left">
						<label for="" class="col-sm-12 col-form-label">Prioridades</label>
						
						</div>
					<div class="row">
						<label class="col-sm-4 col-form-label" >Prioridad:</label>
						<div class="col-sm-4">
							<input type="text" class="" name="prioridadnueva" id="prioridadnueva" style="width: 100%">
							
						</div>
						
					</div>
					<br>
					<div class="row">
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnPrioridades" title="Agregar Prioridad">Agregar Prioridad</button>
					</div>
					<br>
					  			<table class="table-bordered table-editable table-hover table-striped  table" width="100%" id="detalleprioridades">
					        		<thead>
					        			<tr class="encabezado">
											<th><i class="fas fa-ban"></i></th>
											<th>Id</th>
											<th>Prioridad</th>
											<th>FechaCreción</th>
					        				<th>UsuarioCreación</th>
				        				</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<th><a href="#" style="color: red" id="eliminarFilaprioridad"><i class="fas fa-trash-alt"></i></a></th>
											<td contenteditable="false" class="id"></td>
											<td contenteditable="false" class="prioridad"></td>
											<td contenteditable="false" class="fechacreacion"></td>
											<td contenteditable="false" class="usuariocreación"></td>
																						
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
				</div>
				<br>
				<br>
						<div class="row">
					  		<div class="col-md-12" id="contenedordetalleprotegidos">
							<div class="row" align="left">
						<label for="" class="col-sm-12 col-form-label">Complejidades</label>
						
						</div>
					<div class="row">
						<label class="col-sm-4 col-form-label" >Complejidad:</label>
						<div class="col-sm-4">
							<input type="text" class="" name="complejidadnueva" id="complejidadnueva" style="width: 100%">
							
						</div>
						
					</div>
					<br>
					<div class="row">
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnComplejidades" title="Agregar Complejidad">Agregar Complejidad</button>
					</div>
					<br>
					  			<table class="table-bordered table-editable table-hover table-striped  table" width="100%" id="detallecomplejidades">
					        		<thead>
					        			<tr class="encabezado">
											<th><i class="fas fa-ban"></i></th>
											<th>Id</th>
											<th>Complejidad</th>
											<th>FechaCreción</th>
					        				<th>UsuarioCreación</th>
				        				</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<th><a href="#" style="color: red" id="eliminarFilacomplejidad"><i class="fas fa-trash-alt"></i></a></th>
											<td contenteditable="false" class="id"></td>
											<td contenteditable="false" class="complejidad"></td>
											<td contenteditable="false" class="fechacreacion"></td>
											<td contenteditable="false" class="usuariocreación"></td>
																						
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
				</div>
				<br>
				<br>
							</div>
						</div>
						</div>
					  	<br>
						
		<?php include "footer.php"; ?>
	</body>
		<script>
		
					function postForm(path, params, method) {
    method = method || 'post';

    var form = document.createElement('form');
    form.setAttribute('method', method);
    form.setAttribute('action', path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}
		
			$("#btnMetas").on('click', function(){
				var meta = $('#metanueva').val();
				var mes = $('#mes option:selected').val();
				var año = $('#año option:selected').val();
				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";	
				
				$.ajax({
					type:'post',
					url: "ofvConsultasMaster/masterinsertameta.php",
					data:{
						meta: meta,
						mes: mes,
						año: año,
						usuariocreacion: usuariocreacion,
					
					},
					success: function(resp){
						
						alert('Meta Registrada Exitosamente');
						$.ajax({
										type:'post',
										url:'ofvConsultasMaster/buscadormetas.php',
										success: function(res){
										    $("#detallemetas tbody").empty();
											$("#detallemetas tbody").append(res);
											
										}
									});
					}
				});
			});
			
				$("#btnSeguimientos").on('click', function(){
				var tiposeguimiento = $('#seguimientonuevo').val();
				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";	
				
				$.ajax({
					type:'post',
					url: "ofvConsultasMaster/masterinsertatiposeguimiento.php",
					data:{
						tiposeguimiento: tiposeguimiento,
						usuariocreacion: usuariocreacion,
					
					},
					success: function(resp){
						
						alert('Tipo Seguimiento Registrado Exitosamente');
						$.ajax({
										type:'post',
										url:'ofvConsultasMaster/buscadortiposeguimientos.php',
										success: function(res){
										    $("#detalleseguimientos tbody").empty();
											$("#detalleseguimientos tbody").append(res);
											
										}
									});
					}
				});
			});
			
			$("#btnPropuestas").on('click', function(){
				var tipopropuesta = $('#propuestanueva').val();
				var codigosiglas = $('#codigonuevo').val();
				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";	
				
				$.ajax({
					type:'post',
					url: "ofvConsultasMaster/masterinsertatipopropuesta.php",
					data:{
						tipopropuesta: tipopropuesta,
						codigosiglas: codigosiglas,
						usuariocreacion: usuariocreacion,
					
					},
					success: function(resp){
								alert('Tipo Propuesta Registrada Exitosamente');
									$.ajax({
											url: 'ofvConsultasMaster/buscadortipospropuestas.php',
											type: 'post',
											
											success: function(response) { 
												
												$('#detallepropuestas tbody').empty();
												$('#detallepropuestas tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
					}
				});
			});
			$("#btnMarcas").on('click', function(){
				var marca = $('#marcanueva').val();
				var codigomarca = $('#codigonuevom').val();
				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";	
				
				$.ajax({
					type:'post',
					url: "ofvConsultasMaster/masterinsertamarca.php",
					data:{
						marca: marca,
						codigomarca: codigomarca,
						usuariocreacion: usuariocreacion,
					
					},
					success: function(resp){
									alert('Marca Registrada Exitosamente');
									$.ajax({
											url: 'ofvConsultasMaster/buscadormarcas.php',
											type: 'post',
											
											success: function(response){
												
												$('#detallemarcas tbody').empty();
												$('#detallemarcas tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
					}
				});
			});
			$("#btnProyectos").on('click', function(){
				var tipoproyecto = $('#proyectonuevo').val();
				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";	
				
				$.ajax({
					type:'post',
					url: "ofvConsultasMaster/masterinsertatipoproyecto.php",
					data:{
						tipoproyecto: tipoproyecto,
						usuariocreacion: usuariocreacion,
					
					},
					success: function(resp){
						
						alert('Tipo Proyecto Registrado Exitosamente');
						$.ajax({
										type:'post',
										url:'ofvConsultasMaster/buscadortiposproyectos.php',
										success: function(res){
										    $("#detalleproyectos tbody").empty();
											$("#detalleproyectos tbody").append(res);
											
										}
									});
					}
				});
			});
			$("#btnPrioridades").on('click', function(){
				var prioridad = $('#prioridadnueva').val();
				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";	
				
				$.ajax({
					type:'post',
					url: "ofvConsultasMaster/masterinsertaprioridad.php",
					data:{
						prioridad: prioridad,
						usuariocreacion: usuariocreacion,
					
					},
					success: function(resp){
						
						alert('Prioridad Registrado Exitosamente');
						$.ajax({
										type:'post',
										url:'ofvConsultasMaster/buscadorprioridades.php',
										success: function(res){
										    $("#detalleprioridades tbody").empty();
											$("#detalleprioridades tbody").append(res);
											
										}
									});
					}
				});
			});
			$("#btnComplejidades").on('click', function(){
				var complejidad = $('#complejidadnueva').val();
				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";	
				
				$.ajax({
					type:'post',
					url: "ofvConsultasMaster/masterinsertacomplejidad.php",
					data:{
						complejidad: complejidad,
						usuariocreacion: usuariocreacion,
					
					},
					success: function(resp){
						
						alert('Complejidad Registrado Exitosamente');
						$.ajax({
										type:'post',
										url:'ofvConsultasMaster/buscadorcomplejidades.php',
										success: function(res){
										    $("#detallecomplejidades tbody").empty();
											$("#detallecomplejidades tbody").append(res);
											
										}
									});
					}
				});
			});
			$("#btnLimiteAutorizacion").on('click', function(){
				var limite = $('#limiteautorizacion').val();
				
				$.ajax({
					type:'post',
					url: "ofvConsultasMaster/masteractualizalimite.php",
					data:{
						limite: limite,
					
					},
					success: function(resp){
						
						alert('Limite Actualizado Exitosamente');
						$.ajax({
										type:'post',
										url:'buscadorlimiteautorizacion.php',
										dataType:'json',
										success: function(res){
											
												$("#limiteautorizacion").val(res['limiteautorizacion'].toFixed(2));
											}
										
										});
					}
				});
			});
			$(document).on('click', '#eliminarFilameta', function (event) {
				
				  event.preventDefault();
				  var currentRow=$(this).closest("tr"); 
				  var valorEscrito=currentRow.find("td:eq(0)").text();
				  $(this).closest('tr').remove();
				  
				    respuestasmetas(valorEscrito);
					
				
			});
			function respuestasmetas(Id){
								 
				   $.ajax({
						type: "post",
						url: "ofvConsultasMaster/eliminameta.php?valor="+Id,

						success  : function(data){
							 //window.location.href="nomina.php";
						},
						error    : function(){
							
						}

					});
			}
			
			$(document).on('click', '#eliminarFilaseguimiento', function (event) {
				
				  event.preventDefault();
				  var currentRow=$(this).closest("tr"); 
				  var valorEscrito=currentRow.find("td:eq(0)").text();
				  $(this).closest('tr').remove();
				  
				    respuestasseguimiento(valorEscrito);
					
				
			});
			function respuestasseguimiento(Id){
								 
				   $.ajax({
						type: "post",
						url: "ofvConsultasMaster/eliminatiposeguimiento.php?valor="+Id,

						success  : function(data){
							 //window.location.href="nomina.php";
						},
						error    : function(){
							
						}

					});
			}
			$(document).on('click', '#eliminarFilapropuesta', function (event) {
				
				  event.preventDefault();
				  var currentRow=$(this).closest("tr"); 
				  var valorEscrito=currentRow.find("td:eq(0)").text();
				  $(this).closest('tr').remove();
				  
				    respuestaspropuestas(valorEscrito);
					
				
			});
			function respuestaspropuestas(Id){
								 
				   $.ajax({
						type: "post",
						url: "ofvConsultasMaster/eliminatipopropuesta.php?valor="+Id,

						success  : function(data){
							 //window.location.href="nomina.php";
						},
						error    : function(){
							
						}

					});
			}
			
				$(document).on('click', '#eliminarFilaproyecto', function (event) {
				
				  event.preventDefault();
				  var currentRow=$(this).closest("tr"); 
				  var valorEscrito=currentRow.find("td:eq(0)").text();
				  $(this).closest('tr').remove();
				  
				    respuestasproyectos(valorEscrito);
					
				
			});
			function respuestasproyectos(Id){
								 
				   $.ajax({
						type: "post",
						url: "ofvConsultasMaster/eliminatipoproyecto.php?valor="+Id,

						success  : function(data){
							 //window.location.href="nomina.php";
						},
						error    : function(){
							
						}

					});
			}
			$(document).on('click', '#eliminarFilamarca', function (event) {
				
				  event.preventDefault();
				  var currentRow=$(this).closest("tr"); 
				  var valorEscrito=currentRow.find("td:eq(0)").text();
				  $(this).closest('tr').remove();
				  
				    respuestasmarcas(valorEscrito);
					
				
			});
			function respuestasmarcas(Id){
								 
				   $.ajax({
						type: "post",
						url: "ofvConsultasMaster/eliminamarca.php?valor="+Id,

						success  : function(data){
							 //window.location.href="nomina.php";
						},
						error    : function(){
							
						}

					});
			}
			$(document).on('click', '#eliminarFilaprioridad', function (event) {
				
				  event.preventDefault();
				  var currentRow=$(this).closest("tr"); 
				  var valorEscrito=currentRow.find("td:eq(0)").text();
				  $(this).closest('tr').remove();
				  
				    respuestasprioridades(valorEscrito);
					
				
			});
			function respuestasprioridades(Id){
								 
				   $.ajax({
						type: "post",
						url: "ofvConsultasMaster/eliminaprioridad.php?valor="+Id,

						success  : function(data){
							 //window.location.href="nomina.php";
						},
						error    : function(){
							
						}

					});
			}
			$(document).on('click', '#eliminarFilacomplejidad', function (event) {
				
				  event.preventDefault();
				  var currentRow=$(this).closest("tr"); 
				  var valorEscrito=currentRow.find("td:eq(0)").text();
				  $(this).closest('tr').remove();
				  
				    respuestascomplejidades(valorEscrito);
					
				
			});
			function respuestascomplejidades(Id){
								 
				   $.ajax({
						type: "post",
						url: "ofvConsultasMaster/eliminacomplejidad.php?valor="+Id,

						success  : function(data){
							 //window.location.href="nomina.php";
						},
						error    : function(){
							
						}

					});
			}
$("#btnProyectistas").on('click', function(){
	postForm('estadisticasproyectistas.php');
	
		});
function consultaEstadisticas(){
				 // var fechaInicio = document.getElementById("FechaInicio").value;
				 // var fechaFin = document.getElementById("FechaFin").value;
										$.ajax({
											url: 'ofvConsultasMaster/buscadormetas.php',
											type: 'post',
											// data: {TipoPropuesta: TipoPropuesta},
											 // data: {fechaInicio:fechaInicio, fechaFin:fechaFin},
											success: function(response){
												
												$('#detallemetas tbody').empty();
												$('#detallemetas tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
										$.ajax({
										type:'post',
										url:'OFVConsultasMaster/buscadorlimiteautorizacion.php',
										dataType:'json',
										success: function(res){
											
												$("#limiteautorizacion").val(res['limiteautorizacion'].toFixed(2));
											}
										
										});
										$.ajax({
											url: 'ofvConsultasMaster/buscadortiposeguimientos.php',
											type: 'post',
											
											success: function(response){
												
												$('#detalleseguimientos tbody').empty();
												$('#detalleseguimientos tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
										$.ajax({
											url: 'ofvConsultasMaster/buscadortipospropuestas.php',
											type: 'post',
											
											success: function(response){
												
												$('#detallepropuestas tbody').empty();
												$('#detallepropuestas tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
										$.ajax({
											url: 'ofvConsultasMaster/buscadortiposproyectos.php',
											type: 'post',
											
											success: function(response){
												
												$('#detalleproyectos tbody').empty();
												$('#detalleproyectos tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
										$.ajax({
											url: 'ofvConsultasMaster/buscadormarcas.php',
											type: 'post',
											
											success: function(response){
												
												$('#detallemarcas tbody').empty();
												$('#detallemarcas tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
										$.ajax({
											url: 'ofvConsultasMaster/buscadorprioridades.php',
											type: 'post',
											
											success: function(response){
												
												$('#detalleprioridades tbody').empty();
												$('#detalleprioridades tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
										$.ajax({
											url: 'ofvConsultasMaster/buscadorcomplejidades.php',
											type: 'post',
											
											success: function(response){
												
												$('#detallecomplejidades tbody').empty();
												$('#detallecomplejidades tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
										// $.ajax({
											// url: 'ofvConsultasMaster/consultaEstadisticasInteriorControl.php',
											// type: 'post',
											// // data: {TipoPropuesta: TipoPropuesta},
											// data: {fechaInicio:fechaInicio, fechaFin:fechaFin},
											// success: function(response){
												
												// $('#detalleinterior tbody').empty();
												// $('#detalleinterior tbody').append(response);
												 // //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											// }
										// });
										// $.ajax({
											// url: 'ofvConsultasMaster/consultaEstadisticasVisitas.php',
											// type: 'post',
											// // data: {TipoPropuesta: TipoPropuesta},
											// data: {fechaInicio:fechaInicio, fechaFin:fechaFin},
											// success: function(response){
												
												// $('#detallevisitas tbody').empty();
												// $('#detallevisitas tbody').append(response);
												 // //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											// }
										// });
										// $.ajax({
											// url: 'ofvConsultasMaster/consultaEstadisticasTiempos.php',
											// type: 'post',
											// // data: {TipoPropuesta: TipoPropuesta},
											// data: {fechaInicio:fechaInicio, fechaFin:fechaFin},
											// success: function(response){
												
												// $('#detalletiempos tbody').empty();
												// $('#detalletiempos tbody').append(response);
												 // //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											// }
										// });
	
	
}
			
$("#buscadorProyectos").keyup(function(){
				var valorEscrito = $('#buscadorProyectos').val();

				if (valorEscrito) {
					$.ajax({
						url: 'ofvConsultasMaster/buscadorGeneralAnteProyectos.php',
						type: 'post',
						data: {valor: valorEscrito, estatus: '%'},
						success: function(response){
							$('#tblBuscarProyectos tbody').empty();
							$('#tblBuscarProyectos tbody').append(response);
							$("#tblBuscarProyectos tbody tr").on('click',function(){
								var codigo = $(this).find('td').eq(0).text();
								
								$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaAtrasAdelante.php', 
								dataType:'json',
								data:{ cdoc: codigo },
								success: function(response){
									
									$("#siglas").val(response['Siglas']);
									$("#tpropuesta").val(response['TipoPropuesta']);
									
									$("#modalBuscarProyectos").modal('hide');
								},
								});
							});
						},
					});

				}else {
					$('#tblBuscarProyectos tbody').empty();
				}
			});
			
				function exportTableToExcel(tableID, filename = ''){
				var downloadLink;
				var dataType = 'application/vnd.ms-excel';
				var tableSelect = document.getElementById(tableID);
				var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
				
				// Specify file name
				filename = filename?filename+'.xls':'estadisticas.xls';
				
				// Create download link element
				downloadLink = document.createElement("a");
				
				document.body.appendChild(downloadLink);
				
				if(navigator.msSaveOrOpenBlob){
					var blob = new Blob(['\ufeff', tableHTML], {
						type: dataType
					});
					navigator.msSaveOrOpenBlob( blob, filename);
				}else{
					// Create a link to the file
					downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
				
					// Setting the file name
					downloadLink.download = filename;
					
					//triggering the function
					downloadLink.click();
				}
			}
			

			
		
			
		</script>
	</html>
