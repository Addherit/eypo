<?php
	//include "conexion.php"
	include "conexioncajero.php";

	session_start();
	if (!isset($_SESSION["usuario"]))
   {
      header("location: login.php");
   }
	$_SESSION['usuario'];
	$empleadoVentas = $_SESSION['usuario'];
	$hoy = date("Y-m-d");
	$mesDespues = date( "Y-m-d", strtotime( "$hoy +1 month" ) );
	$quincena = date( "Y-m-d", strtotime( "$hoy +15 days" ) );

	$sql = "SELECT lastName, firstName, middleName FROM OHEM WHERE U_IV_EY_PV_User = '$empleadoVentas'";
	$consulta = sqlsrv_query($conn2, $sql);
	while ($Row = sqlsrv_fetch_array($consulta)) {
		$nombre = $Row['firstName'];
		$sNombre = $Row['middleName'];
		$apellido = $Row['lastName'];
	}
	$nombreCompleto = $nombre . " " . $sNombre . " " . $apellido;




	?>

	<!DOCTYPE html>
	<html>
	<?php include "header.php"?>
	<body>
		<?php include "nav.php"?>
		<?php include "modalQuerys.php"; ?>
		<?php include "modales.php"; ?>

		<div class="container" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Consultar información de los cajeros</h3>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">

				</div>
			</div>
			<div class="row datosEnc" style="font-size: .7rem">
				<div class="col-md-6">
					<div class="row">
						<label class="col-sm-3 col-form-label" >Id Cajero:</label>
						<div class="col-sm-4">
							<input type="text" class="" name="codcajero" id="codcajero" style="width: 70%">
							<a id="btnCajero" href="#" data-toggle="modal" data-target="#myModalCajero">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cajero"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Nombre Cajero:</label><br>
						<div class="col-sm-4">
							<input type="text" class="" name="NombreC" id="NombreC" style="width: 70%">
							<a href="#" data-toggle="modal" data-target="#myModalCajero" >
								<i class="fas fa-search" style="color: #57b4ea" title="Búsqueda Nombre"></i>
							</a>
						</div>
					</div>

					<!--<div class="row">
						<label for="" class="col-sm-3 col-form-label">Persona de contacto:</label>
						<div class="col-sm-4">
							<select  id="listcontactos" style="width: 70%; height:23px;">
								<option value="" disabled selected>Seleccione</option>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Orden de compra:</label>
						<div class="col-sm-4">
							<input type="text" class="" id="oCompra" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Tipo moneda:</label>
						<div class="col-sm-4">
							<select id="tmoneda" style="height: 23px; width: 70%">
								<option value="" disabled selected>Seleccione</option>
									<?php
								$sql = "SELECT * FROM OCRN";
								$consulta = sqlsrv_query($conn, $sql);
								while ($Row = sqlsrv_fetch_array($consulta)) { ?>
											<option value="<?php echo $Row['CurrCode']; ?>"><?php echo $Row['CurrCode']; ?></option>
									<?php
							} ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Uso principal</label>
						<div class="col-sm-4">
							<select class="" name="" id="usoPrincipal" style="height: 23px; width: 70%">
								<option value="" disabled selected>Seleccione</option>
								<?php
							$sql = "SELECT * FROM IV_EY_PV_UsosCfdi";
							$consulta = sqlsrv_query($conn, $sql);
							while ($Row = sqlsrv_fetch_array($consulta)) {
							$nomUsoPrincipal = $Row['DescUsoCfdi'];	?>
								<option value="<?php echo $Row['CodigoUsoCfdi']; ?>"><?php echo utf8_encode($nomUsoPrincipal) ?></option>
							<?php
						} ?>
							</select>
						</div>
					</div>

					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Método de pago</label>
						<div class="col-sm-4">
							<select class="" name="" style="width: 70%; height:23px;" id="metodoPago">
								<option value="" disabled selected>Seleccione</option>
								<option value="PUE">PUE</option>
								<option value="PPD">PPD</option>
								<!-- <?php
												$sql = "SELECT * FROM IV_EY_PV_MetodosPago";
												$consulta = sqlsrv_query($conn, $sql);
												while ($row = sqlsrv_fetch_array($consulta)) { ?>
									<option value="<?php echo $row['CodigoMetodoPagoSat']; ?>"><?php echo $row['CodigoMetodoPagoSat']; ?></option>
								<?php
						} ?>

							</select>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Forma de pago</label>
						<div class="col-sm-4">
							<select class="" name="" style="width: 70%; height:23px;" id="formaPagoSelect">
									<option value="" disabled selected>Seleccione</option>
							</select>
						</div>
					</div>-->
				</div>
				<div class="col-md-6">
					<!--<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">N°:</label>
						<div class="col-sm-4">
							<select id="ndoc" style="height: 23px">
								<option value="COT-GRAL">COT-GRAL</option>
								<option value="Manual">Manual</option>
							</select>
							<?php
						$consultasql = sqlsrv_query($conn, "SELECT TOP 1 DocNum  FROM dbEypo.dbo.ofertas ORDER BY DocNum DESC");
						$consultasql2 = sqlsrv_query($conn, "SELECT TOP 1 FolioSAP  FROM EYPO.dbo.IV_EY_PV_OfertasVentasCab ORDER BY FolioSAP DESC");
						$Row = sqlsrv_fetch_array($consultasql);
						$Row2 = sqlsrv_fetch_array($consultasql2);
						$folio = $Row['DocNum'];
						$folioSAP = $Row2['FolioSAP'];
						if($folio >= $folioSAP){
							$folio++;
						} else {
							$folio = $folioSAP;
							$folio++;
						}
						?>
							<input type="text" class="" id="cdoc"  size="5" value="<?php echo "$folio"; ?>" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Estado:</label>
						<div class="col-sm-4">
							<input type="text"  class="status" value="Abierto" readonly="true" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Fecha de contabilización:</label>
						<div class="col-sm-4">
							<input type="date" value="<?php echo $hoy ?>" id="fconta" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Fecha de entrega	:</label>
						<div class="col-sm-4">
							<input type="date" value="<?php echo $mesDespues ?>" id="fentrega" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Fecha del documento:</label>
						<div class="col-sm-4">
							<input type="date" value="<?php echo $hoy ?>" id="fdoc" style="width: 70%" >
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Fecha de vencimiento:</label>
						<div class="col-sm-4">
							<input type="date" value="<?php echo $quincena ?>" id="fvencimiento" style="width: 70%" >
						</div>
					</div>
					<div class="row">
						<div class="col-md-7 offset-md-4 text-right" style="margin-top: 10px">
							<button class="btn btn-secundary btn-sm" id="btnCamposDefinidos" title="Campos definidos por el usuario">Campos definidos por el usuario</button>
						</div>
					</div>
					<div class="row" id="camposDefinidos" style="font-size: .7rem; margin-top: 10px">
						<label for="" class="col-sm-3 offset-md-4 col-form-label">Número a letra:</label>
						<div class="col-sm-5">
							<input type="text"id="numLetra" style="width: 90%; height:23px;">
						</div>
						<label for="" class="col-sm-3 offset-md-4 col-form-label">Tipo de factura:</label>
						<div class="col-sm-5">
							<input type="text" id="tipoFactura" style="width: 90%; height:23px;">
							<a href="#" data-toggle="modal" data-target="#modalTipoFactura">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
							</a>
						</div>
						<label for="" class="col-sm-3 offset-md-4 col-form-label">LAB:</label>
						<div class="col-sm-5">
							<input type="text"  id="lab" style="width: 90%; height:23px;">
						</div>
						<label for="" class="col-sm-3 offset-md-4 col-form-label">Condición de pago:</label>
						<div class="col-sm-5">
							<input type="text"  id="condicionPago" style="width: 90%; height:23px;">
							<a href="#" data-toggle="modal" data-target="#modalCondicionPago">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
							</a>
						</div>
					</div> -->
				</div>
			</div>
			<br>

			<div class="row" style="font-size: .7rem">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<?php If ($_SESSION['CodigoPosicion'] == 36 || $_SESSION['CodigoPosicion']== 43 || $_SESSION['CodigoPosicion']== 45 || $_SESSION['CodigoPosicion']== 44 )	 {?>
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Cantidad Actual en Cajero</a>
						</li>
						<?php } ?>
						<?php If ($_SESSION['CodigoPosicion'] == 36 || $_SESSION['CodigoPosicion']==45 || $_SESSION['CodigoPosicion']==44 || $_SESSION['CodigoPosicion']==43 )	 {?>
						<li class="nav-item">
							<a class="nav-link" id="profile-tab" data-toggle="tab" href="#logica" role="tab" aria-controls="logica" aria-selected="false">Detalle de Movimientos</a>
						</li>
						<?php } ?>

					</ul>

					<div class="tab-content" id="myTabContent">

						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
							<?php include "tablaNavCajero/general.php"; ?>

						</div>


						<div class="tab-pane fade" id="logica" role="tabpanel" aria-labelledby="home-tab" >
							<?php include "tablaNavCajero/logica.php"; ?>
						</div>



					</div>
				</div>
			<!--<div class="row" id= btnFoot style="margin-bottom: 30px">
				<div class="col-md-6">
						<button class="btn btn-sm" style="background-color: orange" id="btnCrear" title="Crear">Crear</button>
							<button class="btn btn-sm" style="background-color: orange; display:none;" id="btnActualizar" title="Actualizar">Actualizar</button>
						<a href="">	<button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>
					</div>
				<div class="col-md-6 text-right">
					<div class="dropdown">
						<button class="btn btn-secondary dropdown-toggle btn-sm" type="button" id="copiarA" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
							Copiar a
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#" id="pedidoAlCliente">Pedido al cliente</a>
						</div>
					</div>
				</div>
			</div>
			</div>-->

<!-- Modal -->
<div class="modal fade" id="ModalQR" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">QR Code</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <img id="QR_code_img" src="first.jpg"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



		</div>
	</div>
<iframe id="txtArea1" style="display:none"></iframe>
		<?php include "footer.php"; ?>
	</body>
		<script>

			$("#buscadorCajero").keyup(function(){
				var valorEscrito = $("#buscadorCajero").val();
				if (valorEscrito) {
					$.ajax({
						url: 'buscadorConsultaCajero.php',
						type: 'post',
						data: {valor: valorEscrito},
						success: function(resp){


							$("#tblcajero tbody").empty();
							$("#tblcajero tbody").append(resp);

							$("#tblcajero tr").on('click',function(){


								if (!$('#BackOrderVentas').is(':visible')) {

											var codigo = $(this).find('td').eq(1).text();
											var name = $(this).find('td').eq(2).text();


											agregarCajero(codigo, name);
											$("#tblcajero tbody").empty();
											$("#buscadorcajero").val("");

											$("#nombreCajero").val(name);
											$("#codigoCajero").val(codigo);




								} else {
									var codigo = $(this).find('td').eq(1).text();
									var name = $(this).find('td').eq(2).text();
									$("#nombreCajero").val(name);
									$("#codigoCajero").val(codigo);
									$("#myModalCajero").hide();


								 }
							 });
						 },
					 });
				} else {
					$("#tblcajero tbody").empty();
				}
			});

			$("#myModalCajero").on('click',function() {
				$("#buscadorCajero").val("");
			});



			function agregar ($code, $name){
				var code = $code;
				var name = $name;
				$("#codcajero").val(code);
				$("#NombreC").val(name);
				$('#myModalCajero').modal('hide');
				$.ajax({
					url: 'consultaPersonaContacto.php',
					type: 'post',
					data: {code: code},
					success: function(response){
						$("#listcontactos").append(response);
					}
				});
			}

			function agregarCajero ($code, $name){
				var code = $code;
				var name = $name;
				$("#codcajero").val(code);
				$("#NombreC").val(name);
				var valorEscritoCajero = $code;
				 var fechaInicio = document.getElementById("FechaIC").value;
				 var fechaFin = document.getElementById("FechaFC").value;
				 var tipo = document.getElementById("tipo").value;
				$('#myModalCajero').modal('hide');
				$.ajax({
					url: 'buscadorTotalesCajero.php',
					type: 'post',
					data: {valor: valorEscritoCajero},
					success: function(response){
						$("#detalleoferta tbody").empty();
						$("#detalleoferta tbody").append(response);
                     }
				});
				$.ajax({
					url: 'buscadorTotalesCajeroDetalle.php',
					type: 'post',
					data: {valor: valorEscritoCajero},
					success: function(response){
						$("#detalleofertad tbody").empty();
						$("#detalleofertad tbody").append(response);
                     }
				});
				$.ajax({
					url: 'buscadorMovimientosCajero.php',
					type: 'post',
					data: {fechaInicio:fechaInicio, fechaFin:fechaFin, tipo:tipo, valor:valorEscritoCajero },
					success: function(response){

						$("#detallemovimientos tbody").empty();
						$("#detallemovimientos tbody").append(response);
                     }
				});
				$('#myModalCajero').modal('hide');
			}
			function buscarMovimientosFecha(){
				var valorEscritoCajero = document.getElementById("codcajero").value;
				 var fechaInicio = document.getElementById("FechaIC").value;
				 var fechaFin = document.getElementById("FechaFC").value;
				 var tipo = document.getElementById("tipo").value;
				 console.log(tipo);

				$.ajax({
					url: 'buscadorMovimientosCajero.php',
					type: 'post',
					data: {fechaInicio:fechaInicio, fechaFin:fechaFin, tipo:tipo, valor:valorEscritoCajero},
					success: function(response){

						$("#detallemovimientos tbody").empty();
						$("#detallemovimientos tbody").append(response);
                     }
				});

			}
			function exportTableToExcel(tableID, filename = ''){
				var downloadLink;
				var dataType = 'application/vnd.ms-excel';
				var tableSelect = document.getElementById(tableID);
				var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

				// Specify file name
				filename = filename?filename+'.xls':'movimientos.xls';

				// Create download link element
				downloadLink = document.createElement("a");

				document.body.appendChild(downloadLink);

				if(navigator.msSaveOrOpenBlob){
					var blob = new Blob(['\ufeff', tableHTML], {
						type: dataType
					});
					navigator.msSaveOrOpenBlob( blob, filename);
				}else{
					// Create a link to the file
					downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

					// Setting the file name
					downloadLink.download = filename;

					//triggering the function
					downloadLink.click();
				}
			}


			function getqr(token){
				console.log(token);
				$.ajax({
					url:'generate_code.php',
					type:'POST',
					data: {tok: token },
					success: function(response) {
						var str = response;
						var res = str.replace('"', "");
						var res1 = res.replace('"', "");
						console.log('codes/'+res1);
						str = "codes/"+res1;
						//$("#QR_code_img").attr("src",str);
						//$('#ModalQR').modal('show');
						var pop= window.open(str,'popup','opciones');
						pop.print();
						console.log('jalo');
					},
				});
			}

		</script>
	</html>
