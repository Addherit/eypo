<!DOCTYPE html>
<html lang="en">
	<?php include "header.php"?>
	<body onload="consultar_folio_max_mas_uno('PROTON')">
		<?php include "nav.php"?>
		<?php include "modales.php"?>
		<div class="container formato-font-design" id="contenedorDePagina">				
			<div class="row" style="margin-bottom: 5px; margin-top: 5px">
				<div class="col-md-9" style="background-color: #005580; text-align: center;">
					<img src="img/tituloEypo.png" alt="EYPO" style="max-width: 20%">
				</div>
				<div class="col-md-3" style="border: 1px solid; text-align: center;">
					<img id="imgChange" src="img/Protonlogo.png" alt="EYPO" style="width: 100%">
					<input type="hidden" id="empresa" value="proton">
				</div>
			</div>
			<div class="row" style="color: white; background-color: #336599; margin-bottom: 15px">
				<div class="col-md-8">
					<b style="font-size: 1.3rem; line-height: 2.1">ORDEN DE FABRICACIÓN DE PRODUCTO PROTON - EYPO</b>
				</div>
				<div class="col-4 text-right">
					<?php include "btnEncaArcos.php"?>
				</div>
			</div>
			<div class="mensaje"><div class="col-12"></div></div>
			<div class="row datosEnc">
				<div class="col-md-8 col-lg-7">
					<div class="row">
						<label class="col-4 col-form-label">Fecha Solicitud: </label>
						<div class="col-6">
							<input id="dateInicial" type="date" value="<?php echo "$hoy" ?>" style=" height:23px;">
						</div>
					</div>
					<div class="row">
						<label class="col-4 col-form-label">Fecha Entrega <b style="color: red">(Requerida)* </b>: </label>
						<div class="col-6">
							<input type="date" value="<?php echo "$hoy" ?>" id="dateFinal">
						</div>
					</div>
					<div class="row">
						<label class="col-4 col-form-label">Nombre Solicitante:</label>
						<div class="col-6">
							<input type="text" id="nombreSolicitante" value="<?php echo "$nombreCompleto" ?>">
						</div>
					</div>
					<div class="row">
						<label class="col-4 col-form-label">Nombre Proyecto:</label>
						<div class="col-6">
							<input type="text" value="" id="nombreProyecto">
						</div>
					</div>
					<div class="row">
						<label class="col-4 col-form-label">Numero Orden Venta SAP:</label>
						<div class="col-6">
							<input type="text" id="numeroDeOrden">
							<a href="#" data-toggle="modal" data-target="#modalBuscarOrdenes">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label class="col-4 col-form-label">Nombre Cliente SAP: </label>
						<div class="col-6">
							<input type="text" id="nombreCliente">
						</div>
					</div>
					<div class="row">
						<label class="col-4 col-form-label">Cantidad a Fabricar: </label>
						<div class="col-6">
							<input type="text" id="cantidadFabricar" style="display: inline-block">							
						</div>
					</div>
					<div class="row">
						<label class="col-4 col-form-label">Entregas Parciales Aceptadas: </label>
						<div class="col-6">
							<select name="entregasParciales" id="entregasParciales">
								<option value="" disabled selected>Seleccione</option>
								<option value="si">Si</option>
								<option value="no">No</option>
							</select>
						</div>
					</div>
					<div class="row">
						<label class="col-4 col-form-label">Código y Descripción del Producto: </label>
						<div class="col-6">
							<input type="text" value="" id="codigoProducto" placeholder="Codigo" style="width: 30%">
							<input type="text" value="" id="descripcionProducto" placeholder="Descripcion" style="width: 59%">
							<a href="#" data-toggle="modal" data-target="#myModalArticulos">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label class="col-4 col-form-label">Descripción (Detallado): </label>
						<div class="col-6">
							<input type="text" value="" id="productosFabricarNombre">
						</div>
					</div>
					<div class="row">
						<label class="col-4 col-form-label">Terminado Final: </label>
						<div class="col-6">
							<select name="" id="terminadoFinal">
								<option value=""></option>
								<option value="acero Negro">Acero Negro</option>
								<option value="acero galvanizado en frio">Acero Galvanizado en Frio</option>
								<option value="acero galvanizado en caliente">Acero Galvanizado en caliente</option>
								<option value="primario Rojo Oxido">Primario Rojo Oxido</option>
								<option value="pintado en blanco con electroestatica">Pintado en blanco con electroestatica</option>
								<option value="otros">Otros</option>
							</select>
						</div>
					</div>
					<div class="row">
						<label class="col-4 col-form-label">Observaciones: </label>
						<div class="col-6">
							<textarea id="observaciones" cols="78" rows="3"></textarea>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-lg-5">
					<div class="row">
						<label for="folio_ord_fab" class="col-4 col-lg-3 offset-lg-3 col-form-label">Folio:</label>
						<div class="col-6 col-md-8 col-lg-6">
							<input type="text" class="text-right" id="folio_ord_fab" style="width: 100%" value="<?php echo "$folio"?>" disabled>
						</div>
					</div>
					<div class="row">
						<label for="estatusOrdenesFabricacion" class="col-4 col-lg-3 offset-lg-3 col-form-label">Estatus:</label>
						<div class="col-6 col-md-8 col-lg-6">
							<input type="text" id="estatusOrdenesFabricacion" style="width: 100%" disabled>
						</div>
					</div>
					<div class="row">
						<label for="autorizadoPor" class="col-4 col-lg-3 offset-lg-3 col-form-label">Autorizado por:</label>
						<div class="col-6 col-md-8 col-lg-6">
							<input type="text" id="autorizadoPor" style="width: 100%" disabled>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-8 offset-4" style="margin-bottom: 10px">
							<button class="btn btn-sm btn-block" style="background-color: orange" id="btnAutorizarCoordinador" onclick="autorizar_orden_de_fabricacion()">Autorizar</button>
						</div>
						<div class="col-8 offset-4 soloCoordinadores" style="margin-bottom: 10px"	>
							<button class="btn btn-sm btn-block" style="background-color: orange" id="btnSolicitudEnPlanta" onclick="solicitud_en_planta_orden_de_fabricacion()">Solicitud en planta</button>
						</div>
						<div class="col-8 offset-4 soloCoordinadores" style="margin-bottom: 10px">
							<button class="btn btn-sm btn-block" style="background-color: orange" id="btnProcesoDeFabricacion" onclick="proceso_de_fabricacion_orden_de_fabricacion()">Proceso de fabricación</button>
						</div>
						<div class="col-8 offset-4 soloCoordinadores">
							<button class="btn btn-sm btn-block" style="background-color: orange" id="btnEntregaParcial" onclick="entrega_parcial_orden_de_fabricacion()">Entrega Parcial</button>
						</div>
					</div>									
					<div class="row soloVendedores">
						<label class="col-sm-8 offset-md-4 col-form-label" >Logistica:</label>
						<div class="col-sm-8 offset-md-4">
							<select id="logistica" style="height: 22px; width: 100%">
								<option value="" selected disabled>Selecciona una opción</option>
								<option value="clienteRecoge">1. Cliente recoge </option>
								<option value="flete">2. Flete</option>
								<option value="eypoRecoge">3. EYPO recoge</option>
								<option value="plantaEmbarca">4. Planta embarca</option>
							</select>
						</div>
					</div>
					<div class="row soloVendedores">
						<div class="col-sm-8 offset-md-4">
							<span>Comentarios extras:</span><br>
							<textarea id="comentariosLogistica" cols="40" rows="2"></textarea>
						</div>
					</div>
					<br>
					<div class="row soloVendedores">
						<div class="col-sm-8 offset-md-4">		
							<button class="btn btn-block btn-sm" style="background-color: orange;" id="btnEstatusCerrar" onclick="cerrar_orden_de_fabricacion()">CERRAR</button>
						</div>
					</div>							
				</div>			
			</div>
			<br>
			<div class="row">
				<div class="col-12">
					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Contenido</a>
							<a class="nav-item nav-link" id="nav-comentarios-tab" data-toggle="tab" href="#nav-entregas" role="tab" aria-controls="nav-profile" aria-selected="false">Entregas</a>													
						</div>
					</nav>
					<div class="tab-content" id="nav-tabContent">					
						<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="row">
								<div class="col-md-12">
									<table class="table table-bordered table-hover text-center table-sm">
										<thead>
											<tr>
												<td></td>
												<th>Pieza Muestra</th>
												<th>Ficha Técnica</th>
												<th>Dibujo</th>
												<th>Diagrama</th>
												<th>Otro</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th>Se entrega Para Fabricación</th>
												<td>
													<select name="" id="SePM">
														<option value="" selected disabled></option>
														<option value="no">No</option>
														<option value="si">Si</option>
													</select>
												</td>
												<td>
													<select name="" id="SeFt">
														<option value="" selected disabled></option>
														<option value="no">No</option>
														<option value="si">Si</option>
													</select>
												</td>
												<td>
													<select name="" id="SeDib">
														<option value="" selected disabled></option>
														<option value="no">No</option>
														<option value="si">Si</option>
													</select>
												</td>
												<td>
													<select name="" id="SeDia">
														<option value="" selected disabled></option>
														<option value="no">No</option>
														<option value="si">Si</option>
													</select>
												</td>
												<td>
													<select name="" id="SeOtr">
														<option value="" selected disabled></option>
														<option value="no">No</option>
														<option value="si">Si</option>
													</select>
												</td>
											</tr>
										</tbody>									
									</table>
									<br>
								</div>
							</div>
						</div>
						<div class="tab-pane fade show" id="nav-entregas" role="tabpanel" aria-labelledby="nav-home-tab">
							<div class="row">											
								<div class="col-md-5" style="margin-top: 25px;">
									<a href="#" id="plusNewEntregaParcial" onclick="agregar_nueva_entrega_tbl()"><i class="fas fa-plus-circle"></i>Agregar nueva entrega</a>
									<table class="table table-sm table-bordered table-stripe table-hover text-center" id="tblEntregaParcial">
										<thead>
											<tr>
												<th colspan=4>Tabla Entrega Parcial</th>
											</tr>
											<tr>
												<th>No°</th>
												<th>Piezas</th>
												<th>Fecha</th>												
											</tr>
										</thead>
										<tbody>
											<tr>
												<th scope="row">1</th>
												<td contenteditable="true" class="numPiezas"></td>
												<td class="fecha_td">
													<input type="date" style="border: none">
												</td>											
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-md-7">
									<div class="form-group">
										<label for="exampleInputEmail1">Observaciones de planta:</label>
										<textarea id="observacionesPlanta" rows="4" style="width: 100%"></textarea>
									</div>																								
								</div>
							</div>
							<div class="row">
								<div class="col-6">
									<br>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" id="EntregaTotal">
										<label class="form-check-label" for="defaultCheck1"><b>Entrega Total</b></label>										
										<input class="col-6 offset-1" type="date" id="dateEntregaTotal">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>					
			<div class="row">
				<div class="col-md-12">
					<br>
					<button class="btn btn-sm" style="background-color: orange" id="btnGuardar" onclick="guardar_orden_de_fabriccion()"> Guardar</button>
					<button class="btn btn-sm" style="background-color: orange" id="btnActualizar" onclick="actualizar_orden_de_fabricacion()"> Actualizar</button>
					<button class="btn btn-sm soloCoordinadores" style="background-color: orange" id="btnFinalizar" onclick="entrega_total_orden_de_fabricacion()">Finalizar</button>
					<button class="btn btn-sm" style="background-color: orange" id="btnCancelar" onclick="press_btn_cancelar()"> Cancelar</button>
					<br><br>
				</div>
			</div>
		</div>
		<?php include "footer.php"?>
		<script src="js/arcos_general.js"></script>
	</body>
 </html>
