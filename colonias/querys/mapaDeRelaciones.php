<?php
    include "../../db/Utils.php";
        // session_start();
        // $_SESSION['usuario']; 

        // $empleadoVentas = $_SESSION['usuario'];

        // $sql = "SELECT lastName, firstName, middleName FROM OHEM WHERE U_IV_EY_PV_User = '$empleadoVentas'";
        // $consulta = sqlsrv_query($conn, $sql);
        // $Row = sqlsrv_fetch_array($consulta);

        // $nombreCompleto = $Row['firstName']." ".$Row['middleName']." ".$Row['lastName'];

        // if (isset($_GET['FolioSAP'])) {
        //     $ofertaVenta = $_GET[ 'FolioSAP'];
        // } else {
        //     $ofertaVenta = 0;
        // }

        // if (isset($_GET['ofv'])) {
        //     $ordenVenta = $_GET['ofv'];
        // } else {
        //     $ordenVenta = 0;
        // }

    $fechaIni = $_GET['fi'];
    $fechaFin = $_GET['ff'];

                                
?>

<!DOCTYPE html>
<html lang="en"> 
<?php include "../header.html"?>		
	<body>		
        <?php include "../nav.php" ?>
		<?php include "../../modalQuerys.php" ?>
		<?php include "../../modales.php" ?>
        <div class="container formato-font-design" id="contenedorDePagina">                    
            <div class="row">
                <div class="col-md-12">

                    <section class="table-responsive">
                        <table class="table table-sm table-hover table-striped table-bordered" id="tblMapaRelaciones">
                            <thead>
                                <tr>
                                    <th>Código cliente</th>
                                    <th>Nombre del cliente</th>
                                    <th>Vendedor Asignado</th>
                                    <th>Orden de venta</th>
                                    <th>Fecha de la orden</th>
                                    <th>Entrega</th>
                                    <th>Fecha de la entrega</th>
                                    <th>Factura</th>
                                    <th>Fecha de la factura</th>
                                    <th>Tipo de Factura</th>
                                    <th>Nota de credito</th>
                                    <th>Solicitud de compra</th>
                                    <th>Fecha solicitud</th>
                                    <th>Pedido de compra</th>
                                    <th>Fecha Pedido de compra</th>                                
                                </tr>
                            </thead>
                            <tbody class="text-center"></tbody>
                        </table>
                    </section>
                </div>
            </div>
        </div>                    
        <?php include "../footer.html"; ?>
    </body>
    <script>

    var fi = '<?php echo $fechaIni ?>';
    var ff = '<?php echo $fechaFin ?>';

        $.ajax({
            url:"php/mapaRelaciones.php",
            type:"post",
            data:{ fechaInicio: fi, fechaFin: ff },					
        }).done(function (response) {	                        					
            $("#tblMapaRelaciones tbody").empty();						
            $("#tblMapaRelaciones tbody").append(response);	
                        
            $("#tblMapaRelaciones tbody tr").on('click', function(){
                var folioOrden = $(this).find('td').eq(3).text();
                location.href = 'ordenDeVenta.php?ofv='+folioOrden;						
            })					
        });	

        var gridViewScroll = null;
        window.onload = function () {
            gridViewScroll = new GridViewScroll({
                elementID: "tblMapaRelaciones",
                width: 850,
                height: 350,
                freezeColumn: true,
                freezeFooter: true,
                freezeColumnCssClass: "GridViewScrollItemFreeze",
                freezeFooterCssClass: "GridViewScrollFooterFreeze",
                freezeHeaderRowCount: 1,
                freezeColumnCount: 0,
                onscroll: function (scrollTop, scrollLeft) {
                    console.log(scrollTop + " - " + scrollLeft);
                }
            });
            gridViewScroll.enhance();
        }
        function getScrollPosition() {
            var position = gridViewScroll.scrollPosition;
            alert("scrollTop: " + position.scrollTop + ", scrollLeft: " + position.scrollLeft);
        }
        function setScrollPosition() {
            var scrollPosition = { scrollTop: 50, scrollLeft: 50};

            gridViewScroll.scrollPosition = scrollPosition;
        }

    </script>

    

</html>
