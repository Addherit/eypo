<?php  
    include "../db.Utils.php";
    $valor = $_POST['valorEscrito'];    
    $cont = 0;
    $sql="SELECT TOP 50 ent.FolioSAP, ent.FechaContabilizacion, ent.Estatus, soc.Nombre FROM EYPO.dbo.IV_EY_PV_EntregasVentaCab ent       
        INNER JOIN EYPO.dbo.IV_EY_PV_SociosNegocios soc
        ON soc.CodigoSN =  ent.CodigoSN
        WHERE FechaContabilizacion = '$valor' AND SerieNombre != 'COLONIAS'
        ORDER BY FechaContabilizacion DESC";
    $consulta = sqlsrv_query($conn, $sql);

    WHILE ($row = sqlsrv_fetch_array($consulta)){
        $cont++;
        echo '
            <tr>
                <td width="70px">'.$cont.'</td>
                <td width="130px">'.utf8_encode($row['FolioSAP']).'</td>
                <td width="310px">'.utf8_encode($row['Nombre']).'</td>
                <td width="150px">'.$row['FechaContabilizacion']->format('Y-m-d').'</td>                
                <td width="110px">'.utf8_encode($row['Estatus']).'</td>
            </tr>
        ';               
    }
?>