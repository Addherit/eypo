<?php 
    include "../db/Utils.php";
    $folioSAP = $_POST['folio'];
    $primerRegistro = $_POST['pr'];
    $ultimoRegistro = $_POST['ur'];
    $condicion = $_POST['con'];  
    $tipo = $_POST['tipo'];

    if($folioSAP > $ultimoRegistro OR $folioSAP < $primerRegistro){
        $Array2 = [
            'respuesta' => 1
        ];
        echo json_encode($Array2);
    } else {

        do{

            $sql="SELECT empVC.NombreEmpleadoVC, SOC.Nombre, soc.PersonaContacto, facCli.* FROM EYPO.dbo.IV_EY_PV_FacturasClientesCab facCli
            LEFT JOIN EYPO.dbo.IV_EY_PV_SociosNegocios soc ON facCli.CodigoSN = soc.CodigoSN
            LEFT JOIN EYPO.dbo.IV_EY_PV_EmpleadosVentasCompras empVC ON facCli.CodigoEmpleadoVC = empVC.CodigoEmpleadoVC
            WHERE FolioSAP = '$folioSAP' AND TipoFactura = '$tipo' AND SerieNombre != 'COLONIAS'";
            $consulta = sqlsrv_query($conn, $sql); 
            $row = sqlsrv_fetch_array($consulta);
            $folioInternoCAB = $row["FolioInterno"];


            switch ($condicion) {
                case 'adelante':
                    $folioSAP++;
                    break;
                
                case 'atras': 
                    $folioSAP--;
                    break; 
            }
        }while(!$row);

        $sql2 ="SELECT det.*, stock.EnStock, stock.Comprometido, stock.Solicitado FROM EYPO.dbo.IV_EY_PV_FacturasClientesDet det
        LEFT JOIN EYPO.dbo.IV_EY_PV_Stock stock ON det.CodigoArticulo = stock.CodigoArticulo AND det.Almacen = stock.CodigoAlmacen
        WHERE FolioInterno = '$folioInternoCAB'";
        $consulta2 = sqlsrv_query($conn, $sql2);
        $array_det=[]; 
        $cont=0; 
        WHILE ($RowDet = sqlsrv_fetch_array($consulta2)){
            $cont++;          
            $linea = '<tr>
                <th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>
                <td>' . $cont . '</td>
                <td class="codAticulo">'.$RowDet['CodigoArticulo'].'</td>
                <td class="narticulo">'.$RowDet['NombreArticulo'].'</td>   
                <td class="cantidad" contenteditable="true">'.number_format($RowDet['Quantity'],0,'.',',').'</td>   
                <td class="precioUnitario">'.number_format($RowDet['PrecioUnitario'],2,'.',',').'</td>   
                <td class="porcentajeDescuento">'.number_format($RowDet['PorcentajeDescuento'],2,'.',',').'</td>   
                <td class="impuesto">'.number_format($RowDet['Impuestos'],2,'.',',').'</td>
                <td class="codigoImpuesto">'.$RowDet['CodigoImpuesto'].'</td>
                <td class="sujetoRetencion">'.$RowDet['AplicaRetencion'].'</td>                            
                <td class="total">'.number_format($RowDet['TotalLinea'],2,'.',',').'</td>
                <td class="unidadSAT">'.$RowDet['UnidadMedidaSAT'].'</td>
                <td class="almacen">'.$RowDet['Almacen'].'</td>
                
                <td class="codigoImpuesto">'.$RowDet['ComentarioPartida1'].'</td>
                <td class="sujetoRetencion">'.$RowDet['ComentarioPartida2'].'</td>
                <td class="total">'.number_format($RowDet['EnStock'],2,'.',',').'</td>                  
                <td class="unidadSAT">'.number_format($RowDet['Comprometido'],2,'.',',').'</td> 
                <td class="almacen">'.number_format($RowDet['Solicitado'],2,'.',',').'</td>   
                </tr>';
                    
            array_push($array_det, $linea);
        }

        $arrayContent = [

            'respuesta' => 2,
            'cliente' => utf8_encode($row['CodigoSN']),
            'nombreCliente' => utf8_encode($row['Nombre']),
            'personaContacto' => utf8_encode($row['PersonaContacto']), 
            'tipoMoneda' => utf8_encode($row['Moneda']), 
            'formaPago' => utf8_encode($row['DescFormaPago']), 

            // 'ndoc' => ndoc, 
            'cdoc' => $row['FolioSAP'],
            'estado' => $row['Estatus'],
            'fconta' => $row['FechaContabilizacion']->format('Y-m-d'),
            'fdoc' => $row['FechaDocumento']->format('Y-m-d'),
            'fven' => $row['FechaVencimiento']->format('Y-m-d'),
            'usoPrincipal' => $row['UsoCFDi'],
            'metodoPago' => utf8_encode($row['MetodoPago']),
            'empleado' => utf8_encode($row['NombreEmpleadoVC']),
            'proyectoSN'  =>  utf8_encode($row['Proyecto']),
            'ventasAdic' =>  utf8_encode($row['VentasAdic']),
            'promotor' =>  utf8_encode($row['NombrePromotor']),
            'promotorDeVenta' =>  utf8_encode($row['PromotorVentas']),
            'comentarios' =>  utf8_encode($row['Comentarios']),
            'totalAntesDescuento' => number_format($row['SubTotalDocumento'],2,'.',','),
            // 'descNum' 
            'descAplicado'=> number_format($row['ImporteDescuento'], 2, '.', ','),            
            'redondeo' => number_format($row['Redondeo'], 2, '.', ','),
            'impuestoTotal' => number_format($row['SumaImpuestos'], 2, '.', ','),
            'totalDelDocumento' => number_format($row['TotalDocumento'], 2, '.', ','),
            'importeAplicado'  => number_format($row['Pagado'], 2, '.', ','),
            'soloVencido' => number_format($row['Saldo'], 2, '.', ','),
            'rutaArchivos' => $row['RutaArchivos'],
            'array' => $array_det,

        ];

        echo json_encode($arrayContent);
    }
?>