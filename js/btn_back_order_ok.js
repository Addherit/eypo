
var fechaIni = $("#fechaIni").val()
var fechaFin = $("#fechaFin").val()
var nombreCliente = $("#nombreCliente").val()
var codigoCliente = $("#codigoCliente").val()

$.ajax({
    url:"querys/backOrder.php",
    type:"post",
    data:{ 
        fechaIni : fechaIni,
        fechaFin : fechaFin,
        nombreCliente : nombreCliente,
        codigoCliente : codigoCliente,
    },
    dataType: 'JSON',
}).done(function (back_order) {	
    
    $("#tblbackOrderTable tbody").empty()
    var tr_order = "";

    if(back_order.length > 0) {
        back_order.forEach((element, index) => {
            index++
            tr_order += `<tr>
                <td>${index}</td>
                <td>${element.Catalogo}</td>
                <td>${element.Descripcion}</td>
                <td>${element["Almacen Destino"]}</td>            
                <td>${element["Cantidad Comprada"]}</td>
                <td>${element["Cantidad Abierta"]}</td>
                <td>${element["Existencia Almacen Destino"]}</td>
                <td>${element["Fecha Recepcion"]}</td>
                <td>${element["Comentarios Recepcion"]}</td>
                <td>${element["Codigo Cliente"]}</td>
                <td>${element["Nombre Cliente"]}</td>
                <td>${element["Orden de Venta"]}</td>
                <td>${element.docnum}</td>
                <td>${element["Codigo Proveedor"]}</td>
                <td>${element["Nombre Proveedor"]}</td>
                <td>${element.Referencia}</td>
                <td>${element["Fecha Orden de Compra"].date}</td>
            </tr>`


        })
    } else {
        tr_order += '<tr><td colspan="17">No existe registro alguno con los datos proporcionados</td></tr>' 
    }
    $("#tblbackOrderTable tbody").append(tr_order);			
});		