<?php
	include "conexioncajero.php";

	session_start();
	if (!isset($_SESSION["usuario"]))
   {
      header("location: login.php");
   }
   if (!isset($_SESSION["usuarioN"]))
   {
     $_SESSION["logged_in"]=0;

   } else   {
	$_SESSION["logged_in"]=1;
  }

	$_SESSION['usuario'];
	$empleadoVentas = $_SESSION['usuario'];
	$hoy = date("Y-m-d");
	$mesDespues = date( "Y-m-d", strtotime( "$hoy +1 month" ) );
	$quincena = date( "Y-m-d", strtotime( "$hoy +15 days" ) );

	$sql = "SELECT lastName, firstName, middleName FROM EYPO.dbo.OHEM WHERE U_IV_EY_PV_User = '$empleadoVentas'";
	$consulta = sqlsrv_query($conn, $sql);
	while ($Row = sqlsrv_fetch_array($consulta)) {
		$nombre = $Row['firstName'];
		$sNombre = $Row['middleName'];
		$apellido = $Row['lastName'];
	}
	$nombreCompleto = $nombre . " " . $sNombre . " " . $apellido;



	?>

	<!DOCTYPE html>
	<html>
	<?php include "header.php"; ?>
<!--	<body onload="cargarDeposito()">-->
	<body >

		<?php include "nav.php"; ?>
		<?php include "modalQuerys.php"; ?>
		<?php include "modales.php"; ?>

		<div class="container" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Administración de Despositos a Colaboradores</h3>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">

				</div>
			</div>
			<div class="row datosEnc" style="font-size: .7rem">
				<div class="col-md-6">



				</div>
			</div>
			<br>

			<div class="row" style="font-size: .7rem">
			<br>
				<div class="col-md-6">
					<div class="row">
						<label class="col-sm-3 col-form-label" >Id Cajero:</label>
						<div class="col-sm-4">
							<input type="text" class="" name="codcajero" id="codcajero" style="width: 70%">
							<a id="btnCajero" href="#" data-toggle="modal" data-target="#myModalCajero">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cajero"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Nombre Cajero:</label><br>
						<div class="col-sm-4">
							<input type="text" class="" name="NombreC" id="NombreC" style="width: 70%">
							<a href="#" data-toggle="modal" data-target="#myModalCajero" >
								<i class="fas fa-search" style="color: #57b4ea" title="Búsqueda Nombre"></i>
							</a>
						</div>
					</div>
				</div>
			<br>

				<div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Depositos por Pagar</a>
						</li>


					</ul>
					<div id="auto_content">

							</div>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
							<?php include "tablaNavNomina/general.php"; ?>

						</div>
					</div>
				</div>

		</div>
	</div>

		<?php include "footer.php"; ?>
	</body>
<script>
	if ( window.history.replaceState ) {
		window.history.replaceState( null, null, window.location.href );
	}
	function cambiar(){
		var pdrs = document.getElementById('file-upload').files[0].name;
		document.getElementById('info').innerHTML = pdrs;
	}
	function cargarDeposito ($rid){
		var id = $("#codcajero").val();
		var rid = $rid;
		console.log(rid);
		$.ajax({
			url: 'buscadorTotalesDepositos.php',
			type: 'post',
			data: {"code": id,"rd":rid},
			success: function(response){
				$("#detallenuevo tbody").empty();
				$("#detallenuevo tbody").append(response);
				}
		});

		var check = '<?php echo $_SESSION["logged_in"]; ?>';

		if(check == 1)
		{
			document.getElementById('Autorizacion').style.display='none';
			document.getElementById('Depositos').style.display='block';
		}
		else
		{
			document.getElementById('Autorizacion').style.display='block';
			document.getElementById('Depositos').style.display='none';
		}
	}

	$(document).on('click', '#eliminarFila', function (event) {

		event.preventDefault();
		var currentRow=$(this).closest("tr");
		var valorEscrito=currentRow.find("td:eq(1)").text();
		$(this).closest('tr').remove();
		respuestas(valorEscrito);
		// $.ajax({
				// url:"eliminadeposito.php",
				// type:"POST",
				// data:{
					// valor:valorEscrito,
				// },

			// }).done(function (response) {

				// alert(valorEscrito);
		// });

		// $.ajax({
		// url: 'EliminaDeposito.php',
		// type: 'Post',
		// data: {valor: valorEscrito},
		// success: function(response){

				// alert(response);
			// }
		// });
	});
	function respuestas(Id){

		$.ajax({
			type: "post",
			url: "eliminadeposito.php?valor="+Id,

			success  : function(data){
					//window.location.href="nomina.php";
			},
			error    : function(){
					alert("Could not ");
					alert("EliminaDeposito.php?valor="+Id);
			}

		});
	}
	function exportTableToExcel(tableID, filename = ''){
				var downloadLink;
				var dataType = 'application/vnd.ms-excel';
				var tableSelect = document.getElementById(tableID);

				var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

				// Specify file name
				filename = filename?filename+'.xls':'movimientos.xls';

				// Create download link element
				downloadLink = document.createElement("a");

				document.body.appendChild(downloadLink);

				if(navigator.msSaveOrOpenBlob){
					var blob = new Blob(['\ufeff', tableHTML], {
						type: dataType
					});
					navigator.msSaveOrOpenBlob( blob, filename);
				}else{
					// Create a link to the file
					downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

					// Setting the file name
					downloadLink.download = filename;

					//triggering the function
					downloadLink.click();
				}
			}

			function exportToExcel(tableID, filename = ''){
			var htmls = "";
			var uri = 'data:application/vnd.ms-excel;base64,';
			var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
		var base64 = function(s) {
			return window.btoa(unescape(encodeURIComponent(s)))
		};

	var format = function(s, c) {
		return s.replace(/{(\w+)}/g, function(m, p) {
			return c[p];
		})
	};
				var tableSelect = document.getElementById(tableID);

				var tableHTML = tableSelect.outerHTML;
	htmls = tableHTML;

	var ctx = {
		worksheet : 'Worksheet',
		table : htmls
	}


	var link = document.createElement("a");
	link.download = "Movimientos.xls";
	link.href = uri + base64(format(template, ctx));
	link.click();
}

	//codigo del cajero
	$("#buscadorCajero").keyup(function(){
		var valorEscrito = $("#buscadorCajero").val();
		if (valorEscrito) {
			$.ajax({
				url: 'buscadorConsultaCajero.php',
				type: 'post',
				data: {valor: valorEscrito},
				success: function(resp){


					$("#tblcajero tbody").empty();
					$("#tblcajero tbody").append(resp);

					$("#tblcajero tr").on('click',function(){


						if (!$('#BackOrderVentas').is(':visible')) {
							var codigo = $(this).find('td').eq(1).text();
							var name = $(this).find('td').eq(2).text();
							var id = $(this).find('td').eq(4).text();



							agregarCajero(codigo, name,id);
							$("#tblcajero tbody").empty();
							$("#buscadorcajero").val("");

							$("#nombreCajero").val(name);
							$("#codigoCajero").val(codigo);
							$("#myModalCajero").hide();

						} else {
							var codigo = $(this).find('td').eq(1).text();
							var name = $(this).find('td').eq(2).text();

							$("#nombreCajero").val(name);
							$("#codigoCajero").val(codigo);
							$("#myModalCajero").hide();


							}
						});
					},
				});
		} else {
			$("#tblcajero tbody").empty();
		}
	});

	$("#myModalCajero").on('click',function() {
		$("#buscadorCajero").val("");
	});



	function agregar ($code, $name){
		var code = $code;
		var name = $name;
		$("#codcajero").val(code);
		$("#NombreC").val(name);
		$('#myModalCajero').modal('hide');
		$.ajax({
			url: 'consultaPersonaContacto.php',
			type: 'post',
			data: {code: code},
			success: function(response){
				$("#listcontactos").append(response);
			}
		});
	}

	function agregarCajero ($code, $name,$id){
		var code = $code;
		var name = $name;
		var id = $id;
		console.log(id);
		$("#codcajero").val(code);
		$("#NombreC").val(name);
		$('#myModalCajero').modal('hide');
		cargarDeposito (id);
	}





</script>
</html>
