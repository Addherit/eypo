document.addEventListener('DOMContentLoaded', () => {
    get_ultimo_primero_folioSAP()
    

    switch (localStorage.getItem("codigo_posicion")) {
        case '36':
            document.querySelector("#btnCrear").hidden = false
            break;
        case '47':
            document.querySelector("#btnCrear").hidden = false
            break;
        default:
            document.querySelector("#btnCrear").hidden = true
            break;
    }    
    document.querySelector("#empleado").value = localStorage.getItem("departamento")
    document.querySelector("#mensajes").hidden = true
    document.querySelector("#consultaQuery").hidden = true

})

document.querySelector("#consultaPrimerRegistro").addEventListener('click', () => { consultaClickFactura(document.querySelector("#primer-registro").value, 'nada') })
document.querySelector("#consultaUltimoRegistro").addEventListener('click', () => { (consultaClickFactura(document.querySelector("#ultimo-registro").value, 'nada')) })
document.querySelector("#consulta1Atras").addEventListener('click', () => { consultaClickFactura(parseInt(document.querySelector("#cdoc").value) - 1, 'atras') })
document.querySelector("#consulta1Adelante").addEventListener('click', () => { consultaClickFactura(parseInt(document.querySelector("#cdoc").value) + 1, 'adelante') })

document.querySelector("#btnBuscarGRAL").addEventListener('click', ()=> {
    $("#modalFacturaComun").modal('show')
}) 

function consultaClickFactura(folio, condicion) {
    
    const formData = new FormData()
    formData.append('folio', folio)
    formData.append('con', condicion)
    formData.append('tipo', 'NORMAL')

    fetch('php/rellenarInputsConClick.php', {method: 'POST', body: formData})
    .then(data => data.json())
    .then(data => {		
        mostrarValoresDeBusquedaEnInputs(data)
    })
}

function operaciones_generales(fila) {
    fila.children[9].textContent = parseInt(fila.children[5].textContent) * parseInt(fila.children[4].textContent)                            
    calcularTotalAntesDescuento();
    calcularImpuestoTotal();
    sumarTotalDocumento();						    	
}

function get_ultimo_primero_folioSAP() {

    fetch('php/get_max_min_folioSAP.php')
    .then(data => data.json())
    .then(data => {
        document.querySelector("#cdoc").value = parseInt(data.ultimo) + 1
        document.querySelector("#primer-registro").value = data.primero
        document.querySelector("#ultimo-registro").value = data.ultimo
    })
}


function mostrarValoresDeBusquedaEnInputs(data){

    $("#cliente").val(data.cliente)
    $("#nCliente").val(data.nombreCliente)
    $("#personaContacto").val(data.personaContacto)
    $("#tipoMoneda").val(data.tipoMoneda)
    $("#formaPago").val(data.formaPago)
                    
    $("#cdoc").val(data.cdoc)
    $("#estado").val(data.estado)
    $("#fconta").val(data.fconta)
    $("#fdoc").val(data.fdoc)
    $("#fven").val(data.fven)
    $("#usoPrincipal").val(data.usoPrincipal)
    $("#metodoPago").val(data.metodoPago)

    $("#empleado").val(data.empleado)
    $("#proyectoSN").val(data.proyectoSN)
    $("#ventasAdic").val(data.ventasAdic)
    $("#promotor").val(data.promotor)
    $("#promotorDeVenta").val(data.promotorDeVenta)
    $("#comentarios").val(data.comentarios)

    $("#totalAntesDescuento").val(data.totalAntesDescuento)    
    $("#descAplicado").val(data.descAplicado)
    $("#redondeo").val(data.redondeo)
    $("#impuestoTotal").val(data.impuestoTotal)
    $("#totalDelDocumento").val(data.totalDelDocumento)
    $('#monedaVisor').val($("#tipoMoneda").val()).prop("readonly", true);
    $("#importeAplicado").val(data.importeAplicado)
    $("#soloVencido").val(data.soloVencido)
    $("#rutaArchivos").val(data.rutaArchivos)

    $("#tblFactuaComun tbody").empty();				 
    for (x= 0; x<data.array.length; x++ ){
        $("#tblFactuaComun tbody").append(data.array[x]);
    } 
}

document.querySelector("#bucardorFacturaComun").addEventListener('keyup', () => {

    const formData = new FormData()
    formData.append("valorEscrito", document.querySelector('#bucardorFacturaComun').value)
    formData.append("tipo", 'NORMAL')

    fetch('php/consultaGRALFacturaComun.php', {method: 'POST', body: formData})
    .then(data => data.text())
    .then(data => {
        document.querySelector("#tblFacturaComun tbody").innerHTML = data       
    })
})

document.querySelector("#bucardorFacturaComunFecha").addEventListener('change', () => {
    const formData = new FormData()
    formData.append("valorEscrito", document.querySelector('#bucardorFacturaComunFecha').value)
    formData.append("tipo", 'NORMAL')

    fetch('php/consultaGRALFacturaComunFecha.php', {method: 'POST', body: formData})
    .then(data => data.text())
    .then(data => {
        document.querySelector("#tblFacturaComun tbody").innerHTML = data       
    })
})

function inspeccionar_fila_desde_modal(fila) {
    $("#modalFacturaComun").modal('hide');						
    consultaClickFactura(fila.children[1].textContent, 'nada');
}   



$(document).on('click', '#eliminarFila', (event) => {
    event.preventDefault();
    $(this).closest('tr').remove();
    calcularTotalAntesDescuento();
    calcularImpuestoTotal();
    sumarTotalDocumento();
});

function getDetalle() {
    array_General = new Array();
    var array_codigo = new Array();
    var array_articulo = new Array();				
    var array_cantidad = new Array();
    var array_precioPorUnidad = new Array();
    var array_descuento = new Array();
    var array_indImpuesto = new Array();
    var array_sujetoRetencionImpuesto = new Array();
    var array_total = new Array();
    var array_unidadSAT = new Array();
    var array_almacen = new Array();

    $('.codAticulo').each(() => { array_codigo.push($(this).text()) });
    $('.narticulo').each(() => { array_articulo.push($(this).text()) })
    $('.cantidad').each(() => { array_cantidad.push($(this).text()) })
    $('.precioUnitario').each(() => { array_precioPorUnidad.push($(this).text()) })
    $('.porcentajeDescuento').each(() => { array_descuento.push($(this).text()) })
    $('.impuesto').each(() => { array_indImpuesto.push($(this).text()) })
    $('.codigoImpuesto').each(() => { array_sujetoRetencionImpuesto.push($(this).text()) })
    $('.total').each(() => { array_total.push($(this).text()) })
    $('.unidadSAT').each(() => { array_unidadSAT.push($(this).text()) })
    $('.almacen').each(() => { array_almacen.push($(this).text()) })

    array_General.push(array_codigo, array_articulo, array_cantidad, array_precioPorUnidad, 
    array_descuento, array_indImpuesto, array_sujetoRetencionImpuesto, array_total, array_unidadSAT, array_almacen)

    return array_General;	

}

function getValues() {
    var arrayValores = new Array();

    arrayValores.push($("#cliente").val())
    arrayValores.push($("#nCliente").val())
    arrayValores.push($("#personaContacto").val())
    arrayValores.push($("#tipoMoneda").val())
    arrayValores.push($("#formaPago").val())

    arrayValores.push($("#ndoc").val())
    arrayValores.push($("#cdoc").val()) //6
    arrayValores.push($("#estado").val())
    arrayValores.push($("#fconta").val())
    arrayValores.push($("#fdoc").val())
    arrayValores.push($("#fven").val())
    arrayValores.push($("#usoPrincipal").val())
    arrayValores.push($("#metodoPago").val()) //12

    arrayValores.push($("#empleado").val())
    arrayValores.push($("#proyectoSN").val())
    arrayValores.push($("#ventasAdic").val())
    arrayValores.push($("#promotor").val())
    arrayValores.push($("#promotorDeVenta").val())
    arrayValores.push($("#comentarios").val()) //18
    
    arrayValores.push($("#totalAntesDescuento").val())					
    arrayValores.push($("#descAplicado").val())
    arrayValores.push($("#redondeo").val())
    arrayValores.push($("#impuestoTotal").val())
    arrayValores.push($("#totalDelDocumento").val())
    arrayValores.push( $("#importeAplicado").val())
    arrayValores.push($("#soloVencido").val())
    arrayValores.push('NORMAL')
    
    return arrayValores;
}

document.querySelector("#btnCrear").addEventListener('click', () => {
    $.ajax({
        url:'insertarFacturaComun.php',
        type:'post',
        data:{arrayCabecera: getValues(), arrayDetalle: getDetalle() },
        success: (response) => {

            if(response === "exito"){
                alert("guardado con éxito");
                setTimeout('document.location.reload()',2000);
            }else{
                alert(response);													
            }
        }
    })
});

document.querySelector("#btnPdf").addEventListener('click', () => {
    
    var origen = $("#rutaArchivos").val()+".pdf";
    var nuevoOrigen = origen.replace(/\\/g, "\\\\")	
    var ext = '.pdf';				
    var nombre = Math.floor(Math.random() * 110000000);
    $.ajax({
        url:'copiarArchivo.php',
        type: 'post',						
        data: {ext:ext, origen: nuevoOrigen,  nombre:nombre },
        success: function (response) {
            console.log(response);							
        } 	
    });
    window.location = 'temporales/'+nombre+'.pdf';																													
});	

$("#btnxlm").on('click', function() {
    
    var origen = $("#rutaArchivos").val()+".xml";
    var nuevoOrigen = origen.replace(/\\/g, "\\\\")	
    var ext = '.xml';	
    var nombre = Math.floor(Math.random() * 110000000);								
    
    $.ajax({
        url:'copiarArchivo.php',
        type: 'post',					
        data: {ext: ext, origen: nuevoOrigen,  nombre:nombre },
        success: function (response) {
            console.log(response);							
        } 	
    });
    window.location = 'temporales/'+nombre+'.xml';																													
});	

function calcularTotalAntesDescuento() {
    var totalDeuda = 0;
    $("#tblFactuaComun .total").each(function(){
        totalDeuda += parseFloat($(this).html()) || 0;
    });
    $("#totalAntesDescuento").val(totalDeuda.toFixed(2));
}

function calcularImpuestoTotal() {

    var subTotal = $("#totalAntesDescuento").val();
    var impuesto = subTotal * .16;								
    $("#impuestoTotal").val(impuesto.toFixed(2));
}

function sumarTotalDocumento() {
    var subTotal = parseFloat($("#totalAntesDescuento").val());							
    var descuento = parseFloat($("#descAplicado").val());			
    var impuestos = parseFloat($("#impuestoTotal").val());					
    var totalDocumento = (subTotal + impuestos) - descuento;
    
    $("#totalDelDocumento").val(totalDocumento.toFixed(2));
    var numAlet = numeroALetras($("#totalDelDocumento").val());
    $("#numLetra").val(numAlet);
}


    

