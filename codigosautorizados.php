	<?php
	include "conexion.php";
		session_start();
	?>

	<!DOCTYPE html>
	<html>
		<?php include "header.php"; ?>
	<body onload="cargarProyectos()">
	
		<?php include "nav.php"; ?>
		<?php include "modalQuerys.php"; ?>
		<?php include "modales.php"; ?>
		
		<div class="container" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Codigos por Autorizar</h3>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">
					
				</div>
			</div>
			<div class="row datosEnc" style="font-size: .7rem">
				<div class="col-md-6">
					
					
		
				</div>
			</div>
			<br>

			<div class="row" style="font-size: .7rem">
				<div class="col-md-12">
					<div class="row">
							
					  		<div class="col-md-12">
					  			<table class="table-bordered table-editable table-hover table-striped table-responsive table" width="100%" id="listaespera" >
					        		<thead>
					        			<tr class="encabezado" style="background-color: #005580; color:white;" >
											<th>Ver</th>
											<th>No.</th>
											<th>Marca</th>
											<th>Modelo</th>
					        				<th>CodigoReal</th>
											<th>CodigoProtegido</th>
					        				<th>Tipo</th>
											<th>Potencia</th>
											<th>Optica</th>
					        				<th>Usuario Solicitud</th>
											<th>Fecha Solicitud</th>
											<th>Estatus</th>
											<th style="display:none">Siglas</th>
											
					        			</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<th>
											
											
											<a href="#" style="color: green" id="eliminarFila"  data-toggle="modal" data-target="#myModalCajero"><i class="fas fa-folder-open"></i></a></th>
											<td class="Id"></td> 
											<td class="Marca"></td> 
								            <td class="Modelo"></td>
											<td class="CodigoReal"></td>
								            <td class="CodigoProtegido"></td>
											<td class="Tipo"></td>
											<td class="Potencia"></td>
											<td class="Optica"></td>
											<td class="UsuarioSolicitud"></td>
											<td class="FechaSolicitud"></td>
											<td class="Estatus"></td>
											<td class="Siglas" style="display:none"></td>
										</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>
				</div>
			
		</div>
	</div>

		<?php include "footer.php"; ?>
	</body>
		<script>
				function cargarProyectos (){
				$.ajax({
					url: 'OFVConsultasMaster/buscadorcodigosporautorizar.php',
					type: 'post',
					success: function(response){
						$("#listaespera tbody").empty();
						$("#listaespera tbody").append(response);
                     }
				});
				
				
					
			}
			 if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
	function postForm(path, params, method) {
    method = method || 'post';

    var form = document.createElement('form');
    form.setAttribute('method', method);
    form.setAttribute('action', path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}
			$(document).on('click', '#eliminarFila', function (event) {
				
				  //event.preventDefault();
				  
				  var currentRow=$(this).closest("tr"); 
				  var id=currentRow.find("td:eq(0)").text();
				  var siglas=currentRow.find("td:eq(11)").text();
				  
				  postForm('CodigoProtegido.php', {valor: siglas, id:id, aut:'1'});
				 // $(this).closest('tr').remove();
				    //respuestas(valorEscrito);
					// $.ajax({
							// url:"eliminadeposito.php",
							// type:"POST",
							// data:{ 
								// valor:valorEscrito,	
							// },
							
						// }).done(function (response) {	
											
						   // alert(valorEscrito);
					// });	
					  
					// $.ajax({
					// url: 'EliminaDeposito.php',
					// type: 'Post',
					// data: {valor: valorEscrito},
					// success: function(response){
						 
						 // alert(response);
						// }
					// });
					
				
			});
	function cambiar(){
    var pdrs = document.getElementById('file-upload').files[0].name;
    document.getElementById('info').innerHTML = pdrs;
}
			function respuestas(Id){
								 
				   $.ajax({
						type: "post",
						url: "eliminadeposito.php?valor="+Id,

						success  : function(data){
							 //window.location.href="nomina.php";
						},
						error    : function(){
							 alert("Could not ");
							 alert("EliminaDeposito.php?valor="+Id);
						}

					});
			}
			
			
		
		</script>
	</html>
