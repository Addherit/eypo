<!--Modal BackOrderVentas-->
<div class="modal fade modal2doNivel" id="BackOrderVentas">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Back Order Ventas</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="font-size: .8rem">
                <h4 style="font-size: 1.1rem">Consulta: Criterios de selección</h4><br><br>
                <div class="row">

                    <label class="col-7 col-form-label" require>Fecha Contabilización igual a</label>
                    <div class="col-5">
                        <input type="date" style="width: 100%; height:23px" id="fechaIni">
                    </div>
                </div>
                <div class="row">
                    <label class="col-7 col-form-label" require>Fecha Entrega igual a</label>
                    <div class="col-5">
                        <input type="date" style="width: 100%; height:23px" id="fechaFin">
                    </div>
                </div>
                <div class="row">
                    <label class="col-7 col-form-label" require>Nombre de cliente/proveedor igual a</label>
                    <div class="col-5">
                        <input type="text" style="width: 90%; height:23px" id="nombreCliente_query">
                        <a id="btnCliente" href="#" data-toggle="modal" data-target="#myModal">
                            <i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
                        </a>
                    </div> 
                </div>
                <div class="row">
                    <label class="col-7 col-form-label" require>Código de cliente igual a</label>
                    <div class="col-5">
                        <input type="text" style="width: 90%; height:23px" id="codigoCliente_query">
                        <a id="btnCliente" href="#" data-toggle="modal" data-target="#myModal">
                            <i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
                        </a>
                    </div>
                </div>               
            </div>
            <div class="modal-footer" style="border-top: none">
                <button class="btn btn-sm cancelarModalesQuery" style="background-color: orange" title="Cancelar" data-dismiss="modal" data-target="#modalQuery" data-toggle="modal" >Regresar</button>
                <button class="btn btn-sm" style="background-color: orange" data-dismiss="modal" id="btnBackOrderOk" onclick="query_back_orders()">Ok</button>
            </div>
        </div>
    </div>
</div>


<!--Modal Comisiones-->
<div class="modal fade" id="ComisionesVentas">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Comisiones</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="font-size: .8rem">
                <h4 style="font-size: 1.1rem">Consulta: Criterios de selección</h4>
                <br><br>
                <div class="form-group row">
                    <label class="col-7 col-form-label">Fecha de contabilización superior/igual</label><div class="col-5">
                        <input type="date" style="width: 100%; height:23px" id="fechaSuperior">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-7 col-form-label">Fecha contabilización Menor o igual</label>                 <div class="col-5">
                        <input type="date" style="width: 100%; height:23px" id="fechaMenor">
                    </div>
                </div>
                <br><br>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm cancelarModalesQuery" style="background-color: orange" title="Cancelar" data-dismiss="modal" data-target="#modalQuery" data-toggle="modal">Regresar</button>
                <button class="btn btn-sm" style="background-color: orange" data-dismiss="modal" id="btnComisionesVentas2doNivel" onclick="query_comisiones()">OK</button>
            </div>
        </div>
    </div>
</div>

<!--Modal Entradas Articulos-->
<div class="modal fade" id="EntradasVentas">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Entradas Articulos</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="font-size: .8rem">
                <!--escribir contenido de modal  -->
                <h4 style="font-size: 1.1rem">Consulta: Criterios de selección</h4><br><br>
                <div class="form-group row">
                    <label class="col-7 col-form-label">Fecha de contabilización superior/igual</label>                 <div class="col-5">
                        <input type="date" style="width: 100%; height:23px" id="fechaContabilizacionSuperior">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-7 col-form-label">Fecha de contabilización Menor/igual</label>                 <div class="col-5">
                        <input type="date" style="width: 100%; height:23px" id="fechaContabilizacionMenor">
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border-top: none">
                <button class="btn btn-sm cancelarModalesQuery" style="background-color: orange" title="Cancelar"   data-dismiss="modal" data-target="#modalQuery" data-toggle="modal">Regresar</button>
                <button class="btn btn-sm" style="background-color: orange" data-dismiss="modal" id="btnEntregaArticulos" onclick="entrda_articulos()">OK</button>
            </div>
        </div>
    </div>
</div>

<!--Modal Oferta de Venta línea especial cliente -->
<div class="modal fade" id="OfertaVentaCliente">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Oferta de Venta línea especial cliente</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="font-size: .8rem">
                <h4 style="font-size: 1.1rem">Consulta: Criterios de selección</h4>
                <div class="row">
                    <label class="col-7 col-form-label">Código cliente/proveedor</label>
                    <div class="col-5">
                        <input type="text" style="width: 90%; height:23px" id="clienteProveedor">
                        <a id="btnCliente" href="#" data-toggle="modal" data-target="#myModal">
                            <i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border-top: none">
                <button class="btn btn-sm cancelarModalesQuery" style="background-color: orange" title="Cancelar"   data-dismiss="modal" data-target="#modalQuery" data-toggle="modal">Cancelar</button>
                <button class="btn btn-sm" style="background-color: orange" data-dismiss="modal" id="btnOFVCliente" onclick="query_oferta_de_venta_especial()">OK</button>
            </div>
        </div>
    </div>
</div>

<!--Modal  Oferta de Venta línea especial cliente y fecha-->
<div class="modal fade" id="OfertaClienteF">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Oferta de Venta línea especial cliente y fecha</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="font-size: .8rem">
                <h4 style="font-size: 1.1rem">Consulta: Criterios de selección</h4>
                <br><br>
                 <div class="row">
                    <label class="col-7 col-form-label">Código de clietne/proveedor</label>
                    <div class="col-5">
                        <input type="text" style="width: 90%; height:23px" id="codigoClienteF">
                        <a id="btnCliente" href="#" data-toggle="modal" data-target="#myModal">
                            <i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <label class="col-7 col-form-label">Fecha de contabilización</label>
                    <div class="col-5">
                        <input type="date" style="width: 90%; height:23px" id="fecha1">
                    </div>
                </div>
                <div class="row">
                    <label class="col-7 col-form-label">Fecha de contabilización</label>
                    <div class="col-5">
                        <input type="date" style="width: 100%; height:23px" id="fecha2">
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border-top: none">
                <button class="btn btn-sm cancelarModalesQuery" style="background-color: orange" title="Cancelar"   data-dismiss="modal" data-target="#modalQuery" data-toggle="modal">Cancelar</button>
                <button class="btn btn-sm" style="background-color: orange" data-dismiss="modal" id="btnOFVClienteFecha" onclick="query_oferta_de_veenta_especial_fecha()">OK</button>
            </div>
        </div>
    </div>
</div>

<!-- oferta de venta linea fecha-->
<div class="modal fade" id="ofvLineaFecha">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Oferta de venta linea fecha</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="font-size: .8rem">
                <h4 style="font-size: 1.1rem">Consulta: Criterios de selección</h4>
                <br><br>
                <div class="row">
                    <label class="col-7 col-form-label">Fecha de contabilización</label>
                    <div class="col-5">
                        <input type="date" style="width: 100%; height:23px" id="campoFecha1">
                    </div>
                </div>
                <div class="row">
                    <label class="col-7 col-form-label">Fecha de contabilización</label>
                    <div class="col-5">
                        <input type="date" style="width: 100%; height:23px" id="campoFecha2">
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border-top: none">
                <button class="btn btn-sm cancelarModalesQuery" style="background-color: orange" title="Cancelar"   data-dismiss="modal" data-target="#modalQuery" data-toggle="modal">Cancelar</button>
                <button class="btn btn-sm" style="background-color: orange" data-dismiss="modal" id="btnOFVFecha" onclick="query_oferta_de_venta_fecha()">OK</button>
            </div>
        </div>
    </div>
</div>


<!-- Pedidos del dia-->
<div class="modal fade" id="Pedidos">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pedidos del dia</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="font-size: .8rem">
                <h4 style="font-size: 1.1rem">Consulta: Criterios de selección</h4>
                <br><br>
                <div class="row">
                    <label class="col-7 col-form-label">Fecha de contabilización igual a</label>
                    <div class="col-5">
                        <input type="date" style="width: 100%; height:23px" id="fechaconta">
                    </div>
                </div>
                <div class="row">
                    <label class="col-7 col-form-label">Status de documento igual a</label>
                    <div class="col-5">
                        <select style="width: 100%; height:23px" id="statusDocumento">
                            <option value="" disabled selected>Selecciona</option>
                            <option value="O">Abierto</option>
                            <option value="C">Cerrador</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border-top: none">
                <button class="btn btn-sm cancelarModalesQuery" style="background-color: orange" title="Cancelar"   data-dismiss="modal" data-target="#modalQuery" data-toggle="modal">Cancelar</button>
                <button class="btn btn-sm" style="background-color: orange" data-dismiss="modal" id="btnPedidoDia" onclick="query_pedidos_del_dia()">OK</button>
            </div>
        </div>
    </div>
</div>

<!-- Mapa de relaciones-->
<div class="modal fade" id="mapaDeRelaciones">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mapa de relaciones</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="font-size: .8rem">
                <h4 style="font-size: 1.1rem">Consulta: Criterios de selección</h4>
                <br><br>
                <div class="row">
                    <label class="col-7 col-form-label">Fecha de Inicio</label>
                    <div class="col-5">
                        <input type="date" style="width: 100%; height:23px" id="fechaInicio">
                    </div>
                </div>
                <div class="row">
                    <label class="col-7 col-form-label">fecha Final</label>
                    <div class="col-5">
                        <input type="date" style="width: 100%; height:23px" id="fechaFinito">
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border-top: none">
                <button class="btn btn-sm cancelarModalesQuery" style="background-color: orange" title="Cancelar"   data-dismiss="modal" data-target="#mapaDeRelaciones" data-toggle="modal">Cancelar</button>
                <button class="btn btn-sm" style="background-color: orange" id="btnMapaRelaciones" onclick="query_mapa_de_relaciones()">OK</button>
            </div>
        </div>
    </div>
</div>

<!-- mapa de relaciones 2do nivel-->
<div class="modal fade" id="mapaDeRelaciones2doNivel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mapa de relaciones</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style="font-size: .8rem">
                <div style="height: 600px; overflow: auto;">
                    <table class="table table-hover table-bordered table-responsive" id="tblMapaRelaciones">
                        <thead class="thead-dark text-center">
                            <tr>
                                <th>Código cliente</th>
                                <th>Nombre del cliente</th>
                                <th>Vendedor Asignado</th>
                                <th>Orden de venta</th>
                                <th>Fecha de la orden</th>
                                <th>Entrega</th>
                                <th>Fecha de la entrega</th>
                                <th>Factura</th>
                                <th>Fecha de la factura</th>
                                <th>Tipo de Factura</th>
                                <th>Nota de credito</th>
                                <th>Solicitud de compra</th>
                                <th>Fecha solicitud</th>
                                <th>Pedido de compra</th>
                                <th>Fecha Pedido de compra</th>                                
                            </tr>
                        </thead>
                        <tbody class="text-center"></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer" style="border-top: none">
                <button class="btn btn-sm cancelarModalesQuery btnCerrarMapa" style="background-color: orange" title="Cancelar"   data-dismiss="modal" data-target="#mapaDeRelaciones2doNivel" data-toggle="modal">Cancelar</button>
                <!-- <button class="btn btn-sm" style="background-color: orange" data-dismiss="modal" data-dismiss="modal" data-target="#Pedidos2doNivel" data-toggle="modal">OK</button> -->
            </div>
        </div>
    </div>
</div>
