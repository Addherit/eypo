<!DOCTYPE html>
<html lang="en"> 
<?php include "../header.html"?>		
	<body>		
        <?php include "../nav.php" ?>
		<?php include "../../modalQuerys.php" ?>
		<?php include "../../modales.php" ?>
        <div class="container formato-font-design" id="contenedorDePagina">
			<div class="row">
				<div class="col-12">	
					<br>				
					<input id="fecha_contabilizacion_superiror" type="hidden" value="<?php echo $_GET["fechaContabilizacionSuperior"] ?>">
					<input id="fecha_contabilizacion_menor" type="hidden" value="<?php echo $_GET["fechaContabilizacionMenor"] ?>">
					<section class="table-responsive">
						<table class="table table-bordered table-striped table-hover table-sm" id="tblEntradaArticulos">
							<thead>
								<tr>
									<th>#</th>
									<th>DocNum</th>
									<th>Fecha</th>
									<th>CardName</th>
									<th>Codigo Art.</th>
									<th>Articulo</th>
									<th>ComentarioPartida</th>
									<th>Cant</th>
									<th>Pedido</th>
									<th>O.C. Cliente/Stock</th>
									<th>Cliente</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</section>
				</div>
				<div class="col-5 offset-7">
				<a href="ofertaDeVenta.php">
					<button class="btn btn-primary btn-block">Regresar a OFV</button>
				</a>
				</div>
			</div>
		</div>					
		<?php include "../footer.html"?>	
		<script src="js/entrega_articulos.js"></script>	
	</body>                     
	
</html>