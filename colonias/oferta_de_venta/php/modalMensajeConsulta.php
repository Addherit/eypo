<?php
    include "../../../db/Utils.php";
    $hoy = strtotime(date("Y-m-d"));
    // $nuevafecha = date("Y-m-d", strtotime('-3 month', $hoy));    
    $nuevafecha = '2018-01-01'; 
    // esta se usa como ejemplo para ambiente local

    $sql = "SELECT T0.*,CASE WHEN T0.EstatusReal = 2 THEN 'CREADA'
    WHEN T0.EstatusReal = 0 AND T0.CrearReal = 'S' AND T0.Estatus = 2 THEN 'PENDIENTE CREAR REAL'
    WHEN T0.EstatusReal = -1 THEN 'ERROR CREAR REAL'
    WHEN T0.Estatus = 2 AND T1.Status = 'W' THEN 'BORRADOR CREADO, AUTORIZACION PENDIENTE'
    WHEN T0.Estatus = 2 AND T1.Status = 'Y' THEN 'BORRADOR CREADO, AUTORIZADA'
    WHEN T0.Estatus = 2 AND T1.Status = 'N' THEN 'BORRADOR CREADO, RECHAZADA'
    WHEN T0.Estatus = 2 AND T1.Status IN ('P','A') THEN 'BORRADOR CREADO, CREADA DESDE SAP'
    WHEN T0.Estatus = 2 AND T1.Status = 'C' THEN 'BORRADOR CANCELADO'
    WHEN T0.Estatus = 2 THEN 'BORRADOR CREADO'
    WHEN T0.Estatus = 0 THEN 'PENDIENTE CREAR BORRADOR'
    WHEN T0.Estatus = -1 THEN 'ERROR CREAR BORRADOR'
    END EstatusAutorizacion
    FROM [dbEypo].[dbo].[ordenes] T0
    LEFT JOIN EYPO.dbo.OWDD T1 ON T1.DocEntry = T0.NuevoDocEntry AND T1.ObjType = '17'
    WHERE created_at >= '$nuevafecha'
    ORDER BY created_at DESC";

    $consulta = sqlsrv_query( $conn, $sql );   

    $response = [];
    while( $row = sqlsrv_fetch_array($consulta, SQLSRV_FETCH_ASSOC) ) {        
        $response[] = $row;
    }        
    echo json_encode( $response);
?>
