<a href="#" class="btn-default btn-sm" id="btnBuscarGRAL">
  <i class="fa fa-binoculars fa-2x" aria-hidden="true" title="Búsqueda" onclick="btn_busqueda_general()"></i>
</a>
<div class="dropdown">
  <a id="btnPdf" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      <i class="fas fa-file-pdf" style="font-size: 2rem; color: #f57272" title="PDF"></i>
  </a>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" id="btnPdf" href="#">descripciones reales </a>
    <a class="dropdown-item"  id="btnPdf_protegida" href="#">descripciones protegidas</a>
  </div>
</div>
<a href="#" class="btn btn-default btn-sm" id="btnxlm"><i class="fas fa-file-code" style="font-size: 2rem; color: #f5da72" title="XML"></i></a>
<a href="#" class="btn btn-sm btn-default"  id="consultaPrimerRegistro" onclick="consultar_primer_registro()"><i class="fas fa-arrow-alt-circle-left fa-2x" aria-hidden="true" style="color: #1cc707" title="Primer Registro"></i></a>
<a href="#" class="btn btn-sm btn-default" id="consulta1Atras" onclick="consulta_1_atras()"><i class="fas fa-arrow-left fa-2x" aria-hidden="true" style="color: #1CC707;" title="Consulta una atrás"></i></a>
<a href="#" class="btn btn-sm btn-default" id="consulta1Adelante" onclick="consulta_1_adelante()"><i class="fas fa-arrow-right fa-2x" aria-hidden="true" style="color: #1CC707;" title="Consulta una adelante"></i></a>
<a href="#" class="btn btn-sm btn-default" id="consultaUltimoRegistro" onclick="consultar_ultimo_registro()"><i class="fas fa-arrow-alt-circle-right fa-2x" aria-hidden="true" style="color: #1cc707" title="Último Registro"></i></a>
<a href="#" class="btn btn-sm btn-default" id="consultaQuery" data-target="#modalQuery" data-toggle="modal"><i class="fas fa-database" style="font-size: 2rem; color: #ff8500" title="Query"></i></a>
<a href="#"  class="btn btn-default btn-sm" id="mensajes" data-toggle="modal" data-target="#modalMensaje" title="Mensajes/Alertas" onclick="mostrar_mensajes_ofv()"><i  class="fa fa-envelope fa-2x" aria-hidden="true" style="color: #007bffa6"></i></a>
    