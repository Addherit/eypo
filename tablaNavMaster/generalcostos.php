<br>
						
					  	
<div class="row">
					  		<div class="col-md-12">
							<div class="row" align="center">
						<label for="" class="col-sm-3 col-form-label">Codigos Protegidos:</label>
						
					</div>
					Da doble clic sobre el renglon para buscar precios de referencia
					  			<table class="table table-bordered table-editable table-hover table-striped table-responsive" width="100%" id="detalleprotegidos">
					        		<thead>
					        			<tr class="encabezado" >
											<!--<th><i class="fas fa-ban"></i></th>-->
											<th>#</th>
											<th>Ficha</th>
											<th>Id</th>
											<th>Marca</th>
					        				<th>Genero</th>
					        				<th>CodigoReal</th>
											<th>CodigoProtegido</th>
					        				<th>Descripcion</th>
					        				<th>Tipo</th>
					        				<th>Potencia</th>
					        				<th>Cantidad</th>
											<th>Costo</th>
					        				<th>Moneda</th>
											<th>Utilidad %</th>
											<th>Coeficiente %</th>
											<th>TE</th>
											<th>Precio de Venta</th>
											<th>Moneda</th>
											<th>Check</th>
					        				<th style="display:none;">CodigoBase</th>
											
										</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<!--<th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>-->
											<td contenteditable="false" class="cont"></td>
											<td contenteditable="false" class="Ficha"></td>
											<td contenteditable="false" class="id"></td>
											<td contenteditable="false" class="marca"></td>
								          	<td contenteditable="false" class="modelo"></td>
											<td contenteditable="false" class="codigoreal"></td>
											<td contenteditable="false" class="catalogoprotegido"></td>
											<td contenteditable="false" class="descripcion"></td>
											<td contenteditable="false" class="tipo"></td>
											<td contenteditable="false" class="potencia"></td>
											<td contenteditable="false" class="cantidad"></td>
											<td contenteditable="true" class="costo"></td>
								            <td contenteditable="true" class="monedacosto"></td>
											<td contenteditable="true" class="putilidad"></td>
											<td contenteditable="true" class="coeficiente"></td>
											<td contenteditable="true" class="tiempoentrega"></td>
											<td contenteditable="true" class="precioventa"></td>
											<td contenteditable="true" class="monedaventa"></td>
											<td><input type="checkbox" id="check"> </td>
											
								            <td class="codigobase" style="display:none;"></td>
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>

						
					  	<br>
<div class="row">
					  		<div class="col-md-12">
							<div class="row" align="center">
						<label for="" class="col-sm-3 col-form-label">Codigos Editables:</label>
						
					</div>
					Da doble clic sobre el renglon para buscar precios de referencia
					  			<table class="table table-bordered table-editable table-hover table-striped table-responsive" width="100%" id="detallenoprotegidos">
					        		<thead>
					        			<tr class="encabezado" >
											<th>#</th>
											<th>Id</th>
					        				<th>Marca</th>
					        				<th>Género</th>
					        				<th>CodigoReal</th>
											<th>CodigoProtegido</th>
					        				<th>Descripcion</th>
					        				<th>Tipo</th>
					        				<th>Potencia</th>
					        				<th>Cantidad</th>
											<th>Costo</th>
					        				<th>Moneda</th>
											<th>Utilidad %</th>
											<th>Coeficiente %</th>
											<th>TE</th>
											<th>Precio de Venta</th>
											<th>Moneda</th>
											<th>Check</th>
					        				<th style="display:none;">CodigoBase</th>
											
										</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<td contenteditable="false" class="cont"></td> 
											<td contenteditable="false" class="idn"></td>
											<td contenteditable="false" class="marcan"></td>
								          	<td contenteditable="false" class="modelon"></td>
											<td contenteditable="false" class="codigorealn"></td>
											<td contenteditable="false" class="catalogoprotegidon"></td>
											<td contenteditable="false" class="descripcionn"></td>
											<td contenteditable="false" class="tipon"></td>
											<td contenteditable="false" class="potencian"></td>
											<td contenteditable="false" class="cantidadn"></td>
											<td contenteditable="true" class="coston"></td>
								            <td contenteditable="true" class="monedacoston"></td>
											<td contenteditable="true" class="putilidadn"></td>
											<td contenteditable="true" class="coeficienten"></td>
											<td contenteditable="true" class="tiempoentregan"></td>
											<td contenteditable="true" class="precioventan"></td>
											<td contenteditable="true" class="monedaventan"></td>
											<td><input type="checkbox" id="checkn"></td>
								            <td class="codigobasen" style="display:none;"></td>
											
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>
<br>
				