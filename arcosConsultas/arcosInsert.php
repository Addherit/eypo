<?php
  include "../db/Utils.php";
  $valores = $_POST['valores'];
  $detalle = $_POST['valDet'];  
  $detalle_desarme = $_POST['valDet_Desarme']; 

  $val = ""; 

    for($x=0; $x < count($valores); $x++) {
            
        $val .="'".str_replace("'", "''", $valores[$x])."',"; 
    }
    $val = trim($val, ','); 


  $sql = "INSERT INTO dbEypo.dbo.arcos (fechaSolicitud, fechaEntrega, nombreSolicitante, 
  nombreProyecto, numeroOrden, codigoCliente, nombreCliente, cantidadFabricar, entregasParciales, 
  productoaFabricar, productosFabricarNombre, productoaDesarmar, productosDesarmarNombre,
  observaciones, empresa, FolioIndependiente, estatus)
  VALUES ($val, 'SOLICITADO')";
  $consultasql = sqlsrv_query($conn, $sql);

  if ($consultasql) {


    for($x=0; $x<count($detalle[0]); $x++){
      $string = "";
      for($y=0; $y<4; $y++){                
          $string .= "'".str_replace("'", "''", $detalle[$y][$x])."',";               
      }

      $string .= "'".$valores[15]."'";
      $sql = "INSERT INTO dbEypo.dbo.arcosDet (CodigoArt, NombreArt, piezas, ArmadoDesarmado, folio)
      VALUES ($string)";
      $consultasql = sqlsrv_query($conn, $sql);
    }


    //desarme ------------------------------------------
    for($x=0; $x<count($detalle_desarme[0]); $x++){
      $string = "";
      for($y=0; $y<4; $y++){                
          $string .= "'".str_replace("'", "''", $detalle_desarme[$y][$x])."',";               
      }

      $string .= "'".$valores[15]."'";
      $sql = "INSERT INTO dbEypo.dbo.arcosDet (CodigoArt, NombreArt, piezas, ArmadoDesarmado, folio)
      VALUES ($string)";
      $consultasql = sqlsrv_query($conn, $sql);
    }
    // ------------------------------------
    
    if ($consultasql) {
      echo $respuesta =  '<div class="row" style="background-color: #b0ddb0; padding-top: 10px; padding-bottom: 10px; border-radius: 5px;">
        <div class="col-md-12">
          <span style="color: green;">Formato guardado con éxito</span>
        </div>
      </div>';      
    } else {
      echo $respuesta =  '<div class="row" style="background-color: #f6bbbb; padding-top: 10px; padding-bottom: 10px; border-radius: 5px;">
        <div class="col-md-12">
          <span style="color: red;">Ocurrio un error en el detalle al interntar guardar, favor de verificarlo</span>
        </div>
      </div>';
    }
  } else {
    echo $respuesta =  '<div class="row" style="background-color: #f6bbbb; padding-top: 10px; padding-bottom: 10px; border-radius: 5px;">
        <div class="col-md-12">
          <span style="color: red;">Ocurrio un error en cabecera al interntar guardar, favor de verificarlo</span>
        </div>
      </div>';
  }
?>
