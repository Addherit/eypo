<?php 
    include "../db/Utils.php";
    $folioSAP = $_POST['folio'];
    $primerRegistro = $_POST['pr'];
    $ultimoRegistro = $_POST['ur'];
    $condicion = $_POST['con'];  
    

    if($folioSAP > $ultimoRegistro OR  $folioSAP < $primerRegistro){
        $Array2 = [
            'respuesta' => 1
        ];
        echo json_encode($Array2);
    } else {

        do{

            $sql="SELECT soc.Nombre, dev.* FROM EYPO.dbo.IV_EY_PV_NotasCreditoVentaCab dev
            LEFT JOIN EYPO.dbo.IV_EY_PV_SociosNegocios soc ON dev.CodigoSN = soc.CodigoSN                     
            WHERE FolioSAP = '$folioSAP' AND SerieNombre != 'COLONIAS'";
            $consulta = sqlsrv_query($conn, $sql); 
            $row = sqlsrv_fetch_array($consulta);
            $folioInternoCAB = $row["FolioInterno"];


            switch ($condicion) {
                case 'adelante':
                    $folioSAP++;
                    break;
                
                case 'atras': 
                    $folioSAP--;
                    break; 
            }
        }while(!$row);

        $sql2 ="SELECT det.*, stock.EnStock, stock.Comprometido, stock.Solicitado FROM EYPO.dbo.IV_EY_PV_NotasCreditoVentaDet  det
        LEFT JOIN EYPO.dbo.IV_EY_PV_Stock stock ON det.CodigoArticulo = stock.CodigoArticulo AND det.Almacen = stock.CodigoAlmacen
        WHERE FolioInterno = '$folioInternoCAB'";
        $consulta2 = sqlsrv_query($conn, $sql2);
        $array_det=[]; 
        $cont=0; 
        WHILE ($RowDet = sqlsrv_fetch_array($consulta2)){
            $cont++;          
            $linea = '<tr>                
                <td>' . $cont . '</td>
                <td class="codAticulo">'.$RowDet['CodigoArticulo'].'</td>
                <td class="narticulo">' .  $RowDet['NombreArticulo']. '</td>   
                <td class="darticulo">' .  number_format($RowDet['Quantity'],0,'.',',') . '</td>   
                <td class="darticulo">' .  number_format($RowDet['PrecioUnitario'],2,'.',',') . '</td>                      
                <td class="darticulo">' .  $RowDet['Almacen'] . '</td>  
                <td class="comentario1" contenteditable="true">'.$RowDet['ComentarioPartida1'].'</td>
                <td class="comentario2" contenteditable="true">'.$RowDet['ComentarioPartida2'].'</td>
                <td >'.number_format($RowDet['EnStock'], 2, '.', ',').'</td>
                <td >'.number_format($RowDet['Comprometido'], 2, '.', ',').'</td>
                <td >'.number_format($RowDet['Solicitado'], 2, '.', ',').'</td>                                                
            ';
                    
            array_push($array_det, $linea);
        }

        $arrayContent = [

            'respuesta' => 2,
            'cliente' => $row['CodigoSN'],
            'nombreCliente' => $row['Nombre'],
            'personaContacto' => $row['NombrePersonaContacto'],
            'referencia' => $row['Referencia'],            
            'tipoMoneda' => $row['Moneda'],             

            // 'ndoc' => ndoc, 
            'cdoc' => $row['FolioSAP'],
            'estado' => $row['Estatus'],
            'fconta' => $row['FechaContabilizacion']->format('Y-m-d'),
            'fdoc' => $row['FechaDocumento']->format('Y-m-d'),
            'fven' => $row['FechaVencimiento']->format('Y-m-d'),
            'metodoPago' => $row['MetodoPago'],
            'formaPago' => $row['IdFormaPago'],            
            'usoPrincipal' => $row['UsoCFDi'],
            
                  
            'empleado' => $row['CodigoEmpleadoVC'],
            'propietario'  => $row['CodigoPropietario'],                                     
            'comentarios' => $row['Comentarios'],


            'totalAntesDescuento' =>  number_format($row['SubTotalDocumento'],2,'.',','),
            'porcentajeDescuento' =>  number_format($row['PorcentajeDescuento'],0,'.',','), 
            'descuento' =>  number_format($row['ImporteDescuento'],2,'.',','),         
            'anticipoTotal' =>  number_format($row['Anticipo'],2,'.',','),
            'redondeo' =>  number_format($row['Redondeo'],2,'.',','),
            'impuestoTotal' =>  number_format($row['SumaImpuestos'],2,'.',','),            
            'retencion' =>  number_format($row['ImporteRetenciones'],2,'.',','),
            'totalDelDocumento' =>  number_format($row['TotalDocumento'],2,'.',','),
            'importeAplicado'=>  number_format($row['Pagado'],2,'.',','),            
            'saldoPendiente' => number_format( $row['Saldo'],2,'.',','),
            'rutaArchivos' => $row['RutaArchivos'],
            
            'array' => $array_det,

        ];

        echo json_encode($arrayContent);
    }
?>