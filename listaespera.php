	<?php
	include "conexion.php";
	session_start();
	?>

	<!DOCTYPE html>
	<html>
		<?php include "header.php"; ?>
		<body onload="cargarProyectos()">

		<?php include "nav.php"; ?>
		<?php include "modalQuerys.php"; ?>
		<?php include "modales.php"; ?>

		<div class="container" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Lista de Espera por Asignar a Proyectista</h3>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">

				</div>
			</div>
			<div class="row datosEnc" style="font-size: .7rem">
				<div class="col-md-6">



				</div>
			</div>
			<br>

			<div class="row" style="font-size: .7rem">
				<div class="col-md-12">
					<div class="row">

					  		<div class="col-md-12">
					  			<table class="table-bordered table-editable table-hover table-striped table-responsive table" width="100%" id="listaespera" >
					        		<thead>
					        			<tr class="encabezado" style="background-color: #005580; color:white;" >
											<th>Ver</th>
											<th>No.</th>
											<th>Prioridad</th>
											<th>Nombre del Proyecto</th>
											<th>Fecha de Ingreso</th>
					        				<th>Tipo de propuesta</th>
											<th>Cliente</th>
					        				<th>Telefono</th>
											<th>Correo</th>
											<th>Proy</th>
											<th>Proy Dom</th>
					        				<th>Asesor Ventas</th>
											<th>Estatus</th>

					        			</tr>
					        		</thead>
					        		<tbody>
							        	<tr>
											<th>


											<a href="#" style="color: green" id="eliminarFila"  data-toggle="modal" data-target="#myModalCajero"><i class="fas fa-folder-open"></i></a></th>
											<td class="Id"></td>
											<td class="Prioridad"></td>
											<td class="NombreProyecto"></td>
								            <td class="FechaIngreso"></td>
											<td class="TipoPropuesta"></td>
								            <td class="Cliente"></td>
											<td class="Telefono"></td>
											<td class="Correo"></td>
											<td class="Proyectista"></td>
											<td class="ProyectistaDom"></td>
											<td class="AsesorVentas"></td>
											<td class="Estatus"></td>
										</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>
				</div>

		</div>
	</div>

		<?php include "footer.php"; ?>
	</body>
		<script>
				function cargarProyectos (){

						var tipopropuesta = 'normal';
					
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'1', tipopropuesta:tipopropuesta},
					success: function(response){
						$("#listaespera tbody").empty();
						$("#listaespera tbody").append(response);
                     }
				});



			}
			 if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
	function postForm(path, params, method) {
    method = method || 'post';

    var form = document.createElement('form');
    form.setAttribute('method', method);
    form.setAttribute('action', path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}
			$(document).on('click', '#eliminarFila', function (event) {


				  var currentRow=$(this).closest("tr");
				  var valorEscrito=currentRow.find("td:eq(0)").text();
				  var usuario = currentRow.find("td:eq(12)").text().toUpperCase();
				  var usuarioactual = '<?php echo $_SESSION['usuario']?>'.toUpperCase();
				  if ("<?php echo $_SESSION['CodigoPosicion']?>" == '36'||"<?php echo $_SESSION['CodigoPosicion']?>" == '46' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'||"<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'||"<?php echo $_SESSION['CodigoPosicion']?>" == '50'){
				   postForm('masterproyectos.php', {valor: valorEscrito});
				  }
				  else if (usuario==usuarioactual)
				  {
					 postForm('masterproyectos.php', {valor: valorEscrito});
				  }
				  else
				  {
				  }




			});
	function cambiar(){
    var pdrs = document.getElementById('file-upload').files[0].name;
    document.getElementById('info').innerHTML = pdrs;
}
			function respuestas(Id){

				   $.ajax({
						type: "post",
						url: "eliminadeposito.php?valor="+Id,

						success  : function(data){
							 //window.location.href="nomina.php";
						},
						error    : function(){
							 alert("Could not ");
							 alert("EliminaDeposito.php?valor="+Id);
						}

					});
			}



		</script>
	</html>
