<?php 
    include "../db/Utils.php";

    $folioSAP = $_POST['folio'];
    $primerRegistro = $_POST['pr'];
    $ultimoRegistro = $_POST['ur'];
    $condicion = $_POST['con'];  


    if($folioSAP > $ultimoRegistro OR  $folioSAP < $primerRegistro){
        $Array2 = [
            'respuesta' => 1
        ];
        echo json_encode($Array2);
    } else {
    
        
        do{

            $sql="SELECT * FROM EYPO.dbo.IV_EY_PV_SolicitudesComprasCab WHERE FolioSAP = '$folioSAP'";
            $consulta = sqlsrv_query($conn, $sql); 
            $row = sqlsrv_fetch_array($consulta);
            $folioInternoCAB = $row["FolioInterno"];

            switch ($condicion) {
                case 'adelante':
                    $folioSAP++;
                    break;
                
                case 'atras': 
                    $folioSAP--;
                    break; 
            }
        }while(!$row);

        $sql2 ="SELECT det.CodigoArticulo, det.NombreArticulo, art.CodigoProveedor, det.FechaNecesaria, det.CantidadNecesaria, 
        det.ComentarioPartida1, det.ComentarioPartida2, st.EnStock, st.Comprometido, st.Solicitado  FROM EYPO.dbo.IV_EY_PV_SolicitudesComprasDet det
        LEFT JOIN EYPO.dbo.IV_EY_PV_Articulos art ON det.CodigoArticulo = art.CodigoArticulo 
        LEFT JOIN EYPO.dbo.IV_EY_PV_Stock st ON det.CodigoArticulo = st.CodigoArticulo AND det.Almacen = st.CodigoAlmacen
        WHERE FolioInterno = '$folioInternoCAB'";
        $consulta2 = sqlsrv_query($conn, $sql2);
        $array_det=[]; 
        $cont=0; 
        WHILE ($RowDet = sqlsrv_fetch_array($consulta2)){
            $cont++;          
            $linea = '<tr>
            <th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>
            <td>' . $cont . '</td>
            <td class="codigo">'.$RowDet['CodigoArticulo'].'</td>
            <td class="narticulo">' .$RowDet['NombreArticulo']. '</td>   
            <td class="proveedor">' .$RowDet['CodigoProveedor']. '</td>   
            <td class="fecha">' . $RowDet['FechaNecesaria']-> format('Y-m-d') . '</td>
            <td class="cantidad" contenteditable="true">' . number_format($RowDet['CantidadNecesaria'], 2, '.', ',') . '</td>   
            <td class="comentario1" contenteditable="true">' .  $RowDet['ComentarioPartida1'] . '</td>  
            <td class="comentario2" contenteditable="true">' .  $RowDet['ComentarioPartida2'] . '</td>   
             
            <td class="stock">' .number_format($RowDet['EnStock'], 2, '.', ','). '</td>  
            <td class="comprometido">' .number_format($RowDet['Comprometido'], 2, '.', ','). '</td> 
            <td class="solicitado">' .number_format($RowDet['Solicitado'], 2, '.', ','). '</td>                                        
        ';                    
            array_push($array_det, $linea);
        }

        $arrayContent = [
            'respuesta' => 2,
            'cdoc' => $row['FolioSAP'],
            'codSolicitante' => $row['Solicitante'],
            'nomSolicitante' => $row['NombreSolicitante'],
            'sucursal' => $row['SucursalDescripcion'],
            'departamento' => $row['DepartamentoDescripcion'],
            'ordenVenta' => $row['OrdenVenta'], 
            // 'nDoc' => $Row[''],
            'estado' => $row['Estatus'],
            'fconta' => $row['FechaContabilizacion']->format('Y-m-d'),
            'validoHasta' => $row['FechaVencimiento']->format('Y-m-d'),
            'fdoc' => $row['FechaDocumento']->format('Y-m-d'),
            'fNecesaria' => $row['FechaNecesaria']->format('Y-m-d'),
            'usoPrincipal' => $row['UsoCFDi'],
            'propietario' => $row['Usuario'],
            'comentarios' => $row['Comentarios'],
            'array_Detalle' => $array_det

        ];

        echo json_encode($arrayContent);


    }  



?>