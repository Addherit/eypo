<!DOCTYPE html>
<html lang="en"> 
<?php include "../header.html"?>		
	<body>		
        <?php include "../nav.php" ?>
		<?php include "../../modalQuerys.php" ?>
		<?php include "../../modales.php" ?>
        <div class="container formato-font-design" id="contenedorDePagina">
            <div class="row">
                <input type="hidden" id="fechaS" value="<?php echo $_GET["fechaS"] ?>">
                <input type="hidden" id="fechaM" value="<?php echo $_GET["fechaM"] ?>">
                <div class="col-12">
                    <br>         
                    <section class="table-responsive">       
                        <table class="table table-striped table-sm table-bordered table-hover text-center" id="tblComisiones" >
                            <thead>
                                <tr>
                                    <th>No°</th>
                                    <th>No° Pago</th>
                                    <th>No° Factura</th>
                                    <th>Nombre</th>
                                    <th>Subtotal</th>
                                    <th>Iva</th>
                                    <th>Total</th>
                                    <th>Días vencimiento</th>
                                    <th>Subtotal Pago</th>
                                    <th>Iva Pago</th>
                                    <th>Monto de pago</th>
                                    <th>% de Pago</th>
                                    <th>Saldo Pendiente</th>
                                    <th>Pagado al día</th>
                                    <th>% Pagado al día</th>
                                    <th>L1</th>
                                    <th>L2</th>
                                    <th>L3</th>
                                    <th>L4</th>
                                    <th>L5</th>
                                    <th>L6</th>
                                    <th>L7</th>
                                    <th>L8</th>
                                    <th>L9</th>
                                    <th>L10</th>
                                    <th>Vendedor Principal</th>
                                    <th>Comisión Principal</th>
                                    <th>Vendedor Adicional</th>
                                    <th>Comisión Adicional</th>
                                    <th>Promotor de ventas</th>
                                    <th>Comisión Promotor</th>
                                    <th>Coordinador de ventas</th>
                                    <th>Comisión Coordinador de venta</th>
                                    <th>Proyecto</th>
                                    <th>Proyectista1</th>
                                    <th>Comisión Proyectista1</th>
                                    <th>Proyectista2</th>
                                    <th>Comisión Proyectista2</th>
                                    <th>Proyectista3</th>
                                    <th>Comisión Proyectista3</th>
                                    <th>Proyectista4</th>
                                    <th>Comisión Proyectista4</th>
                                    <th>Proyectista5</th>
                                    <th>Comisión Proyectista5</th>
                                    <th>Coordinador de proyecto</th>
                                    <th>Comisión Coordinador de proyecto</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>            
                    </section>
                </div>
                <div class="col-5 offset-7">
                    <a href="ofertaDeVenta.php">
                        <button class="btn btn-primary btn-block">Regresar a OFV</button>
                    </a>
                </div>
            </div>
        </div>                    
        <?php include "../footer.html"?>
        <script src="js/comisiones_ventas.js"></script>
	</body>              
</html>