<?php
include "conexionCajero.php";
session_start();
if (!isset($_SESSION["usuario"]))
{
   header("location: login.php");
}
$codcaj = $_POST['code'];
$idcaj = $_POST['rd'];

$cont=0;

$query = "SELECT d.Id, u.NOM, u.Nombre, d.Cantidad, d.retirado, d.Concepto, d.CreatedAt, d.FechaRetirar, d.ImporteInicial,s.Name from depositos_empleados d inner join [user] u on d.NOM = u.NOM inner join solution s on s.Id = d.Solution where retirado = 0 and u.Status = 'ALTA' and s.Uid like '%$codcaj%' and u.Solution = '$idcaj' order by d.CreatedAt";
//echo $query;
$consultasql = sqlsrv_query($conn,$query);
while ($Row = sqlsrv_fetch_array($consultasql)) {
  $cont++;
?>
<tr>
  <th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>
  <td><?php echo utf8_encode($cont);?></td>
  <td><?php echo $Row['Id'];?></td>
  <td><?php echo $Row['NOM'];?></td>
  <td><?php echo utf8_encode($Row['Nombre']);?></td>
  <td><?php echo number_format($Row['Cantidad'],2,'.','');?></td>
  <td><?php echo utf8_encode($Row['Concepto']);?></td>
  <td><?php echo $Row['CreatedAt']-> format('Y-m-d H:i:s');?></td>
  <td><?php echo $Row['FechaRetirar']-> format('Y-m-d H:i:s');?></td>
  <td><?php echo number_format($Row['ImporteInicial'],2,'.','');?></td>
  <td><?php echo utf8_encode($Row['Name']);?></td>
 </tr>
<?php } ?>
