<?php
	include "../../../db/Utils.php";

	$fechaIni = $_POST['fechaIni'];
	$fechaFin = $_POST['fechaFin'];
	$nombreCliente = $_POST['nombreCliente'];
	$codigoCliente = $_POST['codigoCliente'];

	$fecha1 = str_replace("-", "", $fechaIni);
	$fecha2 = str_replace("-", "", $fechaFin);

	$sql = "
		declare @fechaini as datetime
		declare @fechafin as datetime
		declare @Prov as varchar(100)
		declare @cliente as varchar(20)

		declare @param1 as datetime
		SET @param1 = '$fecha1'

		declare @param2 as datetime
		SET @param2 = '$fecha2'

		declare @param3 as varchar(100)
		SET @param3 = '$nombreCliente'

		declare @param4 as varchar(100)
		SET @param4 = '$codigoCliente'


		select @fechaini = t0.docdate, @fechafin = t1.docdate, @Prov = t2.cardname, @cliente = t3.U_Codigo_Cliente from OPOR T0, OPOR T1, OPOR T2, POR1 t3 where t0.docdate = @param1 and t1.docdate = @param2 and t2.Cardname = @param3 and t3.U_Codigo_Cliente = @param4
		SET @FechaIni = CONVERT(DATETIME, @param1, 112)
		SET @FechaFin = CONVERT(DATETIME, @param2, 112)
		SET @PROV = @param3
		SET @CLIENTE = @param4

		if((@Prov is null or @Prov ='' or @Prov=' ') and (@cliente is null or @cliente ='' or @cliente =' '))
		begin
		select distinct
			t1.ItemCode[Catalogo],
		case when t1.ItemCode='041-PVEYPO' then t1.U_Texto
		when T1.ItemCode<>'041-PVEYPO' then t1.Dscription
		end [Descripcion],
			t1.whscode[Almacen Destino],
			t1.Quantity[Cantidad Comprada],
			t1.OpenQty[Cantidad Abierta],
			t2.OnHand[Existencia Almacen Destino],
			t1.U_FechaRecepcion'Fecha Recepcion',
			T1.U_Comentarios'Comentarios Recepcion',
			t1.U_Codigo_Cliente[Codigo Cliente],
			t3.CardName[Nombre Cliente],
			t1.U_N_Pedido_Cliente[Orden de Venta],
			t0.docnum, t0.cardcode [Codigo Proveedor], t0.cardname [Nombre Proveedor], t0.numatcard [Referencia],t0.docdate [Fecha Orden de Compra] /*, t0.DocDueDate [Fecha Llegada]*/
		from
		OPOR T0
		LEFT JOIN POR1 T1 ON T0.DocEntry = T1.DocEntry
		LEFT JOIN OITW T2 on ((T1.ItemCode = T2.ItemCode) AND (t1.whscode=t2.whscode))
		LEFT JOIN OCRD T3 ON T1.U_Codigo_Cliente = T3.CardCode
		LEFT JOIN OPCH T4 ON T1.TrgetEntry = T4.DocEntry
		LEFT JOIN PCH1 T5 ON ((T1.TrgetEntry = T5.DocEntry) and (T4.DocEntry = T5.DocEntry))
		where t0.DocDate between @FechaIni and @FechaFin and T1.OpenQty>0

		UNION ALL

		select distinct
			t5.ItemCode [Cod. Art],
		case 	when t5.ItemCode='041-PVEYPO' then t5.U_Texto
			when t5.ItemCode<>'041-PVEYPO'then t5.Dscription
			end [Articulo],
			t5.whscode[Alm.Dest],
			t5.Quantity[Cant.Sol],
			t5.OpenQty[Cant.Ab],
			t2.OnHand[Exist.Alm.Dest],
			t1.U_FechaRecepcion,
			t1.U_Comentarios,
			t5.U_Codigo_Cliente[No. Clie.],
			t3.CardName[Nombre Cliente],
			t5.U_N_Pedido_Cliente[OC Clie.],
			t0.docnum[OC SAP], t0.cardcode [Codigo], t0.cardname, t0.numatcard [Referencia],t0.docdate [Fecha Doc.] /*, t0.DocDueDate [Fecha Llegada]*/
		from
		OPOR T0
		LEFT JOIN POR1 T1 ON T0.DocEntry = T1.DocEntry
		LEFT JOIN OITW T2 on ((T1.ItemCode = T2.ItemCode) AND (t1.whscode=t2.whscode))
		LEFT JOIN OCRD T3 ON T1.U_Codigo_Cliente = T3.CardCode
		LEFT JOIN OPCH T4 ON T1.TrgetEntry = T4.DocEntry
		LEFT JOIN PCH1 T5 ON ((T1.TrgetEntry = T5.DocEntry) and (T4.DocEntry = T5.DocEntry))
		where t0.DocDate between @FechaIni and @FechaFin and T5.OpenQty>0 and T1.TargetType ='18'
		end
		ELSE
		if((@Prov is null or @Prov ='' or @Prov =' ') and (@cliente is not null or @cliente<>'' or @cliente <> ' '))
		begin
		select distinct
			t1.ItemCode[Cod. Art],
		case when t1.ItemCode='041-PVEYPO' then t1.U_Texto
		when T1.ItemCode<>'041-PVEYPO' then t1.Dscription
		end [Articulo],
			t1.whscode[Alm.Dest],
			t1.Quantity[Cant.Sol],
			t1.OpenQty[Cant.Ab],
			t2.OnHand[Exist.Alm.Dest],
			t1.U_FechaRecepcion,
			t1.U_Comentarios,
			t1.U_Codigo_Cliente[No. Clie.],
			t3.CardName[Nombre Cliente],
			t1.U_N_Pedido_Cliente[OC Clie.],
			t0.docnum[OC SAP], t0.cardcode [Codigo], t0.cardname, t0.numatcard [Referencia],t0.docdate [Fecha Doc.] /*, t0.DocDueDate [Fecha Llegada]*/
		from
		OPOR T0
		LEFT JOIN POR1 T1 ON T0.DocEntry = T1.DocEntry
		LEFT JOIN OITW T2 on ((T1.ItemCode = T2.ItemCode) AND (t1.whscode=t2.whscode))
		LEFT JOIN OCRD T3 ON T1.U_Codigo_Cliente = T3.CardCode
		LEFT JOIN OPCH T4 ON T1.TrgetEntry = T4.DocEntry
		LEFT JOIN PCH1 T5 ON ((T1.TrgetEntry = T5.DocEntry) and (T4.DocEntry = T5.DocEntry))
		where t0.DocDate between @FechaIni and @FechaFin and T1.OpenQty>0 and t1.U_Codigo_Cliente = @cliente

		UNION ALL

		select distinct
			t5.ItemCode [Cod. Art],
		case 	when t5.ItemCode='041-PVEYPO' then t5.U_Texto
			when t5.ItemCode<>'041-PVEYPO'then t5.Dscription
			end [Articulo],
			t5.whscode[Alm.Dest],
			t5.Quantity[Cant.Sol],
			t5.OpenQty[Cant.Ab],
			t2.OnHand[Exist.Alm.Dest],
			t1.U_FechaRecepcion,
			t1.U_Comentarios,
			t5.U_Codigo_Cliente[No. Clie.],
			t3.CardName[Nombre Cliente],
			t5.U_N_Pedido_Cliente[OC Clie.],
			t0.docnum[OC SAP], t0.cardcode [Codigo], t0.cardname, t0.numatcard [Referencia],t0.docdate [Fecha Doc.] /*, t0.DocDueDate [Fecha Llegada]*/
		from
		OPOR T0
		LEFT JOIN POR1 T1 ON T0.DocEntry = T1.DocEntry
		LEFT JOIN OITW T2 on ((T1.ItemCode = T2.ItemCode) AND (t1.whscode=t2.whscode))
		LEFT JOIN OCRD T3 ON T1.U_Codigo_Cliente = T3.CardCode
		LEFT JOIN OPCH T4 ON T1.TrgetEntry = T4.DocEntry
		LEFT JOIN PCH1 T5 ON ((T1.TrgetEntry = T5.DocEntry) and (T4.DocEntry = T5.DocEntry))
		where t0.DocDate between @FechaIni and @FechaFin and T5.OpenQty>0 and T1.TargetType ='18'and t5.U_Codigo_Cliente = @cliente
		end
		ELSE
		if((@prov is not null or @prov<>'' or @prov <> ' ') and (@cliente is null or @cliente ='' or @cliente =' '))
		begin
		select distinct
			t1.ItemCode[Cod. Art],
		case when t1.ItemCode='041-PVEYPO' then t1.U_Texto
		when T1.ItemCode<>'041-PVEYPO' then t1.Dscription
		end [Articulo],
			t1.whscode[Alm.Dest],
			t1.Quantity[Cant.Sol],
			t1.OpenQty[Cant.Ab],
			t2.OnHand[Exist.Alm.Dest],
			t1.U_FechaRecepcion,
			t1.U_Comentarios,
			t1.U_Codigo_Cliente[No. Clie.],
			t3.CardName[Nombre Cliente],
			t1.U_N_Pedido_Cliente[OC Clie.],
			t0.docnum[OC SAP], t0.cardcode [Codigo], t0.cardname, t0.numatcard [Referencia],t0.docdate [Fecha Doc.] /*, t0.DocDueDate [Fecha Llegada]*/
		from
		OPOR T0
		LEFT JOIN POR1 T1 ON T0.DocEntry = T1.DocEntry
		LEFT JOIN OITW T2 on ((T1.ItemCode = T2.ItemCode) AND (t1.whscode=t2.whscode))
		LEFT JOIN OCRD T3 ON T1.U_Codigo_Cliente = T3.CardCode
		LEFT JOIN OPCH T4 ON T1.TrgetEntry = T4.DocEntry
		LEFT JOIN PCH1 T5 ON ((T1.TrgetEntry = T5.DocEntry) and (T4.DocEntry = T5.DocEntry))
		where t0.DocDate between @FechaIni and @FechaFin and T1.OpenQty>0 and t0.CardName = @Prov

		UNION ALL

		select distinct
			t5.ItemCode [Cod. Art],
		case 	when t5.ItemCode='041-PVEYPO' then t5.U_Texto
			when t5.ItemCode<>'041-PVEYPO'then t5.Dscription
			end [Articulo],
			t5.whscode[Alm.Dest],
			t5.Quantity[Cant.Sol],
			t5.OpenQty[Cant.Ab],
			t2.OnHand[Exist.Alm.Dest],
			t1.U_FechaRecepcion,
			t1.U_Comentarios,
			t5.U_Codigo_Cliente[No. Clie.],
			t3.CardName[Nombre Cliente],
			t5.U_N_Pedido_Cliente[OC Clie.],
			t0.docnum[OC SAP], t0.cardcode [Codigo], t0.cardname, t0.numatcard [Referencia],t0.docdate [Fecha Doc.] /*, t0.DocDueDate [Fecha Llegada]*/
		from
		OPOR T0
		LEFT JOIN POR1 T1 ON T0.DocEntry = T1.DocEntry
		LEFT JOIN OITW T2 on ((T1.ItemCode = T2.ItemCode) AND (t1.whscode=t2.whscode))
		LEFT JOIN OCRD T3 ON T1.U_Codigo_Cliente = T3.CardCode
		LEFT JOIN OPCH T4 ON T1.TrgetEntry = T4.DocEntry
		LEFT JOIN PCH1 T5 ON ((T1.TrgetEntry = T5.DocEntry) and (T4.DocEntry = T5.DocEntry))
		where t0.DocDate between @FechaIni and @FechaFin and T5.OpenQty>0 and T1.TargetType ='18'and t0.CardName = @Prov
		end
		else
		if((@Prov is not null or @Prov <>'' or @Prov<>' ') and (@cliente is not null or @cliente<>'' or @cliente <> ' '))
		begin
		select distinct
			t1.ItemCode[Cod. Art],
		case when t1.ItemCode='041-PVEYPO' then t1.U_Texto
		when T1.ItemCode<>'041-PVEYPO' then t1.Dscription
		end [Articulo],
			t1.whscode[Alm.Dest],
			t1.Quantity[Cant.Sol],
			t1.OpenQty[Cant.Ab],
			t2.OnHand[Exist.Alm.Dest],
			t1.U_FechaRecepcion,
			t1.U_Comentarios,
			t1.U_Codigo_Cliente[No. Clie.],
			t3.CardName[Nombre Cliente],
			t1.U_N_Pedido_Cliente[OC Clie.],
			t0.docnum[OC SAP], t0.cardcode [Codigo], t0.cardname, t0.numatcard [Referencia],t0.docdate [Fecha Doc.] /*, t0.DocDueDate [Fecha Llegada]*/
		from
		OPOR T0
		LEFT JOIN POR1 T1 ON T0.DocEntry = T1.DocEntry
		LEFT JOIN OITW T2 on ((T1.ItemCode = T2.ItemCode) AND (t1.whscode=t2.whscode))
		LEFT JOIN OCRD T3 ON T1.U_Codigo_Cliente = T3.CardCode
		LEFT JOIN OPCH T4 ON T1.TrgetEntry = T4.DocEntry
		LEFT JOIN PCH1 T5 ON ((T1.TrgetEntry = T5.DocEntry) and (T4.DocEntry = T5.DocEntry))
		where t0.DocDate between @FechaIni and @FechaFin and T1.OpenQty>0 and t0.CardName = @Prov and t1.U_Codigo_Cliente = @cliente

		UNION ALL

		select distinct
			t5.ItemCode [Cod. Art],
		case 	when t5.ItemCode='041-PVEYPO' then t5.U_Texto
			when t5.ItemCode<>'041-PVEYPO'then t5.Dscription
			end [Articulo],
			t5.whscode[Alm.Dest],
			t5.Quantity[Cant.Sol],
			t5.OpenQty[Cant.Ab],
			t2.OnHand[Exist.Alm.Dest],
			t1.U_FechaRecepcion,
			t1.U_Comentarios,
			t5.U_Codigo_Cliente[No. Clie.],
			t3.CardName[Nombre Cliente],
			t5.U_N_Pedido_Cliente[OC Clie.],
			t0.docnum[OC SAP], t0.cardcode [Codigo], t0.cardname, t0.numatcard [Referencia],t0.docdate [Fecha Doc.] /*, t0.DocDueDate [Fecha Llegada]*/
		from
		OPOR T0
		LEFT JOIN POR1 T1 ON T0.DocEntry = T1.DocEntry
		LEFT JOIN OITW T2 on ((T1.ItemCode = T2.ItemCode) AND (t1.whscode=t2.whscode))
		LEFT JOIN OCRD T3 ON T1.U_Codigo_Cliente = T3.CardCode
		LEFT JOIN OPCH T4 ON T1.TrgetEntry = T4.DocEntry
		LEFT JOIN PCH1 T5 ON ((T1.TrgetEntry = T5.DocEntry) and (T4.DocEntry = T5.DocEntry))
		where t0.DocDate between @FechaIni and @FechaFin and T5.OpenQty>0 and T1.TargetType ='18'and t0.CardName = @Prov and t5.U_Codigo_Cliente = @cliente
		end
	";

	$consulta= sqlsrv_query($conn, $sql);

	$response = [];
	while( $row = sqlsrv_fetch_array($consulta, SQLSRV_FETCH_ASSOC) ) {        
		$response[] = $row;
	}
	echo json_encode( $response );
?>
