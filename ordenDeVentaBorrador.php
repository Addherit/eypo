<?php
	include "db/Utils.php";

	session_start();
	$_SESSION['usuario'];
	$empleadoVentas = $_SESSION['usuario'];

	$sqlEmpleado = "SELECT * FROM EYPO.dbo.IV_EY_PV_Usuarios WHERE [User] = '$empleadoVentas' ";
	$consulta = sqlsrv_query($conn, $sqlEmpleado);
	$r = sqlsrv_fetch_array($consulta);
	$nombreCompleto = $r['Nombre']." ".$r['SegundoNombre']." ".$r['Apellidos'];


	if (isset($_GET['FolioSAP'])) {
		$ofertaVenta = $_GET['FolioSAP'];
		$sql ="SELECT NuevoDocEntry FROM dbEypo.dbo.ordenes WHERE CodOferta = '$ofertaVenta'";
		$consulta = sqlsrv_query($conn, $sql);
		$r = sqlsrv_fetch_array($consulta);
		$nuevoDocEntry = $r['NuevoDocEntry'];
	} else {
		$nuevoDocEntry = 0;
	}
?>

<!DOCTYPE html>
<html>
	<?php include "header.php"?>
	<body>
		<?php include "nav.php"?>
		<?php include "modales.php"?>
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h1 style="color: #2fa4e7">Orden de venta (Borrador)</h1>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">
					<?php include "botonesDeControl.php" ?>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
				    <label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Cliente:</label>
				    <div class="col-6">
				    	<input type="text" id="codcliente" disabled>
				    </div>
					</div>
					<div class="row">
				  	<label class="col-3 col-md-4 col-lg-3 col-form-label">Nombre:</label><br>
				    <div class="col-6">
				    	<input type="text" class="NombreC" disabled>
				    </div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Persona de contacto:</label>
						<div class="col-6">
							<input type="text" class="listcontactos" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Oferta de compra:</label>
						<div class="col-6">
							<input type="text" id="oCompra"  disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Tipo moneda:</label>
							<div class="col-6">
								<input type="text" id="tmoneda" disabled>
							</div>
					</div>
					<div class="row">
						<label class="col-3 col-md-4 col-lg-3 col-form-label">Uso principal</label>
						<div class="col-6">
							<select id="usoPrincipal">
								<option value="" disabled selected>Seleccione</option>
								<?php
								$sql = "SELECT * FROM IV_EY_PV_UsosCfdi";
								$consulta = sqlsrv_query($conn, $sql);
								while ($Row = sqlsrv_fetch_array($consulta)) { ?>
									<option value="<?php  echo $Row['CodigoUsoCfdi']; ?>"><?php echo $Row['CodigoUsoCfdi']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Método de pago</label>
						<div class="col-6">
							<select id="metodoPago">
								<option value="" disabled selected>Seleccione</option>
								<?php
									$sql = "SELECT * FROM IV_EY_PV_MetodosPago";
									$consulta = sqlsrv_query ($conn, $sql);
									while ($row = sqlsrv_fetch_array($consulta)) { ?>
									<option value="<?php echo $row['CodigoMetodoPagoSat']; ?>"><?php echo $row['CodigoMetodoPagoSat']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Forma de pago</label>
						<div class="col-6">
							<select id="formaPago">
								<option value="" disabled selected>Seleccione</option>
								<?php
									$sql = "SELECT DISTINCT CodigoFormaPago FROM EYPO.dbo.IV_EY_PV_SN_FormasPago";
									$consulta = sqlsrv_query ($conn, $sql);
									while ($row = sqlsrv_fetch_array($consulta)) { ?>
									<option value="<?php echo $row['CodigoFormaPago']; ?>"><?php echo $row['CodigoFormaPago']; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<label for="" class="col-sm-3 offset-md-4 col-form-label">N° Folio:</label>
						<div class="col-sm-4">							
							<input type="text" id="cdoc" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 offset-md-4 col-form-label">Estado:</label>
							<div class="col-sm-4">
								<input type="text" id="status" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 offset-md-4 col-form-label">Fecha de contabilización:</label>
						<div class="col-sm-4">
							<input type="date" id="fconta" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 offset-md-4 col-form-label">Fecha de entrega	:</label>
						<div class="col-sm-4">
							<input type="date" class="" id="fentrega" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 offset-md-4 col-form-label">Fecha del documento:</label>
						<div class="col-sm-4">
							<input type="date" class="" id="fdoc" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 offset-md-4 col-form-label">Fecha de vencimiento:</label>
						<div class="col-sm-4">
							<input type="date" class="" id="fvencimiento" disabled>
						</div>
					</div>
					<div class="row">
						<div class="col-md-7 offset-md-4 text-right" style="margin-top: 10px">
							<button class="btn btn-secundary btn-sm" id="btnCamposDefinidos" title="Campos definidos por el usuario">Campos definidos por el usuario</button>
						</div>
					</div>
					<div class="row" id="camposDefinidos" style="font-size: .7rem; margin-top: 10px">
						<label for="" class="col-sm-3 offset-md-4 col-form-label">Número a letra:</label>
						<div class="col-sm-5">
							<input type="text"id="numLetra" style="width: 90%; height:23px;">
						</div>
						<label for="" class="col-sm-3 offset-md-4 col-form-label">Tipo de factura:</label>
						<div class="col-sm-5">
							<input type="text" id="tipoFactura" style="width: 90%; height:23px;">
							<a href="#" data-toggle="modal" data-target="#modalTipoFactura">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
							</a>
						</div> 
						<label for="" class="col-sm-3 offset-md-4 col-form-label">LAB:</label>
						<div class="col-sm-5">
							<input type="text"  id="lab" style="width: 90%; height:23px;">
						</div>
						<label for="" class="col-sm-3 offset-md-4 col-form-label">Condición de pago:</label>
						<div class="col-sm-5">
							<input type="text"  id="condicionPago" style="width: 90%; height:23px;">
							<a href="#" data-toggle="modal" data-target="#modalCondicionPago">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
							</a>
						</div>
						<label for="" class="col-sm-3 offset-md-4 col-form-label">RFC:</label>
						<div class="col-sm-5">
							<input type="text" id="rfc_campos_definidos" style="width: 90%; height:23px;">
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row datosEnc">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
						  <a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Contenido</a>
						</li>				
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
					  	<br>
							<div class="row">
								<div class="col-md-6">
									<div class="row">
									    <label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom: 0px">Clase de artículo:</label>
									   <div class="col-sm-4" >
												<select disabled>
													<option value="I">Artículo</option>
													<option value="S">Servicio</option>
												</select>
									    </div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
								    	<label class="col-sm-3  offset-sm-4 col-form-label">Clase de resumen:</label>
								   		<div class="col-sm-4">
									    	<select class="" disabled>
	           									<option>Sin Resumen</option>
	          								</select>
									    </div>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-12">
									<table class="table-bordered table-editable text-center" width="100%" id="detalleoferta" disable>
										<thead>
											<tr class="encabezado">
												<th></th>
												<th>#</th>
												<th  scope="col">Número de artíuclo</th>
												<th  scope="col">Descripción de artíuclo</th>
												<th  scope="col">Cantidad</th>
												<th  scope="col">Precio por unidad</th>											
												<th  scope="col">Total</th>
												<th  scope="col">Almacen</th>
												<th  scope="col">Cantidad pendiente</th>
												<th> Codigo Clasificación SAT</th>
												<th> Unidad medida SAP</th>
												<th>Comentarios de partida 1</th>
												<th>Comentarios de partida 2</th>
												<th>Stock</th>
												<th>Comprometido</th>
												<th>Solicitado</th>		
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="cont">1</td>
												<td class="narticulo"></td>
												<td class="darticulo"></td>
												<td class="cantidad"></td>
												<td class="precio"></td>									
												<td class="total"></td>
												<td class="almacen"></td>
												<td class="pendiente"></td>
												<td class="codigoClasificacionSAT"></td>
												<td class="unidadMedidaSAP"></td>
												<td class="comentario1"></td>
												<td class="comentario2"></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>	
					  </div>
						<br>					  	
					</div>
					<div class="row datosEnc">
						<div class="col-md-6">
							<div class="row">
									<label for="" class="col-sm-3 col-form-label">Empleado de ventas:</label>
									<div class="col-sm-6">
										<input type="text" id="empleado" value="<?php echo "$empleadoVentas"; ?>" disabled>
									</div>
							</div>

							<div class="row">
									<label for="" class="col-sm-3 col-form-label">Propietario:</label>
									<div class="col-sm-6">
										<input type="text" value="<?php echo "$nombreCompleto"; ?>" id="NombrePropietario" disabled>
									</div>
							</div>
							<div class="row">
									<label for="" class="col-sm-3 col-form-label">Comentarios:</label>
									<div class="col-sm-4">
										<textarea id="comentarios" cols="60" rows="3" style="background-color: #ffff002e"></textarea>
									</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
									<label for="" class="col-sm-3 offset-md-4 col-form-label">Total antes descuento:</label>
									<div class="col-sm-4">
										<input class="text-right" type="text" id="totalAntesDescuento" value="0.00" disabled>
									</div>
							</div>
							<div class="row">
									<label for="" class="col-sm-3 offset-md-4 col-form-label">Descuento:</label>
									<div class="col-sm-4">
										<input class="text-center" type="text" id="descNum" value="0" style="width: 20%;" disabled> %
									<input type="text" class="text-right" id="descAplicado" value="0.00" style="width: 70%" disabled>
									</div>
							</div>
							<div class="row">
									<label for="" class="col-sm-3 offset-md-4 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Impuesto:</label>
									<div class="col-sm-4">
										<input type="text" class="text-right" value="0.00" id="impuestoTotal" disabled>
									</div>
							</div>
							<div class="row">
									<label for="" class="col-sm-3 offset-md-4 col-form-label" style="padding-right: 0px; padding-bottom: 0px">Total del documento:</label>
									<div class="col-sm-4">
										<input type="text" class="text-right" value="0.00" id="totalDelDocumento"disabled>
									</div>
							</div>
						</div>
					</div>
					<br><br>
			
					<div class="row" id= btnFoot>
						<div class="col-md-6">
							<button class="btn btn-sm" style="background-color: orange" id="btnCrear" title="Crear">Crear</button>
							<a href="">	<button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include "footer.php"; ?>
	</body>
	<script type="text/javascript">

		var nuevoDocEntry = <?php echo "$nuevoDocEntry" ?>;

		function mostrarDatosEnInputs(response){
			$("#codcliente").val(response['CodCliente']).prop("onlyread", true);
			$(".NombreC").val(response['NombreC']).prop("onlyread", true);
			$(".listcontactos").val(response['ListContactos']).prop("onlyread", true);
			$("#oCompra").val(<?php echo $_GET['FolioSAP'] ?>).prop("onlyread", true);
			$("#tmoneda").val(response['TipoMoneda']).prop("onlyread", true);
			$("#usoPrincipal").val(response['usoPrincipal']);
			$("#metodoPago").val(response['metodoPago']);
			$("#formaPago").val(response['formaPago']);
			// $("#ndoc").val(response['NDoc']).prop("disabled", true);
			var lesito = response['DocNum'];
			$("#cdoc").val(lesito).prop("onlyread", true);
			$("#status").val(response['Status']).prop("onlyread", true);
			$("#fvencimiento").val(response['Fvencimiento']).prop("onlyread", true);
			$("#fconta").val(response['FConta']).prop("onlyread", true);
			// $("#fentrega").val(response['FEntrega']).prop("disabled", true);
			$("#fdoc").val(response['FDoc']).prop("onlyread", true);
			$("#numLetra").val(response['numLetra']);
			$("#tipoFactura").val(response['TipoFactura']);
			$("#lab").val(response['Lab']);
			$("#condicionPago").val(response['CondicionPago']);
			$("#rfc_campos_definidos").val(response['RFC']);
			
			$('#comentarios').val(response['comentarios']);
			var descuento = parseFloat(response['totalDescuento']);
			$('#totalAntesDescuento').val(descuento.toFixed(2)).prop("onlyread", true);
			// $('#descNum').val(response['descNum']).prop("disabled", true);
			// $('#desAplicado').val(response['desAplicado']).prop("disabled", true);
			// $('#redondeo').val(response['redondeo']).prop("disabled", true);

			// $('.empleado').prop("disabled", true);
			// $('#NombrePropietario').prop("disabled", true);

			var impuesto = parseFloat(response['impuesto']);
			$('#impuestoTotal').val(impuesto.toFixed(2)).prop("onlyread", true);
			var totalDoc = parseFloat(response['totalDelDocumento']);
			$('#totalDelDocumento').val(totalDoc.toFixed(2)).prop("onlyread", true);



			$("#detalleoferta tbody").empty();
			$("#detalleoferta tbody").append(response['detalle']);
		}

		if (nuevoDocEntry != 0 ) {

			$.ajax({
				url:'orvConsultas/clicORVConsultaGeneralBorrador.php',
				type:'post',
				dataType:'json',
				data: {codigo: nuevoDocEntry},
			}).done(function (response) {
				console.log(response);
				
			
				mostrarDatosEnInputs(response);

	
					
				

			});
		}

		$("#buscadorOrdenes").keyup(function(){
			var valorEscrito = $('#buscadorOrdenes').val();

			if (valorEscrito) {
				$.ajax({
					url: 'orvConsultas/buscadorORVparaTablaModal.php',
					type: 'post',
					data: {valor: valorEscrito},
					success: function(response){
						$('#tblBuscarOrdenes tbody').empty();
						$('#tblBuscarOrdenes tbody').append(response);

						$("#tblBuscarOrdenes tbody tr").on('click',function(){
							var codigo = $(this).find('td').eq(0).text();

							$.ajax({
							type: 'post',
							url: 'orvConsultas/clicORVConsultaGeneral.php',
							dataType:'json',
							data:{ codigo: codigo},
							success: function(response){

								mostrarDatosEnInputs();

								$.ajax({
									type:'post',
									url:'orvConsultas/consultaORV2.php',
									data: { cdoc: lesito},
									success: function(res){
										$("#detalleoferta tbody").empty();
										$("#detalleoferta tbody").append(res);
									}
								});
								$("#modalBuscarOrdenes").modal('hide');
							},
							});
						});
					},
				});

			}else {
				$('#tblBuscarOfertas tbody').empty();
			}
		});

		$("#mensajes").hide();
		$("#btnbuscarOferta").hide();
		$("#btnPdf").hide();
		$("#btnxls").hide();
		$("#consultaQuery").hide();
		$("#btnbuscarOrden").show();
 
		$("#btnCrear").on('click', function () {
			var ofv = <?PHP echo $_GET['FolioSAP']?>;
			var usoPrincipal = $("#usoPrincipal").val();
			var metodoPago = $("#metodoPago").val();
			var formaPago = $("#formaPago").val();
			var numLetra = $("#numLetra").val();
			var tipoFactura = $("#tipoFactura").val();
			var lab = $("#lab").val();
			var condicionPago = $("#condicionPago").val();
			var comentarios = $("#comentarios").val();
			// var rfc = $("#rfc_campos_definidos").val();

			$.ajax({
				url:'ordenDeVentaBorradorInsert.php',
					type:'post',
				data: {
					ofv :ofv,
					usoPrincipal : usoPrincipal,
					metodoPago : metodoPago,
					formaPago : formaPago,
					numLetra : numLetra,
					tipoFactura : tipoFactura,
					lab : lab,
					condicionPago : condicionPago,
					comentarios : comentarios,
					// rfc: rfc
				},
			}).done(function (response) {
				alert(response);
				window.location.href = 'ofertaDeVenta.php';
			});
		});

		$("#tblTipoFactura tbody tr").on('click', function () {
			var tipoFactura = $(this).find('td').eq(0).text();
			$("#tipoFactura").val(tipoFactura);
			$("#modalTipoFactura").modal('hide');
		})

		$("#tblCondicionPago tbody tr").on('click', function(){
			var descripcion = $(this).find('td').eq(1).text();
			$("#condicionPago").val(descripcion);
			$("#modalCondicionPago").modal('hide');
		});

		$("#consultaPrimerRegistro").hide();
		$("#consultaUltimoRegistro").hide();
		$("#consulta1Atras").hide();
		$("#consulta1Adelante").hide();
	</script>
</html>
