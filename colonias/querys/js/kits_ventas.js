$.ajax({
    url:"php/kits_ventas.php",
    dataType: 'JSON',
}).done(function (response) {		
        
$("#tbl_kit_venta tbody").empty()
var tr_order = "";

if(response.length > 0) {
    response.forEach((element, index) => {
        index++
        tr_order += `<tr>        
        <td>${index}</td>
        <td>${element.Code}</td>
        <td>${element.ItemNme}</td>
        <td>${element.Cant}</td>        
        <td>${element.Tipo}</td>    
        </tr>`
    })
} else {
    tr_order += '<tr><td colspan="17">No existe registro alguno con los datos proporcionados</td></tr>' 
}    
$("#tbl_kit_venta tbody").append(tr_order);			    
});   