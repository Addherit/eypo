<?php
include "../conexion.php";
  
$fechaInicio = $_POST['fechaInicio'];
$fechaFin = $_POST['fechaFin'];

 $valor = $_POST['valor'];
$cont=0;
$conttc=0;
$tipocambio=0;
$consultatc = sqlsrv_query($conn,"SELECT Rate from from ORTT where datepart(year, RateDate) = datepart(year, getdate()) and datepart(month, RateDate) = datepart(month, getdate()) and datepart(day, RateDate) = datepart(day, getdate())  and Currency = 'USD'");
echo $consultatc;
 while ($Rowtc = sqlsrv_fetch_array($consultatc)) {
  $conttc++;
  $tipocambio=$Rowtc['Rate'];
 }
$consultasql = sqlsrv_query($conn,"SELECT NombreEmpleadoVC as Proyectista, 
(Select count(id) from MasterEypo.dbo.Proyectos p2 where p2.TipoProyecto = 'INTERIOR' and p2.Proyectista COLLATE SQL_Latin1_General_CP850_CI_AS = e.NombreEmpleadoVC and p2.Estatus not in ('CONCLUIDO', 'RECHAZADO', 'PERDIDO') and p2.FechaCreacion between '$fechaInicio' and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111)) as CantidadInterior,
(Select count(id) from MasterEypo.dbo.Proyectos p3 where p3.TipoProyecto = 'INTERIOR' and p3.TipoPropuesta like '%Control%' and p3.Proyectista COLLATE SQL_Latin1_General_CP850_CI_AS = e.NombreEmpleadoVC and p3.Estatus not in ('CONCLUIDO', 'RECHAZADO', 'PERDIDO') and p3.FechaCreacion between '$fechaInicio' and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111)) as CantidadConControl,
(Select count(id) from MasterEypo.dbo.Proyectos p4 where p4.TipoProyecto = 'INTERIOR' and p4.TipoPropuesta not like '%Control%' and p4.Proyectista COLLATE SQL_Latin1_General_CP850_CI_AS = e.NombreEmpleadoVC and p4.Estatus not in ('CONCLUIDO', 'RECHAZADO', 'PERDIDO') and p4.FechaCreacion between '$fechaInicio' and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111)) as CantidadSinControl,
(Select '')  as Observaciones,
(Select SUM(case when pd.MonedaVenta = 'USD' Then isnull(PrecioVenta,0) * '$tipocambio'  else isnull(PrecioVenta,0) end) from MasterEypo.dbo.ProyectosD pd inner join MasterEypo.dbo.Proyectos p6 on pd.IdProyecto = p6.id and p6.Proyectista COLLATE SQL_Latin1_General_CP850_CI_AS = e.NombreEmpleadoVC and p6.Estatus not in ('CONCLUIDO', 'RECHAZADO', 'PERDIDO') and pd.Estatus = 'ALTA' and p6.TipoProyecto = 'INTERIOR' and p6.FechaCreacion between '$fechaInicio' and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111)) as MontoProyectado
FROM IV_EY_PV_EmpleadosVentasCompras e
left outer join MasterEypo.dbo.proyectos p on e.NombreEmpleadoVC = p.Proyectista COLLATE SQL_Latin1_General_CP850_CI_AS
where NombreEmpleadoVC like 'P%' 
group by NombreEmpleadoVC
union all
Select 'Totales' as Proyectista, 
(Select count(id) from MasterEypo.dbo.Proyectos p2 where p2.TipoProyecto = 'INTERIOR' and p2.Estatus not in ('CONCLUIDO', 'RECHAZADO', 'PERDIDO') and p2.Proyectista like'P%' and p2.FechaCreacion between '$fechaInicio' and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111)) as CantidadInterior ,
(Select count(id) from MasterEypo.dbo.Proyectos p3 where p3.TipoProyecto = 'INTERIOR' and p3.TipoPropuesta like '%Control%' and p3.Estatus not in ('CONCLUIDO', 'RECHAZADO', 'PERDIDO') and p3.Proyectista like'P%' and p3.FechaCreacion between '$fechaInicio' and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111)) as CantidadConControl,
(Select count(id) from MasterEypo.dbo.Proyectos p4 where p4.TipoProyecto = 'INTERIOR' and p4.TipoPropuesta not like '%Control%' and p4.Estatus not in ('CONCLUIDO', 'RECHAZADO', 'PERDIDO') and p4.Proyectista like'P%' and p4.FechaCreacion between '$fechaInicio' and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111)) as CantidadSinControl,
(Select '')   as Observaciones,
(Select SUM(case when pd.MonedaVenta = 'USD' Then isnull(PrecioVenta,0) * '$tipocambio'  else isnull(PrecioVenta,0) end) from MasterEypo.dbo.ProyectosD pd inner join MasterEypo.dbo.Proyectos p7 on pd.IdProyecto = p7.id  and p7.Estatus not in ('CONCLUIDO', 'RECHAZADO', 'PERDIDO') and pd.Estatus = 'ALTA' and p7.Proyectista like'P%' and p7.TipoProyecto = 'INTERIOR' and p7.FechaCreacion between '$fechaInicio' and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111)) as MontoProyectado
"


); 
while ($Row = sqlsrv_fetch_array($consultasql)) {
  $cont++;
?>
<tr>

  
  <td><?php echo $Row['Proyectista'];?></td>
  <td><?php echo $Row['CantidadInterior'];?></td>
  <td><?php echo ($Row['CantidadConControl']);?></td> 
  <td><?php echo ($Row['CantidadSinControl']);?></td> 
  <td><?php echo ($Row['Observaciones']);?></td> 
  <td><?php echo "$".number_format($Row['MontoProyectado'], 2, '.', '');?></td> 
    
 </tr>
<?php } ?>
