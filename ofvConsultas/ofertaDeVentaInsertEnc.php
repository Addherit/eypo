<?php
  include "../db/Utils.php";

  $datoCab  = $_POST['valores'] ;
  $datoDet = $_POST['detalle'];  
  $response = [];

  
       
  $sql1 ="SELECT MAX(DocNum) as Folio FROM [dbEypo].[dbo].[ofertas] WHERE serie = 'COT-GRAL'";
  $consulta = sqlsrv_query( $conn, $sql1);
  $row = $Row = sqlsrv_fetch_array($consulta);
  $max_mas_uno = $row['Folio'] + 1;

  $sql = "INSERT INTO dbEypo.dbo.ofertas ( 
      CodCliente, created_at, NombreC, ListContactos, OrdCompra, TipoMoneda, UsoPrincipal, MetodoPago, FormaPago, 
      DocNum, Status, FConta, FEntrega, FDoc, Fvencimiento, numLetra, TipoFactura, Lab, CondicionPago, RFC,
      Usuario, NombreUsuario, ProyectoSN, VentasAdic, Promotor, CordVenta, Comentarios, TotalDescuento, DescNum, 
      DesAplicado, Redondeo, Impuesto, totalDelDocumento, Actualizar, EstatusActualizar, Cancelar, EstatusCancelar, serie
      ) VALUES (
        '$datoCab[ClienteCod]', getDate(), '$datoCab[NombreCliente]', '$datoCab[PersonaContacto]', '$datoCab[OrdenCompra]',
        '$datoCab[TipoMoneda]', '$datoCab[UsoPrincipal]', '$datoCab[MetodoPago]', '$datoCab[FormaPago]', 
        '$max_mas_uno', '$datoCab[Status]', '$datoCab[FechaConta]', '$datoCab[FechaEntrega]', '$datoCab[FechaDoc]',
        '$datoCab[FechaVencimiento]', '$datoCab[NumeroLetra]', '$datoCab[TipoFactura]', '$datoCab[LAB]', 
        '$datoCab[CondicionPago]', '$datoCab[RFC]', '$datoCab[CodEmpleado]', '$datoCab[NombrePropietario]', 
        '$datoCab[ProyectoSN]', '$datoCab[VentasAdic]', '$datoCab[Promotor]', '$datoCab[CordVenta]', '$datoCab[Comentarios]',
        '$datoCab[Subtotal]', '$datoCab[DescPorcentaje]', '$datoCab[DescuentoCant]', '$datoCab[Redondeo]', 
        '$datoCab[ImpuestoTotal]', '$datoCab[TotalDocumento]', 'N', 0, 'N', 0, 'COT-GRAL'
      )";

    $consultasql = sqlsrv_query($conn, $sql);

    if( $consultasql === false ) {
      $response = [];
      echo "Algo falló al insertar los datos de cabecera.";
    } else {
      foreach ($datoDet as $tr) {
        $string = "";
        foreach ($tr as $td) {
          $string .= "'".str_replace("'", "''", $td)."',";
        }
        $string .= "'".$max_mas_uno."'";
        $sql = "INSERT INTO dbEypo.dbo.ofertaDet (articulo, descripcion, cantidad, precioUnidad, total, almacen, 
        cantidadPendiente, CodigoPlanificacionSAP, unidadMedidaSAP, comentarioPartida1, comentarioPartida2, stock, comprometido, solicitado,
        TreeType, lineNum, Hijo, DocNum)
        VALUES ($string)";
        $consulta2 = sqlsrv_query($conn, $sql);  
      }

      if( $consulta2 === false ) {
        $response = ['resp' => 0 , 'mnj' => 'Algo falló al insertar los datos de detalle', 'sql' => $sql];        
      } else { 
        $response = ['resp' => 2 , 'mnj' => 'Se creo correctamente la oferta de venta'];
      } 
    }
  
    echo json_encode( $response );

 ?>
