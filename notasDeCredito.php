<?php
	include "db/Utils.php";

	$sql = "SELECT min(FolioSAP) as primer, max(FolioSAP) as ultimo FROM EYPO.dbo.IV_EY_PV_NotasCreditoVentaCab"; 
	$consulta = sqlsrv_query($conn, $sql);
	$r = sqlsrv_fetch_array($consulta);
	$primerRegistro = $r['primer'];
	$ultimoRegistro = $r['ultimo'];
	$new_name = rand();
?>
<!DOCTYPE html>
<html> 
	<?php include "header.php"?>
	<body>
		<?php include "nav.php"?>
		<?php include "modalQuerys.php"?>
		<?php include "modales.php"?>
		
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h1 style="color: #2fa4e7">Notas de crédito</h1>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">
					<?php include "botonesDeControl.php" ?>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<label class="col-3 col-md-4 col-lg-3 col-form-label" >Cliente:</label>
						<div class="col-6">
							<input type="text" id="cliente">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Nombre:</label><br>
						<div class="col-6">
							<input type="text" id="nombreDelCliente">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Persona de contacto:</label>
						<div class="col-6">
							<input type="text" id="personaContacto">								
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Referencia:</label>
						<div class="col-6">
							<input type="text" class="" id="referencia">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Tipo moneda:</label>
						<div class="col-6">
							<input type="text" id="tipoMoneda">								
						</div>
					</div>	
					<div class="row">								
						<div class="col-3 col-md-4 col-lg-3 col-form-label">
							<input type="text" id="rutaArchivos" style="display: none">	
						</div>
					</div>									
				</div>
				<div class="col-md-6">
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">N° Folio:</label>
						<div class="col-6">													
							<input type="text" id="cdoc" class="text-right" disabled value="<?php echo $ultimoRegistro + 1 ?>">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Estado:</label>
						<div class="col-6">
							<input type="text" id="estado" class="text-right" value="Abierto" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha de contabilización:</label>
						<div class="col-6">
							<input type="date" value="<?php echo $hoy ?>" id="fconta" disabled>
						</div>
					</div>						
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha de vencimiento:</label>
						<div class="col-6">
							<input type="date" value="<?php echo $quincena ?>" id="fven" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha del documento:</label>
						<div class="col-6">
							<input type="date" value="<?php echo $hoy ?>" id="fdoc" disabled>
						</div>
					</div>		
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Método de pago:</label>
						<div class="col-6">
							<input type="text" id="metodoPago" disabled>
						</div>
					</div>		
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Forma de pago:</label>
						<div class="col-6">
							<input type="text" id="formaPago" disabled>
						</div>
					</div>		
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Uso principal:</label>
						<div class="col-6">
							<input type="text" id="usoPrincipal" disabled>
						</div>
					</div>						
				</div>
			</div>				
			<div class="row datosEnc">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Contenido</a>
						</li>						
					</ul>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
						<br>
						<div class="row">
							<div class="col-md-12">
								<table class=" table table-sm table-bordered table-striped text-center" id="tblNotasDeCredito">
									<thead>
										<tr class="encabezado" >											
											<th>#</th>
											<th>Código articulo</th>
											<th style="width: 100%">Descripción de artíuclo</th>					        				
											<th>Cantidad</th>
											<th>PrecioUnitario</th>
											<th>Almacén</th>	
											<th>Comentarios de partida 1</th>
											<th>Comentarios de partida 2</th>	
											<th>Stock</th>
											<th>Comprometido</th>
											<th>Solicitado</th>																																													
										</tr>
									</thead>
									<tbody> 
										<tr>
											<!-- <th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th> -->
											<td class="cont"></td>
											<td class="narticulo"></td>
											<td class="darticulo"></td>
											<td contenteditable="true" class="cantidad"></td>
											<td class="PrecioUnitario"></td>
											<td class="almacen"></td>		
											<td contenteditable="true" class="comentario1"></td>
											<td contenteditable="true" class="comentario2"></td>
											<td class="stock"></td>
											<td class="comprometido"></td>
											<td class="solicitado"></td>																									
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<br>
						<div class="row datosEnc">
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Empleado de ventas:</label>
									<div class="col-sm-6">
										<input type="text" id="empleado" value="<?php echo $empleadoVentas ?>">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Propietario:</label>
									<div class="col-sm-6">
										<input type="text" id="propietario" value="<?php echo $nombreCompleto ?>">
									</div>
								</div>
																						
								<div class="row">
									<label for="" class="col-sm-3 col-form-label" >Comentarios:</label>
									<div class="col-sm-4">
										<textarea name="" id="comentarios" cols="60" rows="4" style="background-color: #ffff002e;"></textarea>
									</div>
								</div>
							</div>

							<div class="col-md-6">								
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Subtotal:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="totalAntesDescuento">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Descuento:</label>
									<div class="col-6">
										<input class="text-center" value="0" type="text" id="descNum" style="width: 16%;">
										<span whidth="6%">%</span>
										<input class="text-right" value="0.00" type="text" id="descuento" style="width: 68% ">
									</div>
								</div>	
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >Anticipo total:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="anticipoTotal">
									</div>
								</div>		
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >Redondeo:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="redondeo">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >Impuesto:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="impuestoTotal">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >WImpte.retención:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="retencion">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >Total del documento:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="totalDocumento" style="width: 74%">
										<input class="text-right" type="text" id="monedaVisor" style="width: 15%">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >Importe Aplicado:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="importeAplicado">
									</div>
								</div>			
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >Saldo pendiente:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="saldoPendiente">
									</div>
								</div>																		
							</div>								
						</div>		 				
					</div>
				</div>
				<div class="row" id= btnFoot style="margin-bottom: 30px">
					<div class="col-md-6">				                            
						<a href=""><button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>					
					</div>	
					
				</div>	
			</div>						
		</div> 
		<?php include "footer.php"; ?>
	</body>
	<script>
		$("#mensajes").hide();

		function btn_busqueda_general () {
			$("#modalNotaDeCredito").modal('show') 
		}

		function consultaClickFactura(folio, condicion){

			var condicion = condicion;
			var ultimoRegistro = <?php echo "$ultimoRegistro"?>;
			var primerRegistro = <?php echo "$primerRegistro"?>;								
			
			$.ajax({ 
				url:'notasDeCredito/rellenarInputs.php',
				type:'POST', 
				dataType: 'JSON', 
				data:{folio: folio, ur: ultimoRegistro, pr: primerRegistro, con: condicion}, 
			}).done((response) => {
								
				switch(response['respuesta']){
					case 1:
						console.log("No hay mas resultados.");								 
					break;
					case 2: 
						mostrarValoresDeBusquedaEnInputs(response)	
					break;
				}				
			});
			
		}

		function mostrarValoresDeBusquedaEnInputs(data){

			$("#cliente").val(data['cliente']);
			$("#nombreDelCliente").val(data['nombreCliente']);
			$("#personaContacto").val(data['personaContacto']);
			$("#referencia").val(data['referencia']);				
			$("#tipoMoneda").val(data['tipoMoneda']);
			
			// $("#nDoc").val(data['']);
			$("#cdoc").val(data['cdoc']);				
			$("#estado").val(data['estado']);		
			$("#fconta").val(data['fconta']);	
			$("#fdoc").val(data['fdoc']);							
			$("#fven").val(data['fven']);
			$("#metodoPago").val(data['metodoPago']);							
			$("#formaPago").val(data['formaPago']);
			$("#usoPrincipal").val(data['usoPrincipal']);
						

			$("#empleado").val(data['empleado']);	
			$("#propietario").val(data['propietario']);				
			$("#comentarios").val(data['comentarios']);

			$("#totalAntesDescuento").val(data['totalAntesDescuento']);
			$("#descNum").val(data['porcentajeDescuento']);
			$("#descuento").val(data['descuento']);
			$("#anticipoTotal").val(data['anticipoTotal']);
			$("#redondeo").val(data['redondeo']);
			$("#impuestoTotal").val(data['impuestoTotal']);
			$("#retencion").val(data['retencion']);
			$("#totalDocumento").val(data['totalDelDocumento']);
			$('#monedaVisor').val($("#tipoMoneda").val()).prop("readonly", true);

			$("#importeAplicado").val(data['importeAplicado']);	
			$("#saldoPendiente").val(data['saldoPendiente']);	
			$("#rutaArchivos").val(data['rutaArchivos']);		
											

			$("#tblNotasDeCredito tbody").empty();				
			for (x= 0; x<data.array.length; x++ ){
				$("#tblNotasDeCredito tbody").append(data.array[x]);
			} 
		}

		$("#buscadorNotaDeCredito").keyup(function() {
			var valorEscrito = $('#buscadorNotaDeCredito').val();
			$.ajax({ 
				url:'notasDeCredito/consultaNotasDeCredito.php',
				type:'POST',
				data:{valorEscrito: valorEscrito},
			}).done((response) => {
				$("#tblNotaDeCredito tbody").empty().append(response)
				$("#tblNotaDeCredito tbody tr").on('click',function() {
					var cdoc = $(this).find('td').eq(1).text();		
					var condicion = 'nada';											
					consultaClickFactura(cdoc, condicion);								
					$("#modalNotaDeCredito").modal('hide');
				});									
			})
		});

		$("#buscadorNotaDeCreditoFecha").change(function(){
			var valorEscrito = $('#buscadorNotaDeCreditoFecha').val();						

			$.ajax({ 
				url:'notasDeCredito/consultaNotasDeCreditoFecha.php',
				type:'POST',
				data:{valorEscrito: valorEscrito},
				success: function(response){
					$("#tblNotaDeCredito tbody").empty()
					$("#tblNotaDeCredito tbody").append(response)

					$("#tblNotaDeCredito tbody tr").on('click',function(){
						var cdoc = $(this).find('td').eq(1).text();		
						var condicion = 'nada';											
						consultaClickFactura(cdoc, condicion);								
						$("#modalNotaDeCredito").modal('hide');
					});
				}					
			})
		});

		$("#consultaPrimerRegistro").on('click', function(){								
			var folioSAP = <?php echo "$primerRegistro"?>;	
			var condicion = 'nada';
			consultaClickFactura(folioSAP, condicion);

		});

		$("#consultaUltimoRegistro").on('click', function(){
			var folioSAP = <?php echo "$ultimoRegistro"?>;
			var condicion = 'nada';
			consultaClickFactura(folioSAP, condicion);
		});

		$("#consulta1Atras").on('click', function(){
			var folioSAP = ($("#cdoc").val()) - 1;
			var condicion = 'atras';	
			consultaClickFactura(folioSAP, condicion);

		});
		$("#consulta1Adelante").on('click', function(){
			var folioSAP = $("#cdoc").val();	
			folioSAP ++;				
			var condicion = 'adelante';
			consultaClickFactura(folioSAP, condicion);
			
		});		

		$("#btnPdf").on('click', function(){
					
			var origen = $("#rutaArchivos").val()+".pdf";
			var nuevoOrigen = origen.replace(/\\/g, "\\\\")	
			var ext = '.pdf';	
			console.log(nuevoOrigen);
						
			
			$.ajax({
				url:'copiarArchivo.php',
				type: 'post',						
				data: {ext:ext, origen: nuevoOrigen,  nombre:'<?php echo "$new_name"?>' },
				success: function (response) {
					console.log(response);							
				} 	
			});
			window.location = 'temporales/<?php echo "$new_name"?>.pdf';																													
		});	
				
		$("#btnxlm").on('click', function(){
			
			var origen = $("#rutaArchivos").val()+".xml";
			var nuevoOrigen = origen.replace(/\\/g, "\\\\")	
			var ext = '.xml';				
			console.log(nuevoOrigen);
			
			$.ajax({
				url:'copiarArchivo.php',
				type: 'post',					
				data: {ext: ext, origen: nuevoOrigen,  nombre:'<?php echo "$new_name"?>' },
				success: function (response) {
					console.log(response);							
				} 	
			});
			window.location = 'temporales/<?php echo "$new_name"?>.xml';																													
		});	
	</script>
</html>
