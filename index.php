<!DOCTYPE html>
<html>
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="bootstrap/css/bootstrap-grid.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- <link rel="stylesheet" href="GridViewScroll/css/web.css"> -->
        <!-- <script src="GridViewScroll/js/gridviewscroll.js"></script> -->

	<?php include "conexion.php"; ?>
<body class="bg-login">
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #005580">
  		<a class="navbar-brand" href="login.php" style="width: 8%">
			<img src="img/tituloEypo.png" alt="" style="width: 100%">
		</a>
	</nav> 
	<div class="container">
		<br>
		<div class="row">
			<div class="col-md-6 col-lg-5 offset-md-6 offset-lg-7" style="border-radius: 5px; background-color: #f4f4f4cf">
				<br>
				<div class="mensajeRespuesta" style="text-align: center;"></div>
				<br>
				<center>
					<i class="fas fa-user-circle" style="font-size: 8rem; color: #005580"></i><br>
					<span style="color: #005580">Inicia Sesión</span>
				</center>
				<br>
				<div class="text-center">
					<img src="img/tituloEypoAzul.png" alt="" style="width: 50%;">
				</div>
				<br>
				<div class="input-group form-group">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-user"></i></span>
					</div>
					<input  type="text" class="form-control inEnter" id="user" placeholder="Usuario">
			  	</div>
			  	<div class="input-group form-group">
			  		<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-key"></i></span>
					</div>
					<input type="password" class="form-control inEnter" id="pass" maxlenght="20" placeholder="Contraseña">
				</div>
				<div class="row" style="margin: auto;">
					<a href="recuperarPassword.php">
						<span style="color: grey;">¿Olvidaste tu password?</span>
					</a>
				</div>
				<div class=" text-center">
					<br><br>
					<button class="btn btn-success btn-block" type="button" id="iniciarSesion" onclick="iniciarSesion()">Iniciar Sesión</button>
				</div>
					<br>
			</div>
		</div>
	</div>
  <?php include "footer.php"; ?>
</body>
<script src="js/login.js"></script>
</html>
