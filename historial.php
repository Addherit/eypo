	<?php
	include "conexion.php";
		session_start();
	$month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;

$FechaInicio = date( "Y-m-d", strtotime( "$today -12 month" ) );
	?>

	<!DOCTYPE html>
	<html>
		<?php include "header.php"; ?>
	<body>
	
		<?php include "nav.php"; ?>
		<?php include "modalQuerys.php"; ?>
		<?php include "modales.php"; ?>
		
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Historial</h3>
				</div>			
			</div>


			<div class="form-row">
				<div class="form-group col-sm-4">
					<label>Buscador</label> 
					<input type="text" id="buscador" placeholder="Buscar" title="Buscador">	
				</div>

				<div class="form-group col-6 col-sm-4">
					<label>Fecha Inicio:</label> 
					<input type="date" onchange="cargarProyectos();" id="FechaInicio" value="<?php echo $FechaInicio; ?>">
				</div>

				<div class="form-group col-6 col-sm-4">
					<label>Fecha Fin:</label> 
					<input type="date" onchange="cargarProyectos();" id="FechaFin" value="<?php echo $today; ?>">
				</div>
			</div>
			<br>

			<div class="row" style="font-size: .7rem">
				<div class="col-md-12">
					<div class="row">
							
					  		<div class="col-md-12" style="height: 600px">
					  			<table class="table table-bordered table-editable table-hover table-striped table-responsive table" width="100%" id="listaespera" style="height: inherit">
					        		<thead>
					        			<tr class="encabezado" style="background-color: #005580; color:white;" >
											<th>Ver</th>
											<th>No.</th>
											<th>Prioridad</th>
											<th>Nombre del Proyecto</th>
											<th>Fecha de Ingreso</th>
					        				<th>Tipo de propuesta</th>
											<th>Cliente</th>
					        				<th>Telefono</th>
											<th>Correo</th>
											<th>Proy</th>
											<th>Proy Dom</th>
					        				<th>Asesor Ventas</th>
											<th>Estatus</th>
											
					        			</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<th>
											
											
											<a href="#" style="color: green" id="eliminarFila"  data-toggle="modal" data-target="#myModalCajero"><i class="fas fa-folder-open"></i></a></th>
											<td class="Id"></td> 
											<td class="Prioridad"></td> 
											<td class="NombreProyecto"></td>
								            <td class="FechaIngreso"></td>
											<td class="TipoPropuesta"></td>
								            <td class="Cliente"></td>
											<td class="Telefono"></td>
											<td class="Correo"></td>
											<td class="Proyectista"></td>
											<td class="ProyectistaDom"></td>
											<td class="AsesorVentas"></td>
											<td class="Estatus"></td>
										</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>
				</div>
			
		</div>
	</div>

		<?php include "footer.php"; ?>
	</body>
		<script>
			
			$("#buscador").keyup(function() {
				 
				cargarProyectos();

			});

			function cargarProyectos() {

				var fechaInicio = document.getElementById("FechaInicio").value;
				var fechaFin = document.getElementById("FechaFin").value;
				var valorescrito = $("#buscador").val();
				 
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'4', valorescrito:valorescrito, fechainicio:fechaInicio, fechafin:fechaFin},
					success: function(response){
						
						$("#listaespera tbody").empty();
						$("#listaespera tbody").append(response);
					}
				});								
					
			}
			 if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
	function postForm(path, params, method) {
    method = method || 'post';

    var form = document.createElement('form');
    form.setAttribute('method', method);
    form.setAttribute('action', path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}
			$(document).on('click', '#eliminarFila', function (event) {
				
				  
				  
				  var currentRow=$(this).closest("tr"); 
				  var valorEscrito=currentRow.find("td:eq(0)").text();
				  
				   var usuario = currentRow.find("td:eq(12)").text().toUpperCase();
				  var usuarioactual = '<?php echo $_SESSION['usuario']?>'.toUpperCase();
				  
				  if ("<?php echo $_SESSION['CodigoPosicion']?>" == '36'||"<?php echo $_SESSION['CodigoPosicion']?>" == '46' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'||"<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'||"<?php echo $_SESSION['CodigoPosicion']?>" == '50'){
				  postForm('masterproyectosseguimiento.php', {valor: valorEscrito, aut:'1'});
				  }
				  else if (usuario==usuarioactual)
				  {
					postForm('masterproyectosseguimiento.php', {valor: valorEscrito, aut:'1'});
				  }
				  else
				  {
				  }
				 // $(this).closest('tr').remove();
				    //respuestas(valorEscrito);
					// $.ajax({
							// url:"eliminadeposito.php",
							// type:"POST",
							// data:{ 
								// valor:valorEscrito,	
							// },
							
						// }).done(function (response) {	
											
						   // alert(valorEscrito);
					// });	
					  
					// $.ajax({
					// url: 'EliminaDeposito.php',
					// type: 'Post',
					// data: {valor: valorEscrito},
					// success: function(response){
						 
						 // alert(response);
						// }
					// });
					
				
			});
	function cambiar(){
    var pdrs = document.getElementById('file-upload').files[0].name;
    document.getElementById('info').innerHTML = pdrs;
}
			function respuestas(Id){
								 
				   $.ajax({
						type: "post",
						url: "eliminadeposito.php?valor="+Id,

						success  : function(data){
							 //window.location.href="nomina.php";
						},
						error    : function(){
							 alert("Could not ");
							 alert("EliminaDeposito.php?valor="+Id);
						}

					});
			}
			
			
		
		</script>
	</html>
