<?php
	include "../../db/Utils.php";
	session_start();
	if (!isset($_SESSION["cliente"]) || $_SESSION["cliente"] == '0' ) {	
		$empleadoVentas = $_SESSION['usuario'];
		$nombreCompleto = $_SESSION['NombreCompleto'];	
		// $esCliente = 0;	
		// $es_empleado = 2;		   
	} else {		
		$cliente = $_SESSION["cliente"];			
		$sql = "select t2.lastName, t2.firstName, t2.middleName, t2.U_IV_EY_PV_User AS agentecliente
		from OCRD t0 inner join OSLP t1 on t0.SlpCode=t1.SlpCode
		inner join OHEM t2 on t1.SlpCode=t2.salesPrson
		inner join OUSR t3 on t2.userId =t3.INTERNAL_K
		where CardType = 'C' and CardCode = '$cliente'";

		$consulta = sqlsrv_query($conn, $sql);
		while ($Row = sqlsrv_fetch_array($consulta)) {
			$nombreCompleto = $Row['firstName'] . " " . $Row['middleName'] . " " . $Row['lastName'];	
		}
		// $esCliente = 2;
		// $es_empleado = 0;	
	}
	if ($empleadoVentas == "" && $cliente == "" ) {
		$yourURL = "./index.php";
		echo ("<script>location.href='$yourURL'</script>");
	}

	$hoy = date('Y-m-d');
	$mesDespues = date('Y-m-d', strtotime( "$hoy +1 month"));
	$quincena = date('Y-m-d', strtotime( "$hoy +15 days" ));
?>

<nav class="navbar navbar-expand-lg" style="background-color: #005580">
	<a class="navbar-brand" href="" style="width: 8%"> <img src="../../img/tituloEypo.png" alt="" style="width: 100px"> </a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNav">
	<ul class="navbar-nav">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="a_a_ventas" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<b>Ventas</b>
				</a>
				<div class="dropdown-menu" aria-labelledby="a_a_ventas">
					<a id="a_ofv" class="dropdown-item" href="../../ofertaDeVenta.php" title="Oferta de venta"><b>Oferta de venta</b></a>
					<a id="a_orv" class="dropdown-item ocultarAlCte" href="../../ordenDeVenta.php" title="Orden de venta"><b>Orden de venta</b></a>
					<a id="a_entrega" class="dropdown-item ocultarAlCte" href="../../entrega.php" title="Entrega"><b>Entrega</b></a>
					<a id="a_f_comun" class="dropdown-item ocultarAlCte" href="../../facturaComun.php" title="Factura Común"><b>Factura Común</b></a>
					<a id="a_f_reserva" class="dropdown-item ocultarAlCte" href="../../facturaReserva.php" title="Factura Reserva"><b>Factura Reserva</b></a>
					<a id="a_factura_anticipo" class="dropdown-item ocultarAlCte" href="f../../acturaAnticipo.php" title="Factura de anticipo"><b>Factura de anticipo</b></a>
					<a id="a_socios_negocio" class="dropdown-item ocultarAlCte" href="../../socios.php" id="btnSocios" title="Socios Negocios"><b>Socios Negocios</b></a>
					<a id="a_inventario" class="dropdown-item ocultarAlCte" href="../../inventario.php" title="Inventario"><b>Inventario</b></a>
					<a id="a_notas_credito" class="dropdown-item ocultarAlCte" href="../../notasDeCredito.php" title="Notas de Credito"><b>Notas de crédito</b></a>
				</div>
			</li>
			<li class="nav-item dropdown ocultarAlCte">
				<a class="nav-link dropdown-toggle" href="#" id="a_a_compras" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<b>Compras</b>
				</a>
				<div class="dropdown-menu ocultarAlCte" aria-labelledby="a_a_compras">
					<a id="a_solicitud_compra" class="dropdown-item" href="solicitudDeCompra.php" title="Solicitud de compra"><b>Solicitud de compra</b></a>
					<a id="a_orden_compra" class="dropdown-item" href="#" title="Orden de compra"><b>Orden de compra</b></a>
				</div>
			</li>

			<li class="nav-item dropdown ocultarAlCte">
				<a class="nav-link dropdown-toggle" href="#" id="a_a_compras" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<b>Colonias</b>
				</a>
				<div class="dropdown-menu ocultarAlCte" aria-labelledby="a_a_compras">
					<a class="dropdown-item" href="../oferta_de_venta" title="Oferta de venta"><b>Oferta de venta</b></a>
					<a class="dropdown-item" href="../orden_de_venta" title="Orden de venta"><b>Orden de venta</b></a>
					<a class="dropdown-item" href="../entrega" title="Entrega"><b>Entregas</b></a>
					<a class="dropdown-item" href="../factura_comun" title="Factura común"><b>Factura Común</b></a>
					<a class="dropdown-item" href="../notas_de_credito" title="Notas de Crédito"><b>Notas de Crédito</b></a>
				</div>
			</li>
			<?php If ($_SESSION['CodigoPosicion'] == 36 || $_SESSION['CodigoPosicion']== 44 || $_SESSION['CodigoPosicion']== 45 || $_SESSION['CodigoPosicion']== 43 )	 {?>
			<li class="nav-item dropdown ocultarAlCte">
				<a class="nav-link dropdown-toggle" href="#" id="a_a_quiosco" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<b>Quiosco</b>
				</a>
				<div class="dropdown-menu" aria-labelledby="a_a_quiosco">
					<?php If ($_SESSION['CodigoPosicion'] == 36 || $_SESSION['CodigoPosicion']== 43 || $_SESSION['CodigoPosicion']== 44 || $_SESSION['CodigoPosicion']== 45 )	 {?>
					<a id="a_consultar_cajer" class="dropdown-item" href="../../cajero.php" title="Consultar Cajero"><b>Consultar cajero</b></a>
					<?php } ?>
					<?php If ($_SESSION['CodigoPosicion'] == 36 || $_SESSION['CodigoPosicion']== 43 || $_SESSION['CodigoPosicion']== 45 || $_SESSION['CodigoPosicion']== 44 )	 {?>
					<a id="a_nomina" class="dropdown-item" href="../../nomina.php" title="Nómina"><b>Nómina</b></a>
					<?php } ?>
					<?php If ($_SESSION['CodigoPosicion'] == 36 || $_SESSION['CodigoPosicion']== 43 || $_SESSION['CodigoPosicion']== 45 || $_SESSION['CodigoPosicion']== 44 )	 {?>
					<a id="a_nomina_historial" class="dropdown-item" href="../../historialnomina.php" title="Historial Nómina"><b>Historial Nómina</b></a>
					<?php } ?>
					<?php If ($_SESSION['CodigoPosicion'] == 36 || $_SESSION['CodigoPosicion']== 43 || $_SESSION['CodigoPosicion']== 45 )	 {?>
					<a id="a_empleados" class="dropdown-item" href="../../empleados.php" title="Empleados"><b>Empleados</b></a>
					<?php } ?>
					<?php If ($_SESSION['CodigoPosicion'] == 36 || $_SESSION['CodigoPosicion']== 43 || $_SESSION['CodigoPosicion']== 45 )	 {?>
					<a id="a_cortes_diarios" class="dropdown-item" href="../../cortes_diarios.php" title="Cortes Diarios"><b>Cortes Diarios</b></a>					
					<?php } ?>
				</div>

			</li>
			<?php } ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="a_a_master_control" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<b>Master de Control</b>
				</a>
				<div class="dropdown-menu" aria-labelledby="a_a_master_control">
					<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '37'||$_SESSION['CodigoPosicion'] == '46'||$_SESSION['CodigoPosicion'] == '49'||$_SESSION['CodigoPosicion'] == '50'||$_SESSION['CodigoPosicion'] == '51'||$_SESSION['CodigoPosicion'] == '52'||$_SESSION['CodigoPosicion'] == '53'||$_SESSION['CodigoPosicion'] == '54') {?>
						<a class="notification" href="../../master.php" id="btnNuevoProyecto" title="Nuevo Proyecto"><b>Registro Nuevo</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '37'||$_SESSION['CodigoPosicion'] == '46'||$_SESSION['CodigoPosicion'] == '49'||$_SESSION['CodigoPosicion'] == '50'||$_SESSION['CodigoPosicion'] == '51'||$_SESSION['CodigoPosicion'] == '54') {?>
						<a class="notification" href="../../listaautorizacion.php" id="btnLista"  title="Lista de Proyectos por Autorizar"><span class="badge"><?php echo $_SESSION['listaautorizacion']?></span><b>Lista de Proyectos por Autorizar</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '46'||$_SESSION['CodigoPosicion'] == '49'||$_SESSION['CodigoPosicion'] == '50'||$_SESSION['CodigoPosicion'] == '51'||$_SESSION['CodigoPosicion'] == '52'||$_SESSION['CodigoPosicion'] == '53') {?>
						<a class="notification" href="../../listaespera.php" id="btnListabtnLista" title="Lista de Proyectos"><span class="badge"><?php echo $_SESSION['listaespera']?></span><b>Lista de Espera</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '46'||$_SESSION['CodigoPosicion'] == '49'||$_SESSION['CodigoPosicion'] == '50'||$_SESSION['CodigoPosicion'] == '51'||$_SESSION['CodigoPosicion'] == '52'||$_SESSION['CodigoPosicion'] == '53') {?>
						<a class="notification" href="../../revision.php" id="btnRevisión" title="Revisión"><b>Revisión</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '46'||$_SESSION['CodigoPosicion'] == '49'||$_SESSION['CodigoPosicion'] == '50'||$_SESSION['CodigoPosicion'] == '51') {?>
						<a class="notification" href="../../opcioncodigos.php" id="btnCodigosNuevo" title="Registro de Nuevos Codigos"><b>Nuevo Codigo</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '46'||$_SESSION['CodigoPosicion'] == '49'||$_SESSION['CodigoPosicion'] == '50'||$_SESSION['CodigoPosicion'] == '51') {?>
						<a class="notification" href="../../codigosautorizados.php" id="btnCodigosAut" title="Autorización de Codigos"><span class="badge"><?php echo $_SESSION['cantidadcodigos']?></span><b>Autorización de Codigos</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '46'||$_SESSION['CodigoPosicion'] == '49'||$_SESSION['CodigoPosicion'] == '50'||$_SESSION['CodigoPosicion'] == '51') {?>
						<a class="notification" href="../../codigosrevisar.php" id="btnCodigosRev" title="Codigos Protegidos Autorizados"><b>Codigos Protegidos Autorizados</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '52'||$_SESSION['CodigoPosicion'] == '53'||$_SESSION['CodigoPosicion'] == '54') {?>
						<a class="notification" href="../../listacostos.php" id="btnCostos" title="Costos"><span class="badge"><?php echo $_SESSION['listacostos']?></span><b>Costos</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '54') {?>
						<a class="notification" href="../../autorizacionproyectos.php" id="btnAutorizacionProy" title="Autorización Proyectos"><span class="badge"><?php echo $_SESSION['listaautorizacionventas']?></span><b>Autorización de Proyecto</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '37'||$_SESSION['CodigoPosicion'] == '46'||$_SESSION['CodigoPosicion'] == '49'||$_SESSION['CodigoPosicion'] == '50'||$_SESSION['CodigoPosicion'] == '51'||$_SESSION['CodigoPosicion'] == '52'||$_SESSION['CodigoPosicion'] == '53'||$_SESSION['CodigoPosicion'] == '54') {?>
						<a class="notification" href="../../seguimiento.php" id="btnSeguimiento" title="Seguimiento a Proyectos"><b>Seguimiento</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '37'||$_SESSION['CodigoPosicion'] == '46'||$_SESSION['CodigoPosicion'] == '49'||$_SESSION['CodigoPosicion'] == '50'||$_SESSION['CodigoPosicion'] == '51'||$_SESSION['CodigoPosicion'] == '52'||$_SESSION['CodigoPosicion'] == '53'||$_SESSION['CodigoPosicion'] == '54') {?>
						<a class="notification" href="../../cotizaciones.php" id="btnCotizaciones" title="Cotizaciones"><b>Cotizaciones</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '46'||$_SESSION['CodigoPosicion'] == '49'||$_SESSION['CodigoPosicion'] == '50'||$_SESSION['CodigoPosicion'] == '51'||$_SESSION['CodigoPosicion'] == '54') {?>
						<a class="notification" href="../../estadisticas.php" id="btnEstadisticas" title="Estadisticas"><b>Estadisticas</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36' ||$_SESSION['CodigoPosicion'] == '46') {?>
						<a class="notification" href="../../configuracion.php" id="btnConfiguracion" title="Configuracion"><b>Configuracion</b></a>
						<?php }?>
						<?php if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '37'||$_SESSION['CodigoPosicion'] == '46'||$_SESSION['CodigoPosicion'] == '49'||$_SESSION['CodigoPosicion'] == '50'||$_SESSION['CodigoPosicion'] == '51'||$_SESSION['CodigoPosicion'] == '52'||$_SESSION['CodigoPosicion'] == '53'||$_SESSION['CodigoPosicion'] == '54') {?>
						<a class="notification" href="../../historial.php" id="btnHistorial" title="Historial"><b>Historial</b></a>
						<?php }?>
				</div>
			</li>
			<li class="nav-item dropdown ocultarAlCte">
				<a class="nav-link dropdown-toggle" href="#" id="a_a_armado_fab" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<b>Armado/Fabricación</b>
				</a>
				<div class="dropdown-menu" aria-labelledby="a_a_armado_fab">
					<a  id="a_eypo" class="dropdown-item h_hidde eypo_h" href="../../arcos.php" title="Consultar Cajero"><b>Eypo</b></a>
					<a  id="a_novalux" class="dropdown-item h_hidde novalux_h" href="../../arcosNovalux.php" title="Notas de Credito"><b>Novalux</b></a>
					<a  id="a_pec" class="dropdown-item h_hidde pec_h" href="../../arcosPec.php" title="Nómina"><b>Pec</b></a>
					<a  id="a_tj" class="dropdown-item h_hidde tj_h" href="../../arcosTj.php" title="Empleados"><b>TJ</b></a>
					<a  id="a_proton" class="dropdown-item h_hidde proton_h" href="../../arcosProton.php" title="Empleados"><b>Proton</b></a>
					<a  class="dropdown-item" href="../../historial_ordenes_fabricacion.php" title="Empleados" id="vista-sistemas"><b>Historial Ordenes de fab.</b></a>
				</div>
			</li>
		</ul>

		<div id="navbarSupportedContent" style="flex-grow: 1;  display: flex!important;">
			<span class="mr-auto"></span>
			<div class="form-inline my-2 my-lg-0">
				<span class="nav-link text-right" id="user_nav" title="user"><b><?php echo strtoupper($_SESSION['usuario'])?></b></span>
				<input type="hidden" id="codigo_posicion_nav" value="<?php echo $_SESSION['CodigoPosicion'] ?>">				
				<a class="nav-link text-right" href="destroySession.php" id="btnCerrarSesion" title="Cerrar sesión"><i class="fas fa-sign-out-alt"></i><b> Cerrar sesión</b></a>
			</div>
	</div>
	</div>
</nav>