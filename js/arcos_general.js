$(".soloVendedores").hide();
$("#btnAutorizarCoordinador").hide();
$(".soloCoordinadores").hide();
$("#btnFinalizar").hide();

function consultar_folio_max_mas_uno(empresa) {
    $.ajax({
        url:'arcosConsultas/consultar_folio_max.php',
        type:'POST',
        data: {empresa: empresa},
        dataType: 'JSON',
    }).done((response) =>{
        var folio_max = parseInt(response[0].Folio);
        $("#folio_ord_fab").val(folio_max + 1)
    })
}

function buscador_orf() {
    $.ajax({
        url:'arcosConsultas/consulta_segun_empresa_gral.php',
        type:'POST',
        data:{ empresa : $("#empresa").val() }
    }).done ((response) => {        
        $("#tblarcosGeneral tbody").empty().append(response)
    })
}

function click_seleccion_tr_orf(tr_orf) {
    $.ajax({
        url:'arcosConsultas/arcosConsultaGeneral.php',
        type:'POST',
        data: {folio: tr_orf.children[1].textContent, empresa: tr_orf.children[3].textContent },
        dataType:'JSON',
    }).done((resp) => {    
        mostrarValoresInputs(resp);                                  
        caracteristicasEstatus(resp['estatus']);
        consultar_entrega_parcial(tr_orf.children[1].textContent)
        $("#modalArcosConsultaGeneral").modal('hide')
    });	
}

function typear_buscar_orf_arcos() {
    $.ajax({
        url:'arcosConsultas/buscarOrdenFabricacion.php',
        type:'POST',    
        data: {valor: $("#buscadorArcos").val(), empresa: $("#empresa").val()},
    }).done ((resp) => {
        $("#tblarcosGeneral tbody").empty().append(resp);
    });
}

function typear_buscar_orv(valor_escrito) {
    if (valor_escrito.value) {
        $.ajax({
            url: 'orvConsultas/buscadorGeneralORV.php',
            type: 'POST',
            data: { valor: valor_escrito.value },
        }).done((response) => {    
            $('#tblBuscarOrdenes tbody').empty().append(response)

            $("#tblBuscarOrdenes tbody tr").on('click',function() {                
                $("#numeroDeOrden").val($(this).find('td').eq(1).text()).prop("disabled", true);
                $("#nombreCliente").val($(this).find('td').eq(2).text()).prop("disabled", true); 
                $("#modalBuscarOrdenes").modal("hide");
            });            
        });
    }else {
        $('#tblBuscarOfertas tbody').empty();
    }  
}

function consultar_orv_por_fecha(val_fecha) {

    if (val_fecha.value) {
        $.ajax({
            url: 'orvConsultas/buscadorGeneralORVFecha.php',
            type: 'post',
            data: {valor: val_fecha.value},
        }).done((response) => {

            $('#tblBuscarOrdenes tbody').empty().append(response)

            $("#tblBuscarOrdenes tbody tr").on('click',function() {                
                $("#numeroDeOrden").val($(this).find('td').eq(1).text()).prop("disabled", true);
                $("#nombreCliente").val($(this).find('td').eq(2).text()).prop("disabled", true); 
                $("#modalBuscarOrdenes").modal("hide");
            });                                                
        });

    }else {
        $('#tblBuscarOfertas tbody').empty();
    }
}

function guardar_orden_de_fabriccion() {
    $.ajax({
        type:'post',
        url:"arcosConsultas/arcosInsertManual.php",
        data:{
            folio: $('#folio_ord_fab').val(),
            dateInicial: $('#dateInicial').val(),
            dateFinal: $('#dateFinal').val(),
            nombreSolicitante: $('#nombreSolicitante').val(),
            nombreProyecto: $('#nombreProyecto').val(),
            numeroDeOrden: $('#numeroDeOrden').val(),
            nombreCliente: $('#nombreCliente').val(),
            cantidadFabricar: $('#cantidadFabricar').val(),
            entregasParciales: $('#entregasParciales').val(),
            codigoProducto : $('#codigoProducto').val(),
            descripcionProducto : $('#descripcionProducto').val(),
            productosFabricarNombre: $('#productosFabricarNombre').val(),
            terminadoFinal: $('#terminadoFinal').val(),
            observaciones: $('#observaciones').val(),
            empresa: $('#empresa').val(),
            SePM : $('#SePM').val(),
            SeFt : $('#SeFt').val(),
            SeDib : $('#SeDib').val(),
            SeDia : $('#SeDia').val(),
            SeOtr : $('#SeOtr').val(),
        },
        dataType: 'JSON',
    }).done((response) => {
        pintar_mensaje(response.resp, response.msj)              
    });
}	

function ejecutar_buscador_de_articulo(e) {

    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) {
        var valorEscrito = $("#buscadorArticulo").val();
        if (valorEscrito) {
            $.ajax({
                url: 'ofvConsultas/consulta_articulo.php',
                type: 'post',
                data: {valor: valorEscrito},
                dataType: 'JSON'
            }).done((response) => {					
				$("#tblarticulo tbody").empty();
				var tr_articulo = "";
				if (response.length) {
					response.forEach((element,index) => {                        	
						tr_articulo += `<tr onclick="seleccionar_articulo_click_desde_table(this)" class="cursor"><td>${index++}</td><td class="codigo_articulo">${element.CodigoArticulo}</td><td class="descripcion_articulo">${element.NombreArticulo}</td><td>${element.EnStock}</td><td>${element.Comprometido}</td><td>${element.Solicitado}</td><td class="codigo_almacen">${element.CodigoAlmacen}</td><td>${element.NombreAlmacen}</td></tr>`	
					});
				} else {
					var tr_articulo = '<tr><td colspan="8" class="text-center">No hay registros para mostrar</td></tr>';
				}
				$("#tblarticulo tbody").append(tr_articulo);
			})                        
        } else {
            $("#tblarticulo tbody").empty();
            $("#buscadorArticulo").val("");
        }
    }
}

function seleccionar_articulo_click_desde_table(tr_articulo) {

	tr_articulo.childNodes.forEach(element => {
		switch (element.className) {
			case 'codigo_articulo':                
                $("#codigoProducto").val(element.textContent)
			break;
			case 'descripcion_articulo':
                $("#descripcionProducto").val(element.textContent)
			break;
		}
	})	
	$("#buscadorArticulo").val("");
	$("#myModalArticulos").modal('hide');	
}	

function buscar_orf_arcos_por_fecha() {
    $.ajax({
        url:'arcosConsultas/buscarOrdenFabricacionFecha.php',
        type:'POST',        
        data: {valor: $("#buscadorArcosFecha").val(), empresa: $("#empresa").val() },
    }).done ((resp) => {
        $("#tblarcosGeneral tbody").empty().append(resp);
    });
}

switch ($("#codigo_posicion_nav").val()) {
    case '36': //coordinador grupo eypo
        $("#btnAutorizarCoordinador").show();
        $("#btnGuardar").hide();
        $("#btnActualizar").hide();
        $("#EntregaParcial").prop('disabled', true)
        $("#EntregaTotal").prop('disabled', true)
        $("#dateEntregaParcial").prop('disabled', true)
        $("#tblEntregaParcial tbody").find('td').eq(0).prop('contenteditable',false);
        $("#plusNewEntregaParcial").hide()
        $("#dateEntregaTotal").prop('disabled', true)
        $("#observacionesPlanta").prop('disabled', true)
        break;
    case '37': //cliente
        $("#EntregaParcial").prop('disabled', true)
        $("#EntregaTotal").prop('disabled', true)
        $("#dateEntregaParcial").prop('disabled', true)
        $("#tblEntregaParcial tbody").find('td').eq(0).prop('contenteditable',false);
        $("#plusNewEntregaParcial").hide()
        $("#dateEntregaTotal").prop('disabled', true)
        $("#observacionesPlanta").prop('disabled', true)
        $(".soloVendedores").show();
    break;
    case '38': // planta novalux
        $("#btnGuardar").hide();
        $(".soloCoordinadores").show();        
        $("#EntregaParcial").prop('disabled', false)
        $("#EntregaTotal").prop('disabled', false)
        $("#btnGuardar").hide();
        $("#btnPec").hide();
        $("#btnTj").hide();
        $("#btnProton").hide();        
        var usuario = 'pec';
        break;
    case '39': //planta pec
        $("#btnGuardar").hide() 
        $(".soloCoordinadores").show();        
        $("#EntregaParcial").prop('disabled', false)
        $("#EntregaTotal").prop('disabled', false)
        $("#btnGuardar").hide();        
        $("#btnTj").hide();
        $("#btnProton").hide();
        $("#btnNovalux").hide();
        var usuario = 'tj';
        break;
    case '40': //planta tj
        $("#btnGuardar").hide();
        $(".soloCoordinadores").show();        
        $("#EntregaParcial").prop('disabled', false)
        $("#EntregaTotal").prop('disabled', false)
        $("#btnGuardar").hide();
        $("#btnPec").hide();        
        $("#btnProton").hide();
        $("#btnNovalux").hide();
        var usuario = 'novalux';

        $("#dateInicial").prop('disabled', true)
        $("#dateFinal").prop('disabled', true)
        $("#nombreSolicitante").prop('disabled', true)
        $("#nombreProyecto").prop('disabled', true)
        $("#numeroDeOrden").prop('disabled', true)

        $("#nombreCliente").prop('disabled', true)
        $("#cantidadFabricar").prop('disabled', true)
        $("#entregasParciales").prop('disabled', true)
        $("#codigoProducto").prop('disabled', true)
        $("#descripcionProducto").prop('disabled', true)
        $("#productosFabricarNombre").prop('disabled', true)

        $("#terminadoFinal").prop('disabled', true)
        $("#observaciones").prop('disabled', true)

        $("#SePM").prop('disabled', true)
        $("#SeFt").prop('disabled', true)
        $("#SeDib").prop('disabled', true)
        $("#SeDia").prop('disabled', true)
        $("#SeOtr").prop('disabled', true)
        break;
    case '41': //planta proton
        $(".soloCoordinadores").show();
        $("#btnGuardar").hide();        
        $("#EntregaParcial").prop('disabled', false)
        $("#EntregaTotal").prop('disabled', false)
        $("#btnGuardar").hide();
        $("#btnPec").hide();
        $("#btnTj").hide();        
        $("#btnNovalux").hide();
        var usuario = 'proton';

        $("#dateInicial").prop('disabled', true)
        $("#dateFinal").prop('disabled', true)
        $("#nombreSolicitante").prop('disabled', true)
        $("#nombreProyecto").prop('disabled', true)
        $("#numeroDeOrden").prop('disabled', true)

        $("#nombreCliente").prop('disabled', true)
        $("#cantidadFabricar").prop('disabled', true)
        $("#entregasParciales").prop('disabled', true)
        $("#codigoProducto").prop('disabled', true)
        $("#descripcionProducto").prop('disabled', true)
        $("#productosFabricarNombre").prop('disabled', true)

        $("#terminadoFinal").prop('disabled', true)
        $("#observaciones").prop('disabled', true)

        $("#SePM").prop('disabled', true)
        $("#SeFt").prop('disabled', true)
        $("#SeDib").prop('disabled', true)
        $("#SeDia").prop('disabled', true)
        $("#SeOtr").prop('disabled', true)
        break;
}

function consultar_entrega_parcial(folio) {
    $.ajax({
        type: "POST",
        url: "arcosConsultas/arcosConsultaGeneral2.php",
        data: { folio : folio, empresa : $("#empresa").val() },
    }).done((response) => {    
        if (response) {
            $("#tblEntregaParcial tbody").empty().append(response); 
        }
    }); 
}

function mostrarValoresInputs(resp) {
    
    $("#estatusOrdenesFabricacion").val(resp['estatus'])
    $("#folio_ord_fab").val(resp['folio']).prop("readonly", true);
    $("#dateInicial").val(resp['fechaSolicitud'])
    $("#dateFinal").val(resp['fechaEntrega'])
    $("#nombreSolicitante").val(resp['nombreSolicitante'])
    $("#nombreProyecto").val(resp['nombreProyecto'])
    $("#numeroDeOrden").val(resp['numeroOrden'])

    $("#nombreCliente").val(resp['nombreCliente'])
    $("#cantidadFabricar").val(resp['cantidadFabricar'])
    $("#entregasParciales").val(resp['entregasParciales'])
    $("#codigoProducto").val(resp['codigoProducto']).attr('title', resp['codigoProducto'])
    $("#descripcionProducto").val(resp['descripcionProducto']).attr('title', resp['descripcionProducto'])
    $("#productosFabricarNombre").val(resp['productosFabricarNombre']).attr('title', resp['productosFabricarNombre'])

    $("#material").val(resp['material'])
    $("#terminadoFinal").val(resp['terminadoFinal'])
    $("#observaciones").val(resp['observaciones'])

    $("#SePM").val(resp['SePM'])
    $("#SeFt").val(resp['SeFt'])
    $("#SeDib").val(resp['SeDib'])
    $("#SeDia").val(resp['SeDia'])
    $("#SeOtr").val(resp['SeOtr'])

    if (resp['entregaTotal'] ===  1) {
        $("#EntregaTotal").prop('checked', true)
        $("#dateEntregaTotal").val(resp['FechaEntregaTotal'])
    }
    $("#observacionesPlanta").val(resp['observacionesPlanta'])
    $("#logistica").val(resp['logistica'])
    $("#comentariosLogistica").val(resp['comentarioLogistica'])     
    
    
    if($("#entregasParciales").val() === 'no') {
        $("#btnEntregaParcial").prop('disabled', true)
    }
}

function caracteristicasEstatus(estatus) {
    switch (estatus) {
        case "SOLICITUD":
            $("#btnGuardar").prop('disabled', true);    
            $("#btnActualizar").attr('disabled', false)
            $("#autorizadoPor").val("")
            break;
        case "AUTORIZADA":
            $("#btnGuardar").prop('disabled', true); 
            $("#btnActualizar").attr('disabled', true)
            $("#btnAutorizarCoordinador").prop('disabled', true)
            $("#btnActualizar").prop('disabled', true)
            $("#btnSolicitudEnPlanta").prop('disabled', false)
            $("#autorizadoPor").val("LUIS ADAME")
            break;
        case "PROCESO DE FABRICACION":
            $("#btnGuardar").prop('disabled', true); 
            $("#btnAutorizarCoordinador").prop('disabled', true)
            $("#btnSolicitudEnPlanta").prop('disabled', true)
            $("#btnProcesoDeFabricacion").prop('disabled', true)
            $("#btnActualizar").prop('disabled', true)
            $("#autorizadoPor").val("LUIS ADAME")

            break;
        case "SOLICITUD EN PLANTA":
            $("#btnGuardar").prop('disabled', true); 
            $("#btnAutorizarCoordinador").prop('disabled', true)
            $("#btnSolicitudEnPlanta").prop('disabled', true)
            $("#btnActualizar").prop('disabled', true)            
            $("#autorizadoPor").val("LUIS ADAME")
            break;
        case "ENTREGA PARCIAL":
            $("#btnGuardar").prop('disabled', true); 
            $("#btnActualizar").attr('disabled', true)
            $("#btnAutorizarCoordinador").prop('disabled', true)
            $("#btnSolicitudEnPlanta").prop('disabled', true)
            $("#btnProcesoDeFabricacion").prop('disabled', true)
            $("#btnActualizar").prop('disabled', true) 
            $("#autorizadoPor").val("LUIS ADAME")
            break;
        case "ENTREGA TOTAL":
            $("#btnGuardar").prop('disabled', true); 
            $("#btnActualizar").attr('disabled', true)
            $("#btnAutorizarCoordinador").prop('disabled', true)
            $("#btnSolicitudEnPlanta").prop('disabled', true)
            $("#btnProcesoDeFabricacion").prop('disabled', true)
            $("#btnEntregaParcial").prop('disabled', true)
            $("#btnFinalizar").prop('disabled', true)            
            $("#btnActualizar").prop('disabled', true)            
            $("#EntregaTotal").prop('checked', true)
            $("#EntregaTotal").prop('disabled', true)
            $("#dateEntregaTotal").attr('disabled', true);
            $("#dateEntregaParcial").attr('disabled', true);
            $('.numPiezas').each(function() {
            $(this).prop('contenteditable', false)
            $("#autorizadoPor").val("LUIS ADAME")
            })
            break;
        case "CERRADO":
            $("#btnGuardar").prop('disabled', true); 
            $("#btnActualizar").attr('disabled', true)
            $("#btnEstatusCerrar").attr('disabled', true)
            $("#btnEntregaParcial").prop('disabled', true)
            $("#autorizadoPor").val("LUIS ADAME")
            $("#btnFinalizar").prop('disabled', true) 
            $("#btnProcesoDeFabricacion").prop('disabled', true)
            $("#btnSolicitudEnPlanta").prop('disabled', true)
            break;
        default:                    
            $("#btnActualizar").attr('disabled', false)
            break;
    }
}

function pintar_mensaje(resp, mensaje) {
    switch (resp) {
        case 'ok':
            $(".mensaje :first-child").empty().addClass('mensaje-style-success').append(mensaje)
            setTimeout(() => { location.reload() }, 2000);
            break;
        case 'err':
            $(".mensaje :first-child").empty().addClass('mensaje-style-error').append(mensaje)
            break;        
    }    
    $('html, body').animate({scrollTop:0}, 'slow');
    
}

//------------------------------------------------------------Metodos para cambio de estatus

function cambio_de_estatus(estatus) {    
    var tr_parciales = [];
    $("#tblEntregaParcial tbody tr").each((index, element) => {     
        var linea_single = {}     
        element.childNodes.forEach(element => {            
            if (element.localName === 'td') {                             
                if (element.className === 'numPiezas') { linea_single["piezas"] = element.textContent } 
                if (element.className === 'fecha_td') {
                    element.childNodes.forEach(element => {                        
                        if(element.type === 'date') { linea_single["fecha"] = element.value }
                    })                                
                }
            }                                                
        })
        tr_parciales.push(linea_single)       
    })     
    $.ajax({
        url: 'arcosConsultas/cambio_de_estatus.php',
        type:'POST',        
        data: {
            estatus: estatus,
            logistica: $("#logistica").val(), 
            comExt: $("#comentariosLogistica").val(), 
            folio: $("#folio_ord_fab").val(), 
            empresa: $("#empresa").val(),
            entregas_parciales:tr_parciales,
            obParcial: $("#observacionesPlanta").val(),
            checado : 1,
            fechaEntregaTotal : $("#dateEntregaTotal").val(),
        },
        dataType: 'JSON',
    }).done((response) => {                
        pintar_mensaje(response.resp, response.mnj)                 
    })
}

function autorizar_orden_de_fabricacion() {
    cambio_de_estatus("AUTORIZADA")
}

function solicitud_en_planta_orden_de_fabricacion() { 
    cambio_de_estatus("SOLICITUD EN PLANTA")            
}

function proceso_de_fabricacion_orden_de_fabricacion() {
    cambio_de_estatus("PROCESO DE FABRICACION")
}

function entrega_parcial_orden_de_fabricacion() {    
    if (Boolean($("#observacionesPlanta").val())) {
        cambio_de_estatus("ENTREGA PARCIAL")  
    } else {
        pintar_mensaje('err', "Favor de agregar ENTREGAS PARCIALES con COMENTARIOS" )  
    }
}

function entrega_total_orden_de_fabricacion() {

    if($("#EntregaTotal").is(':checked') && Boolean($("#dateEntregaTotal").val())) {
        if ($("#entregasParciales").val() === 'no') {
            if ($("#estatusOrdenesFabricacion").val() == 'PROCESO DE FABRICACION') {
                cambio_de_estatus("ENTREGA TOTAL")     
            } else {
                pintar_mensaje( 'err', "Antes de FINALIZAR la ordern tiene que estar en PROCESO DE FABRICACIÓN.")            
            }
        }
        if ($("#entregasParciales").val() === 'si') {
            if ($("#estatusOrdenesFabricacion").val() == 'ENTREGA PARCIAL') {
                cambio_de_estatus("ENTREGA TOTAL")     
            } else {
                pintar_mensaje( 'err', "Antes de FINALIZAR la ordern tiene que estar en ENTREGA PARCIAL.")            
            }
        }        
    } else {
        pintar_mensaje( 'err', "Favor de marcar la casilla  de ENTREGA TOTAL y poner una fecha de ENTREGA TOTAL")        
    }    
}

function cerrar_orden_de_fabricacion() {
    if ($("#estatusOrdenesFabricacion").val() === 'ENTREGA TOTAL') {
        if ($("#logistica").val() && $("#comentariosLogistica").val()) {
            cambio_de_estatus("CERRADO")
        } else {
            pintar_mensaje('err', 'Favor de completar los campos de LOGISTICA Y COMENTARIOS EXTRAS para poder cambiar el estatus a CERRADO')            
            }
    } else {
        pintar_mensaje('err', 'La orden debe estar en estatus ENTREGA TOTAL para poder cerrarla')        
    }
}
//-----------------------------------------------------------------------------------------//

function press_btn_cancelar() {
    location.reload();
}

function actualizar_orden_de_fabricacion() {
    $.ajax({
        type:'POST',
        url:"arcosConsultas/actualizarOrdenFab.php",
        data:{
            folio : $('#folio_ord_fab').val(),
            fechaInicio: $('#dateInicial').val(),
            dateFinal: $('#dateFinal').val(),
            nombreSolicitante: $('#nombreSolicitante').val(),
            nombreProyecto: $('#nombreProyecto').val(),
            numeroDeOrden: $('#numeroDeOrden').val(),
            nombreCliente: $('#nombreCliente').val(),
            cantidadFabricar: $('#cantidadFabricar').val(),
            entregasParciales: $('#entregasParciales').val(),
            codigoProducto : $('#codigoProducto').val(),
            descripcionProducto : $('#descripcionProducto').val(),
            productosFabricarNombre: $('#productosFabricarNombre').val(),
            terminadoFinal: $('#terminadoFinal').val(),
            observaciones: $('#observaciones').val(),
            empresa: $('#empresa').val(),

            SePM : $('#SePM').val(),
            SeFt : $('#SeFt').val(),
            SeDib : $('#SeDib').val(),
            SeDia : $('#SeDia').val(),
            SeOtr : $('#SeOtr').val(),
        },
        dataType: 'JSON',
    }).done((response) => {      
        pintar_mensaje(response.resp, response.msj);       
    })
}

function agregar_nueva_entrega_tbl() {
    var ultimo_num = parseInt($("#tblEntregaParcial tr:last").find('th').eq(0).text())
    var new_tr = `<tr><th>${ultimo_num + 1 }</th><td contenteditable="true" class="numPiezas"></td><td class="fecha_td"><input type="date" style="border: none"></td></tr>`;    
    $("#tblEntregaParcial tbody").append(new_tr);
}


function ArcosPdf(){
	var folio =  $("#folio_ord_fab").val();			
    var empresa =  $("#empresa").val();			
    console.log(folio);
    console.log(empresa);
    console.log('http://187.188.40.90:85/CrystalReportViewer/ReporteArmado.aspx?empresa='+empresa+'&folio='+folio);
	$.ajax({
		type: 'POST',
		url: 'http://187.188.40.90:85/CrystalReportViewer/ReporteArmado.aspx?empresa='+empresa+'&folio='+folio,
	}).done((params) => {
    });
	var winFeature ='location=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes';
    wait(2200);
	window.open('visor/tres/RA '+folio+'-'+empresa+'.pdf','_blank','menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=no,resizable=no,dependent,width=800,height=620,left=0,top=0');
}

function wait(ms) {
	var start = new Date().getTime();
	var end = start;
	while(end < start + ms) {
		end = new Date().getTime();
	}
}