<?php
	session_start();
	include "../conexion.php";


	$valor = $_POST['valor'];
	$valorescrito = ($_POST['valorescrito']);
	$tipopropuesta = $_POST['tipopropuesta'];
	$fechainicio = $_POST['fechainicio'];
	$fechafin = $_POST['fechafin'];

	$sql = "SELECT Id, Prioridad, NombreProyecto, FechaSolicitud, TipoPropuesta, NombreCliente, Telefono, Email, AsesorVentas, AdminVentas, Estatus, Proyectista, ProyectistaDom, UsuarioCreacion from MasterEypo.dbo.Proyectos"; 		
	$cont=0;

	If ($valor == 1) {
		if ($tipopropuesta == 'normal') { $consultasql = sqlsrv_query($conn, $sql." where Estatus = 'ANTEPROYECTO' order by prioridad desc, Id asc"); }
		if ($tipopropuesta == 'dom') { $consultasql = sqlsrv_query($conn, $sql." where Estatus = 'ANTEPROYECTO') order by prioridad desc, Id asc"); }
		if ($tipopropuesta == 'nodom') { $consultasql = sqlsrv_query($conn, $sql." where Estatus = 'ANTEPROYECTO') order by prioridad desc, Id asc"); }
		if ($tipopropuesta == 'crossn') { $consultasql = sqlsrv_query($conn, $sql." where Estatus = 'ANTEPROYECTO' order by prioridad desc, Id asc"); }
		if ($tipopropuesta == 'crossd') { $consultasql = sqlsrv_query($conn, $sql." where Estatus = 'ANTEPROYECTO'  order by prioridad desc, Id asc"); }
	}
	If ($valor == 2) {
		if ($tipopropuesta == 'normal') { $consultasql = sqlsrv_query($conn, $sql." where Estatus = 'PORAUTORIZARCPROY' or Estatus = 'PORAUTORIZARC' or Estatus = 'PORAUTORIZARCPROYDOM' or Estatus = 'PORAUTORIZARCDOM' order by prioridad desc, Id asc"); }
		if ($tipopropuesta == 'dom') { $consultasql = sqlsrv_query($conn, $sql." where Estatus = 'PORAUTORIZARCDOM' or Estatus = 'PORAUTORIZARC' or Estatus = 'PORAUTORIZARCPROYDOM' order by prioridad desc, Id asc"); }
		if ($tipopropuesta == 'nodom') { $consultasql = sqlsrv_query($conn, $sql." where Estatus = 'PORAUTORIZARCPROY' or Estatus = 'PORAUTORIZARC' or Estatus = 'PORAUTORIZARCPROYDOM' order by prioridad desc, Id asc"); }
	}
	If ($valor == 3) {
		if ($tipopropuesta == 'normal') { $consultasql = sqlsrv_query($conn, $sql." where Estatus in ('VOLUMETRIA', 'PRECIOSPARCIALES') order by prioridad desc, Id asc"); }
		if ($tipopropuesta == 'dom') { $consultasql = sqlsrv_query($conn, $sql." where Estatus in ('VOLUMETRIA', 'PRECIOSPARCIALES') and tipopropuesta in ('Iluminación + Control + Fotovoltaico', 'Iluminación + Fotovoltaico','Iluminación + Domótica','Iluminación + Domótica + Fotovoltaico','Control + Fotovoltaico','Domótica + Fotovoltaico','Domótica','Fotovoltaico') order by prioridad desc, Id asc"); }
		if ($tipopropuesta == 'nodom') { $consultasql = sqlsrv_query($conn, $sql." where Estatus in ('VOLUMETRIA', 'PRECIOSPARCIALES') and tipopropuesta in ('Iluminación', 'Iluminación + Control','Control','Iluminación + Control + Fotovoltaico','Iluminación + Fotovoltaico','Iluminación + Domótica','Iluminación + Domótica + Fotovoltaico','Control + Fotovoltaico') order by prioridad desc, Id asc"); }
		if ($tipopropuesta == 'crossn') { $consultasql = sqlsrv_query($conn, $sql." where Estatus in ('VOLUMETRIA', 'PRECIOSPARCIALES') and tipopropuesta like 'cross%' or  tipopropuesta not like '%dom%' order by prioridad desc, Id asc"); }
		if ($tipopropuesta == 'crossd') { $consultasql = sqlsrv_query($conn, $sql." where Estatus in ('VOLUMETRIA', 'PRECIOSPARCIALES') and tipopropuesta like 'cross%' or tipopropuesta like '%dom%' order by prioridad desc, Id asc"); }
	}
	If ($valor == 4) {
		$consultasql = sqlsrv_query($conn, $sql." where (Id LIKE '%$valorescrito%' OR Prioridad LIKE '%$valorescrito%' OR NombreProyecto LIKE '%$valorescrito%' OR NombreCliente LIKE '%$valorescrito%' OR Telefono LIKE '%$valorescrito%' OR Email LIKE '%$valorescrito%' OR AsesorVentas LIKE '%$valorescrito%' OR AdminVentas LIKE '%$valorescrito%' OR Estatus LIKE '%$valorescrito%' OR Siglas LIKE '%$valorescrito%') and FechaSolicitud between '$fechainicio' and CONVERT(DATETIME, CONVERT(varchar(11),'$fechafin', 111 ) + ' 23:59:59', 111)  order by prioridad desc, Id asc");
	}
	If ($valor == 5) {
		$consultasql = sqlsrv_query($conn,"SELECT p.Id, p.Prioridad, p.NombreProyecto, p.FechaSolicitud, p.TipoPropuesta, p.NombreCliente, p.Telefono, p.Email, p.AsesorVentas, p.AdminVentas, p.Proyectista,p.UsuarioCreacion, (Select count(s.id) from MasterEypo.dbo.seguimientos s where s.idProyecto = p.id and Tipo = 'Llamada' and s.Estatus = 'ALTA') as TotalLlamadas, (Select count(s.id) from MasterEypo.dbo.seguimientos s where s.idProyecto = p.id and Tipo = 'Visita' and s.Estatus = 'ALTA') as TotalVisitas, p.ProyectistaDom, p.Estatus from MasterEypo.dbo.Proyectos p  where (p.Id LIKE '%$valorescrito%' OR p.Prioridad LIKE '%$valorescrito%' OR p.NombreProyecto LIKE '%$valorescrito%' OR p.NombreCliente LIKE '%$valorescrito%' OR p.Proyectista LIKE '%$valorescrito%' OR p.Telefono LIKE '%$valorescrito%' OR p.Email LIKE '%$valorescrito%' OR p.AsesorVentas LIKE '%$valorescrito%' OR p.AdminVentas LIKE '%$valorescrito%' OR p.Estatus LIKE '%$valorescrito%')  order by prioridad desc, p.Id asc");
	}
	If ($valor == 6) {
		$consultasql = sqlsrv_query($conn, $sql." where Estatus in ('PORAUTORIZARCOSTODEVENTAS') order by prioridad desc, Id asc");
	}
	If ($valor == 7) {
		$consultasql = sqlsrv_query($conn, $sql." where NuevaRevision = 1 order by prioridad desc, Id asc");
		// echo $sql;
	}
	If ($valor == 8) {
		$cont=0;
		$consultasql = sqlsrv_query($conn,"SELECT p.Id, p.Prioridad, p.NombreProyecto, p.FechaSolicitud, p.TipoPropuesta, p.NombreCliente, p.Telefono, p.Email, p.AsesorVentas, p.AdminVentas, p.Proyectista, p.UsuarioCreacion, (Select count(s.id) from MasterEypo.dbo.seguimientos s where s.idProyecto = p.idProyecto and Tipo = 'Llamada' and s.Estatus = 'ALTA') as TotalLlamadas, (Select count(s.id) from MasterEypo.dbo.seguimientos s where s.idProyecto = p.idProyecto and Tipo = 'Visita' and s.Estatus = 'ALTA') as TotalVisitas, p.ProyectistaDom, p.Estatus from MasterEypo.dbo.Cotizaciones p  where (p.Id LIKE '%$valorescrito%' OR p.Prioridad LIKE '%$valorescrito%' OR p.NombreProyecto LIKE '%$valorescrito%' OR p.NombreCliente LIKE '%$valorescrito%' OR p.Proyectista LIKE '%$valorescrito%' OR p.Telefono LIKE '%$valorescrito%' OR p.Email LIKE '%$valorescrito%' OR p.AsesorVentas LIKE '%$valorescrito%' OR p.AdminVentas LIKE '%$valorescrito%' OR p.Estatus LIKE '%$valorescrito%')  order by prioridad desc, p.Id asc");
	}
	while ($Row = sqlsrv_fetch_array($consultasql)) {
		$cont++;


		If ($Row['Prioridad'] == "URGENTE") {
			If ($valor == 5 || $valor == 8) {
				?>
					<tr>
						<th><a href="#" style="color: green" id="eliminarFila"><i class="fas fa-folder-open"></i></a></th>
						<td style="color:red;"><?php echo $Row['Id'];?></td>
						<td style="color:red;"><?php echo $Row['Prioridad'];?></td>
						<td style="color:red;"><?php echo ($Row['NombreProyecto']);?></td>
						<td style="color:red;"><?php echo $Row['FechaSolicitud']-> format('Y-m-d H:i:s');?></td>
						<td style="color:red;"><?php echo ($Row['TipoPropuesta']);?></td>
						<td style="color:red;"><?php echo ($Row['NombreCliente']);?></td>
						<td style="color:red;"><?php echo ($Row['Telefono']);?></td>
						<td style="color:red;"><a href="mailto:<?php echo ($Row['Email']);?>?Subject=Seguimiento Proyectos" target="_top"><?php echo ($Row['Email']);?></a></td>
						<td style="color:red;"><?php echo ($Row['Proyectista']);?></td>
						<td style="color:red;"><?php echo ($Row['ProyectistaDom']);?></td>
						<td style="color:red;"><?php echo ($Row['AsesorVentas']);?></td>
						<td style="color:red;"><?php echo ($Row['TotalLlamadas']);?></td>
						<td style="color:red;"><?php echo ($Row['TotalVisitas']);?></td>
						<td style="color:red;"><?php echo ($Row['Estatus']);?></td>
						<td style="color:red; display:none"><?php echo ($Row['UsuarioCreacion']);?></td>
					</tr>
				<?php
			} else {
				?>
				<tr>
					<th><a href="#" style="color: green" id="eliminarFila"><i class="fas fa-folder-open"></i></a></th>
					<td style="color:red;"><?php echo $Row['Id'];?></td>
					<td style="color:red;"><?php echo $Row['Prioridad'];?></td>
					<td style="color:red;"><?php echo ($Row['NombreProyecto']);?></td>
					<td style="color:red;"><?php echo $Row['FechaSolicitud']-> format('Y-m-d H:i:s');?></td>
					<td style="color:red;"><?php echo ($Row['TipoPropuesta']);?></td>
					<td style="color:red;"><?php echo ($Row['NombreCliente']);?></td>
					<td style="color:red;"><?php echo ($Row['Telefono']);?></td>
					<td style="color:red;"><a href="mailto:<?php echo ($Row['Email']);?>?Subject=Seguimiento Proyectos" target="_top"><?php echo ($Row['Email']);?></a></td>
					<td style="color:red;"><?php echo ($Row['Proyectista']);?></td>
					<td style="color:red;"><?php echo ($Row['ProyectistaDom']);?></td>
					<td style="color:red;"><?php echo ($Row['AsesorVentas']);?></td>
					<td style="color:red;"><?php echo ($Row['Estatus']);?></td>
					<td style="color:red; display:none"><?php echo ($Row['UsuarioCreacion']);?></td>
				</tr>
				<?php
			}
		} else {
			if ($valor == 5 || $valor == 8 ) {
				?>
				<tr>
					<th><a href="#" style="color: green" id="eliminarFila"><i class="fas fa-folder-open"></i></a></th>
					<!--<th><a href="#" style="color: red" id="eliminarFila"  data-toggle="modal" data-target="#myModalNomina"><i class="fas fa-trash-alt"></i></a></th>-->
					<td><?php echo $Row['Id'];?></td>
					<td><?php echo $Row['Prioridad'];?></td>
					<td><?php echo ($Row['NombreProyecto']);?></td>
					<td><?php echo $Row['FechaSolicitud']-> format('Y-m-d H:i:s');?></td>
					<td><?php echo ($Row['TipoPropuesta']);?></td>
					<td><?php echo ($Row['NombreCliente']);?></td>
					<td><?php echo ($Row['Telefono']);?></td>
					<td><a href="mailto:<?php echo ($Row['Email']);?>?Subject=Seguimiento Proyectos" target="_top"><?php echo ($Row['Email']);?></a></td>
					<td><?php echo ($Row['Proyectista']);?></td>
					<td><?php echo ($Row['ProyectistaDom']);?></td>
					<td><?php echo ($Row['AsesorVentas']);?></td>
					<td style="color:red;"><?php echo ($Row['TotalLlamadas']);?></td>
					<td style="color:red;"><?php echo ($Row['TotalVisitas']);?></td>
					<td><?php echo ($Row['Estatus']);?></td>
					<td style="display:none"><?php echo ($Row['UsuarioCreacion']);?></td>
					</tr>
				<?php
			} else {
				?>
				<tr>
					<th><a href="#" style="color: green" id="eliminarFila"><i class="fas fa-folder-open"></i></a></th>
					<!--<th><a href="#" style="color: red" id="eliminarFila"  data-toggle="modal" data-target="#myModalNomina"><i class="fas fa-trash-alt"></i></a></th>-->
					<td><?php echo $Row['Id'];?></td>
					<td><?php echo $Row['Prioridad'];?></td>
					<td><?php echo ($Row['NombreProyecto']);?></td>
					<td><?php echo $Row['FechaSolicitud']-> format('Y-m-d H:i:s');?></td>
					<td><?php echo ($Row['TipoPropuesta']);?></td>
					<td><?php echo ($Row['NombreCliente']);?></td>
					<td><?php echo ($Row['Telefono']);?></td>
					<td><a href="mailto:<?php echo ($Row['Email']);?>?Subject=Seguimiento Proyectos" target="_top"><?php echo ($Row['Email']);?></a></td>
					<td><?php echo ($Row['Proyectista']);?></td>
					<td><?php echo ($Row['ProyectistaDom']);?></td>
					<td><?php echo ($Row['AsesorVentas']);?></td>
					<td><?php echo ($Row['Estatus']);?></td>
					<td style="display:none"><?php echo ($Row['UsuarioCreacion']);?></td>
				</tr>
				<?php
			}
		}
	}
	If ($valor == 3) {
		if ($cont == 0) {
			session_start();
			$_SESSION['listacostos']= '';
		} else {
			session_start();
			$_SESSION['listacostos']= $cont;
		}
	}
	If ($valor == 2) {
		if ($cont == 0) {
			session_start();
			$_SESSION['listaautorizacion']= '';
		} else {
			session_start();
			$_SESSION['listaautorizacion']= $cont;
		}
	}
	If ($valor == 1) {
		if ($cont == 0) {
			session_start();
			$_SESSION['listaespera']= '';
		} else {
			session_start();
			$_SESSION['listaespera']= $cont;
			session_commit();
		}
	}
	If ($valor == 6) {
		if ($cont == 0) {
			session_start();
			$_SESSION['listaautorizacionventas'] = "";
		} else {
			session_start();
			$_SESSION['listaautorizacionventas'] = $cont;
		}
	}
	If ($valor == 8) {
		if ($cont == 0) {
			session_start();
			$_SESSION['listacotizaciones'] = "";
		} else {
			session_start();
			$_SESSION['listacotizaciones'] = $cont;
		}
	}
?>
