<?php
        if (!empty($_GET['folio'])) {
            $folio = $_GET['folio']; 
        }
    ?> 
    <script>

            var folio = <?php echo "$folio" ?>;        
            
            var empresa = $("#empresa").val()
            consultarOrdenFabricacion(folio, empresa);        			            

            $(".soloVendedores").hide();
            $("#btnAutorizarCoordinador").hide();
            $(".soloCoordinadores").hide();
            $("#btnFinalizar").hide();
            var codigoCoordinador = <?php echo "$CodigoPosicion"; ?>;  
            var codigoCoordinador = parseInt(codigoCoordinador)          
            
            switch (codigoCoordinador) {
                case 38: // planta novalux
                    $("#btnGuardar").hide();
                    $(".soloCoordinadores").show();
                    $(".coordinadorPlanta").show();

                    // $("#EntregaParcial").prop('disabled', false)
                    // $("#EntregaTotal").prop('disabled', false)
                    $("#btnGuardar").hide();
                    $("#btnPec").hide();
                    $("#btnTj").hide();
                    $("#btnProton").hide();
                    // $("#btnNovalux").hide();
                    var usuario = 'pec';
                    break;
                case 39: //planta pec
                    $("#btnGuardar").hide() 
                    $(".soloCoordinadores").show();
                    // $(".coordinadorPlanta").show();
                    $("#EntregaParcial").prop('disabled', false)
                    $("#EntregaTotal").prop('disabled', false)
                    $("#btnGuardar").hide();
                    // $("#btnPec").hide();
                    $("#btnTj").hide();
                    $("#btnProton").hide();
                    $("#btnNovalux").hide();
                    var usuario = 'tj';
                    break;
                case 40: //planta tj
                    $("#btnGuardar").hide();
                    $(".soloCoordinadores").show();
                    // $(".coordinadorPlanta").show();
                    $("#EntregaParcial").prop('disabled', false)
                    $("#EntregaTotal").prop('disabled', false)
                    $("#btnGuardar").hide();
                    $("#btnPec").hide();
                    // $("#btnTj").hide();
                    $("#btnProton").hide();
                    $("#btnNovalux").hide();
                    var usuario = 'novalux';

                    $("#dateInicial").prop('disabled', true)
                    $("#dateFinal").prop('disabled', true)
                    $("#nombreSolicitante").prop('disabled', true)
                    $("#nombreProyecto").prop('disabled', true)
                    $("#numeroDeOrden").prop('disabled', true)

                    $("#nombreCliente").prop('disabled', true)
                    $("#cantidadFabricar").prop('disabled', true)
                    $("#entregasParciales").prop('disabled', true)
                    $("#codigoProducto").prop('disabled', true)
                    $("#descripcionProducto").prop('disabled', true)
                    $("#productosFabricarNombre").prop('disabled', true)

                    $("#terminadoFinal").prop('disabled', true)
                    $("#observaciones").prop('disabled', true)

                    $("#SePM").prop('disabled', true)
                    $("#SeFt").prop('disabled', true)
                    $("#SeDib").prop('disabled', true)
                    $("#SeDia").prop('disabled', true)
                    $("#SeOtr").prop('disabled', true)


                    break;
                case 41: //planta proton
                    $(".soloCoordinadores").show();
                    $("#btnGuardar").hide();
                    // $(".coordinadorPlanta").show();
                    $("#EntregaParcial").prop('disabled', false)
                    $("#EntregaTotal").prop('disabled', false)
                    $("#btnGuardar").hide();
                    $("#btnPec").hide();
                    $("#btnTj").hide();
                    // $("#btnProton").hide();
                    $("#btnNovalux").hide();
                    var usuario = 'proton';

                    $("#dateInicial").prop('disabled', true)
                    $("#dateFinal").prop('disabled', true)
                    $("#nombreSolicitante").prop('disabled', true)
                    $("#nombreProyecto").prop('disabled', true)
                    $("#numeroDeOrden").prop('disabled', true)

                    $("#nombreCliente").prop('disabled', true)
                    $("#cantidadFabricar").prop('disabled', true)
                    $("#entregasParciales").prop('disabled', true)
                    $("#codigoProducto").prop('disabled', true)
                    $("#descripcionProducto").prop('disabled', true)
                    $("#productosFabricarNombre").prop('disabled', true)

                    $("#terminadoFinal").prop('disabled', true)
                    $("#observaciones").prop('disabled', true)

                    $("#SePM").prop('disabled', true)
                    $("#SeFt").prop('disabled', true)
                    $("#SeDib").prop('disabled', true)
                    $("#SeDia").prop('disabled', true)
                    $("#SeOtr").prop('disabled', true)
                    break;

                case 36: //coordinador grupo eypo
                    $("#btnAutorizarCoordinador").show();
                    $("#btnGuardar").hide();
                    $("#btnActualizar").hide();
                    $("#EntregaParcial").prop('disabled', true)
                    $("#EntregaTotal").prop('disabled', true)
                    $("#dateEntregaParcial").prop('disabled', true)
                    $("#tblEntregaParcial tbody").find('td').eq(0).prop('contenteditable',false);
                    $("#plusNewEntregaParcial").hide()
                    $("#dateEntregaTotal").prop('disabled', true)
                    $("#observacionesPlanta").prop('disabled', true)
                    break;

                default: //37 usuario normal
                    $("#EntregaParcial").prop('disabled', true)
                    $("#EntregaTotal").prop('disabled', true)
                    $("#dateEntregaParcial").prop('disabled', true)
                    $("#tblEntregaParcial tbody").find('td').eq(0).prop('contenteditable',false);
                    $("#plusNewEntregaParcial").hide()
                    $("#dateEntregaTotal").prop('disabled', true)
                    $("#observacionesPlanta").prop('disabled', true)
                    $(".soloVendedores").show();
                    break;


            } 

            function consultarOrdenFabricacion(folio, empresa) {

                $.ajax({
                    url:'arcosConsultas/arcosConsultaGeneral.php',
                    type:'post',
                    dataType:'json',
                    data: {folio: folio, empresa: empresa },
                    success: function(resp){
                        
                        mostrarValoresInputs(resp);                                  
                        caracteristicasEstatus(resp['estatus']);
                                        

                        $.ajax({
                            type: "POST",
                            url: "arcosConsultas/arcosConsultaGeneral2.php",
                            data: {folio: folio },
                            success: function (response) {                                  
                                if (response) {
                                    $("#tblEntregaParcial tbody").empty();
                                    $("#tblEntregaParcial tbody").append(response); 
                                }                                                                                                                                                                                              
                            }
                        });
                    },
                });	
                
            }      

            function mostrarValoresInputs(resp) {
                
                $("#estatusOrdenesFabricacion").val(resp['estatus'])
                $("#folio").val(resp['folio']).prop("readonly", true);
                $("#dateInicial").val(resp['fechaSolicitud'])
                $("#dateFinal").val(resp['fechaEntrega'])
                $("#nombreSolicitante").val(resp['nombreSolicitante'])
                $("#nombreProyecto").val(resp['nombreProyecto'])
                $("#numeroDeOrden").val(resp['numeroOrden'])

                $("#nombreCliente").val(resp['nombreCliente'])
                $("#cantidadFabricar").val(resp['cantidadFabricar'])
                $("#entregasParciales").val(resp['entregasParciales'])
                $("#codigoProducto").val(resp['codigoProducto'])
                $("#descripcionProducto").val(resp['descripcionProducto']).prop('title', resp['descripcionProducto']);
                $("#productosFabricarNombre").val(resp['productosFabricarNombre'])

                $("#material").val(resp['material'])
                $("#terminadoFinal").val(resp['terminadoFinal'])
                $("#observaciones").val(resp['observaciones'])

                $("#SePM").val(resp['SePM'])
                $("#SeFt").val(resp['SeFt'])
                $("#SeDib").val(resp['SeDib'])
                $("#SeDia").val(resp['SeDia'])
                $("#SeOtr").val(resp['SeOtr'])
                // $("#EntregaParcial").val(resp['EntregaParcial'])
                if (resp['EntregaTotal']) {
                    $("#EntregaTotal").prop('checked', true)
                    $("#dateEntregaTotal").val(resp['FechaEntregaTotal'])
                }
                $("#observacionesPlanta").val(resp['observacionesPlanta'])
                $("#logistica").val(resp['Logistica'])
                $("#comentariosLogistica").val(resp['ComentarioLogistica'])        
            }   

            function caracteristicasEstatus(estatus) {
                switch (estatus) {
                    case "SOLICITUD":
                        $("#btnGuardar").prop('disabled', true);    
                        $("#btnActualizar").attr('disabled', false)
                        break;
                    case "AUTORIZADA":
                        $("#btnGuardar").prop('disabled', true); 
                        $("#btnActualizar").attr('disabled', true)
                        $("#btnAutorizarCoordinador").prop('disabled', true)
                        $("#btnActualizar").prop('disabled', true)
                        $("#autorizadoPor").val("LUIS ADAME")
                        break;
                    case "PROCESO DE FABRICACION":
                        $("#btnGuardar").prop('disabled', true); 
                        $("#btnAutorizarCoordinador").prop('disabled', true)
                        $("#btnSolicitudEnPlanta").prop('disabled', true)
                        $("#btnProcesoDeFabricacion").prop('disabled', true)
                        $("#btnActualizar").prop('disabled', true)
                        $("#autorizadoPor").val("LUIS ADAME")                        
                        $("#btnEntregaParcial").prop('disabled', false)

                        break;
                    case "SOLICITUD EN PLANTA":
                        $("#btnGuardar").prop('disabled', true); 
                        $("#btnAutorizarCoordinador").prop('disabled', true)
                        $("#btnSolicitudEnPlanta").prop('disabled', true)
                        $("#btnActualizar").prop('disabled', true)
                        $("#autorizadoPor").val("LUIS ADAME");
                        $("#btnSolicitudEnPlanta").attr('disabled', true)
                        break;
                    case "ENTREGA PARCIAL":
                        $("#btnGuardar").prop('disabled', true); 
                        $("#btnActualizar").attr('disabled', true)
                        $("#btnAutorizarCoordinador").prop('disabled', true)
                        $("#btnSolicitudEnPlanta").prop('disabled', true)
                        $("#btnProcesoDeFabricacion").prop('disabled', true)
                        $("#btnActualizar").prop('disabled', true) 
                        $("#autorizadoPor").val("LUIS ADAME")
                        $("#btnEntregaParcial").prop('disabled', false)
                        break;
                    case "ENTREGA TOTAL":
                        $("#btnGuardar").prop('disabled', true); 
                        $("#btnActualizar").attr('disabled', true)
                        $("#btnAutorizarCoordinador").prop('disabled', true)
                        $("#btnSolicitudEnPlanta").prop('disabled', true)
                        $("#btnProcesoDeFabricacion").prop('disabled', true)
                        $("#btnEntregaParcial").prop('disabled', true)
                        $("#btnFinalizar").prop('disabled', true)                        
                        $("#btnActualizar").prop('disabled', true)
                        $("#autorizadoPor").val("LUIS ADAME")
                        $("#EntregaTotal").prop('checked', true)
                        $("#EntregaTotal").prop('disabled', true)
                        $("#dateEntregaTotal").attr('disabled', true);
                        $("#dateEntregaParcial").attr('disabled', true);                        
                        $('.numPiezas').each(function() {
                        $(this).prop('contenteditable', false)
                        })
                        break;
                    case "CERRADO":
                    $("#btnGuardar").prop('disabled', true); 
                        $("#btnActualizar").attr('disabled', true)
                        $("#btnEstatusCerrar").attr('disabled', true)
                        break;
                    default:                    
                        $("#btnActualizar").attr('disabled', false)
                        break;
                }
            }
        
            $("#btnEstatusCerrar").on('click', function () {
                var logistica = $("#logistica").val();
                var comentarios_logistica = $("#comentariosLogistica").val()      
                var folio = $("#folio").val()     
                var estatus_actual = $("#estatusOrdenesFabricacion").val();
                var empresa = $("#empresa").val()
                console.log(estatus_actual);
                

                if (estatus_actual === 'ENTREGA TOTAL') {

                    if (logistica && comentarios_logistica ) {
                        $.ajax({
                            url: 'arcosConsultas/cambio_de_estatus.php',
                            type:'POST',
                            data: {estatus: "CERRADO", logistica: logistica, comExt: comentarios_logistica, folio: folio, empresa: empresa }
                        }).done(function (params) {
                            alert(params)
                            location.reload();                    
                        })


                        } else {
                        alert("Favor de completar los campos de LOGISTICA Y COMENTARIOS EXTRAS para poder cambiar el estatus a cerrado. ")

                        }

                } else {
                    alert("La orden debe estar en estatus ENTREGA TOTAL para poder cerrarla.");
                }
                            
                
            }) 

            $("#numeroDeOrden").on('change', function(){
                var valor = this.value;

                $.ajax({
                    url: 'arcosConsultas/sacarNombreCliente.php',
                    type: 'POST',
                    data: {codigoCte : valor}

                }).done(function(response) {
                    $("#nombreClientecant").val(response);
                });
            });

            $("#btnGuardar").on('click', function(){

                var folio = $('#folio').val();
                var dateInicial = $('#dateInicial').val();
                var dateFinal = $('#dateFinal').val();
                var nombreSolicitante = $('#nombreSolicitante').val();
                var nombreProyecto = $('#nombreProyecto').val();
                var numeroDeOrden = $('#numeroDeOrden').val();
                var nombreCliente = $('#nombreCliente').val();
                var cantidadFabricar = $('#cantidadFabricar').val();
                var entregasParciales = $('#entregasParciales').val();
                var codigoProducto = $('#codigoProducto').val();
                var descripcionProducto = $('#descripcionProducto').val();
                var productosFabricarNombre = $('#productosFabricarNombre').val();
                var terminadoFinal =  $('#terminadoFinal').val();
                var observaciones = $('#observaciones').val();
                var empresa = $('#empresa').val();
                var SePM = $ ('#SePM').val();
                var SeFt = $ ('#SeFt').val();
                var SeDib = $ ('#SeDib').val();
                var SeDia = $ ('#SeDia').val();
                var SeOtr = $ ('#SeOtr').val();

                $.ajax({
                    type:'post',
                    url:"arcosInsertManual.php",
                    data:{
                        folio: folio,
                        dateInicial: dateInicial,
                        dateFinal: dateFinal,
                        nombreSolicitante: nombreSolicitante,
                        nombreProyecto: nombreProyecto,
                        numeroDeOrden: numeroDeOrden,
                        nombreCliente: nombreCliente,
                        cantidadFabricar: cantidadFabricar,
                        entregasParciales: entregasParciales,
                        codigoProducto : codigoProducto,
                        descripcionProducto : descripcionProducto,
                        productosFabricarNombre: productosFabricarNombre,
                        terminadoFinal: terminadoFinal,
                        observaciones: observaciones,
                        empresa: empresa,
                        SePM : SePM,
                        SeFt : SeFt,
                        SeDib : SeDib,
                        SeDia : SeDia,
                        SeOtr : SeOtr,
                    },
                    success: function(resp){
                        $(".mensaje").append(resp);
                        setTimeout('document.location.reload()',2000);
                    },
                });
            });		

            $("#buscadorArticulo").keypress(function(e){
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) {
                    var valorEscrito = $("#buscadorArticulo").val();
                    if (valorEscrito) {
                        $.ajax({
                            url: 'buscadorConsultaArticulo.php',
                            type: 'post',
                            data: {valor: valorEscrito},
                            success: function(response){
                                $("#tblarticulo tbody").empty();
                                $("#tblarticulo tbody").append(response);

                                $("#tblarticulo tbody tr").on('click',function() {
                                    $("#codigoProducto").val($(this).find('td').eq(1).text());
                                    $("#descripcionProducto").val($(this).find('td').eq(2).text());
                                    $("#buscadorArticulo").val("");
                                    $("#myModalArticulos").modal('hide');
                                });
                            },
                        });
                    } else {
                        $("#tblarticulo tbody").empty();
                        $("#buscadorArticulo").val("");
                    }
                }
            });

            $("#myModalArticulos").on('click',function() {
                $("#buscadorArticulo").val("");
                $("#tblarticulo tbody").empty();
            });

            $("#buscadorOrdenes").keyup(function(){
                var valorEscrito = $('#buscadorOrdenes').val();

                if (valorEscrito) {
                    $.ajax({
                        url: 'orvConsultas/buscadorGeneralORV.php',
                        type: 'post',
                        data: {valor: valorEscrito},
                        success: function(response){
                            $('#tblBuscarOrdenes tbody').empty();
                            $('#tblBuscarOrdenes tbody').append(response);

                            $("#tblBuscarOrdenes tbody tr").on('click',function(){
                                var codigo = $(this).find('td').eq(1).text();
                                $("#numeroDeOrden").val(codigo);
                                $("#modalBuscarOrdenes").modal("hide");

                                $.ajax({
                                    type: 'post',
                                    url: 'orvConsultas/clicORVConsultaGeneral.php', 
                                    dataType:'json',
                                    data:{ codigo: codigo},
                                    success: function(response){
                                        $("#nombreCliente").val(response['NombreC']).prop("readonly", true);                                    
                                    },
                                });
                            });
                        },
                    });

                }else {
                    $('#tblBuscarOfertas tbody').empty();
                }
            });

            $("#buscadorOrdenesFecha").change(function(){
                var valorEscrito = $('#buscadorOrdenesFecha').val();

                if (valorEscrito) {
                    $.ajax({
                        url: 'orvConsultas/buscadorGeneralORVFecha.php',
                        type: 'post',
                        data: {valor: valorEscrito},
                        success: function(response){
                            $('#tblBuscarOrdenes tbody').empty();
                            $('#tblBuscarOrdenes tbody').append(response);

                            $("#tblBuscarOrdenes tbody tr").on('click',function(){
                                var codigo = $(this).find('td').eq(1).text();
                                $("#numeroDeOrden").val(codigo);
                                $("#modalBuscarOrdenes").modal("hide");

                                $.ajax({
                                    type: 'post',
                                    url: 'orvConsultas/clicORVConsultaGeneral.php', 
                                    dataType:'json',
                                    data:{ codigo: codigo},
                                    success: function(response){
                                        $("#nombreCliente").val(response['NombreC']).prop("readonly", true);                                    
                                    },
                                });
                            });
                        },
                    });

                }else {
                    $('#tblBuscarOfertas tbody').empty();
                }
            });


            $("#buscadorArcos").keyup(function () {
                            
                var empresa = $("#empresa").val();
                var valorEscrito = $(this).val();
                $.ajax({
                    url:'arcosConsultas/buscarOrdenFabricacion.php',
                    type:'post',
                    dataType:'html',
                    data: {valor: valorEscrito, empresa: empresa },
                }).done (function (resp) {
                    $("#tblarcosGeneral tbody").empty();
                    $("#tblarcosGeneral tbody").append(resp);

                    $("#tblarcosGeneral tbody tr").on('click', function () {
                        var folio = $(this).find('td').eq(1).text();                       
                                        
                        consultarOrdenFabricacion(folio, empresa);  
                        $("#modalArcosConsultaGeneral").modal('hide');
                    })
                });
            })

            $("#buscadorArcosFecha").change(function () {
                console.log($(this).val());
                
                var empresa = $("#empresa").val();
                var fecha = $(this).val();
                $.ajax({
                    url:'arcosConsultas/buscarOrdenFabricacionFecha.php',
                    type:'post',
                    dataType:'html',
                    data: {valor: fecha, empresa: empresa },
                }).done (function (resp) {
                    $("#tblarcosGeneral tbody").empty();
                    $("#tblarcosGeneral tbody").append(resp);

                    $("#tblarcosGeneral tbody tr").on('click', function () {
                        var folio = $(this).find('td').eq(1).text();                    
                        consultarOrdenFabricacion(folio, empresa);  
                        $("#modalArcosConsultaGeneral").modal('hide');
                    })
                });
            })
        

            $("#btnAutorizarCoordinador").on('click', function() {
                var folio = $("#folio").val()
                var empresa = $("#empresa").val()
                $.ajax({
                    url:'arcosConsultas/autorizar.php',
                    type:'POST',
                    data:{
                        folio:folio, empresa: empresa
                    },
                }).done( function (data) {
                    console.log(data);

                    if (data === 'AUTORIZADA') {
                        $("#estatusOrdenesFabricacion").val(data)
                        $("#autorizadoPor").val("LUIS ADAME");
                        $("#btnAutorizarCoordinador").attr('disabled', true)
                        $("#btnActualizar").prop('disabled', true)
                    }
                    alert(data);
                })
            });

            $("#btnSolicitudEnPlanta").on('click', function() {
                var folio = $("#folio").val()
                var empresa = $("#empresa").val()
                $.ajax({
                    url:'arcosConsultas/solicitudEnPlanta.php',
                    type:'POST',
                    data:{ folio:folio, empresa: empresa },
                }).done( function (data) {
                    
                    alert(data);
                    document.location.reload();
                    
                })
            });

            $("#btnProcesoDeFabricacion").on('click', function() {
                var folio = $("#folio").val()
                var empresa = $("#empresa").val()
                $.ajax({
                    url:'arcosConsultas/procesoDeFabricacion.php',
                    type:'POST',
                    data:{
                        folio:folio,
                        empresa: empresa
                    },
                }).done( function (data) {
                    
                    alert(data);
                    document.location.reload();
                })
            });

            $("#btnEntregaParcial").on('click', function() {     
                var estatusActual = $("#estatusOrdenesFabricacion").val();       
                
                if ( estatusActual != 'PROCESO DE FABRICACION' && estatusActual != 'ENTREGA PARCIAL') {
                        alert("Favor de cambiar primero a estatus PROCESO DE FABRICACIÓN antes de pasar a ENTREGA PARCIAL")
                } else {
                    var array_piezas = [];
                    var array_fechas = [];
                    var folio = $("#folio").val()
                    var empresa = $("#empresa").val()

                    $(".numPiezas").each(function() {
                        array_piezas.push($(this).text())
                    })
                    $(".fechaNumPiezas").each(function() {
                        array_fechas.push($(this).text())
                    })
        
                    var observacionesPlanta = $("#observacionesPlanta").val();

                    if (observacionesPlanta && array_fechas &&  array_piezas) {
                        $.ajax({
                            type: "post",
                            url: "arcosConsultas/insertArcosEntregaParcial.php",
                            data: {
                                folio:folio,
                                piezas:array_piezas,
                                fechas:array_fechas,
                                obParcial: observacionesPlanta,
                                empresa: empresa
                            },                                                 
                        }).done (function (response) {
                            alert(response);
                            document.location.reload();                        
                        })
                    } else {
                        alert('Favor de completar la tabla ENTREGA PARCIAL y llenar campo de OBSERVACIONES  DE PLANTA')
                    }                
                }
            });

            $("#btnFinalizar").on('click', function() {
                var folio = $("#folio").val()
                var fechaEntregaTotal = $("#dateEntregaTotal").val()
                var empresa = $("#empresa").val()
                if($("#EntregaTotal").is(':checked')) {
                    if(Boolean($("#dateEntregaTotal").val())){
                    var checado = 1;

                    $.ajax({
                        url:'arcosConsultas/finalizar.php',
                        type:'POST',
                        data:{
                            folio : folio,
                            checado : checado,
                            fechaEntregaTotal:fechaEntregaTotal,
                            empresa: empresa
                        },
                    }).done(function(data) {
                        console.log(data);

                        
                        alert(data);
                        document.location.reload();
                    });
                    } else {
                        alert("Favor de poner una fecha de ENTREGA TOTAL")
                    }
                } else {
                    alert("Favor de marcar la casilla  de ENTREGA TOTAL")
                }
            });

           
            $("#btnCancelar").on('click', function() {
                switch ($('#empresa').val()) {
                case 'eypo':
                window.location.href = 'arcosNovaluxarcos.php';
                    break;
                    case 'novalux':
                    window.location.href = 'arcosNovalux.php';
                    break;
                    case 'pec':
                    window.location.href = 'arcosPec.php';
                    break;
                    case 'proton':
                    window.location.href = 'arcosProton.php';
                    break;
                    case 'tj':
                    window.location.href = 'arcosTj.php';
                    break;
            
                default:
                    break;
            }
                
            });

            $("#btnActualizar").on('click', function () {
                var empresa = $('#empresa').val();
                var folio = $('#folio').val();
                var dateInicial = $('#dateInicial').val();
                var dateFinal = $('#dateFinal').val();
                var nombreSolicitante = $('#nombreSolicitante').val();
                var nombreProyecto = $('#nombreProyecto').val();
                var numeroDeOrden = $('#numeroDeOrden').val();
                var nombreCliente = $('#nombreCliente').val();
                var cantidadFabricar = $('#cantidadFabricar').val();
                var entregasParciales = $('#entregasParciales').val();
                var codigoProducto = $('#codigoProducto').val();
                var descripcionProducto = $('#descripcionProducto').val();
                var productosFabricarNombre = $('#productosFabricarNombre').val();
                var terminadoFinal =  $('#terminadoFinal').val();
                var observaciones = $('#observaciones').val();
                var SePM = $ ('#SePM').val();
                var SeFt = $ ('#SeFt').val();
                var SeDib = $ ('#SeDib').val();
                var SeDia = $ ('#SeDia').val();
                var SeOtr = $ ('#SeOtr').val();

                $.ajax({
                    type:'post',
                    url:"arcosConsultas/actualizarOrdenFab.php",
                    data:{
                        folio : folio,
                        fechaInicio: dateInicial,
                        dateFinal: dateFinal,
                        nombreSolicitante: nombreSolicitante,
                        nombreProyecto: nombreProyecto,
                        numeroDeOrden: numeroDeOrden,
                        nombreCliente: nombreCliente,
                        cantidadFabricar: cantidadFabricar,
                        entregasParciales: entregasParciales,
                        codigoProducto : codigoProducto,
                        descripcionProducto : descripcionProducto,
                        productosFabricarNombre: productosFabricarNombre,
                        terminadoFinal: terminadoFinal,
                        observaciones: observaciones,
                        empresa: empresa,

                        SePM : SePM,
                        SeFt : SeFt,
                        SeDib : SeDib,
                        SeDia : SeDia,
                        SeOtr : SeOtr,
                    },
                    success: function(resp){
                        $(".mensaje").append(resp);
                        setTimeout('document.location.reload()',2000);
                        console.log(resp);

                    },
                });
            });

        
            $("#plusNewEntregaParcial").on('click', function(){
                $("#tblEntregaParcial tr:last").clone().appendTo("#tblEntregaParcial");
                var numrow = parseInt($("#tblEntregaParcial tr:last").find('th').eq(0).text())
                numrow ++;
                $("#tblEntregaParcial tr:last").find('th').eq(0).text(numrow)
                $("#tblEntregaParcial tr:last").find('td').eq(0).empty()
                $("#tblEntregaParcial tr:last").find('td').eq(0).prop('contenteditable', true)
                $("#tblEntregaParcial tr:last").find('td').eq(1).empty()
                $("#tblEntregaParcial tr:last").find('td').eq(1).append(<?php echo date('Y-m-d')?>)
                $("#tblEntregaParcial tr:last").find('td').eq(1).prop('contenteditable', true)
            })
        </script>
