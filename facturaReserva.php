<?php
	include "db/Utils.php";

	$sql = "SELECT min(FolioSAP) as primer, max(FolioSAP) as ultimo FROM EYPO.dbo.IV_EY_PV_FacturasClientesCab
			WHERE TipoFactura = 'RESERVA'"; 
			
	$consulta = sqlsrv_query($conn, $sql);
	$r = sqlsrv_fetch_array($consulta);
	$primerRegistro = $r['primer'];
	$ultimoRegistro = $r['ultimo'];
	$new_name =  rand();

?>
	<!DOCTYPE html>
		<html>
			<?php include "header.php"; ?>
			<body>
				<?php include "nav.php"; ?>
				<?php include "modalQuerys.php"; ?>
				<?php include "modales.php"; ?>
				
				<div class="container formato-font-design" id="contenedorDePagina">
					<br>
					<div class="row">
						<div class="col-md-6">
							<h1 style="color: #2fa4e7">Factura Reserva</h1>
						</div>
						<div id="btnEnca" class="col-md-6" style="font-size: 2rem">
							<?php include "botonesDeControl.php" ?>
						</div>
					</div>
					<hr>
					<div class="row datosEnc">
						<div class="col-md-6">
							<div class="row">
								<label class="col-3 col-md-4 col-lg-3 col-form-label" >Cliente:</label>
								<div class="col-6">
									<input type="text" id="cliente">
								</div>
							</div>
							<div class="row">
								<label class="col-3 col-md-4 col-lg-3 col-form-label">Nombre:</label><br>
								<div class="col-6">
									<input type="text" id="nCliente">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Persona de contacto:</label>
								<div class="col-6">
									<input type="text" id="personaContacto">								
								</div>
							</div>					
							<div class="row">
								<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Tipo moneda:</label>
								<div class="col-6">
									<input type="text" id="tipoMoneda">								
								</div>
							</div>						
							<div class="row">
								<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Orden de compra</label>
								<div class="col-6">
									<input type="text" id="ordenCompra">	
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">N° Folio:</label>
								<div class="col-6">																
									<input type="text" id="cdoc" class="text-right" value="<?php echo $ultimoRegistro + 1 ?>" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Estado:</label>
								<div class="col-6">
									<input type="text" id="estado" class="text-right" value="Abierto" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha de contabilización:</label>
								<div class="col-6">
									<input type="date" value="<?php echo $hoy ?>" id="fconta" readonly="true">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha del documento:</label>
								<div class="col-6">
									<input type="date" value="<?php echo $hoy ?>" id="fdoc" readonly="true">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha de vencimiento:</label>
								<div class="col-6">
									<input type="date" value="<?php echo $quincena ?>" id="fven" readonly="true">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Uso principal:</label>
								<div class="col-6">
									<input type="text" id="usoPrincipal">								
								</div>
							</div>
							<div class="row">
								<label class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Método de pago:</label>
								<div class="col-6">
									<input type="text" id="metodoPago">								
								</div>
							</div>
							<div class="row">
								<label class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Forma de pago</label>
								<div class="col-6">
									<input type="text" id="formaPago">	
								</div>
							</div>
						</div>
					</div>					
					<div class="row datosEnc">
						<div class="col-md-12">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Contenido</a>
								</li>						
							</ul>
							<div class="tab-content" id="nav-tabContent">
								<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
								<br>
								<div class="row">
									<div class="col-md-12">
										<table class=" table table-sm table-bordered table-striped text-center" id="tblFactuaComun">
											<thead>
												<tr class="encabezado" >
													<th><i class="fas fa-ban"></i></th>
													<th>#</th>
													<th>Código articulo</th>
													<th>Nombre artíuclo</th>					        				
													<th>Cantidad</th>
													<th>Precio por unidad</th>
													<th>Descuento</th>
													<th>Ind. Impuesto</th>
													<th>Sujeto a retención de impuesto</th>										
													<th>Total</th>
													<th>Unidad SAT</th>		
													<th>Almacen</th>	
													<th>Comentarios de partida 1</th>
													<th>Comentarios de partida 2</th>	
													<th>Stock</th>
													<th>Comprometido</th>
													<th>Solicitado</th>																																		
												</tr>
											</thead>
											<tbody> 
												<tr>
													<th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>
													<td class="cont"></td>
													<td data-toggle="modal" data-target="#myModalArticulos" class="narticulo"></td>
													<td data-toggle="modal" data-target="#myModalArticulos" class="darticulo"></td>
													<td contenteditable="true" class="cantidad"></td>
													<td contenteditable="true" class="precio"></td>
													<td contenteditable="true" class="descuento"></td>
													<td class="impu"></td> 
													<td class="impuesto" style="display:none"></td>
													<td class="total"></td>
													<th class="unidadSAT"></th>													
													<td class="almacen"></td>	
													<td contenteditable="true" class="comentario1"></td>
													<td contenteditable="true" class="comentario2"></td>
													<td class="stock"></td>
													<td class="comprometido"></td>
													<td class="solicitado"></td>
													<td></td>																							
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<br>
								<div class="row datosEnc">
									<div class="col-md-6">
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Empleado de ventas:</label>
											<div class="col-sm-6">
												<input type="text" id="empleado" value="<?php echo $empleadoVentas ?>">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Proyecto SN:</label>
											<div class="col-sm-6">
												<input type="text" id="proyectoSN">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Ventas Adicional:</label>
											<div class="col-sm-6">
												<input type="text" id="ventasAdic">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Promotor:</label>
											<div class="col-sm-6">
												<input type="text" id="promotor">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Promotor de venta:</label>
											<div class="col-sm-6">
												<input type="text" id="promotorDeVenta">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Comentarios:</label>
											<div class="col-sm-4">
												<textarea name="" id="comentarios" cols="60" rows="4" style="background-color: #ffff002e;"></textarea>
											</div>
										</div>
									</div>
									<div class="col-md-6">
									
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Subtotal:</label>
											<div class="col-6">
												<input type="text" id="totalAntesDescuento" value="0.00" class="text-right">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Descuento:</label>
											<div class="col-6">
												<input type="text" id="descNum" value="0" class="text-center" style="width: 16%">
												<span whidth="6%">%</span>
												<input type="text" value="0.00" class="text-right" id="descAplicado" style="width: 68%">
											</div>
										</div>									
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Redondeo:</label>
											<div class="col-6">
												<input type="text" id="redondeo" value="0.00" class="text-right">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Impuesto:</label>
											<div class="col-6">
												<input type="text" value="0.00" class="text-right" id="impuestoTotal">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Total:</label>
											<div class="col-6">
												<input type="text" id="totalDelDocumento" value="0.00" style="width: 74%" class="text-right">
												<input type="text" id="monedaVisor" style="width: 15%" class="text-center">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Inporte aplicado:</label>
											<div class="col-6">
													<input type="text" id="importeAplicado" value="0.00" class="text-right">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Solo vencido:</label>
											<div class="col-6">
													<input type="text" id="soloVencido" value="0.00" class="text-right">
											</div>
										</div>
									</div>								
								</div>		 				
							</div>
						</div>
						<div class="row" id= btnFoot style="margin-bottom: 30px">
							<div class="col-md-6">	<br>					
								<!-- <a href="#"><button class="btn btn-sm" style="background-color: orange" title="Crear Devoluciones" id="btnCrear">Crear devolución</button></a>					 -->
								<a href=""><button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>					
							</div>			
						</div>	
					</div>						
				</div> 
				<?php include "footer.php"; ?>
			</body>
			<script>

				function btn_busqueda_general() {
					$("#modalFacturaComun").modal('show')
				}

				function consultaClickFactura(folio, condicion) {

					var condicion = condicion;
					var tipo = 'RESERVA';
					var ultimoRegistro = <?php echo "$ultimoRegistro"?>;
					var primerRegistro = <?php echo "$primerRegistro"?>;									
					$.ajax({
						url:'facturasConsultas/rellenarInputsConClick.php',
						type:'POST', 
						dataType: 'json', 
						data:{
							folio : folio, 
							ur : ultimoRegistro, 
							pr : primerRegistro, 
							con : condicion,	
							tipo : tipo						
						},
					}).done((response) => {				
																
						switch(resp = response['respuesta']){
							case 1:									
								alert("No hay mas resultados.");								 
							break;
							case 2: 									
								mostrarValoresDeBusquedaEnInputs(response)	
							break;
						}
					})
				}

				function mostrarValoresDeBusquedaEnInputs(data){

					$("#cliente").val(data['cliente']);
					$("#nCliente").val(data['nombreCliente']);
					$("#personaContacto").val(data['personaContacto']);							
					$("#tipoMoneda").val(data['tipoMoneda']);
					$("#formaPago").val(data['formaPago']);

					// $("#nDoc").val(data['']);
					$("#cdoc").val(data['cdoc']);				
					$("#estado").val(data['estado']);		
					$("#fconta").val(data['fconta']);				
					$("#fdoc").val(data['fdoc']);
					$("#fven").val(data['fven']);
					$("#usoPrincipal").val(data['usoPrincipal']);
					$("#metodoPago").val(data['metodoPago']);

					$("#empleado").val(data['empleado']);	
					$("#proyectoSN").val(data['proyectoSN']);
					$("#ventasAdic").val(data['ventasAdic']);
					$("#promotor").val(data['promotor']);
					$("#promotorDeVenta").val(data['promotorDeVenta']);
					$("#comentarios").val(data['comentarios']);

					$("#totalAntesDescuento").val(data['totalAntesDescuento']);
					// $("#descNum").val(data['']);
					$("#descAplicado").val(data['descAplicado']);				
					$("#redondeo").val(data['redondeo']);
					$("#impuestoTotal").val(data['impuestoTotal']);
					$("#totalDelDocumento").val(data['totalDelDocumento']);
					$('#monedaVisor').val($("#tipoMoneda").val()).prop("readonly", true);
					$("#importeAplicado").val(data['importeAplicado']);
					$("#soloVencido").val(data['soloVencido']);

					$("#tblFactuaComun tbody").empty();				
					for (x= 0; x<data.array.length; x++ ){
						$("#tblFactuaComun tbody").append(data.array[x]);
					} 
				}

				$("#bucardorFacturaComun").keyup(function(){
					var valorEscrito = $('#bucardorFacturaComun').val();		
					var tipo = 'RESERVA';		 

					$.ajax({
						url:'facturasConsultas/consultaGRALFacturaComun.php',
						type:'POST',
						data:{valorEscrito: valorEscrito, tipo: tipo},
						success: function(response){
							$("#tblFacturaComun tbody").empty()
							$("#tblFacturaComun tbody").append(response)

							$("#tblFacturaComun tbody tr").on('click',function(){
								var cdoc = $(this).find('td').eq(1).text();		
								var condicion = 'nada';											
								consultaClickFactura(cdoc, condicion);								
								$("#modalFacturaComun").modal('hide');
							});
						}					
					})
				});

				$("#bucardorFacturaComunFecha").change(function(){
					var valorEscrito = $('#bucardorFacturaComunFecha').val();		
					var tipo = 'RESERVA';		 

					$.ajax({
						url:'facturasConsultas/consultaGRALFacturaComunFecha.php',
						type:'POST',
						data:{valorEscrito: valorEscrito, tipo: tipo},
						success: function(response){
							$("#tblFacturaComun tbody").empty()
							$("#tblFacturaComun tbody").append(response)

							$("#tblFacturaComun tbody tr").on('click',function(){
								var cdoc = $(this).find('td').eq(1).text();		
								var condicion = 'nada';											
								consultaClickFactura(cdoc, condicion);								
								$("#modalFacturaComun").modal('hide');
							});
						}					
					})
				});

				function consultar_primer_registro() {
					var folioSAP = <?php echo "$primerRegistro"?>;	
					var condicion = 'nada';
					consultaClickFactura(folioSAP, condicion);
				}

				function consultar_ultimo_registro() {
					var folioSAP = <?php echo "$ultimoRegistro"?>;
					var condicion = 'nada';
					consultaClickFactura(folioSAP, condicion);
				}

				function consulta_1_atras() {
					var folioSAP = ($("#cdoc").val()) - 1;
					var condicion = 'atras';	
					consultaClickFactura(folioSAP, condicion);

				}
				function consulta_1_adelante() {
					var folioSAP = $("#cdoc").val();	
					folioSAP ++;				
					var condicion = 'adelante';
					consultaClickFactura(folioSAP, condicion);	
				}

				$("#btnPdf").on('click', function(){
					var folio = $("#cdoc").val();
					var origen = 'C:\\FAE_PRODUCTIVO\\Factura\\'+folio+'.pdf';

					$.ajax({
						url:'copiarArchivo.php',
						type: 'post',
						dataType: 'json',
						data: {origen: origen,  nombre:'<?php echo "$new_name"?>' },
						success: function (response) {
							console.log(response);							
						} 	
					});
					window.location = 'temporales/<?php echo "$new_name"?>.pdf','_blank';																									
				});		
					


			</script>
		</html>
