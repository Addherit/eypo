<!DOCTYPE html>
<html lang="en"> 
<?php include "../header.html"?>		
	<body>		
        <?php include "../nav.php" ?>
		<?php include "../../modalQuerys.php" ?>
		<?php include "../../modales.php" ?>
        <div class="container formato-font-design" id="contenedorDePagina">
			<div class="row">
				<div class="col-12">	
					<input type="hidden" value="<?php echo $_GET["clienteProveedor"]?>" id="cliente_proveedor_modal">		
					<br>	
					<section class="table-responsive"> 					
						<table class="table table-striped table-sm table-bordered table-editable text-center" id="tblOfvCliente">
							<thead>
								<tr>
									<th>#</th>
									<th>DocStatus</th>
									<th>dOCNUM</th>
									<th>docdate</th>
									<th>CadCode</th>
									<th>ItemCode</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</section>					
				</div>
				<div class="col-5 offset-7">
					<a href="ofertaDeVenta.php">
						<button class="btn btn-primary btn-block">Regresar a OFV</button>
					</a>
				</div>
			</div>						
		</div>					
		<?php include "../footer.html" ?>	
		<script src="js/ofv_cliente2do_nivel.js"></script>
	</body>
</html>