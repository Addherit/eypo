<?php
    include "../db/Utils.php";

    $valores = $_POST['valores'];
    $detalle = $_POST['valDet'];  
    $detalle_desarme = $_POST['valDet_Desarme']; 

  
    $empresa = $valores[14];
    $folio = $valores[15];

    $fechaSolicitud = $valores[0];
    $fechaEntrega = $valores[1];
    $nombreSolicitante = $valores[2];
    $nombreProyecto = $valores[3];
    $numeroOrden = $valores[4];
    
    $codigoCliente = $valores[5];
    $nombreCliente = $valores[6];
    $cantidadFabricar = $valores[7];
    $entregasParciales = $valores[8];

    $codigoProducto = $valores[9];
    $productosFabricarNombre = str_replace ( "'" , "''" , $valores[10]);

    $productoDesarmar = $valores[11];
    $productoDesarmarNombre = str_replace ( "'" , "''" , $valores[12]);

    $observaciones = $valores[13];
    $respuesta = [];

    $sql ="UPDATE dbEypo.dbo.arcos SET 
    fechaSolicitud = '$fechaSolicitud', 
    fechaEntrega = '$fechaEntrega', 
    nombreSolicitante = '$nombreSolicitante',
    nombreProyecto = '$nombreProyecto', 
    numeroOrden = '$numeroOrden', 
    codigoCliente = '$codigoCliente', 
    nombreCliente =  '$nombreCliente', 
    cantidadFabricar = '$cantidadFabricar',
    entregasParciales = '$entregasParciales',
    productoaFabricar = '$codigoProducto', 
    productosFabricarNombre = '$productosFabricarNombre', 
    productoaDesarmar =  '$productoDesarmar', 
    productosDesarmarNombre = '$productoDesarmarNombre',
    observaciones = '$observaciones', 
    empresa = '$empresa', 
    FolioIndependiente = '$folio'
    WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'";

    $consulta = sqlsrv_query($conn, $sql);
    if ($consulta === false) {
        // echo "0";
        $respuesta = ['resp' => 'err', 'mensaje' => "No se pudo actualizar la orden de fabricación", 'sql' => $sql ];
    } else {
        // echo "2";
        $respuesta = ['resp' => 'ok', 'mensaje' => "Actualizado Correctamente", 'det' => empty($detalle), 'detdesarme' => empty($detalle_desarme), count($detalle[0])];
        //aqui va el actualizado de armado y desarmado detalle 

        //se limpia detalle para prepararlo e insertar actualizaciones si hay.
        $sql = "DELETE FROM dbEypo.dbo.arcosDet WHERE Folio = '$folio'";
        $consultasql = sqlsrv_query($conn, $sql);

        for($x=0; $x<count($detalle[0]); $x++) {
            $string = "";
            for($y=0; $y<4; $y++){                
                $string .= "'".str_replace("'", "''", $detalle[$y][$x])."',";               
            }
      
            $string .= "'".$valores[15]."'";
            $sql = "INSERT INTO dbEypo.dbo.arcosDet (CodigoArt, NombreArt, piezas, ArmadoDesarmado, Folio)
            VALUES ($string)";
            $consultasql = sqlsrv_query($conn, $sql);
        }

          //desarme ------------------------------------------
        for($x=0; $x<count($detalle_desarme[0]); $x++) {
            $string = "";
            for($y=0; $y<4; $y++){                
                $string .= "'".str_replace("'", "''", $detalle_desarme[$y][$x])."',";               
            }
      
            $string .= "'".$valores[15]."'";
            $sql = "INSERT INTO dbEypo.dbo.arcosDet (CodigoArt, NombreArt, piezas, ArmadoDesarmado, Folio)
            VALUES ($string)";
            $consultasql = sqlsrv_query($conn, $sql);
        }
        //   ------------------------------------


    }

    echo json_encode($respuesta);

?>
