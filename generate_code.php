<?php 
if(isset($_POST) && !empty($_POST)) {
    include('./phpqrcode/qrlib.php'); 
    $codesDir = "codes/";   
    $codeFile = date('d-m-Y-h-i-s').'.png';
    QRcode::png($_POST['tok'], $codesDir.$codeFile,'H', 10); 
//    echo '<img class="img-thumbnail" src="'.$codesDir.$codeFile.'" />';
    $response['qrcode'] = $codeFile;
    echo json_encode($codeFile);
} else {
    header('location:./');
}
?>
