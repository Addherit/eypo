<?php
    include "conexion.php";
    $hoy = strtotime(date("Y-m-d"));
    $nuevafecha = date("Y-m-d", strtotime('-3 month', $hoy));

    $sql3 = "SELECT T0.*,CASE WHEN T0.EstatusReal = 2 THEN 'CREADA'
WHEN T0.EstatusReal = 0 AND T0.CrearReal = 'S' AND T0.Estatus = 2 THEN 'PENDIENTE CREAR REAL'
WHEN T0.EstatusReal = -1 THEN 'ERROR CREAR REAL'
WHEN T0.Estatus = 2 AND T1.Status = 'W' THEN 'BORRADOR CREADO, AUTORIZACION PENDIENTE'
WHEN T0.Estatus = 2 AND T1.Status = 'Y' THEN 'BORRADOR CREADO, AUTORIZADA'
WHEN T0.Estatus = 2 AND T1.Status = 'N' THEN 'BORRADOR CREADO, RECHAZADA'
WHEN T0.Estatus = 2 AND T1.Status IN ('P','A') THEN 'BORRADOR CREADO, CREADA DESDE SAP'
WHEN T0.Estatus = 2 AND T1.Status = 'C' THEN 'BORRADOR CANCELADO'
WHEN T0.Estatus = 2 THEN 'BORRADOR CREADO'
WHEN T0.Estatus = 0 THEN 'PENDIENTE CREAR BORRADOR'
WHEN T0.Estatus = -1 THEN 'ERROR CREAR BORRADOR'
END EstatusAutorizacion
FROM [dbEypo].[dbo].[ordenes] T0
LEFT JOIN EYPO.dbo.OWDD T1 ON T1.DocEntry = T0.NuevoDocEntry AND T1.ObjType = '17'
--------------
WHERE created_at >= '$nuevafecha'
 ORDER BY created_at DESC";

    $consulta3 = sqlsrv_query($conn, $sql3);
    while ($Row = sqlsrv_fetch_array($consulta3)) {

        $estatusAutorizacion = $Row['EstatusAutorizacion'];
        // switch ($estatusAutorizacion) {
        //     case 'CREADA':
        //         $respuesta = "Documento autorizado y creado";
        //         break;
        //     case 'PENDIENTE CREAR REAL':
        //         $respuesta = "Documento en proceso de creación";
        //         break;
        //     case 'AUTORIZADA':
        //         $respuesta = "Documento autorizado pendiente de creación";
        //         break;
        //     case 'ERROR CREAR REAL':
        //         $respuesta = "Error al crear documento";
        //         break;
        //     case 'PENDIENTE CREAR BORRADOR':
        //         $respuesta = "Documento pendiente de creación";
        //         break;
        //     case 'ERROR CREAR BORRADOR':
        //         $respuesta = "Error en proceso de creación";
        //         break;
        //     case '':
        //         $respuesta = "Falta autorizar documento";
        //         break;
        // }

        echo $tblMensajes = '
            <tr class="text-center">
                <td width="100px">' . $Row['CodOferta'] . '</td>
                <td width="400px">' . $estatusAutorizacion . '</td>
                <td width="130px">' . $Row['created_at']->format('Y-m-d') . '</td>
                <td width="100px">' . $Row['created_at']->format('h:i:s') . '</td>
                <td style="display: none">'. $Row['EstatusAutorizacion']. '</td>
                <td style="display: none">' . $Row['DocEntryReal'] . '</td>
                <td><i class="far fa-envelope"></i></td>
            </tr>';
    }
?>
