<?php 

    include "../../../db/Utils.php";

    $sql = "SELECT MAX(FolioSAP) AS ultimo, MIN(FolioSAP) AS primero FROM EYPO.dbo.IV_EY_PV_NotasCreditoVentaCab WHERE SerieNombre = 'COLONIAS'";
    $consulta = sqlsrv_query($conn, $sql); 
    $row = sqlsrv_fetch_array($consulta);

    echo json_encode($row);
?>