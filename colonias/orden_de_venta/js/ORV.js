
document.addEventListener("DOMContentLoaded", () => {
    let searchParams = new URLSearchParams(window.location.search);    
    var ordenVenta = searchParams.get('ofv')   
    if (ordenVenta != null ) { consultaORV(ordenVenta) }
     
    document.querySelector("#consultaQuery").hidden = true
    document.querySelector("#cdoc").value = parseInt(obtener_ultimo_registro_orv()) + 1
    document.querySelector("#empleadoVentas").value = localStorage.getItem("departamento")
    document.querySelector("#propietario").value = localStorage.getItem("nombre_completo")
})


function mostrarElementosEnInputs(response) {
    document.querySelector("#folioInterno").value = response.FolioInterno

    $("#codcliente").val(response.CodigoCliente).prop("readonly", true);
    $("#nombreCliente").val(response.NombreCliente).prop("readonly", true);
    $("#personaContacto").val(response.ListContactos).prop("readonly", true);
    $("#ofertaDeCompra").val(response.OrdCompra).prop("readonly", true);
    $("#tipoMoneda").val(response.TipoMoneda).prop("readonly", true);
    $("#usoPrincipal").val(response.usoPrincipal);
    $("#metodoPago").val(response.metodoPago);
    $("#formaPago").val(response.formaPago);

    $("#ndoc").val(response.NDoc).prop("readonly", true);					
    $("#cdoc").val(response.DocNum).prop("readonly", true);
    $("#estatus").val(response.Status).prop("readonly", true);					
    $("#fconta").val(response.FConta).prop("readonly", true);
    $("#fentrega").val(response.FEntrega).prop("readonly", true);
    $("#fdoc").val(response.FDoc).prop("readonly", true);
    $("#fvencimiento").val(response.Fvencimiento).prop("readonly", true);

    $("#numLetra").val(response.numLetra);
    $("#tipoFactura").val(response.TipoFactura);
    $("#lab").val(response.Lab);
    $("#condicionPago").val(response.CondicionPago);

    $('#empleadoVentas').val(response.usuario).prop("readonly", true);
    $('#propietario').val(response.nombreUsuario).prop("readonly", true);
    $('#comentarios').val(response.comentarios);					

    $('#totalAntesDescuento').val(response.subTotal).prop("readonly", true);    
    $('#impuestoTotal').val(response.impuesto).prop("readonly", true);
    $('#totalDelDocumento').val(response.totalDelDocumento).prop("readonly", true);	
    $('#monedaVisor').val($("#tipoMoneda").val()).prop("readonly", true)	

    $("#detalleoferta tbody").empty();				
    for (x= 0; x<response.detalle.length; x++ ){
        $("#detalleoferta tbody").append(response.detalle[x]);
    }
}

function consultaORV(folio) {
    const formData = new FormData()
    formData.append('cdoc', folio)
    fetch('php/consultaOrdenDeVenta.php', {method: 'POST', body: formData})
    .then(data => data.json())
    .then(data => {
        mostrarElementosEnInputs(data);				
    }) 
} 

function typear_buscar_orv() {    
    const formData = new FormData()
    formData.append('valor', document.querySelector('#buscadorOrdenes').value)
    fetch('php/buscadorGeneralORV.php', {method: 'POST', body: formData})
    .then(data => data.json())
    .then(data => {
        var linea = ''
        data.forEach((dato, index) => {
            index++
            linea += `<tr class="cursor" onclick="seleccionar_orv_de_modal(this)">
                <td width="70px">${index}</td>
                <td width="130px">${dato.FolioSAP}</td>
                <td width="310px">${dato.Nombre}</td>
                <td width="150px">${dato.FechaContabilizacion.date}</td>
                <td width="110px">${dato.Estatus}</td>
                </tr>`
        })
        document.querySelector('#tblBuscarOrdenes tbody').innerHTML = linea
    })
}

function consultar_orv_por_fecha() {

    const formData = new FormData()
    formData.append('valor', document.querySelector('#buscadorOrdenesFecha').value)
    fetch('php/buscadorGeneralORVFecha.php', {method: 'POST', body: formData})
    .then(data => data.json())
    .then(data => {
        var linea = ''
        data.forEach((dato, index) => {
            index++
            linea += `<tr class="cursor" onclick="seleccionar_orv_de_modal(this)">
                <td width="70px">${index}</td>
                <td width="130px">${dato.FolioSAP}</td>
                <td width="310px">${dato.Nombre}</td>
                <td width="150px">${dato.FechaContabilizacion.date}</td>
                <td width="110px">${dato.Estatus}</td>
                </tr>`
        })
        document.querySelector('#tblBuscarOrdenes tbody').innerHTML = linea
    })
}

function seleccionar_orv_de_modal(fila) {    
    consultaORV(fila.children[1].textContent);	
    $("#modalBuscarOrdenes").modal('hide');	
}

document.querySelector("#btnBuscarGRAL").addEventListener('click', () => { $("#modalBuscarOrdenes").modal() })

document.querySelector("#consulta1Atras").addEventListener('click', () => { consultaORV(document.querySelector("#cdoc").value - 1) })
document.querySelector("#consulta1Adelante").addEventListener('click', () => { consultaORV(parseInt(document.querySelector("#cdoc").value) + 1) })
document.querySelector("#consultaPrimerRegistro").addEventListener('click', () => { consultaORV(obtener_primer_registro_orv()) })
document.querySelector("#consultaUltimoRegistro").addEventListener('click', () => { consultaORV(obtener_ultimo_registro_orv()) })


function obtener_primer_registro_orv() {
	var primero
	$.ajax({
		url:'php/consultaPrimerRegistroORV.php',
		dataType:'JSON',
		async: false	
	}).done((response) => {
		primero = response.DocNum
	})
	 return primero
} //lista

function obtener_ultimo_registro_orv() {
	var maximini
	$.ajax({
		url:'php/consultaUltimoRegistroORV.php',
		dataType:'JSON',
		async: false	
	}).done((response) => {		
		maximini = response.DocNum
	})
	 return maximini
} //lista



$("#btnPdf").on("click", function (params) {
        var folio =  $("#folioInterno").val();

        console.log(folio);

        $.ajax({
            url: 'http://187.188.40.90:85/CrystalReportViewer/ReporteORV.aspx?nom='+folio+'&objecto=17',
        
        }).done((params) => {					
        })
        var winFeature =
        'location=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes';
        wait(2200);
        window.open('../../visor/dos/OV '+folio+'.pdf','_blank','menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=no,resizable=no,dependent,width=800,height=620,left=0,top=0');
    })

function wait(ms) {
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
        end = new Date().getTime();
    }
}

function mostrar_mensajes_ofv() {

	fetch('../oferta_de_venta/php/modalMensajeConsulta.php')
	.then(data => data.json())
	.then(data => {
		lineas = ''
		data.forEach(element => {
			lineas += `
            <tr class="text-center cursor" onclick="inspeccionar_mensaje(this)">
                <td width="100px">${element.CodOferta}</td>
                <td width="400px">${element.EstatusAutorizacion}</td>
                <td width="130px">${element.created_at.date}</td>
                <td width="100px">${element.created_at.date}</td>
                <td style="display: none">${element.EstatusAutorizacion}</td>
                <td style="display: none">${element.DocEntryReal}</td>
                <td><i class="far fa-envelope"></i></td>
            </tr>`
		});
		document.querySelector("#tblMensajes tbody").innerHTML = lineas				
	})
}

function inspeccionar_mensaje(linea) {
	var ordenVenta = linea.children[5].textContent
	var FolioSAP = linea.children[0].textContent
	var estatus = linea.children[1].textContent	
	var inspeccion = `
		<tr onclick="redireccion_de_mensajes(${ordenVenta}, ${FolioSAP}, '${estatus}')">
			<td>1</td>
			<td>${ordenVenta}</td>
			<td>${FolioSAP}</td>
			<td>${estatus}</td>			
		</tr>`;
	document.querySelector("#tblMensajes2 tbody").innerHTML = inspeccion
}

 function redireccion_de_mensajes(ordenVenta, FolioSAP, estatus) {
	switch (estatus) {
		case 'CREADA':
			window.location.href = '../orden_de_venta/index.php?FolioSAP='+FolioSAP+'&ofv='+ordenVenta;
			break;
		case 'PENDIENTE CREAR REAL':
			alert("Documento en proceso de creación en firme");
			break;
		case 'ERROR CREAR REAL':
			alert("Error en proceso de creación en firme");
			break;
		case 'BORRADOR CREADO':
			window.location.href = '../orden_de_venta/ordenDeVentaBorrador.php?FolioSAP='+FolioSAP;
			break;
		case 'PENDIENTE CREAR BORRADOR':
			alert("Documento en proceso de creación de borrador");
			break;
		case 'ERROR CREAR BORRADOR':
			alert("Error en proceso de creación de borrador");
			break;
	}
 }
