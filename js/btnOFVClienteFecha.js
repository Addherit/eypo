var codigoCliente = $("#codigo_cliente").val();
var fecha1 = $("#fecha1").val();
var fecha2 = $("#fecha2").val();

$.ajax({
    url:"querys/ofertaClienteFecha.php",
    type:"POST",
    data:{ 
        codigoCliente: codigoCliente,
        fecha1: fecha1, 
        fecha2: fecha2			
    },
    dataType: 'JSON',
}).done((comisiones) => {	

    $("#tblComisiones tbody").empty()
    var tr_order = "";

    if(comisiones.length > 0) {
        comisiones.forEach((element, index) => {
            index++
            tr_order += `<tr>
            <td>${index}</td>
            <td>${element.DocStatus}</td>
            <td>${element.dOCNUM}</td>
            <td>${element.docdate.date}</td>
            <td>${element.CardCode}</td>
            <td>${element.ItemCode}</td>
            </tr>`
        })
    } else {
        tr_order += '<tr><td colspan="17">No existe registro alguno con los datos proporcionados</td></tr>' 
    }    
    $("#tblOfvClienteFecha tbody").append(tr_order);    				
});		