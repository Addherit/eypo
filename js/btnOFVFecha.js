var fecha1 = $("#fecha1").val()
var fecha2 = $("#fecha2").val()

$.ajax({
        url:"querys/ofertaFecha.php",
        type:"POST",
        data:{ 
            fecha1 : fecha1,
            fecha2 : fecha2			
        },
        dataType: 'JSON',
}).done((response) => {	
    
    $("#tblOfvFecha tbody").empty()
    var tr_order = "";

    if(response.length > 0) {
        response.forEach((element, index) => {
            index++
            tr_order += `<tr>
            <td>${index}</td>
            <td>${element.DocStatus}</td>
            <td>${element.dOCNUM}</td>
            <td>${element.docdate.date}</td>
            <td>${element.CardCode}</td>
            <td>${element.ItemCode}</td>
            </tr>`
        })
    } else {
        tr_order += '<tr><td colspan="17">No existe registro alguno con los datos proporcionados</td></tr>' 
    }    
    $("#tblOfvFecha tbody").append(tr_order);        			
});		