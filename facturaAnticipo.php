<?php
		include "db/Utils.php";
		
		$sql = "SELECT min(FolioSAP) as primer, max(FolioSAP) as ultimo FROM EYPO.dbo.IV_EY_PV_FacturasClientesCab
				WHERE TipoFactura = 'DEBITO'"; 
				
		$consulta = sqlsrv_query($conn, $sql);
		$r = sqlsrv_fetch_array($consulta);
		$primerRegistro = $r['primer'];
		$ultimoRegistro = $r['ultimo'];
	?>
	<!DOCTYPE html>
		<html>
			<?php include "header.php"?>
			<body>
				<?php include "nav.php"?>
				<?php include "modalQuerys.php"?>
				<?php include "modales.php"?>
				
				<div class="container formato-font-design" id="contenedorDePagina">
					<br>
					<div class="row">
						<div class="col-md-6">
							<h1 style="color: #2fa4e7">Factura de Anticipo</h1>
						</div>
						<div id="btnEnca" class="col-md-6" style="font-size: 2rem">
							<?php include "botonesDeControl.php" ?>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<label class="col-3 col-md-4 col-lg-3 col-form-label" >Cliente:</label>
								<div class="col-6">
									<input type="text" id="cliente">
								</div>
							</div>
							<div class="row">
								<label class="col-3 col-md-4 col-lg-3 col-form-label">Nombre:</label><br>
								<div class="col-6">
									<input type="text" id="nCliente">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Persona de contacto:</label>
								<div class="col-6">
									<input type="text" id="personaContacto">								
								</div>
							</div>					
							<div class="row">
								<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Tipo moneda:</label>
								<div class="col-6">
									<input type="text" id="tipoMoneda">								
								</div>
							</div>						
							<div class="row">
								<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Forma de pago</label>
								<div class="col-6">
									<input type="text" id="formaPago">	
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">N° Folio:</label>
								<div class="col-6">															
									<input type="text" id="cdoc" class="text-right" value="<?php echo $ultimoRegistro + 1 ?>" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Estado:</label>
								<div class="col-6">
									<input type="text" id="estado" class="text-right" value="Abierto" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha de contabilización:</label>
								<div class="col-6">
									<input type="date" value="<?php echo $hoy ?>" id="fconta" readonly="true">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha del documento:</label>
								<div class="col-6">
									<input type="date" value="<?php echo $hoy ?>" id="fdoc" readonly="true">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha de vencimiento:</label>
								<div class="col-6">
									<input type="date" value="<?php echo $quincena ?>" id="fven" readonly="true">
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Uso principal:</label>
								<div class="col-6">
									<input type="text" id="usoPrincipal">								
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Método de pago:</label>
								<div class="col-6">
									<input type="text" id="metodoPago">								
								</div>
							</div>
						</div>
					</div>					
					<div class="row">
						<div class="col-md-12">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Contenido</a>
								</li>						
							</ul>
							<div class="tab-content" id="nav-content">
								<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
								<br>
								<div class="row">
									<div class="col-md-12">
										<table class=" table table-sm table-bordered table-striped text-center" id="tblFactuaComun">
											<thead>
												<tr class="encabezado" >
													<th><i class="fas fa-ban"></i></th>
													<th>#</th>
													<th>Código articulo</th>
													<th>Nombre artíuclo</th>					        				
													<th>Cantidad</th>
													<th>Precio por unidad</th>
													<th>Descuento</th>
													<th>Ind. Impuesto</th>
													<th>Sujeto a retención de impuesto</th>										
													<th>Total</th>
													<th>Unidad SAT</th>		
													<th>Almacen</th>	
													<th>Comentarios de partida 1</th>
													<th>Comentarios de partida 2</th>	
													<th>Stock</th>
													<th>Comprometido</th>
													<th>Solicitado</th>																																	
												</tr>
											</thead>
											<tbody> 
												<tr>
													<th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>
													<td class="cont"></td>
													<td data-toggle="modal" data-target="#myModalArticulos" class="narticulo"></td>
													<td data-toggle="modal" data-target="#myModalArticulos" class="darticulo"></td>
													<td contenteditable="true" class="cantidad"></td>
													<td contenteditable="true" class="precio"></td>
													<td contenteditable="true" class="descuento"></td>
													<td class="impu"></td> 
													<td class="impuesto" style="display:none"></td>
													<td class="total"></td>
													<th class="unidadSAT"></th>													
													<td class="almacen"></td>	
													<td contenteditable="true" class="comentario1"></td>
													<td contenteditable="true" class="comentario2"></td>
													<td class="stock"></td>
													<td class="comprometido"></td>
													<td class="solicitado"></td>																						
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<br>
								<div class="row datosEnc">
									<div class="col-md-6">
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Empleado de ventas:</label>
											<div class="col-sm-6">
												<input type="text" id="empleado" value="<?php echo $empleadoVentas ?>">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Proyecto SN:</label>
											<div class="col-sm-6">
												<input type="text" id="proyectoSN">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Ventas Adicional:</label>
											<div class="col-sm-6">
												<input type="text" id="ventasAdic">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Promotor:</label>
											<div class="col-sm-6">
												<input type="text" id="promotor">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-sm-3 col-form-label">Promotor de venta:</label>
											<div class="col-sm-6">
												<input type="text" id="promotorDeVenta">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Comentarios:</label>
											<div class="col-sm-4">
												<textarea name="" id="comentarios" cols="60" rows="4" style="background-color: #ffff002e;"></textarea>
											</div>
										</div>
									</div>
									<div class="col-md-6">
									
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Subtotal:</label>
											<div class="col-6">
												<input type="text" value="0.00" class="text-right" id="totalAntesDescuento">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Descuento:</label>
											<div class="col-6">
												<input type="text" id="descNum" value="0" class="text-center" style="width: 16%;">
												<span whidth="6%">%</span>
												<input type="text" value="0.00" class="text-right" id="descAplicado" style="width: 68% ">
											</div>
										</div>									
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Redondeo:</label>
											<div class="col-6">
												<input type="text" id="redondeo" value="0.00" class="text-right">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Impuesto:</label>
											<div class="col-6">

												<input type="text" id="impuestoTotal" value="0.00" class="text-right">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Total:</label>
											<div class="col-6">
												<input type="text" id="totalDelDocumento" value="0.00" style="width: 74%" class="text-right">
												<input type="text" id="monedaVisor" style="width: 15%" class="text-center">
										</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Inporte aplicado:</label>
											<div class="col-6">
													<input type="text" id="importeAplicado" value="0.00" class="text-right">
											</div>
										</div>
										<div class="row">
											<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Solo vencido:</label>
											<div class="col-6">
												<input type="text" id="soloVencido" value="0.00" class="text-right">
											</div>
										</div>
									</div>								
								</div>		 				
							</div>
						</div>
						<div class="row" id= btnFoot style="margin-bottom: 30px">
							<div class="col-md-6">	<br>					
								<a href="#"><button class="btn btn-sm" style="background-color: orange" title="Crear Devoluciones" id="btnCrear">Crear devolución</button></a>					
								<a href=""><button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>					
							</div>			
						</div>	
					</div>						
				</div> 
				<?php include "footer.php"; ?>
			</body>
			<script>				
				function btn_busqueda_general() {
					$("#modalFacturaComun").modal('show')
				}

				function consultaClickFactura(folio, condicion){

					var condicion = condicion;
					var tipo = 'DEBITO';
					var ultimoRegistro = <?php echo "$ultimoRegistro"?>;
					var primerRegistro = <?php echo "$primerRegistro"?>;									
					$.ajax({
						url:'facturasConsultas/rellenarInputsConClick.php',
						type:'POST', 
						dataType: 'json', 
						data:{
							folio : folio, 
							ur : ultimoRegistro, 
							pr : primerRegistro, 
							con : condicion,	
							tipo : tipo						
						}, 
					}).done ((response) => {
						
						var resp = response['respuesta'];							
						switch(resp){
							case 1:									
								alert("No hay mas resultados.");								 
							break;
							case 2: 									
								mostrarValoresDeBusquedaEnInputs(response)	
							break;
						}
					})
					
				}

				function mostrarValoresDeBusquedaEnInputs(data){

					$("#cliente").val(data['cliente']);
					$("#nCliente").val(data['nombreCliente']);
					$("#personaContacto").val(data['personaContacto']);							
					$("#tipoMoneda").val(data['tipoMoneda']);
					$("#formaPago").val(data['formaPago']);

					// $("#nDoc").val(data['']);
					$("#cdoc").val(data['cdoc']);				
					$("#estado").val(data['estado']);		
					$("#fconta").val(data['fconta']);				
					$("#fdoc").val(data['fdoc']);
					$("#fven").val(data['fven']);
					$("#usoPrincipal").val(data['usoPrincipal']);
					$("#metodoPago").val(data['metodoPago']);

					$("#empleado").val(data['empleado']);	
					$("#proyectoSN").val(data['proyectoSN']);
					$("#ventasAdic").val(data['ventasAdic']);
					$("#promotor").val(data['promotor']);
					$("#promotorDeVenta").val(data['promotorDeVenta']);
					$("#comentarios").val(data['comentarios']);

					$("#totalAntesDescuento").val(data['totalAntesDescuento']);
					// $("#descNum").val(data['']);
					$("#descAplicado").val(data['descAplicado']);				
					$("#redondeo").val(data['redondeo']);
					$("#impuestoTotal").val(data['impuestoTotal']);
					$("#totalDelDocumento").val(data['totalDelDocumento']);
					$("#importeAplicado").val(data['importeAplicado']);
					$("#soloVencido").val(data['soloVencido']);

					$("#tblFactuaComun tbody").empty();				
					for (x= 0; x<data.array.length; x++ ){
						$("#tblFactuaComun tbody").append(data.array[x]);
					} 
				}

				$("#bucardorFacturaComun").keyup(function(){
					var valorEscrito = $('#bucardorFacturaComun').val();		
					var tipo = 'DEBITO';		 

					$.ajax({
						url:'facturasConsultas/consultaGRALFacturaComun.php',
						type:'POST',
						data:{valorEscrito: valorEscrito, tipo: tipo},
						success: function(response){
							$("#tblFacturaComun tbody").empty()
							$("#tblFacturaComun tbody").append(response)

							$("#tblFacturaComun tbody tr").on('click',function(){
								var cdoc = $(this).find('td').eq(1).text();		
								var condicion = 'nada';											
								consultaClickFactura(cdoc, condicion);								
								$("#modalFacturaComun").modal('hide');
							});
						}					
					})
				});

				$("#bucardorFacturaComunFecha").change(function(){
					var valorEscrito = $('#bucardorFacturaComunFecha').val();		
					var tipo = 'DEBITO';		 

					$.ajax({
						url:'facturasConsultas/consultaGRALFacturaComunFecha.php',
						type:'POST',
						data:{valorEscrito: valorEscrito, tipo: tipo},
						success: function(response){
							$("#tblFacturaComun tbody").empty()
							$("#tblFacturaComun tbody").append(response)

							$("#tblFacturaComun tbody tr").on('click',function(){
								var cdoc = $(this).find('td').eq(1).text();		
								var condicion = 'nada';											
								consultaClickFactura(cdoc, condicion);								
								$("#modalFacturaComun").modal('hide');
							});
						}					
					})
				});

				function consultar_primer_registro() {								
					var folioSAP = <?php echo "$primerRegistro"?>;	
					var condicion = 'nada';
					consultaClickFactura(folioSAP, condicion);
				}

				function consultar_ultimo_registro() {
					var folioSAP = <?php echo "$ultimoRegistro"?>;
					var condicion = 'nada';
					consultaClickFactura(folioSAP, condicion);
				}

				function consulta_1_atras() {
					var folioSAP = ($("#cdoc").val()) - 1;
					var condicion = 'atras';	
					consultaClickFactura(folioSAP, condicion);
				}

				function consulta_1_adelante() {
					var folioSAP = $("#cdoc").val();	
					folioSAP ++;				
					var condicion = 'adelante';
					consultaClickFactura(folioSAP, condicion);	
				}

				$(document).on('click', '#eliminarFila', function (event) {
					event.preventDefault();
					$(this).closest('tr').remove();
				});

				function getDetalle(){
					array_General = new Array();

					var array_codigo = new Array();
					var array_articulo = new Array();				
					var array_cantidad = new Array();
					var array_precioPorUnidad = new Array();
					var array_descuento = new Array();
					var array_indImpuesto = new Array();
					var array_sujetoRetencionImpuesto = new Array();
					var array_total = new Array();
					var array_unidadSAT = new Array();

					$('.codAticulo').each(function(){
						array_codigo.push($(this).text());
					});
					$('.narticulo').each(function(){
						array_articulo.push($(this).text());
					});
					$('.cantidad').each(function(){
						array_cantidad.push($(this).text());
					});
					$('.precioUnitario').each(function(){
						array_precioPorUnidad.push($(this).text());
					});
					$('.porcentajeDescuento').each(function(){
						array_descuento.push($(this).text());
					});
					$('.impuesto').each(function(){
						array_indImpuesto.push($(this).text());
					});
					$('.codigoImpuesto').each(function(){
						array_sujetoRetencionImpuesto.push($(this).text());
					});
					$('.total').each(function(){
						array_total.push($(this).text());
					});
					$('.unidadSAT').each(function(){
						array_unidadSAT.push($(this).text());
					});

					array_General.push(array_codigo, array_articulo, array_cantidad, array_precioPorUnidad, 
					array_descuento, array_indImpuesto, array_sujetoRetencionImpuesto, array_total, array_unidadSAT)

					return array_General;	

				}

				function getValues(){
					var arrayValores = new Array();

					arrayValores.push($("#cliente").val())
					arrayValores.push($("#nCliente").val())
					arrayValores.push($("#personaContacto").val())
					arrayValores.push($("#tipoMoneda").val())
					arrayValores.push($("#formaPago").val())
					arrayValores.push($("#ndoc").val())
					arrayValores.push($("#cdoc").val()) //6
					arrayValores.push($("#estado").val())
					arrayValores.push($("#fconta").val())
					arrayValores.push($("#fdoc").val())
					arrayValores.push($("#fven").val())
					arrayValores.push($("#usoPrincipal").val())
					arrayValores.push($("#metodoPago").val()) //12
					arrayValores.push($("#empleado").val())
					arrayValores.push($("#proyectoSN").val())
					arrayValores.push($("#ventasAdic").val())
					arrayValores.push($("#promotor").val())
					arrayValores.push($("#promotorDeVenta").val())
					arrayValores.push($("#comentarios").val()) //18
					arrayValores.push($("#totalAntesDescuento").val())					
					arrayValores.push($("#descAplicado").val())
					arrayValores.push($("#redondeo").val())
					arrayValores.push($("#impuestoTotal").val())
					arrayValores.push($("#totalDelDocumento").val())
					arrayValores.push( $("#importeAplicado").val())
					arrayValores.push($("#soloVencido").val())
					arrayValores.push('DEBITO')
					
					return arrayValores;
				}

				$("#btnCrear").on('click', function(){
					var valores = getValues();																		
					var detalle = getDetalle();															
					
					$.ajax({
						url:'insertarFacturaComun.php',
						type:'post',
						data:{arrayCabecera: valores, arrayDetalle: detalle},
						success: function(response){		

							if(response === "exito"){
								alert("guardado con éxito");
								setTimeout('document.location.reload()',2000);
							}else{
								alert(response);													
							}
						}
					})
				});

				$("#btnPdf").on('click', function(){
					var folio = $("#cdoc").val();															
					// location.href ="visorPDF.php?folio="+folio;
					location.href ="file:///C://FAE_PRODUCTIVO//Factura//1.pdf";
				});
					


			</script>
		</html>
