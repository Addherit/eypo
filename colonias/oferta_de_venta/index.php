<!DOCTYPE html>
<?php include "../header.html" ?>
<html lang="en">
	<body>
		<?php include "../nav.php" ?>
		<?php include "../../modalQuerys.php" ?>
		<?php include "../../modales.php" ?>
		
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-lg-5 col-xl-6">
					<h1 style="color: #2fa4e7">Oferta de venta</h1>
				</div>
				<div id="btnEnca" class="col-lg-7 col-xl-6" style="font-size: 2rem">
					<?php include "../../botonesDeControl.php" ?>
				</div>
			</div>
			<hr>
			<div class="row mensaje"><div class="col-12"></div></div>
			<input type="hidden" id="mini-folio">
			<input type="hidden" id="maxi-folio">
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<input type="hidden" value="<?php echo $cliente ?>" id="hide-cliente">
						<label for="codcliente" class="col-3 col-md-4 col-lg-3 col-form-label" >Cliente:</label>
						<div class="col-6">
							<input type="text" class="" name="codcliente" id="codcliente">
							<a id="btnCliente" href="#" data-toggle="modal" data-target="#myModal" class="ocultarAlCte" onclick="typea_y_busca_cliente()">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label for="NombreC" class="col-3 col-md-4 col-lg-3 col-form-label">Nombre:</label><br>
						<div class="col-6">
							<input type="text" class="" name="NombreC" id="NombreC">
							<a href="#" data-toggle="modal" data-target="#myModal" class="ocultarAlCte">
								<i class="fas fa-search" style="color: #57b4ea" title="Búsqueda Nombre"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label for="listcontactos" class="col-3 col-md-4 col-lg-3 col-form-label">Persona de contacto:</label>
						<div class="col-6">
							<select  id="listcontactos" name="listcontactos">
								<option disabled selected>Seleccionar Persona</option>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="oCompra" class="col-3 col-md-4 col-lg-3 col-form-label">Orden de compra:</label>
						<div class="col-6">
							<input type="text" id="oCompra" name="oCompra">
						</div>
					</div>
					<div class="row">
						<label for="tmoneda" class="col-3 col-md-4 col-lg-3 col-form-label">Tipo moneda:</label>
						<div class="col-6">
							<select id="tmoneda" name="tmoneda" onchange="cambio_de_moneda()"></select>
						</div>
					</div>
					<div class="row">
						<label for="usoPrincipal" class="col-3 col-md-4 col-lg-3 col-form-label">Uso principal</label>
						<div class="col-6">
							<select class="" name="usoPrincipal" id="usoPrincipal"></select>
						</div>
					</div>
					<div class="row">
						<label for="metodoPago" class="col-3 col-md-4 col-lg-3 col-form-label">Método de pago</label>
						<div class="col-6">
							<select id="metodoPago" name="metodoPago">
								<option disabled selected>Seleccionar</option>
								<option value="PUE">PUE</option>
								<option value="PPD">PPD</option>								
							</select>
						</div>
					</div>
					<div class="row">
						<label for="formaPagoSelect" class="col-3 col-md-4 col-lg-3 col-form-label">Forma de pago</label>
						<div class="col-6">						
							<select id="formaPagoSelect" name="formaPagoSelect">
								<option selected disabled>Seleccionar</option>
								<option value="CHEQUE MN">CHEQUE MN</option>
								<option value="CHEQUE USD">CHEQUE USD</option>
								<option value="EFECTIVO">EFECTIVO</option>
								<option value="NO APLICA">NO APLICA</option>
								<option value="OTROS">OTROS</option>
								<option value="TARJ. CREDITO">TARJ. CREDITO</option>
								<option value="TARJ. DEBITO">TARJ. DEBITO</option>
								<option value="TRANSF. MN">TRANSF. MN</option>
								<option value="TRANSF. USD">TRANSF. USD</option>														
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-7 offset-5 text-center timer-style">
							<b><i class="far fa-clock"></i><span id="timer">60</span><span>s Actualización</span></b><br>
							<span>Ultima actualización </span><span id="last-timer">11:20 AM</span>							
						</div>
					</div>
					<div class="row">
						<label for="cdoc" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">N° Folio:</label>
						<div class="col-6">		
							<input type="text" class="text-right" id="cdoc" name="cdoc" disabled>
							<input type="hidden" id="folioInterno">
						</div>
					</div>
					<div class="row">
						<label for="status" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Estado:</label>
						<div class="col-6">
							<input type="text"  name="status" id="status" class="status text-right" value="Abierto" disabled>
						</div>
					</div>
					<div class="row">
						<label for="fconta" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha Contabilización:</label>
						<div class="col-6">
							<input type="date" name="fconta" value="<?php echo $hoy ?>" id="fconta">
						</div>
					</div>
					<div class="row">
						<label for="fentrega" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha Entrega:</label>
						<div class="col-6">
							<input type="date" name="fentrega" value="<?php echo $mesDespues ?>" id="fentrega">
						</div>
					</div>
					<div class="row">
						<label for="fdoc" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha Documento:</label>
						<div class="col-6">
							<input type="date" name="fdoc" value="<?php echo $hoy ?>" id="fdoc">
						</div>
					</div>
					<div class="row">
						<label for="fvencimiento" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha Vencimiento:</label>
						<div class="col-6">
							<input type="date" name="fvencimiento" value="<?php echo $quincena ?>" id="fvencimiento">
						</div>
					</div>
					
				
					<!-- <button class="btn btn-block btn-secundary btn-sm" id="btnCamposDefinidos" title="Campos definidos por el usuario">Campos definidos por el usuario</button> -->
					
					<!-- //aqui -->
					<!-- <div class="row" id="camposDefinidos" style="font-size: .7rem; margin-top: 10px">
						<label for="numLetra" class="col-sm-3 offset-md-4 col-form-label">Número a letra:</label>
						<div class="col-sm-5">
							<input type="text" id="numLetra" name="numLetra">
						</div>
						<label for="tipoFactura" class="col-sm-3 offset-md-4 col-form-label">Tipo de factura:</label>
						<div class="col-sm-5">
							<input type="text" name="tipoFactura" id="tipoFactura">
							<a href="#" data-toggle="modal" data-target="#modalTipoFactura">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
							</a>
						</div>
						<label for="lab" class="col-sm-3 offset-md-4 col-form-label">LAB:</label>
						<div class="col-sm-5">
							<input type="text"  name="lab" id="lab">
						</div>
						<label for="condicionPago" class="col-sm-3 offset-md-4 col-form-label">Condición de pago:</label>
						<div class="col-sm-5">
							<input type="text"  name="condicionPago" id="condicionPago">
							<a href="#" data-toggle="modal" data-target="#modalCondicionPago">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
							</a>
						</div>
						<label for="" class="col-sm-3 offset-md-4 col-form-label">RFC:</label>
						<div class="col-sm-5">
							<input type="text" name="rfc" id="rfc">		
						</div>
					</div> -->


				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<nav>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Contenido</a>
							<a class="nav-item nav-link" id="nav-comentarios-tab" data-toggle="tab" href="#nav-comentarios-seguimiento" role="tab" aria-controls="nav-profile" aria-selected="false" onclick="pintar_tabla_seguimiento()">Comentarios</a>						
							<a class="nav-item nav-link" id="nav-campos-tab" data-toggle="tab" href="#nav-campos-por-usuario" role="tab" aria-controls="nav-profile" aria-selected="false">Campos de usuario</a>						
						</div>
					</nav>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
							<br>
							<div class="row"> 
								<div class="col-md-12">
									<table class="table table-sm table-bordered table-editable text-center" id="detalleoferta">
										<thead>
											<tr>
												<th class=u_precio_h></th>
												<th class="eliminar_h"></th>
												<th class="cont_h">N°</th>
												<th>Número de artíuclo</th>
												<th>Descripción de artíuclo</th>
												<th>Cantidad</th>
												<th>Precio por unidad</th>																	
												<th>Total</th>
												<th>Almacén</th>
												<th>Cantidad pendiente</th>
												<th>Código Planificación SAT</th>
												<th>Unidad medida SAT</th>
												<th>Comentarios de partida 1</th>
												<th>Comentarios de partida 2</th>	
												<th>Stock</th>
												<th>Comprometido</th>
												<th>Solicitado</th>	
												<th>TreeType</th>
												<th>LineNum</th>
												<th>Hijo</th>
											</tr>
										</thead>
										<tbody> 
											<tr class="last_tr">
												<td class="u_precio" style="color: green"><i class="fas fa-dollar-sign"></i>U precio</td>
												<td class="eliminar" style="color: red"><i class="fas fa-trash-alt"></i>Eliminar</td>
												<td class="cont"></td>
												<td data-toggle="modal" data-target="#myModalArticulos" class="cursor" title="click para agregar articulo"></td>
												<td data-toggle="modal" data-target="#myModalArticulos" class="cursor" title="click para agregar articulo"></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>	
												<td></td>
												<td></td>
												<td></td>	
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-6">
									<div class="row ">
										<label for="empleado" class="col-3 col-md-4 col-lg-3 col-form-label">Empleado de ventas:</label>
										<div class="col-6">
											<input type="text" name="empleado" id="empleado" value="<?php echo "$empleadoVentas"?>" disabled> 
										</div>
									</div>
									<div class="row ">
										<label for="NombrePropietario" class="col-3 col-md-4 col-lg-3 col-form-label">Propietario:</label>
										<div class="col-6">
											<input type="text" name="NombrePropietario" id="NombrePropietario" value="<?php echo "$nombreCompleto"?>" disabled>
										</div>
									</div>
									<div class="row ocultarAlCte">
										<label for="proyectoSN" class="col-3 col-md-4 col-lg-3 col-form-label">Proyecto SN:</label>
										<div class="col-6">
											<select name="proyectoSN" id="proyectoSN"></select>
										</div>
									</div>
									<div class="row ocultarAlCte">
										<label for="ventasAdic" class="col-3 col-md-4 col-lg-3 col-form-label">Ventas Adicional:</label>
										<div class="col-6">
											<select name="ventasAdic" id="ventasAdic"></select>
										</div>
									</div>
									<div class="row ocultarAlCte">
										<label for="promotor" class="col-3 col-md-4 col-lg-3 col-form-label">Promotor:</label>
										<div class="col-6">
											<select name="promotor" id="promotor"></select>
										</div>
									</div>
									<div class="row ocultarAlCte">
										<label for="cordVenta" class="col-3 col-md-4 col-lg-3 col-form-label">Coordinador de venta:</label>
										<div class="col-6">
											<select name="cordVenta" id="cordVenta"></select>
										</div>
									</div>
									<div class="row ocultarAlCte">
										<label for="comentarios" class="col-sm-3 col-form-label">Comentarios:</label>
										<div class="col-8">
											<textarea name="comentarios" id="comentarios" class="form-control" rows="2"></textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row">
										<label for="totalAntesDescuento" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Subtotal:</label>
										<div class="col-6">
											<input type="text" name="totalAntesDescuento" value="0.00" id="totalAntesDescuento" class="text-right"disabled>
										</div>
									</div>
									<div class="row">
										<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Descuento:</label>
										<div class="col-6">
											<input type="text" value="0" name="descNum" id="descNum" style="width: 16%" class="text-center" onkeyup="calcular_porcentaje_descuento(this)">
											<span whidth="6%">%</span>
											<input type="text" value="0.00" name="descAplicado" id="descAplicado" style="width: 68%" class="text-right"disabled>
										</div>
									</div>
									<div class="row">
										<label for="redondeo" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Redondeo:</label>
										<div class="col-6">
											<input type="text" value="0.00" name="redondeo" id="redondeo" class="text-right"disabled>
										</div>
									</div>
									<div class="row">
										<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Impuesto:</label>
										<div class="col-6">

											<input type="text" value="0.00" name="impuestoTotal" id="impuestoTotal" class="text-right"disabled>
										</div>
									</div>
									<div class="row">
										<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Total del documento:</label>
										<div class="col-6">
											<input type="text" value="0.00" name="totalDelDocumento" id="totalDelDocumento" style="width: 69%" class="text-right"disabled onchange="numero_a_letra()">
											<input class="text-center" type="text" name="monedaVisor" id="monedaVisor" style="width: 20%" class="text-right"disabled>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="nav-comentarios-seguimiento" role="tabpanel" aria-labelledby="nav-comentarios-tab">
							<br><br>
							<div class="row" style="margin-bottom: 30px">
								<div class="col-md-6">
									<div class="form-group">
										<label for="txt_area_comentarios_llamada">Comentarios Llamada</label>
										<textarea class="form-control" id="txt_area_comentarios_llamada" rows="2"></textarea>
									</div>
									<div class="form-group">
										<select id="slt-comentarios-llamada">
											<option>Seleccionar</option>
											<option>Seguimiento</option>
											<option>Levantamiento</option>			
										</select>
									</div>
									<button class="btn btn-sm btn-primary" style="background-color: #005580; color: white" onclick="agregar_llamada()" id="btn-agregar-llamada">Agregar Llamada</button>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="txt_area_comentarios_visita">Comentarios Vista</label>
										<textarea class="form-control" id="txt_area_comentarios_visita" rows="2"></textarea>
									</div>
									<div class="form-group">						
										<select id="slt-comentarios-visita">
											<option>Seleccionar</option>
											<option>Seguimiento</option>
											<option>Levantamiento</option>			
										</select>
									</div>
									<button class="btn btn-sm btn-primary" style="background-color: #005580; color: white" onclick="agregar_visita()" id="btn-agregar-visita">Agregar Visita</button>
								</div>				
							</div>
							<div class="row">
								<div class="col-md-12">
									<table class="table table-bordered table-striped table-hover table-sm" id="tbl_seguimiento">
										<thead>
											<tr>
												<th></th>
												<th>#</th>
												<th>Visita/Llamada</th>
												<th>Usuario</th>
												<th>TipoSeguimiento</th>
												<th>Comentarios</th>
												<th>FechaCreación</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
								<br>
							</div>
						</div>			
						<div class="tab-pane fade" id="nav-campos-por-usuario" role="tabpanel" aria-labelledby="nav-campos-tab">	

						<div class="row">
							<div class="col-8">
								<div class="row">
									<label for="numLetra" class="col-4 col-md-4 col-lg-3 col-form-label">Número a letra:</label>
									<div class="col-6">
										<input type="text" id="numLetra" name="numLetra" disabled>
									</div>
								</div>
								<div class="row">
									<label for="tipoFactura" class="col-4 col-md-4 col-lg-3 col-form-label">Tipo de factura:</label>
									<div class="col-6">
										<input type="text" name="tipoFactura" id="tipoFactura">
										<a href="#" data-toggle="modal" data-target="#modalTipoFactura">
											<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
										</a>
									</div>
								</div>
								<div class="row">
									<label for="lab" class="col-4 col-md-4 col-lg-3 col-form-label">LAB:</label>
									<div class="col-6">
										<input type="text"  name="lab" id="lab">
									</div>
								</div>
								<div class="row">
									<label for="condicionPago" class="col-4 col-md-4 col-lg-3 col-form-label">Condición de pago:</label>
									<div class="col-6">
										<input type="text"  name="condicionPago" id="condicionPago">
										<a href="#" data-toggle="modal" data-target="#modalCondicionPago">
											<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
										</a>
									</div>
								</div>
								<div class="row">
									<label for="" class="col-4 col-md-4 col-lg-3 col-form-label">RFC:</label>
									<div class="col-6">
										<input type="text" name="rfc" id="rfc">		
									</div>
								</div>	
								<div class="row">
									<label for="" class="col-4 col-md-4 col-lg-3 col-form-label">Razón social:</label>
									<div class="col-6">
										<input type="text" name="razon-social" id="razon-social">		
									</div>
								</div>
								<!-- </div> -->
							</div>
						</div>			
					</div>
				</div>
			</div>									
			<div class="row" id= btnFoot style="width: 100%">
				<div class="col-md-6">
					<button class="btn btn-sm" style="background-color: orange" id="btnCrear" onclick="crear_ofv()" title="Crear">Crear</button>
					<button class="btn btn-sm" style="background-color: orange; display:none;" id="btnActualizar" onclick="actualizar_ofv()" title="Actualizar">Actualizar</button>
					<a href="">	<button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>
				</div>
				<div class="col-md-6 text-right">
					<div class="dropdown">
						<button class="btn btn-secondary dropdown-toggle btn-sm" type="button" id="copiarA" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" disabled>
							Copiar a
						</button>						
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#" id="pedidoAlCliente" onclick="mandar_ofv_pedido_al_cliente()">Pedido al cliente</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</body>
	<?php include "../footer.html" ?>
	<script src="js/OFV.js"></script>
</html>
