	<?php
	include "conexion.php";
		session_start();
		if (!isset($_SESSION["usuario"]))
   {
      header("location: login.php");
   }
	 If ($_SESSION['CodigoPosicion'] == 36 || $_SESSION['CodigoPosicion']== 43 || $_SESSION['CodigoPosicion']== 45 || $_SESSION['CodigoPosicion']== 44 )
   {

   }
	 else {
	 	  header("location: login.php");
	 }
	$month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;

$FechaInicio = date( "Y-m-d", strtotime( "$today -6 month" ) );
	?>

	<!DOCTYPE html>
	<html>
	<?php include "header.php"; ?>
	<body>

		<?php include "nav.php"; ?>
		<?php include "modalQuerys.php"; ?>
		<?php include "modales.php"; ?>

		<div class="container" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Historial de Depósitos de Nómina</h3>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">

				</div>
			</div>




			<div class="row datosEnc" style="font-size: .7rem">
	 			
			<br>
				<div class="col-md-6">
					<div class="row">
						<label class="col-sm-3 col-form-label" >Id Cajero:</label>
						<div class="col-sm-4">
							<input type="text" class="" name="codcajero" id="codcajero" style="width: 70%">
							<a id="btnCajero" href="#" data-toggle="modal" data-target="#myModalCajero">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cajero"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Nombre Cajero:</label><br>
						<div class="col-sm-4">
							<input type="text" class="" name="NombreC" id="NombreC" style="width: 70%">
							<a href="#" data-toggle="modal" data-target="#myModalCajero" >
								<i class="fas fa-search" style="color: #57b4ea" title="Búsqueda Nombre"></i>
							</a>
						</div>
					</div>
				</div>
			<br>

				<div class="col-md-12">
					<div class="col-md-12">
              		<input type="text" id="buscador" placeholder="Buscar" title="Buscador">

			  		<label>Fecha Inicio:</label>
			  			<input type="date" onchange="cargarProyectos();" id="FechaIN" value="<?php echo $FechaInicio; ?>">
			  		<label>Fecha Fin:</label>
						<input type="date" onchange="cargarProyectos();" id="FechaFN" value="<?php echo $today; ?>">
            	</div>

			</div>
		</div>
			<br>

			<div class="row" style="font-size: .7rem">
				<div class="col-md-12">
					<div class="row">
							<button onclick="exportTableToExcel('listaespera')">Exportar a Excel</button>
					  		<div class="col-md-12">
					  			<table class="table-bordered table-editable table-hover table-striped table-responsive table" width="100%" id="listaespera" >
					        		<thead>
					        			<tr class="encabezado">
											<th>Id</th>
											<th>IdUsuario</th>
											<th>Nombre Empleado</th>
											<th>Concepto</th>
											<th>Importe Disponible</th>
											<th>Fecha de Deposito</th>
					        				<th>Fecha Programada Para Retirar</th>
											<th>Importe Inicial</th>
					        				<th>Fecha Retiro</th>
											<th>Usuario Deposito</th>

					        			</tr>
					        		</thead>
					        		<tbody>
							        	<tr>

											<td class="Id"></td>
											<td class="NOM"></td>
											<td class="Nombre"></td>
											<td class="Concepto"></td>
								            <td class="ImporteActual"></td>
											<td class="FechaCreacion"></td>
								            <td class="FechaRetirar"></td>
											<td class="ImporteInicial"></td>
											<td class="FechaRetirado"></td>
											<td class="UsuarioDeposito"></td>
										</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>
				</div>

		</div>
	</div>

		<?php include "footer.php"; ?>
	</body>
		<script>
			$("#buscador").keyup(function(){

				cargarProyectos();
			});

			function cargarProyectos(){
				var fechaInicio = document.getElementById("FechaIN").value;
				 var fechaFin = document.getElementById("FechaFN").value;
				 var valorescrito = $("#buscador").val();
				 var valorescrito = $("#buscador").val();
				var codcaj = $("#codcajero").val();
				 console.log(fechaInicio);
				 console.log(fechaFin);
				 console.log(valorescrito);
				 console.log(codcaj);
				if (codcaj === "") {
				}else{
					$.ajax({
						url: 'ofvConsultasMaster/buscadorhistorialnomina.php',
						type: 'post',
						data: {valorescrito:valorescrito, fechainicio:fechaInicio, fechafin:fechaFin,codigo:codcaj},
						success: function(response){

							$("#listaespera tbody").empty();
							$("#listaespera tbody").append(response);
						}
					});
				}
			}

			 if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }

	function postForm(path, params, method) {
    method = method || 'post';

    var form = document.createElement('form');
    form.setAttribute('method', method);
    form.setAttribute('action', path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}
			$(document).on('click', '#eliminarFila', function (event) {

				  event.preventDefault();

				  var currentRow=$(this).closest("tr");
				  var valorEscrito=currentRow.find("td:eq(0)").text();

				  postForm('masterproyectosseguimiento.php', {valor: valorEscrito});
				 // $(this).closest('tr').remove();
				    //respuestas(valorEscrito);
					// $.ajax({
							// url:"eliminadeposito.php",
							// type:"POST",
							// data:{
								// valor:valorEscrito,
							// },

						// }).done(function (response) {

						   // alert(valorEscrito);
					// });

					// $.ajax({
					// url: 'EliminaDeposito.php',
					// type: 'Post',
					// data: {valor: valorEscrito},
					// success: function(response){

						 // alert(response);
						// }
					// });


			});
	function cambiar(){
    var pdrs = document.getElementById('file-upload').files[0].name;
    document.getElementById('info').innerHTML = pdrs;
}
			function respuestas(Id){

				   $.ajax({
						type: "post",
						url: "eliminadeposito.php?valor="+Id,

						success  : function(data){
							 //window.location.href="nomina.php";
						},
						error    : function(){
							 alert("Could not ");
							 alert("EliminaDeposito.php?valor="+Id);
						}

					});
			}

			function exportTableToExcel(tableID, filename = ''){
				var downloadLink;
				var dataType = 'application/vnd.ms-excel';
				var tableSelect = document.getElementById(tableID);
				var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

				// Specify file name
				filename = filename?filename+'.xls':'movimientos.xls';

				// Create download link element
				downloadLink = document.createElement("a");

				document.body.appendChild(downloadLink);

				if(navigator.msSaveOrOpenBlob){
					var blob = new Blob(['\ufeff', tableHTML], {
						type: dataType
					});
					navigator.msSaveOrOpenBlob( blob, filename);
				}else{
					// Create a link to the file
					downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

					// Setting the file name
					downloadLink.download = filename;

					//triggering the function
					downloadLink.click();
				}
			}



			$("#buscadorCajero").keyup(function(){
				var valorEscrito = $("#buscadorCajero").val();
				if (valorEscrito) {
					$.ajax({
						url: 'buscadorConsultaCajero.php',
						type: 'post',
						data: {valor: valorEscrito},
						success: function(resp){


							$("#tblcajero tbody").empty();
							$("#tblcajero tbody").append(resp);

							$("#tblcajero tr").on('click',function(){


								if (!$('#BackOrderVentas').is(':visible')) {
									var codigo = $(this).find('td').eq(1).text();
									var name = $(this).find('td').eq(2).text();


									agregarCajero(codigo, name);
									$("#tblcajero tbody").empty();
									$("#buscadorcajero").val("");

									$("#nombreCajero").val(name);
									$("#codigoCajero").val(codigo);
									$("#myModalCajero").hide();

								} else {
									var codigo = $(this).find('td').eq(1).text();
									var name = $(this).find('td').eq(2).text();
									$("#nombreCajero").val(name);
									$("#codigoCajero").val(codigo);
									$("#myModalCajero").hide();


								 }
							 });
						 },
					 });
				} else {
					$("#tblcajero tbody").empty();
				}
			});

			$("#myModalCajero").on('click',function() {
				$("#buscadorCajero").val("");
			});



			function agregar ($code, $name){
				var code = $code;
				var name = $name;
				$("#codcajero").val(code);
				$("#NombreC").val(name);
				$('#myModalCajero').modal('hide');
				$.ajax({
					url: 'consultaPersonaContacto.php',
					type: 'post',
					data: {code: code},
					success: function(response){
						$("#listcontactos").append(response);
					}
				});
			}

			function agregarCajero ($code, $name){
				var code = $code;
				var name = $name;
				$("#codcajero").val(code);
				$("#NombreC").val(name);
				$('#myModalCajero').modal('hide');
			}





		</script>
	</html>
