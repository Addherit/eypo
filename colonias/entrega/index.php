<!DOCTYPE html>
<?php include "../header.html" ?>
<html lang="en">
	<body onload="cargar_funciones_principales()">
		<?php include "../nav.php" ?> 
		<?php include "../../modalQuerys.php" ?>
		<?php include "../../modales.php" ?>
		
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-lg-5 col-xl-6">
					<h1 style="color: #2fa4e7">Entrega</h1>
				</div>
				<div id="btnEnca" class="col-lg-7 col-xl-6" style="font-size: 2rem">
					<?php include "../../botonesDeControl.php" ?>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<label class="col-3 col-md-4 col-lg-3 col-form-label" >Cliente:</label>
						<div class="col-6">
							<input type="text" id="cliente">
						</div>
					</div>
					<div class="row">
						<label class="col-3 col-md-4 col-lg-3 col-form-label">Nombre:</label><br>
						<div class="col-6">
							<input type="text" id="nCliente">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Persona de contacto:</label>
						<div class="col-6">
							<input type="text" id="personaContacto">								
						</div>
					</div>					
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Tipo moneda:</label>
						<div class="col-6">
							<input type="text" id="tipoMoneda">								
						</div>
					</div>													
				</div>
				<div class="col-md-6">
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">N° Folio:</label>
						<div class="col-6">							
							<input type="text" id="cdoc" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Estado:</label>
						<div class="col-6">
							<input type="text" id="estado" value="ABIERTO" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha contabilización:</label>
						<div class="col-6">
							<input type="date" value="<?php echo $hoy ?>" id="fconta" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha documento:</label>
						<div class="col-6">
							<input type="date" value="<?php echo $hoy ?>" id="fdoc" readonly="true">
						</div>
					</div>							
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Uso principal:</label>
						<div class="col-6 ">
							<input type="text" id="usoPrincipal" readonly="true">								
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Método de pago:</label>
						<div class="col-6">
							<input type="text" id="metodoPago" readonly="true">								
						</div>
					</div>
					<div class="row">
						<label class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Forma de pago:</label>
						<div class="col-6">
							<input type="text" id="formaPago" readonly="true">								
						</div>
					</div>
				</div>
			</div>					
			<div class="row">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Contenido</a>
						</li>						
					</ul>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
						<br>		 
						<div class="row">
							<div class="col-md-12"> 
								<section class="table-responsive">
									<table class="table table-sm table-hover table-bordered table-striped text-center" id="tblEntregaVista">
										<thead>
											<tr>													
												<th>#</th>
												<th>Código articulo</th>
												<th style="width: 100%">Nombre artíuclo</th>					        				
												<th>Cantidad</th>
												<th>Precio por unidad</th>
												<th>Descuento %</th>
												<th>Indicador Impuesto</th>
												<th>Sujeto a retención de impuesto</th>										
												<th>Total</th>
												<th>Unidad SAT</th>	
												<th>Almacén</th>																																		
											</tr>
										</thead>
										<tbody>
											<tr style="height: 27px">											
												<td class="cont"></td>
												<td class="narticulo"></td>
												<td class="darticulo"></td>
												<td class="cantidad"></td>
												<td class="precio"></td>
												<td class="descuento"></td>
												<td class="impuesto"></td>
												<td class="codigoImpuesto"></td> 													
												<td class="total"></td>
												<td class="unidadSAT"></td>
												<td class="almacen"></td>																																				
											</tr>
										</tbody>
									</table>
								</section>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Empleado de ventas:</label>
									<div class="col-sm-6">
										<input type="text" id="empleado" readonly="true">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Propietario:</label>
									<div class="col-sm-6">
										<input type="text" id="propietario">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Proyecto SN:</label>
									<div class="col-sm-6">
										<input type="text" id="proyectoSN" readonly="true">
									</div>
								</div>										
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Promotor:</label>
									<div class="col-sm-6">
										<input type="text" id="promotor" readonly="true">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Ventas Adicional:</label>
									<div class="col-sm-6">
										<input type="text" id="ventasAdic" readonly="true">
									</div>
								</div>								
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Comentarios:</label>
									<div class="col-8">
										<textarea name="" id="comentarios" class="form-control" rows="3" readonly="true"></textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
							
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Subtotal:</label>
									<div class="col-6">
										<input type="text" id="totalAntesDescuento" value="0.00" class="text-right" readonly="true">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Descuento:</label>
									<div class="col-6">
										<input type="text" id="descNum" value="0" class="text-center" style="width: 16%;">
										<span whidth="6%">%</span>
										<input type="text" value="0.00" class="text-right" id="descAplicado" style="width: 68%" readonly="true">
									</div>
								</div>									
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Redondeo:</label>
									<div class="col-6">
										<input type="text" id="redondeo" value="0.00" class="text-right" readonly="true">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Impuesto:</label>
									<div class="col-6">
										<input type="text" value="0.00" class="text-right" id="impuestoTotal" readonly="true">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Total:</label>
									<div class="col-6">
										<input type="text" id="totalDelDocumento" value="0.00" class="text-right" readonly="true">
									</div>
								</div>										
							</div>								
						</div>		 				
					</div>
				</div>
				<div class="row" id= btnFoot style="margin-bottom: 30px">
					<div class="col-md-6">	<br>													
						<a href=""><button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>					
					</div>			
				</div>	
			</div>						
		</div> 
    </body>
	<?php include "../footer.html" ?>
	<script src="js/entrega.js"></script>
</html>
