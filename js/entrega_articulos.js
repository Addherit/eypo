

var fechaContabilizacionSuperior = $("#fecha_contabilizacion_superiror").val()
var fechaContabilizacionMenor = $("#fecha_contabilizacion_menor").val()

$.ajax({
        url:"querys/entregaArticulos.php",
        type:"post",
        data:{ 
            fechaContabilizacionSuperior : fechaContabilizacionSuperior,
            fechaContabilizacionMenor : fechaContabilizacionMenor,
        },
        dataType: 'JSON',
    }).done(function (response) {		
        
        
    $("#tblEntradaArticulos tbody").empty()
    var tr_order = "";

    if(response.length > 0) {
        response.forEach((element, index) => {
            index++
            tr_order += `<tr>
            <td>${index}</td>
            <td>${element.DocNum}</td>
            <td>${element.Fecha.date}</td>
            <td>${element.CardName}</td>
            <td>${element["Codigo Art"]}</td>
            <td>${element.Articulo}</td>
            <td>${element["Comentario Partida"]}</td>
            
            <td>${element.Cant}</td>
            <td>${element.Pedido}</td>
            <td>${element["O.C. Cliente/Stock"]}</td>
            <td>${element.Cliente}</td>
            </tr>`
        })
    } else {
        tr_order += '<tr><td colspan="17">No existe registro alguno con los datos proporcionados</td></tr>' 
    }    
    $("#tblEntradaArticulos tbody").append(tr_order);			    
    });    