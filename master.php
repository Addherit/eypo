	<?php

	include "conexion.php";
	session_start();
	$_SESSION['usuario'];

	If ($_POST['valor'] != '')
	{
	$_SESSION['valor'] = $_POST['valor'];
	}
	else
	{
		$_SESSION['valor'] = '0';
	}
	 $folio = $_SESSION['valor'];
	  If ($_POST['aut'] != '')
	{
	$_SESSION['aut'] = $_POST['aut'];
	}
	else
	{
		$_SESSION['aut'] = '';
	}
	 $aut = $_SESSION['aut'];
	$month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;

$FechaInicio = date( "Y-m-d", strtotime( "$today -6 month" ) );

	?>

	<!DOCTYPE html>
	<html>
	<?php include "header.php"; ?>


		<body onload="consultaOFV(<?php echo $folio;?>);">

		<?php include "nav.php"; ?>

		<?php include "modales.php"; ?>

		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Datos del Proyecto:</h3>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">
					<?php include "botonesDeControlMaster.php" ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<label class="col-sm-3 col-form-label" >Cliente:</label>
						<div class="col-sm-9">
							<input type="text" class="" name="codcliente" id="codcliente" style="width: 70%">
							<a id="btnCliente" href="#" data-toggle="modal" data-target="#myModal">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Nombre del Cliente:</label><br>
						<div class="col-sm-9">
							<input type="text" class="" name="NombreC" id="NombreC" style="width: 70%">
							<a href="#" data-toggle="modal" data-target="#myModal" >
								<i class="fas fa-search" style="color: #57b4ea" title="Búsqueda Nombre"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Persona de Contacto:</label>
						<div class="col-sm-9">
							<input type="text" class="" id="Contacto" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Ubicación del Proyecto:</label>
						<div class="col-sm-9">
							<input type="text" class="" id="Ubicacion" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Celular del Contacto:</label>
						<div class="col-sm-9">
							<input type="text" class="" id="Telefono" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Correo:</label>
						<div class="col-sm-9">
							<input type="email" class="" id="Correo" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Nombre del Proyecto:</label>
						<div class="col-sm-9">
							<input type="text" class="" id="NombreProyecto" style="width: 70%">
						</div>
					</div>

					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Fecha de Posible Venta:</label>
						<div class="col-sm-9">
							<input type="date" class="" id="FechaVenta" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Fecha de Promesa de Entrega:</label>
						<div class="col-sm-9">
							<input type="date" class="" id="FechaEntrega" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Tipo de Propuesta:</label>
						<div class="col-sm-9">
							<select name="" id="tpropuesta" style="width: 70%">
								<option value="" disabled selected>Seleccione</option>
									<?php
									if ($_SESSION['CodigoPosicion'] == '52'||$_SESSION['CodigoPosicion'] == '53'){
										$sql = "SELECT * FROM MasterEypo.dbo.TiposPropuestas where Estatus = 'ALTA' and TipoPropuesta like 'cross%'";
									}
									if ($_SESSION['CodigoPosicion'] == '50'||$_SESSION['CodigoPosicion'] == '51'){
										$sql = "SELECT * FROM MasterEypo.dbo.TiposPropuestas where Estatus = 'ALTA' and TipoPropuesta like '%dom%'";
									}
									if ($_SESSION['CodigoPosicion'] == '36'||$_SESSION['CodigoPosicion'] == '37'||$_SESSION['CodigoPosicion'] == '54'){
										$sql = "SELECT * FROM MasterEypo.dbo.TiposPropuestas where Estatus = 'ALTA'";
									}
									if ($_SESSION['CodigoPosicion'] == '46'||$_SESSION['CodigoPosicion'] == '49'){
										$sql = "SELECT * FROM MasterEypo.dbo.TiposPropuestas where Estatus = 'ALTA' and TipoPropuesta not like '%dom%'";
									}
								$consulta = sqlsrv_query($conn, $sql);
								while ($row = sqlsrv_fetch_array($consulta)) { ?>
											<option value="<?php echo ($row['TipoPropuesta']); ?>"><?php echo ($row['TipoPropuesta']); ?></option>
									<?php
							} ?>
							</select>
						</div>
					</div>

					<div class="row">
						    <label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Asesor de Ventas:</label>
						    <div class="col-sm-3">
									<select name="" id="AsesorV">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT iv.NombreEmpleadoVC+ ' ' + isnull(firstName,'') + ' ' + isnull(middleName,'') + ' ' + isnull(lastName,'') as Nombre, iv.* FROM IV_EY_PV_EmpleadosVentasCompras iv
left outer join OHEM e on iv.CodigoEmpleadoVC   = e.salesPrson";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['NombreEmpleadoVC']); ?>"> <?php echo ($row['Nombre']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
							<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Administrador de Ventas:</label>
						    <div class="col-sm-3">
									<select name="" id="AdminV" >
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT iv.NombreEmpleadoVC+ ' ' + isnull(firstName,'') + ' ' + isnull(middleName,'') + ' ' + isnull(lastName,'') as Nombre, iv.* FROM IV_EY_PV_EmpleadosVentasCompras iv
left outer join OHEM e on iv.CodigoEmpleadoVC   = e.salesPrson";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['NombreEmpleadoVC']); ?>"> <?php echo ($row['Nombre']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
						</div>


				</div>
				<div class="col-md-6">
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">N°:</label>
						<div class="col-sm-4">
							<select id="ndoc">
								<option value="COT-GRAL">PROYECTO</option>
								<!--<option value="Manual">Manual</option>-->
							</select>
							<?php
						If ($folio == '0')
						{
						$consultasql = sqlsrv_query($conn, "SELECT TOP 1 Id  FROM MasterEypo.dbo.Proyectos ORDER BY Id DESC");
						// $consultasql2 = sqlsrv_query($conn, "SELECT TOP 1 FolioSAP  FROM EYPO.dbo.IV_EY_PV_OfertasVentasCab ORDER BY FolioSAP DESC");
						$Row = sqlsrv_fetch_array($consultasql);
						// $Row2 = sqlsrv_fetch_array($consultasql2);
						$folio = $Row['Id'];
						// $folioSAP = $Row2['FolioSAP'];
						$folio++;
						$aut = 0;
						}
						// if($folio >= $folioSAP){
							// $folio++;
						// } else {
							// $folio = $folioSAP;
							// $folio++;
						// }
						?>
							<input type="text" class="" id="cdoc" name="cdoc" size="5" value="<?php echo "$folio"; ?>" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Prioridad:</label>
						 <div class="col-sm-4">
									<select id="Prioridad">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT * FROM MasterEypo.dbo.Prioridades where Estatus = 'ALTA' order by prioridad";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['Prioridad']); ?>"> <?php echo ($row['Prioridad']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>

					</div>


					<div class="row">
						<label for=""  class="col-sm-4 offset-md-4 col-form-label">Estatus:</label>
						<div class="col-sm-4">
							<input type="text" disabled="true" class="" id="Estatus">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Ventas Directivas:</label>
						<div class="col-sm-4">
							<input type="text" id="vDirectivas">
						</div>
					</div>

					<div class="row">
					<label for="" class="col-sm-4 offset-md-4 col-form-label table-hover table-striped table-responsive table">Archivos:</label>
					</div>
					<table class="col-sm-4 offset-md-4" id="Adjuntos" style="border-spacing:5px; border-collapse: separate;">
						  <tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						  </tr>
						  <tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						  </tr>
						  </table>
					<div class="row">
					<div class="col-md-7 offset-md-4 text-Right" style="margin-top: 10px ">

							<!--<form id="Adjuntos" action = "" method = "POST" enctype = "multipart/form-data">-->
								<input type="text" class="" id="idproyecto" name="idproyecto" size="5" value="<?php echo "$folio"; ?>" readonly="true" style="display:none">
								<label for="file-upload" class="custom-file-upload">
								<input type = "file" onchange='cambiar()' name = "file" id="file-upload" class="file-upload"/>

								  </label>
								  <button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnAdjuntar" title="Adjuntar">Adjuntar Archivo</button>
								<!--  <input type = "submit" value="Adjuntar Archivo" class="btn"/>-->


								<div id="info"></div>





							<!--</form>-->
						</div>

					</div>
						<?php If($aut==1){
					?>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Complejidad:</label>
						 <div class="col-sm-3">
									<select name="" id="Complejidad">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT * FROM MasterEypo.dbo.Complejidades where Estatus = 'ALTA'  order by Complejidad";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['Complejidad']); ?>"> <?php echo ($row['Complejidad']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>

					</div>
					<?php }?>


				</div>
			</div>
			<div class="row" style="font-size: .7rem">
			<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Techo Financiero:</label>
						<div class="col-sm-12">
							<textarea name="TechoFinanciero" id = "TechoFinanciero" cols="40" rows="5" style="width: 70%"></textarea>
						</div>
			</div>
			<div class="row" style="font-size: .7rem">

			<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Observaciones de Ventas:</label>
						<div class="col-sm-12">
							<textarea name="Observaciones" id = "Observaciones" cols="40" rows="5" style="width: 70%"></textarea>
						</div>
			</div>
			<div class="row" id="ObservacionesP" style="font-size: .7rem">
			<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Observaciones de Proyectos:</label>
						<div class="col-sm-12">
							<textarea disabled = "true" name="ObservacionesProyectos" id = "ObservacionesProyectos" cols="40" rows="5" style="width: 70%"></textarea>
						</div>
			</div>

			<br>


<div class="row" id= btnFoot style="margin-bottom: 30px">
					<div class="col-md-12" align="center">
						<?php If($aut==1){
					?>
					<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnAutorizar" title="Autorizar">AUTORIZAR</button>
					<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnRechazar" title="Autorizar">RECHAZAR</button>
					<?php
					}else{?>
					<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnCrear" title="Crear">GUARDAR</button>
					<button class="btn btn-sm" style="background-color: #005580; color:white; display:none;" id="btnPerdido" title="CANCELAR" href="#" data-toggle="modal" data-target="#myModalPerdido">CANCELAR</button>
					<button class="btn btn-sm" style="background-color: #005580; color:white; display:none;" id="btnSolicitar" title="Crear">SOLICITAR NUEVA REVISION</button>

					<?php }?>
					</div>


					</div>

			</div>
		<?php include "footer.php"; ?>
	</body>
		<script>
		$("#btnPerder").on('click', function(){

				var Id = $('#cdoc').val();
				var observacionescostos = $('#observacionesp').val();
				var estatus = 'PERDIDO';


				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterActualizaCostos.php",
					data:{

						Id: Id,
						observacionescostos: observacionescostos,
						estatus: estatus,

					},
					success: function(resp){

							alert('El proyecto ha sido almacenado como PERDIDO');

						$("#MyModalPerdido").modal('hide');
					postForm('seguimiento.php');


					}

				});
			});

			function validar_email( email )
{
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}
			function consultanotificaciones(){

					//contador proyectos lista de espera
					if ("<?php echo $_SESSION['CodigoPosicion']?>" == '50'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'){
						var tipopropuesta = 'dom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'){
						var tipopropuesta = 'nodom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '52'){
						var tipopropuesta = 'crossn';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '53'){
						var tipopropuesta = 'crossd';
					}
					else
					{
						var tipopropuesta = 'normal';
					}
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'1', tipopropuesta:tipopropuesta},
					success: function(response){


                     }
				});
						//carga contador lista espera autorizacion coordinador proyectos
					if ("<?php echo $_SESSION['CodigoPosicion']?>" == '50'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'){
						var tipopropuesta = 'dom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'){
						var tipopropuesta = 'nodom';
					}
					else
					{
						var tipopropuesta = 'normal';
					}

				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'2', tipopropuesta:tipopropuesta},
					success: function(response){

                     }
				});
				//Cargar contador autorizaciones ventas
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'6'},
					success: function(response){

                     }
				});
				//Cargar contador codigos por autorizar
					$.ajax({
					url: 'OFVConsultasMaster/buscadorcodigosporautorizar.php',
					type: 'post',
					success: function(response){
						//alert(response);
                     }
				});
				if ("<?php echo $_SESSION['CodigoPosicion']?>" == '50'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'){
						var tipopropuesta = 'dom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'){
						var tipopropuesta = 'nodom';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '52'){
						var tipopropuesta = 'crossn';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '53'){
						var tipopropuesta = 'crossd';
					}
					else
					{
						var tipopropuesta = 'normal';
					}
				$.ajax({
					url: 'ofvConsultasMaster/buscadorlistaespera.php',
					type: 'post',
					data: {valor:'3', tipopropuesta:tipopropuesta},
					success: function(response){

                     }
				});
			};

			$("#buscadorCliente").keyup(function(){
				var valorEscrito = $("#buscadorCliente").val();
				if (valorEscrito) {
					$.ajax({
						url: 'socios/buscadorConsultaClientesYN.php',
						type: 'post',
						data: {valor: valorEscrito},
						success: function(resp){


							$("#tblcliente tbody").empty();
							$("#tblcliente tbody").append(resp);

							$("#tblcliente tr").on('click',function(){


								if (!$('#BackOrderVentas').is(':visible')) {
									if (!$('#OfertaVentaCliente').is(':visible')) {
										if (!$('#OfertaClienteF').is(':visible')) {
											var codigo = $(this).find('td').eq(1).text();
											var name = $(this).find('td').eq(2).text();
											agregar(codigo, name);
											$("#tblcliente tbody").empty();
											$("#buscadorCliente").val("");

											$.ajax({
												url:'rellenarSelectFormaPago.php',
												type:'POST',
												data:{codigo:codigo},

											}).done(function (response) {
												$("#formaPagoSelect").append(response);
											});
										} else {
											var codigo = $(this).find('td').eq(1).text();
											$("#codigoClienteF").val(codigo);
											$("#myModal").hide();
										}
									} else {
										var codigo = $(this).find('td').eq(1).text();
										$("#clienteProveedor").val(codigo);
										$("#myModal").hide();
									}
								} else {
									var codigo = $(this).find('td').eq(1).text();
									var name = $(this).find('td').eq(2).text();
									$("#nombreCliente").val(name);
									$("#codigoCliente").val(codigo);
									$("#myModal").hide();
								}
							});
						},
					});
				} else {
					$("#tblcliente tbody").empty();
				}
			});

			$("#myModal").on('click',function() {
				$("#buscadorCliente").val("");
			});



			function agregar ($code, $name){
				var code = $code;
				var name = $name;
				$("#codcliente").val(code);
				$("#NombreC").val(name);
				$('#myModal').modal('hide');
				$.ajax({
					url: 'consultaPersonaContacto.php',
					type: 'post',
					data: {code: code},
					success: function(response){
						$("#listcontactos").append(response);
					}
				});
			}

					function cargarAdjuntos (){
				var valor = $('#cdoc').val();
				$.ajax({
					url: 'OFVConsultasMaster/buscadorArchivosAdjuntos.php',
					type: 'post',
					data: {valor: valor},
					success: function(response){
						$("#Adjuntos tbody").empty();
						$("#Adjuntos tbody").append(response);
                     }
				});



			}
			 if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }

			$("#btnAdjuntar").on('click', function(){
						var file_data = $('#file-upload').prop('files')[0];
						var idproyecto = $('#cdoc').val();
						var form_data = new FormData();
						form_data.append('file', file_data);
						form_data.append('idproyecto', idproyecto);

						$.ajax({
						url: 'OFVConsultasMaster/masteradjuntararchivo.php', // point to server-side PHP script
						dataType: 'text',  // what to expect back from the PHP script, if anything
						cache: false,
						contentType: false,
						processData: false,
						data: form_data,
						type: 'post',
						success: function(php_script_response){
								$("#file-upload").val('');
								$.ajax({
									url: 'OFVConsultasMaster/buscadorArchivosAdjuntos.php',
									type: 'post',
									data: {valor: idproyecto},
									success: function(response){
										$("#Adjuntos tbody").empty();
										$("#Adjuntos tbody").append(response);
										 window.history.replaceState( null, null, window.location.href );
									 }
								});
							}
						 });
			});
			$(document).on('click', '#eliminarArchivo', function (event) {
				event.preventDefault();
				var id =  $(this).attr("alt");
				var idproyecto = $('#cdoc').val();
				$.ajax({
						url: 'OFVConsultasMaster/mastereliminaarchivo.php', // point to server-side PHP script
						data: {valor: id},
						type: 'post',
						success: function(response){
								$("#file-upload").val('');
								$.ajax({
									url: 'OFVConsultasMaster/buscadorArchivosAdjuntos.php',
									type: 'post',
									data: {valor: idproyecto},
									success: function(response){

										$("#Adjuntos tbody").empty();
										$("#Adjuntos tbody").append(response);
										 window.history.replaceState( null, null, window.location.href );
									 }
								});
							}
						 });

			});

			function postForm(path, params, method) {
				method = method || 'post';

				var form = document.createElement('form');
				form.setAttribute('method', method);
				form.setAttribute('action', path);

				for (var key in params) {
					if (params.hasOwnProperty(key)) {
						var hiddenField = document.createElement('input');
						hiddenField.setAttribute('type', 'hidden');
						hiddenField.setAttribute('name', key);
						hiddenField.setAttribute('value', params[key]);

						form.appendChild(hiddenField);
					}
				}

				document.body.appendChild(form);
				form.submit();
			}

			$("#btnCrear").on('click', function(){
				if ($('#NombreC').val() == '') {
					alert("El Nombre del Cliente es Obligatorio");
				} else if ($('#Contacto').val() == '') {
					alert("El Nombre del Contacto es Obligatorio");
				} else if ($('#Ubicacion').val() == '')	{
					alert("La Ubicación es Obligatoria");
				} else if ($('#Telefono').val() == '') { 
					alert("El Celular del Contacto es Obligatorio");
				} else if (validar_email($('#Correo').val()) == false ) {
					alert("El Correo No es Correcto");
				} else if ($('#NombreProyecto').val() == '') {
					alert("El Nombre del Proyecto es Obligatorio");
				} else if ($('#TechoFinanciero').val() == '') {
					alert("El Techo Financiero es Obligatorio");
				} else if ($('#Observaciones').val() == '') {
					alert("Las Observaciones de Ventas son Obligatorias");
				} else if ($('#tpropuesta').val() == null) {
					alert("Debes Seleccionar el Tipo de Propuesta");
				} else if ($('#AsesorV').val() == null) {
					alert("Debes Seleccionar el Asesor de Ventas");
				} else if ($('#AdminV').val() == null) {
					alert("Debes Seleccionar el Administrador de Ventas");
				} else if ($('#Prioridad').val() == null) {
					alert("Debes Seleccionar una Prioridad");
				} else if ($('#FechaVenta').val() == '') {
					alert("Debes Seleccionar la Fecha de Posible Venta");
				}
				else if ($('#FechaEntrega').val() == '') {
					alert("Debes Seleccionar la Fecha de Promesa de Entrega");
				} else {
					var Id = $('#cdoc').val();
					var cliente = $('#codcliente').val();
					var nombre = $('#NombreC').val();
					var personaContacto = $('#Contacto').val();
					var telefono = $('#Telefono').val();
					var correo = $('#Correo').val();
					var tipopropuesta = $('#tpropuesta').val();
					var asesorventas = $('#AsesorV').val();
					var administradorventas = $('#AdminV').val();
					var prioridad = $('#Prioridad').val();
					var ventasdirectivas = $('#vDirectivas').val();
					var observaciones =  $('#Observaciones').val();
					var fechaventa = $('#FechaVenta').val();
					var fechaentrega = $('#FechaEntrega').val();
					var nombreproyecto = $('#NombreProyecto').val();
					var ubicacion = $('#Ubicacion').val();
					var techo = $('#TechoFinanciero').val();
					var observacionesproyectos = $('#ObservacionesProyectos').val();
					if (tipopropuesta == 'Iluminación + Control + Fotovoltaico' || tipopropuesta == 'Iluminación + Fotovoltaico' ||  tipopropuesta == 'Iluminación + Domótica' || tipopropuesta == 'Iluminación + Domótica + Fotovoltaico' || tipopropuesta == 'Control + Fotovoltaico' ) {
						var estatus =  'PORAUTORIZARCPROYDOM';
					} else if (tipopropuesta == 'Domótica + Fotovoltaico' || tipopropuesta == 'Domótica' || tipopropuesta == 'Fotovoltaico') {
						var estatus =  'PORAUTORIZARCDOM';
					} else {
						var estatus =  'PORAUTORIZARCPROY'; 
					}
					var usuariocreacion = "<?php echo $_SESSION['usuario']?>";
					var complejidad =  '';
					$.ajax({
						type:'post',
						url: "OFVConsultasMaster/masterinsertanteproyecto.php",
						data:{
							Id: Id,
							cliente: cliente,
							nombre: nombre,
							personaContacto: personaContacto,
							telefono: telefono,
							correo: correo,
							tipopropuesta: tipopropuesta,
							asesorventas: asesorventas,
							administradorventas: administradorventas,
							prioridad: prioridad,
							ventasdirectivas: ventasdirectivas,
							observaciones: observaciones,
							usuariocreacion: usuariocreacion,
							estatus: estatus,
							complejidad: complejidad,
							fechaventa: fechaventa,
							fechaentrega: fechaentrega,
							nombreproyecto: nombreproyecto,
							ubicacion: ubicacion,
							techo: techo,
							observacionesproyectos: observacionesproyectos,
						},						
					}).done((resp) => {
						alert('Se guardo correctamente el proyecto');
						consultanotificaciones();
						location.reload();
					})					
				}
			});

			$("#btnSolicitar").on('click', function(){

				var Id = $('#cdoc').val();

					$.ajax({
					type:'post',
					url: "OFVConsultasMaster/mastersolicitanuevarevision.php",
					data:{
						Id: Id,

					},
					success: function(resp){

						alert('Se solicitó la revision satisfactoriamente');
						consultanotificaciones();
						//postForm('listaautorizacion.php')

					}
				});
				});


				$("#btnRechazar").on('click', function(){

				var Id = $('#cdoc').val();
				var cliente = $('#codcliente').val();
				var nombre = $('#NombreC').val();
				var personaContacto = $('#Contacto').val();
				var direccion = $('#Direccion').val();
				var telefono = $('#Telefono').val();
				var correo = $('#Correo').val();
				var tipopropuesta = $('#tpropuesta').val();
				var asesorventas = $('#AsesorV').val();
				var administradorventas = $('#AdminV').val();
				var prioridad = $('#Prioridad').val();
				var ventasdirectivas = $('#vDirectivas').val();
				var observaciones =  $('#Observaciones').val();

				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";
				var estatusactual = $('#Estatus').val();
				var estatus = 'RECHAZADO';


				var complejidad = $('#Complejidad').val();
				var fechaventa = $('#FechaVenta').val();
				var fechaentrega = $('#FechaEntrega').val();
				var nombreproyecto = $('#NombreProyecto').val();
				var ubicacion = $('#Ubicacion').val();
				var techo = $('#TechoFinanciero').val();
				var observacionesproyectos = $('#ObservacionesProyectos').val();


				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterinsertanteproyecto.php",
					data:{
						Id: Id,
						cliente: cliente,
						nombre: nombre,
						personaContacto: personaContacto,
						direccion: direccion,
						telefono: telefono,
						correo: correo,
						tipopropuesta: tipopropuesta,
						asesorventas: asesorventas,
						administradorventas: administradorventas,
						prioridad: prioridad,
						ventasdirectivas: ventasdirectivas,
						observaciones: observaciones,
						usuariocreacion: usuariocreacion,
						estatus: estatus,
						complejidad: complejidad,
						fechaventa: fechaventa,
						fechaentrega: fechaentrega,
						nombreproyecto: nombreproyecto,
						ubicacion: ubicacion,
						techo: techo,
						observacionesproyectos: observacionesproyectos,
					},
					success: function(resp){

						alert('Se rechazó correctamente el proyecto');
						consultanotificaciones();
						postForm('listaautorizacion.php')

					}
				});

			});


			$("#btnAutorizar").on('click', function(){

				var Id = $('#cdoc').val();
				var cliente = $('#codcliente').val();
				var nombre = $('#NombreC').val();
				var personaContacto = $('#Contacto').val();
				var direccion = $('#Direccion').val();
				var telefono = $('#Telefono').val();
				var correo = $('#Correo').val();
				var tipopropuesta = $('#tpropuesta').val();
				var asesorventas = $('#AsesorV').val();
				var administradorventas = $('#AdminV').val();
				var prioridad = $('#Prioridad').val();
				var ventasdirectivas = $('#vDirectivas').val();
				var observaciones =  $('#Observaciones').val();

				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";
				var estatusactual = $('#Estatus').val();
				if (estatusactual == 'PORAUTORIZARCPROYDOM')
				{
					if ("<?php echo $_SESSION['CodigoPosicion']?>" == '51'){
						var estatus = 'PORAUTORIZARCPROY';
					}
					else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '46'){

						var estatus = 'PORAUTORIZARCDOM';
					}
					else
					{
						var estatus = 'ANTEPROYECTO';
					}
				}
				else
				{
					var estatus = 'ANTEPROYECTO';
				}



				var complejidad = $('#Complejidad').val();
				var fechaventa = $('#FechaVenta').val();
				var fechaentrega = $('#FechaEntrega').val();
				var nombreproyecto = $('#NombreProyecto').val();
				var ubicacion = $('#Ubicacion').val();
				var techo = $('#TechoFinanciero').val();
				var observacionesproyectos = $('#ObservacionesProyectos').val();

				if (complejidad != null)
				{
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterinsertanteproyecto.php",
					data:{
						Id: Id,
						cliente: cliente,
						nombre: nombre,
						personaContacto: personaContacto,
						direccion: direccion,
						telefono: telefono,
						correo: correo,
						tipopropuesta: tipopropuesta,
						asesorventas: asesorventas,
						administradorventas: administradorventas,
						prioridad: prioridad,
						ventasdirectivas: ventasdirectivas,
						observaciones: observaciones,
						usuariocreacion: usuariocreacion,
						estatus: estatus,
						complejidad: complejidad,
						fechaventa: fechaventa,
						fechaentrega: fechaentrega,
						nombreproyecto: nombreproyecto,
						ubicacion: ubicacion,
						techo: techo,
						observacionesproyectos: observacionesproyectos,
					},
					success: function(resp){

						alert('Se autorizo correctamente el proyecto');
						consultanotificaciones();
						postForm('listaautorizacion.php')

					}
				});
				}
				else
				{
					alert('Debes Seleccionar una Complejidad');
				}
			});




			function cargarProyectos(){
				var  fechaInicio = document.getElementById("FechaInicio").value;
				 var fechaFin = document.getElementById("FechaFin").value;
				 var valorescrito = $("#buscadorProyectos").val();

				 if (valorescrito) {
				$.ajax({
					url: 'ofvConsultasMaster/buscadorGeneralAnteProyectos.php',
					type: 'post',
					data: {valor:valorescrito, fechainicio:fechaInicio, fechafin:fechaFin},
					success: function(response){

							$('#tblBuscarProyectos tbody').empty();
							$('#tblBuscarProyectos tbody').append(response);
							$("#tblBuscarProyectos tbody tr").on('click',function(){
								var codigo = $(this).find('td').eq(0).text();
								var usuario = $(this).find('td').eq(8).text().toUpperCase();
								var usuarioactual = '<?php echo $_SESSION['usuario']?>'.toUpperCase();

								if ("<?php echo $_SESSION['CodigoPosicion']?>" == '36'||"<?php echo $_SESSION['CodigoPosicion']?>" == '46' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'||"<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53'||"<?php echo $_SESSION['CodigoPosicion']?>" == '54'){

									consultaOFV(codigo);
								}
								else if (usuario==usuarioactual)
								{
									consultaOFV(codigo);
								}
								else
								{
								}

						// });
					});
					}
				});

				 }else {
					$('#tblBuscarProyectos tbody').empty();
				 }

				};
			$("#buscadorProyectos").keyup(function(){
				cargarProyectos();
			});
			$("#consulta1Atras").on('click', function(){
				var cdoc = $("#cdoc").val();
				if ("<?php echo $_SESSION['CodigoPosicion']?>" == '36'||"<?php echo $_SESSION['CodigoPosicion']?>" == '46' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'||"<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53'||"<?php echo $_SESSION['CodigoPosicion']?>" == '54'){
				cdoc = cdoc -1;

				consultaOFV(cdoc);
				 }
				 else
				 {

					 var adelanteatras = 'ATRAS';
					 var usuario = '<?php echo $_SESSION['usuario']?>';
					 		$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaatrasadelanteusuario.php',
								dataType:'json',
								data:{
								id: cdoc,
								adelanteatras: adelanteatras,
								usuario: usuario,
								},
								success: function(response){

										consultaOFV(response['IdProyecto']);
								},
							});

				}
			});

			$("#consulta1Adelante").on('click', function(){
				var cdoc = $("#cdoc").val();

				if ("<?php echo $_SESSION['CodigoPosicion']?>" == '36'||"<?php echo $_SESSION['CodigoPosicion']?>" == '46' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'||"<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53'||"<?php echo $_SESSION['CodigoPosicion']?>" == '54'){
				cdoc = parseInt(cdoc)+1;

				consultaOFV(cdoc);
				 }
				 else
				 {

					 var adelanteatras = 'ADELANTE';
					 var usuario = '<?php echo $_SESSION['usuario']?>';
					 		$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaatrasadelanteusuario.php',
								dataType:'json',
								data:{
								id: cdoc,
								adelanteatras: adelanteatras,
								usuario: usuario,
								},
								success: function(response){

										consultaOFV(response['IdProyecto']);
								},
							});

				}
			});

			function consultaOFV(folio) {
				var cdoc = folio;
							if (cdoc >= 1){
								$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaAtrasAdelante.php',
								dataType:'json',
								data:{ cdoc: cdoc },
								success: function(response){

									var estatus = response['Estatus'];
									if (estatus == 'PORAUTORIZARC' || estatus == 'PORAUTORIZARCPROY' || estatus == 'PORAUTORIZARCPROYDOM' || estatus == 'PORAUTORIZARCDOM')
									{
										$("#btnCrear").prop("disabled", false);
										$("#btnCrear").show();
										$("#btnPerdido").prop("disabled", false);
										$("#btnPerdido").show();
										$("#btnAutorizar").prop("disabled", false);
										$("#btnAutorizar").show();
										$("#btnRechazar").prop("disabled", false);
										$("#btnRechazar").show();


										$("#codcliente").val(response['Cliente']).prop("disabled", true);
										$("#NombreC").val(response['NombreCliente']).prop("disabled", true);
										$("#Contacto").val(response['Contacto']).prop("disabled", false);
										$("#Prioridad").val(response['Prioridad']).prop("disabled", false);
										$("#Telefono").val(response['Telefono']).prop("disabled", false);
										$("#Correo").val(response['Email']).prop("disabled", false);
										$("#AsesorV").val(response['AsesorVentas']).prop("disabled", false);
										$("#AdminV").val(response['AdminVentas']).prop("disabled", false);
										$("#cdoc").val(response['IdProyecto']);
										$("#idproyecto").val(response['IdProyecto']).prop("disabled", false);
										$("#vDirectivas").val(response['VentasDirectivas']).prop("disabled", false);
										$("#tpropuesta").val(response['TipoPropuesta']).prop("disabled", false);
										$("#Observaciones").val(response['Observaciones']).prop("disabled", false);
										$("#FechaVenta").val(response['FechaVenta']).prop("disabled", false);
										$("#FechaEntrega").val(response['FechaEntrega']).prop("disabled", false);
										$("#NombreProyecto").val(response['NombreProyecto']).prop("disabled", false);
										$("#Ubicacion").val(response['Ubicacion']).prop("disabled", false);
										$("#TechoFinanciero").val(response['TechoFinanciero']).prop("disabled", false);
										$("#Estatus").val(response['Estatus']).prop("disabled", true);
										$("#ObservacionesProyectos").val(response['ObservacionesProyectos']).prop("disabled", true);
										if ("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'||"<?php echo $_SESSION['CodigoPosicion']?>" == '36'){


											$("#btnAutorizar").prop("disabled", false);
											$("#btnAutorizar").show();
											$("#btnRechazar").prop("disabled", false);
											$("#btnRechazar").show();

											if ($("#btnCrear").prop("disabled") == false)
											{
												$("#btnSolicitar").prop("disabled", true);
												$("#btnSolicitar").hide();
												$("#ObservacionesProyectos").prop("disabled",true);
											}
											else
											{
												$("#ObservacionesProyectos").prop("disabled",false);
											}

										}
										else
										{

											$("#btnAutorizar").prop("disabled", true);
											$("#btnAutorizar").hide();
											$("#btnRechazar").prop("disabled", true);
											$("#btnRechazar").hide();
											$("#ObservacionesProyectos").prop("disabled",true);

										}

									}
									else
									{
										$("#btnCrear").prop("disabled", true);
										$("#btnCrear").hide();
										$("#btnPerdido").prop("disabled", true);
										$("#btnPerdido").hide();
										$("#btnAutorizar").prop("disabled", true);
										$("#btnAutorizar").hide();
										$("#btnRechazar").prop("disabled", true);
										$("#btnRechazar").hide();


										$("#codcliente").val(response['Cliente']).prop("disabled", true);
										$("#NombreC").val(response['NombreCliente']).prop("disabled", true);
										$("#Contacto").val(response['Contacto']).prop("disabled", true);
										$("#Prioridad").val(response['Prioridad']).prop("disabled", true);
										$("#Telefono").val(response['Telefono']).prop("disabled", true);
										$("#Correo").val(response['Email']).prop("disabled", true);
										$("#AsesorV").val(response['AsesorVentas']).prop("disabled", true);
										$("#AdminV").val(response['AdminVentas']).prop("disabled", true);
										$("#cdoc").val(response['IdProyecto']).prop("disabled", true);
										$("#idproyecto").val(response['IdProyecto']).prop("disabled", true);
										$("#vDirectivas").val(response['VentasDirectivas']).prop("disabled", true);
										$("#tpropuesta").val(response['TipoPropuesta']).prop("disabled", true);
										$("#Observaciones").val(response['Observaciones']).prop("disabled", true);
										$("#FechaVenta").val(response['FechaVenta']).prop("disabled", true);
										$("#FechaEntrega").val(response['FechaEntrega']).prop("disabled", true);
										$("#NombreProyecto").val(response['NombreProyecto']).prop("disabled", true);
										$("#Ubicacion").val(response['Ubicacion']).prop("disabled", true);
										$("#TechoFinanciero").val(response['TechoFinanciero']).prop("disabled", true);
										$("#Estatus").val(response['Estatus']).prop("disabled", true);
										$("#ObservacionesProyectos").val(response['ObservacionesProyectos']).prop("disabled", true);
										if ($('#Estatus').val() == 'ANTEPROYECTO'||$('#Estatus').val() == 'CANCELADO'||$('#Estatus').val() == 'RECHAZADO')
										{
												$("#btnSolicitar").prop("disabled", true);
												$("#btnSolicitar").hide();
										}
										else
										{
												$("#btnSolicitar").prop("disabled", false);
												$("#btnSolicitar").show();
										}

										$("#btnAutorizar").prop("disabled", true);
										$("#btnAutorizar").hide();
										$("#btnRechazar").prop("disabled", true);
										$("#btnRechazar").hide();
										$("#ObservacionesProyectos").prop("disabled",true);

									}
									// if ("<?php echo $_SESSION['CodigoPosicion']?>" == '37'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'||"<?php echo $_SESSION['CodigoPosicion']?>" == '50'){

										// //$("#btnCrear").prop("disabled", true);

									// }

									cargarAdjuntos();


									$("#modalBuscarProyectos").modal('hide');
								},
								});

							}


			}

			$("#consultaPrimerRegistro").on('click', function(){
				var usuario = '';
				if ("<?php echo $_SESSION['CodigoPosicion']?>" == '36'||"<?php echo $_SESSION['CodigoPosicion']?>" == '46' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '51' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53'||"<?php echo $_SESSION['CodigoPosicion']?>" == '54'){
					usuario = 'coordinador';
				}
				else
				{
					usuario = '<?php echo $_SESSION['usuario']?>';
				}
				var orden = "ASC"
				$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaPrimerUltimoRegistro.php',
								dataType:'json',
								data:{ orden: orden, usuario: usuario },
								success: function(response){

										consultaOFV(response['IdProyecto']);
								},
				});


			});
			$("#consultaUltimoRegistro").on('click', function consultaUltimoRegistro(){
				var usuario = '';
				if ("<?php echo $_SESSION['CodigoPosicion']?>" == '36'||"<?php echo $_SESSION['CodigoPosicion']?>" == '46' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '51' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53'||"<?php echo $_SESSION['CodigoPosicion']?>" == '54'){
					usuario = 'coordinador';
				}
				else
				{
					usuario = '<?php echo $_SESSION['usuario']?>';
				}
				var orden = "DESC"
				$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaPrimerUltimoRegistro.php',
								dataType:'json',
								data:{ orden: orden, usuario: usuario },
								success: function(response){
										consultaOFV(response['IdProyecto']);
								},
				});
				consultaOFV(cdoc);
			});








			$("#btnPdf").on('click', function(){
				var cdoc = $("#cdoc").val();
				window.open('reportes/pdfOfv.php?folio='+cdoc, "nombre de la ventana", "width=1024, height=325");
			});




		</script>
	</html>
