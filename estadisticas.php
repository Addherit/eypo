	<?php
	
	include "conexion.php";
		session_start();

	$_SESSION['usuario'];

	  $month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;

$FechaInicio = date( "Y-m-d", strtotime( "$today -12 month" ) );
	
	
	?>

	<!DOCTYPE html>
	<html>
		<?php include "header.php"; ?>
		
		
		<body onload="consultaEstadisticas();">
		
		<?php include "nav.php"; ?>		
		<?php include "modales.php"; ?>
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Estadisticas:</h3>
				</div>
				
			</div>
			<div class="row datosEnc" style="font-size: .7rem">
				<div class="col-md-6">
					
			<br>
			
			
<div class="row" id= btnFoot style="margin-bottom: 30px">
				<div class="col-md-12" align="left">
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnProyectistas" title="Crear">Estadisticas Proyectistas</button>
						
						
							
					</div>
				
			</div>
			<div class="row">
									<div class="col-sm-3">
									<label>Fecha Inicio:</label> 
									
										<input type="date" onchange="consultaEstadisticas();" id="FechaInicio" value="<?php echo $FechaInicio; ?>">
									</div>
									<div class="col-sm-3">
									<label>Fecha Fin:</label> 
									
										<input type="date" onchange="consultaEstadisticas();" id="FechaFin" value="<?php echo $today; ?>">
									</div>
			</div>
			
					<div class="row">
					  		<div class="col-md-12" id="contenedordetalleprotegidos">
							<div class="row" align="center">
						<label for="" class="col-sm-12 col-form-label">Estadisticas Totales:</label>
						
						</div>
								<button onclick="exportTableToExcel('detalleestadisticas')">Exportar a Excel</button>
					  			<table class="table table-sm table-bordered table-editable table-hover table-striped table-responsive" width="100%" id="detalleestadisticas">
					        		<thead>
					        			<tr class="encabezado">
											<!--<th><i class="fas fa-ban"></i></th>-->
											<th>Proyectistas</th>
											<th>Proyectos Vendidos</th>
											<th>Proyectos Vigentes</th>
											<th>Proyectos Perdidos</th>
					        				<th>Proyectos Sin Respuesta</th>
					        				<th>Proyectos Totales</th>
					        				<th>Monto Vendido</th>
					        				
											
										</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<!--<th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>-->
											<td contenteditable="false" class="proyectista"></td>
											<td contenteditable="false" class="proyectosvendidos"></td>
											<td contenteditable="false" class="proyectosvigentes"></td>
											<td contenteditable="false" class="proyectosperdidos"></td>
								          	<td contenteditable="false" class="proyectossinrespuesta"></td>
											<td contenteditable="false" class="proyectostotales"></td>
											<td contenteditable="false" class="montovendido"></td>
											
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>

						
					  	<br>
								<div class="row">
					  		<div class="col-md-12" id="contenedordetalleprotegidos">
							<div class="row" align="center">
						<label for="" class="col-sm-12 col-form-label">Proyectos de Interior Con Control:</label>
						
					</div>
								<button onclick="exportTableToExcel('detalleinterior')">Exportar a Excel</button>
					  			<table class="table table-sm table-bordered table-editable table-hover table-striped table-responsive" width="100%" id="detalleinterior">
					        		<thead>
					        			<tr class="encabezado" >
											<!--<th><i class="fas fa-ban"></i></th>-->
											<th>Proyectistas</th>
											<th>Cantidad Proy. Interior</th>
											<th>Proyectos con control</th>
											<th>Proyectos sin control</th>
					        				<th>Observaciones</th>
					        				<th>Monto Proyectado</th>
										</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<!--<th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>-->
											<td contenteditable="false" class="proyectista"></td>
											<td contenteditable="false" class="cantidadinterior"></td>
											<td contenteditable="false" class="cantidadconcontrol"></td>
											<td contenteditable="false" class="cantidadsincontrol"></td>
								          	<td contenteditable="false" class="observaciones"></td>
											<td contenteditable="false" class="montoproyectado"></td>
																					
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>
						
						<br>
						<br>
								<div class="row">
					  		<div class="col-md-12" id="contenedordetalleprotegidos">
							<div class="row" align="center">
						<label for="" class="col-sm-12 col-form-label">Visitas a Clientes VS Proyectos Realizados:</label>
						
					</div>
								<button onclick="exportTableToExcel('detallevisitas')">Exportar a Excel</button>
					  			<table class="table table-sm table-bordered table-editable table-hover table-striped table-responsive" width="100%" id="detallevisitas">
					        		<thead>
					        			<tr class="encabezado" >
											<!--<th><i class="fas fa-ban"></i></th>-->
											<th>Proyectistas</th>
											<th>Cantidad de Visitas Realizadas</th>
											<th>Cantidad de Proyectos</th>
											<th>Cantidad de Visitas que se necesitan</th>
					        			</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<!--<th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>-->
											<td contenteditable="false" class="proyectista"></td>
											<td contenteditable="false" class="cantidadvisitasr"></td>
											<td contenteditable="false" class="proyectosvendidos"></td>
											<td contenteditable="false" class="cantidadvisitasn"></td>
								       																					
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>
						
						<br>
								<div class="row">
					  		<div class="col-md-12" id="contenedordetalleprotegidos">
							<div class="row" align="center">
						<label for="" class="col-sm-12 col-form-label">Tiempos de Respuesta:</label>
						
					</div>
								<button onclick="exportTableToExcel('detalletiempos')">Exportar a Excel</button>
					  			<table class="table table-sm table-bordered table-editable table-hover table-striped table-responsive" width="100%" id="detalletiempos">
					        		<thead>
					        			<tr class="encabezado" >
											<!--<th><i class="fas fa-ban"></i></th>-->
											<th>Proyectistas</th>
											<th>Entregado TYF</th>
											<th>Retrasado + 24 Hrs</th>
										</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<!--<th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>-->
											<td contenteditable="false" class="proyectista"></td>
											<td contenteditable="false" class="entregadotyf"></td>
											<td contenteditable="false" class="retrasado"></td>
																			       																					
					        			</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>
						
		<?php include "footer.php"; ?>
	</body>
		<script>
		
					function postForm(path, params, method) {
    method = method || 'post';

    var form = document.createElement('form');
    form.setAttribute('method', method);
    form.setAttribute('action', path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}
		
			$("#btnCrear").on('click', function(){
				
				var Siglas = $('#siglas').val();
				var TipoPropuesta = $('#tpropuesta').val();
				
			
				$.ajax({
					type:'post',
					url: "ofvConsultasMaster/buscarevision.php",
					data:{
						TipoPropuesta: TipoPropuesta,
						Siglas: Siglas,
					},
					success: function(resp){
						event.preventDefault();
						postForm('masterproyectosvolumetria.php', {valor: resp});
						
					}
				});
			});

$("#btnProyectistas").on('click', function(){
	postForm('estadisticasproyectistas.php');
	
		});
function consultaEstadisticas(){
				 var fechaInicio = document.getElementById("FechaInicio").value;
				 var fechaFin = document.getElementById("FechaFin").value;
							$.ajax({
											url: 'ofvConsultasMaster/consultaEstadisticasTotales.php',
											type: 'post',
											// data: {TipoPropuesta: TipoPropuesta},
											 data: {fechaInicio:fechaInicio, fechaFin:fechaFin},
											success: function(response){
												
												$('#detalleestadisticas tbody').empty();
												$('#detalleestadisticas tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
										$.ajax({
											url: 'ofvConsultasMaster/consultaEstadisticasInteriorControl.php',
											type: 'post',
											// data: {TipoPropuesta: TipoPropuesta},
											data: {fechaInicio:fechaInicio, fechaFin:fechaFin},
											success: function(response){
												
												$('#detalleinterior tbody').empty();
												$('#detalleinterior tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
										$.ajax({
											url: 'ofvConsultasMaster/consultaEstadisticasVisitas.php',
											type: 'post',
											// data: {TipoPropuesta: TipoPropuesta},
											data: {fechaInicio:fechaInicio, fechaFin:fechaFin},
											success: function(response){
												
												$('#detallevisitas tbody').empty();
												$('#detallevisitas tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
										$.ajax({
											url: 'ofvConsultasMaster/consultaEstadisticasTiempos.php',
											type: 'post',
											// data: {TipoPropuesta: TipoPropuesta},
											data: {fechaInicio:fechaInicio, fechaFin:fechaFin},
											success: function(response){
												
												$('#detalletiempos tbody').empty();
												$('#detalletiempos tbody').append(response);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
	
	
}
			
$("#buscadorProyectos").keyup(function(){
				var valorEscrito = $('#buscadorProyectos').val();

				if (valorEscrito) {
					$.ajax({
						url: 'ofvConsultasMaster/buscadorGeneralAnteProyectos.php',
						type: 'post',
						data: {valor: valorEscrito, estatus: '%'},
						success: function(response){
							$('#tblBuscarProyectos tbody').empty();
							$('#tblBuscarProyectos tbody').append(response);
							$("#tblBuscarProyectos tbody tr").on('click',function(){
								var codigo = $(this).find('td').eq(0).text();
								
								$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaAtrasAdelante.php', 
								dataType:'json',
								data:{ cdoc: codigo },
								success: function(response){
									
									$("#siglas").val(response['Siglas']);
									$("#tpropuesta").val(response['TipoPropuesta']);
									
									$("#modalBuscarProyectos").modal('hide');
								},
								});
							});
						},
					});

				}else {
					$('#tblBuscarProyectos tbody').empty();
				}
			});
			
				function exportTableToExcel(tableID, filename = ''){
				var downloadLink;
				var dataType = 'application/vnd.ms-excel';
				var tableSelect = document.getElementById(tableID);
				var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
				
				// Specify file name
				filename = filename?filename+'.xls':'estadisticas.xls';
				
				// Create download link element
				downloadLink = document.createElement("a");
				
				document.body.appendChild(downloadLink);
				
				if(navigator.msSaveOrOpenBlob){
					var blob = new Blob(['\ufeff', tableHTML], {
						type: dataType
					});
					navigator.msSaveOrOpenBlob( blob, filename);
				}else{
					// Create a link to the file
					downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
				
					// Setting the file name
					downloadLink.download = filename;
					
					//triggering the function
					downloadLink.click();
				}
			}
			

			
		
			
		</script>
	</html>
