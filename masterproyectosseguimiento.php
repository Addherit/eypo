	<?php
	
	include "conexion.php";
	session_start();
	$_SESSION['usuario'];
	
	If ($_POST['valor'] != '')
	{
	$_SESSION['valor'] = $_POST['valor'];
	}
	 
	If ($_POST['revision'] != '')
	{
	$_SESSION['revision'] = $_POST['revision'];
	}
	 $folio = $_SESSION['valor'];
	 $rev = $_SESSION['revision'];
		$month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;

$FechaInicio = date( "Y-m-d", strtotime( "$today -6 month" ) );
	?>

	<!DOCTYPE html>
	<html>
	<?php include "header.php"; ?>
		
		
		<body onload="consultaOFV(<?php echo $folio;?>,<?php echo $rev?>);">
		
		<?php include "nav.php"; ?>		
		<?php include "modales.php"; ?>
		
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Seguimiento a Proyectos:</h3>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">
					<?php include "botonesDeControlMaster.php" ?>
				</div>
			</div>
				<div class="row">
		<div class="col-md-6">
					<div class="row">
						<label class="col-sm-3 col-form-label" >Cliente:</label>
						<div class="col-sm-9">
							<input type="text" class="" name="codcliente" id="codcliente" style="width: 70%">
							<a id="btnCliente" href="#" data-toggle="modal" data-target="#myModal">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Nombre del Cliente:</label><br>
						<div class="col-sm-9">
							<input type="text" class="" name="NombreC" id="NombreC" style="width: 70%">
							<a href="#" data-toggle="modal" data-target="#myModal" >
								<i class="fas fa-search" style="color: #57b4ea" title="Búsqueda Nombre"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Persona de Contacto:</label>
						<div class="col-sm-9">
							<input type="text" class="" id="Contacto" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Ubicación del Proyecto:</label>
						<div class="col-sm-9">
							<input type="text" class="" id="Ubicacion" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Celular del Contacto:</label>
						<div class="col-sm-9">
							<input type="text" class="" id="Telefono" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Correo:</label>
						<div class="col-sm-9">
							<input type="email" class="" id="Correo" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Nombre del Proyecto:</label>
						<div class="col-sm-9">
							<input type="text" class="" id="NombreProyecto" style="width: 70%">
						</div>
					</div>
					
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Fecha de Posible Venta:</label>
						<div class="col-sm-9">
							<input type="date" class="" id="FechaVenta" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Fecha de Promesa de Entrega:</label>
						<div class="col-sm-9">
							<input type="date" class="" id="FechaEntrega" style="width: 70%">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Tipo de Propuesta:</label>
						<div class="col-sm-9">
							<select name="" id="tpropuesta" style="height: 23px; width: 70%">
								<option value="" disabled selected>Seleccione</option>
									<?php
								$sql = "SELECT * FROM MasterEypo.dbo.TiposPropuestas where Estatus = 'ALTA'";
								$consulta = sqlsrv_query($conn, $sql);
								while ($row = sqlsrv_fetch_array($consulta)) { ?>
											<option value="<?php echo ($row['TipoPropuesta']); ?>"><?php echo ($row['TipoPropuesta']); ?></option>
									<?php 
							} ?>
							</select>
						</div>
					</div>
					<div class="row">
						    <label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Asesor de Ventas:</label>
						    <div class="col-sm-3">
									<select name="" id="AsesorV" style="width: 70%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT * FROM IV_EY_PV_EmpleadosVentasCompras";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['NombreEmpleadoVC']); ?>"> <?php echo ($row['NombreEmpleadoVC']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
							<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Administrador de Ventas:</label>
						    <div class="col-sm-3">
									<select name="" id="AdminV" style="width: 70%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT * FROM IV_EY_PV_EmpleadosVentasCompras";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['NombreEmpleadoVC']); ?>"> <?php echo ($row['NombreEmpleadoVC']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">N°:</label>
						<div class="col-sm-4">
							<select id="ndoc" style="height: 23px">
								<option value="COT-GRAL">PROYECTO</option>
								<!--<option value="Manual">Manual</option>-->
							</select>
							
							<input type="text" class="" id="cdoc" name="cdoc" size="5" value="<?php echo "$folio"; ?>" readonly="true">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Revisiones:</label>
						 <div class="col-sm-3">
								<select onchange="cambiarevision()" name="" id="Revisiones" style="width: 70%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT distinct CAST(Revision AS INTEGER) as Revision FROM MasterEypo.dbo.ProyectosD where IdProyecto = '".$_SESSION['valor']."' order by Revision";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['Revision']); ?>"> <?php echo ($row['Revision']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
												
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Prioridad:</label>
						 <div class="col-sm-3">
									<select name="" id="Prioridad" style="width: 70%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT * FROM MasterEypo.dbo.Prioridades where Estatus = 'ALTA' order by prioridad";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['Prioridad']); ?>"> <?php echo ($row['Prioridad']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
												
					</div>
					
					<div class="row">
						<label for=""  class="col-sm-4 offset-md-4 col-form-label">Estatus:</label>
						<div class="col-sm-4">
							<input type="text" disabled="true" class="" id="Estatus" style="width: 100%">
						</div>
					</div>
					
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Ventas Directivas*:</label>
						<div class="col-sm-4">
							<input type="text" class="" id="vDirectivas" style="width: 70%">
						</div>
					</div>
					<div class="row">
					<label for="" class="col-sm-4 offset-md-4 col-form-label table-hover table-striped table-responsive table">Archivos:</label>
					</div>
					<table class="col-sm-4 offset-md-4" id="Adjuntos" style="border-spacing:5px; border-collapse: separate;">
						  <tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						  </tr>
						  <tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						  </tr>
						  </table>
					<div class="row">
					<div class="col-md-7 offset-md-4 text-Right" style="margin-top: 10px ">
					
							<!--<form id="Adjuntos" action = "" method = "POST" enctype = "multipart/form-data">-->
								<input type="text" class="" id="idproyecto" name="idproyecto" size="5" value="<?php echo "$folio"; ?>" readonly="true" style="display:none">
								<label for="file-upload" class="custom-file-upload">
								<input type = "file" onchange='cambiar()' name = "file" id="file-upload" class="file-upload"/>
								
								  </label>
								  <button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnAdjuntar" title="Adjuntar">Adjuntar Archivo</button>
								<!--  <input type = "submit" value="Adjuntar Archivo" class="btn"/>-->
								
								
								<div id="info"></div>
								 
								

								 
			
							<!--</form>-->
						</div>
						
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Complejidad:</label>
						 <div class="col-sm-3">
									<select name="" id="Complejidad" style="width: 70%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT * FROM MasterEypo.dbo.Complejidades where Estatus = 'ALTA'  order by Complejidad";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['Complejidad']); ?>"> <?php echo ($row['Complejidad']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
												
					</div>
				
				</div>
				</div>
			<div class="row" style="font-size: .7rem">
			<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Techo Financiero:</label>
						<div class="col-sm-12">
							<textarea name="TechoFinanciero" id = "TechoFinanciero" cols="40" rows="5" style="width: 70%"></textarea>
						</div>
			</div>
			<div class="row" style="font-size: .7rem">
			
			<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Observaciones de Ventas:</label>
						<div class="col-sm-12">
							<textarea name="Observaciones" id = "Observaciones" cols="40" rows="5" style="width: 70%"></textarea>
						</div>
			</div>
			<div class="row" id="ObservacionesP" style="font-size: .7rem">
			<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Observaciones de Proyectos:</label>
						<div class="col-sm-12">
							<textarea  name="ObservacionesProyectos" id = "ObservacionesProyectos" cols="40" rows="5" style="width: 70%"></textarea>
						</div>
			</div>
			<div class="row" style="font-size: .7rem">
			
			<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Observaciones Volumetria:</label>
						<div class="col-sm-12">
							<textarea name="observacionesvolumetria" id = "observacionesvolumetria" cols="40" rows="5" style="width: 70%"></textarea>
						</div>
			</div>
			<div class="row" style="font-size: .7rem">
			<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Observaciones Costos:</label>
						<div class="col-sm-12">
							<textarea name="observacionescostos" id = "observacionescostos" cols="40" rows="5" style="width: 70%"></textarea>
						</div>
			</div>
			<br>
			<br>
			<hr>
				<div class="row datosProy" style="font-size: .7rem">
				<div class="col-md-6">
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Fecha Compromiso*:</label>
						<div class="col-sm-9">
							<input type="date" class="" id="FechaCompromiso" style="width: 70%">
						</div>
					</div>
					<div class="row">
						    <label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Proyectista:</label>
						    <div class="col-sm-3">
									<select name="" id="Proyectista" style="width: 70%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT iv.NombreEmpleadoVC+ ' ' + isnull(firstName,'') + ' ' + isnull(middleName,'') + ' ' + isnull(lastName,'') as Nombre, iv.* FROM IV_EY_PV_EmpleadosVentasCompras iv
left outer join OHEM e on iv.CodigoEmpleadoVC   = e.salesPrson where iv.NombreEmpleadoVC like 'P%'";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['NombreEmpleadoVC']); ?>"> <?php echo ($row['Nombre']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
							<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Coordinador:</label>
						    <div class="col-sm-3">
									<select name="" id="Coordinador" style="width: 70%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT iv.NombreEmpleadoVC+ ' ' + isnull(firstName,'') + ' ' + isnull(middleName,'') + ' ' + isnull(lastName,'') as Nombre, iv.* FROM IV_EY_PV_EmpleadosVentasCompras iv
left outer join OHEM e on iv.CodigoEmpleadoVC   = e.salesPrson where iv.NombreEmpleadoVC like 'P%'";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['NombreEmpleadoVC']); ?>"> <?php echo ($row['Nombre']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
							<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Proyectista Domótica:</label>
						    <div class="col-sm-3">
									<select name="" id="ProyectistaDom" style="width: 70%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT iv.NombreEmpleadoVC+ ' ' + isnull(firstName,'') + ' ' + isnull(middleName,'') + ' ' + isnull(lastName,'') as Nombre, iv.* FROM IV_EY_PV_EmpleadosVentasCompras iv
left outer join OHEM e on iv.CodigoEmpleadoVC   = e.salesPrson where iv.NombreEmpleadoVC like 'P%'";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['NombreEmpleadoVC']); ?>"> <?php echo ($row['Nombre']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
					</div>
					
					
					
						
					
				</div>
				<div class="col-md-6">
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Tipo de Proyecto*:</label>
						<div class="col-sm-4">
							<select name="" id="TipoProyecto" style="height: 23px; width: 70%">
								<option value="" disabled selected>Seleccione</option>
									<?php
								$sql = "SELECT * FROM MasterEypo.dbo.TiposProyectos where Estatus = 'ALTA'";
								$consulta = sqlsrv_query($conn, $sql);
								while ($row = sqlsrv_fetch_array($consulta)) { ?>
											<option value="<?php echo ($row['Tipo']); ?>"><?php echo ($row['Tipo']); ?></option>
									<?php 
							} ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label" >Siglas*:</label>
						<div class="col-sm-4">
							<input type="text" class="" id="Siglas" style="width: 70%; font-weight: bold;">
						</div>
					</div>
					<div class="row">
						
					</div>
					
					<div class="row">
						
						<label for="" class="col-sm-4 offset-md-4 col-form-label">No. de Proyecto*:</label>
						<div class="col-sm-4">
						
							<input type="text" class="" id="NoProyecto" style="width: 70%; font-weight: bold;">
						</div>
						<input type="text" class="" id="Revision" style="width: 70%; font-weight: bold; display:none;">
						<input type="text" class="" id="Revisionmax" style="width: 70%; font-weight: bold; display:none;">
					</div>
					<div class="row">
						<label for="" class="col-sm-4 offset-md-4 col-form-label">Tipo de Cambio del dia:</label>
						<div class="col-sm-4">
							<input type="text" class="" id="tipocambio" style="width: 70%; font-weight: bold;">
						</div>
						<input type="text" class="" id="tipocambio" style="width: 70%; font-weight: bold; display:none;">
						<input type="text" class="" id="nombreproyectista" style="width: 70%; font-weight: bold; display:none;">
					</div>
							
				</div>
					
				
				</div>
			
			<br>
			
			<div class="row" style="font-size: .7rem">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Codigos</a>
						</li>
					
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
							<?php include "tablaNavMaster/generalseguimiento.php"; ?>
						</div>
						
						
							<br>
							
							
				</div>
							
				</div>
							
			</div>
			<div class="col-md-6 text-right" style="font-size: .7rem">
						<div class="row">
						    <label for="" class="col-sm-4 offset-md-4 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Subtotal:</label>
						    <div class="col-sm-4">

						    <input type="text" class="monto" id="subtotal" style="width: 100%" >
							<input type="text" class="monto" id="subtotalc" style="width: 100%; display:none" >
						    </div>
						</div>
						<div class="row">
						    <label for="" class="col-sm-4 offset-md-4 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Impuestos:</label>
						    <div class="col-sm-4">
						    	<input type="text" name="" id="impuestos" value="" style="width: 100% ">
						    </div>
						</div>
						<div class="row">
						    <label for="" class="col-sm-4 offset-md-4 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Total:</label>
						    <div class="col-sm-4">
						    	<input type="text" id="total" name="" value=""style="width: 100%">
						    </div>
						</div>
						<!--<div class="row">
						    <label for="" class="col-sm-4 offset-md-4 col-form-label" style="padding-right: 0px; padding-bottom:  0px">LimiteAutorizacion:</label>
						    <div class="col-sm-4">
						    	<input type="text" id="limiteautorizacion" name="" value=""style="width: 100%">
						    </div>
						</div>-->
						
			</div>
				<div class="row" style="font-size: .7rem">
			
						<div class="col-md-6" align="left">
						<!--<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnNuevaLlamada" title="Nueva Llamada">Nueva Llamada</button>-->
						
						<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Comentarios Llamada:</label>
						<div class="col-sm-12">
							<textarea name="observacionesllamada" id = "observacionesllamada" cols="40" rows="3" style="width: 100%"></textarea>
						</div>
						<div class="col-sm-6">
									<select name="" id="tiposeguimientollamada" style="width: 100%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT * FROM MasterEypo.dbo.TiposSeguimientos where Estatus = 'ALTA'";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['TipoSeguimiento']); ?>"> <?php echo ($row['TipoSeguimiento']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
						<br>
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnAgregarLlamada" title="Agregar">Agregar Llamada</button>
					</div>
					
					<br/>
					<br/>
					<div class="col-md-6" align="left">
						<!--<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnNuevaVisita" title="Nueva Visita">Nueva Visita</button>-->
						
						<label for="" class="col-md-12 offset-md-12 col-form-label text-left">Comentarios Visita:</label>
						<div class="col-sm-12">
							<textarea name="observacionesvisita" id = "observacionesvisita" cols="40" rows="3" style="width: 100%"></textarea>
						</div>
						 <div class="col-sm-6">
									<select name="" id="tiposeguimientovisita" style="width: 100%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT * FROM MasterEypo.dbo.TiposSeguimientos where Estatus = 'ALTA'";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo ($row['TipoSeguimiento']); ?>"> <?php echo ($row['TipoSeguimiento']); ?> </option>
												<?php } ?>

						    	</select>
						    </div>
						<br>
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnAgregarVisita" title="Agregar">Agregar Visita</button>
					</div>
			</div>
					
					
					
							
							<br>
					<div class="row" style="font-size: .7rem">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Seguimientos</a>
						</li>
					
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
							<?php include "tablaNavMaster/generalvisitasllamadas.php"; ?>
						</div>
						
						
							<br>
							
							
				</div>
							
				</div>
							
			</div>
					<div class="row" id= btnFoot style="margin-bottom: 30px">
					
					<div class="col-md-12" align="center">
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnCotizacion" title="COPIAR A COTIZACION">COPIAR A COTIZACION</button>
						
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnGuardar" title="TomarProyecto">GUARDAR</button>
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnPerdido" title="CANCELAR" href="#" data-toggle="modal" data-target="#myModalPerdido">CANCELAR</button>
						
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnConcluir" title="CONCLUIR PROYECTO">CONCLUIR PROYECTO</button>
						<!-- <button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnPerdido" title="Proyecto Perdido" href="#" data-toggle="modal" data-target="#myModalPerdido">Proyecto Perdido</button>-->
						
							
					</div>
				
			</div>
						
				</div>
					
		
			
		<?php include "footer.php"; ?>
	</body>
		<script>
	function cambiarevision(){
				var cdoc = $("#cdoc").val();
				var revision = $("#Revisiones").val();
					postForm('masterproyectosseguimiento.php', {valor: cdoc, revision: revision});
					
				
			}


		    $("#Siglas").keyup(function(){
				var code = $('#Siglas').val();
				var TipoPropuesta = $('#tpropuesta').val();
							
										$.ajax({
											url: 'ofvConsultasMaster/buscacodigosiglas.php',
											type: 'post',
											data: {TipoPropuesta: TipoPropuesta},
											success: function(response){
												
												 var siglas = response;
												 
												 var revision = 'R0';
												
												 $("#NoProyecto").val(code+'-'+siglas+'-'+revision).prop("disabled", true);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
			})
			$("#buscadorCliente").keyup(function(){
				var valorEscrito = $("#buscadorCliente").val();
				if (valorEscrito) {
					$.ajax({
						url: 'socios/buscadorConsultaClientesYN.php',
						type: 'post',
						data: {valor: valorEscrito},
						success: function(resp){

							
							$("#tblcliente tbody").empty();
							$("#tblcliente tbody").append(resp);

							$("#tblcliente tr").on('click',function(){


								if (!$('#BackOrderVentas').is(':visible')) { 
									if (!$('#OfertaVentaCliente').is(':visible')) { 
										if (!$('#OfertaClienteF').is(':visible')) { 
											var codigo = $(this).find('td').eq(1).text();
											var name = $(this).find('td').eq(2).text();							 
											agregar(codigo, name);
											$("#tblcliente tbody").empty();
											$("#buscadorCliente").val("");

											$.ajax({
												url:'rellenarSelectFormaPago.php',
												type:'POST',
												data:{codigo:codigo},

											}).done(function (response) {
												$("#formaPagoSelect").append(response);								
											});										 
										} else {
											var codigo = $(this).find('td').eq(1).text();
											$("#codigoClienteF").val(codigo);	
											$("#myModal").hide();
										}
									} else {
										var codigo = $(this).find('td').eq(1).text();
										$("#clienteProveedor").val(codigo);	
										$("#myModal").hide();
									}
								} else {
									var codigo = $(this).find('td').eq(1).text();
									var name = $(this).find('td').eq(2).text();	
									$("#nombreCliente").val(name);
									$("#codigoCliente").val(codigo);
									$("#myModal").hide();
								}																								
							});
						},
					});
				} else {
					$("#tblcliente tbody").empty();
				}
			});

			$("#myModal").on('click',function() {
				$("#buscadorCliente").val("");
			});
			
			$("#detallenoprotegidos").keydown(function(event) {
				// alert ($(this).closest('tr').next().length);
				if(event.which == 9 && !$(this).closest('tr').next().length ) {
					if ($("#detallenoprotegidos tbody tr:last td:eq(1)").text() != ''){
   $('#detallenoprotegidos tr:last').clone().appendTo('#detallenoprotegidos');
										for (let index = 0; index <= 15; index++) {
											$("#detallenoprotegidos tbody tr:last td:eq("+index+")").empty();
										}
}
				}
				

});
			
			function marcaeditable(){
				
				if ($("#detallenoprotegidos tbody tr:last td:eq(1)").text() != ''){
					$('#detallenoprotegidos tr:last').clone().appendTo('#detallenoprotegidos');
										for (let index = 0; index <= 15; index++) {
											$("#detallenoprotegidos tbody tr:last td:eq("+index+")").empty();
										}
									}	
			}
			String.prototype.replaceAt=function(index, char) {
				var a = this.split("");
				a[index] = char;
				return a.join("");
			}

			$("#buscadorCodigo").keypress(function(e){
				
				var code = (e.keyCode ? e.keyCode : e.which);
				if (code == 13) {
					
					var valorEscrito = $("#buscadorCodigo").val();
					if (valorEscrito) {
						$.ajax({
							url: 'OFVConsultasMaster/buscadorConsultaCodigos.php',
							type: 'post',
							data: {valor: valorEscrito},
							success: function(response){
								
								$("#tblcodigo tbody").empty();
								$("#tblcodigo tbody").append(response);

								$("#tblcodigo tbody tr").on('click',function() {
									var Id = $(this).find('td').eq(1).text();
									var Marca = $(this).find('td').eq(2).text();
									var Modelo = $(this).find('td').eq(3).text();
									var CodigoReal = $(this).find('td').eq(4).text();
									var siglas = $('#Siglas').val();
									
									var CodigoProtegido = $(this).find('td').eq(5).text();
									alert(CodigoProtegido);
									CodigoProtegido = CodigoProtegido.replaceAt(4, siglas);
									CodigoProtegido = CodigoProtegido.replaceAt(8, '-');
									CodigoProtegido = CodigoProtegido.slice(0,9)+CodigoProtegido.slice(12,20);
								
									
									var CodigoBase = $(this).find('td').eq(5).text();
									var Descripcion = $(this).find('td').eq(6).text();
									var Tipo = $(this).find('td').eq(7).text();
									var Potencia = $(this).find('td').eq(8).text();
									
									
									$("#buscadorCodigo").val("");
									// $("#detalleprotegidos tbody tr:last td:eq(1)").text(Id);
									$("#detalleprotegidos tbody tr:last td:eq(1)").text(Marca);
									$("#detalleprotegidos tbody tr:last td:eq(2)").text(Modelo);
									$("#detalleprotegidos tbody tr:last td:eq(3)").text(CodigoReal);
									$("#detalleprotegidos tbody tr:last td:eq(4)").text(CodigoProtegido);
									$("#detalleprotegidos tbody tr:last td:eq(5)").text(Descripcion);
									$("#detalleprotegidos tbody tr:last td:eq(6)").text(Tipo);
									$("#detalleprotegidos tbody tr:last td:eq(7)").text(Potencia);
									$("#detalleprotegidos tbody tr:last td:eq(8)").text(1);
									$("#detalleprotegidos tbody tr:last td:eq(9)").text(CodigoBase);
									$('#detalleprotegidos tr:last').clone().appendTo('#detalleprotegidos');
										for (let index = 0; index <= 15; index++) {
											$("#detalleprotegidos tbody tr:last td:eq("+index+")").empty();
										}
										$("#tblcodigo tbody").empty();
				
									$("#myModalCodigos").modal('hide');
								});
							},
						});
					} else {
						$("#tblcodigo tbody").empty();
						$("#buscadorCodigo").val("");
					}
				}
			});

			$("#myModalCodigos").on('click',function() {
				$("#buscadorCodigo").val("");
				$("#tblcodigo tbody").empty();
			});
			
			
			function Siglas(){
				var code = $("#Siglas").val;
				var TipoPropuesta = $("#TipoPropuesta").val;
				
				$.ajax({
					url: 'ofvConsultasMaster/buscacodigosiglas.php',
					type: 'post',
					data: {TipoPropuesta: TipoPropuesta},
					success: function(response){
						
						 var siglas = response['CodigoSiglas'];
						 var revision = 'R0';
					
						 $("#NoProyecto").val(code+'-'+siglas+'-'+revision).prop("disabled", true);
						 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
					}
				});
			}

			
			function agregar ($code, $name){
				var code = $code;
				var name = $name;
				$("#codcliente").val(code);
				$("#NombreC").val(name);
				$('#myModal').modal('hide');
				$.ajax({
					url: 'consultaPersonaContacto.php',
					type: 'post',
					data: {code: code},
					success: function(response){
						$("#listcontactos").append(response);
					}
				});
			}

			function agregarCodigoATabla (codes, articulo, precio, almacen){

				var tasa = 0.16;
				var impuestoUnitario = tasa * precio;
				var descuento = 0;

				$("#detalleoferta tbody tr").on('keyup', function() {

					var cantidad = parseInt($(this).find('td').eq(3).text());
					var precioUnitario = parseFloat($(this).find('td').eq(4).text());
					var descuento = parseFloat($(this).find('td').eq(5).text());
					var precioPorArticulo = parseFloat((cantidad * precioUnitario) - descuento);
					var iva = precioPorArticulo * tasa;

					$(this).find('td').eq(7).text(iva);
					$(this).find('td').eq(8).text(precioPorArticulo);
					calcularTotalAntesDescuento();
					calcularImpuestoTotal();
					sumarTotalDocumento();
				});

				$("#detalleoferta tbody tr:last td:eq(1)").text(codes);
				$("#detalleoferta tbody tr:last td:eq(2)").text(articulo);
				$("#detalleoferta tbody tr:last td:eq(3)").text(1);
				$("#detalleoferta tbody tr:last td:eq(4)").text(parseFloat(precio));
				$("#detalleoferta tbody tr:last td:eq(5)").text(descuento);
				$("#detalleoferta tbody tr:last td:eq(6)").text(tasa);
				$("#detalleoferta tbody tr:last td:eq(7)").text(impuestoUnitario);
				$("#detalleoferta tbody tr:last td:eq(8)").text(parseFloat(precio));
				$("#detalleoferta tbody tr:last td:eq(9)").text(almacen);

				calcularTotalAntesDescuento();
				calcularImpuestoTotal();
				sumarTotalDocumento();
				crearNuevaFilaTablaDetalleOferta();
			}

			function crearNuevaFilaTablaDetalleOferta(){

				$('#detalleoferta tr:last').clone().appendTo('#detalleoferta');
				for (let index = 0; index <= 15; index++) {
					$("#detalleoferta tbody tr:last td:eq("+index+")").empty();
				}
				$("#tblarticulo tbody").empty();
				$('#myModalCodigos').modal('hide');
				conDetalleOferta();
			}

			// Funciones Operaciones Globales
			function calculartotal() {
				
				var subtotal = 0
				
				$('#detalleprotegidos tr').each(function() {
					
					var cantidad = parseFloat($(this).find('td').eq(10).text());
					var precioventa = parseFloat($(this).find('td').eq(13).text());
					var monedaventa = $(this).find('td:eq(15) option:selected').text();

					
					//array_monedaventa.push($('option:selected',this).text());
					
					var tipocambio = parseFloat($('#tipocambio').val());
					
					
					if (cantidad >=0)
					{
						if (monedaventa == 'USD')
						{
							if (tipocambio >=0)
							{
								var precioventapesos = precioventa * tipocambio;
								subtotal += (cantidad * precioventapesos);
							}
							else
							{
								var precioventapesos = precioventa * 0;
								subtotal += (cantidad * precioventapesos);
							}
							
						}
						else
						{
							subtotal += (cantidad * precioventa);
						}
						
					}
					
					
					
				}); 
				$('#detallenoprotegidos tr').each(function() {
					
					var cantidad = parseFloat($(this).find('td').eq(9).text());
					var precioventa = parseFloat($(this).find('td').eq(12).text());
					var monedaventa = $(this).find('td:eq(14) option:selected').text();

					
					//array_monedaventa.push($('option:selected',this).text());
					
					var tipocambio = parseFloat($('#tipocambio').val());
					
					
					if (cantidad >=0)
					{
						if (monedaventa == 'USD')
						{
							if (tipocambio >=0)
							{
								var precioventapesos = precioventa * tipocambio;
								subtotal += (cantidad * precioventapesos);
							}
							else
							{
								var precioventapesos = precioventa * 0;
								subtotal += (cantidad * precioventapesos);
							}
							
						}
						else
						{
							subtotal += (cantidad * precioventa);
						}
						
					}
					
					
					
				}); 
			    
				 $("#subtotal").val('$' +subtotal.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')).prop("disabled", true);
				 $("#subtotalc").val(subtotal.toFixed(2)).prop("disabled", true);
				 $("#impuestos").val('$' +(subtotal*.16).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')).prop("disabled", true);
				 $("#total").val('$' +((subtotal*.16)+subtotal).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')).prop("disabled", true);
				 
			}

			
			
			
			function cargarAdjuntos (){
				
				var valor = $('#cdoc').val();
				
				$.ajax({
					url: 'OFVConsultasMaster/buscadorArchivosAdjuntossineliminar.php',
					type: 'post',
					data: {valor: valor},
					success: function(response){
						$("#Adjuntos tbody").empty();
						$("#Adjuntos tbody").append(response);
                     }
				});
				
				
					
			}
			 if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
		
			$("#btnAdjuntar").on('click', function(){
						var file_data = $('#file-upload').prop('files')[0];
						var idproyecto = $('#cdoc').val();
						var form_data = new FormData();                  
						form_data.append('file', file_data);
						form_data.append('idproyecto', idproyecto);
						
						$.ajax({
						url: 'OFVConsultasMaster/masteradjuntararchivo.php', // point to server-side PHP script 
						dataType: 'text',  // what to expect back from the PHP script, if anything
						cache: false,
						contentType: false,
						processData: false,
						data: form_data,                         
						type: 'post',
						success: function(php_script_response){
								$("#file-upload").val('');
								$.ajax({
									url: 'OFVConsultasMaster/buscadorArchivosAdjuntossineliminar.php',
									type: 'post',
									data: {valor: idproyecto},
									success: function(response){
										$("#Adjuntos tbody").empty();
										$("#Adjuntos tbody").append(response);
										 window.history.replaceState( null, null, window.location.href );
									 }
								});
							}
						 });
			});
			$(document).on('click', '#eliminarArchivo', function (event) {
				event.preventDefault();
				var id =  $(this).attr("alt");
				var idproyecto = $('#cdoc').val();
				$.ajax({
						url: 'OFVConsultasMaster/mastereliminaarchivo.php', // point to server-side PHP script 
						data: {valor: id},
						type: 'post',
						success: function(response){
								$("#file-upload").val('');
								$.ajax({
									url: 'OFVConsultasMaster/buscadorArchivosAdjuntossineliminar.php',
									type: 'post',
									data: {valor: idproyecto},
									success: function(response){
										alert(id);
										$("#Adjuntos tbody").empty();
										$("#Adjuntos tbody").append(response);
										 window.history.replaceState( null, null, window.location.href );
									 }
								});
							}
						 });
				
			});
			
			
        $("#detalleprotegidos tbody").on("keypress", "td", function (event) {
				
		         if (event.keyCode == 13) {
					  
					  if ($(this).attr('class')=="costo")
					  {
						  
						  event.preventDefault();
						$(this).closest('td').next().next().focus();
					  }
					  else if  ($(this).attr('class')=="precioventa")
					  {
						  
						  event.preventDefault();
						$(this).closest('td').next().next().focus();
					  }
					  else
					  {
						event.preventDefault();
						$(this).closest('td').next().focus();
					  }
                }
            
        });
		$("#detallenoprotegidos tbody").on("keypress", "td", function (event) {
		          if (event.keyCode == 13) {
					 if ($(this).attr('class')=="coston")
					  {
						  
						  event.preventDefault();
						$(this).closest('td').next().next().focus();
					  }
					  else if  ($(this).attr('class')=="precioventan")
					  {
						  
						  event.preventDefault();
						$(this).closest('td').next().next().focus();
					  }
					  else
					  {
						event.preventDefault();
						$(this).closest('td').next().focus();
					  }
                    
                }
            
        });
		$("#detalleprotegidos tbody").on('keyup', "tr", function() {
					
					var costo = parseFloat($(this).find('td').eq(11).text());
					var utilidad = (parseFloat($(this).find('td').eq(13).text())/100);
					var coeficiente = (parseFloat($(this).find('td').eq(14).text())/100);
					var precioventa = costo+(costo * utilidad);
				precioventa = precioventa+(precioventa*coeficiente);
					$(this).find('td').eq(16).text(precioventa);
					calculartotal();
					
		});
		$("#detallenoprotegidos tbody").on('keyup', "tr", function() {
					
					var costo = parseFloat($(this).find('td').eq(10).text());
					var utilidad = (parseFloat($(this).find('td').eq(12).text())/100);
					var coeficiente = (parseFloat($(this).find('td').eq(13).text())/100);
					var precioventa = costo+(costo * utilidad);
				precioventa = precioventa+(precioventa*coeficiente);
					$(this).find('td').eq(15).text(precioventa);
					calculartotal();
		});
    	$("#btnAgregarLlamada").on('click', function(){
				
				var idProyecto = $('#cdoc').val();
				var tiposeguimiento = $('#tiposeguimientollamada option:selected').text();
				var comentarios = $('#observacionesllamada').val();
				var tipo = "Llamada";
				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";
				var estatus = "ALTA";
				
			
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterinsertaseguimiento.php",
					data:{
						idProyecto: idProyecto,
						tipo: tipo,
						comentarios: comentarios,
						tiposeguimiento: tiposeguimiento,
						usuariocreacion: usuariocreacion,
						estatus: estatus,
					
					},
					success: function(resp){
						alert('Llamada registrada exitosamente');
						$.ajax({
										type:'post',
										url:'OFVConsultasMaster/buscadorseguimientos.php',
										data: {valor: idProyecto},
										success: function(res){
											
											$("#detalleseguimiento tbody").empty();
											$("#detalleseguimiento tbody").append(res);
											
										}
									});
					}
					
				});
			});
				$("#btnAgregarVisita").on('click', function(){
				
				var idProyecto = $('#cdoc').val();
				var tiposeguimiento = $('#tiposeguimientovisita option:selected').text();
				var comentarios = $('#observacionesvisita').val();
				var tipo = "Visita";
				var usuariocreacion = "<?php echo $_SESSION['usuario']?>";
				var estatus = "ALTA";
				
			
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterinsertaseguimiento.php",
					data:{
						idProyecto: idProyecto,
						tipo: tipo,
						comentarios: comentarios,
						tiposeguimiento: tiposeguimiento,
						usuariocreacion: usuariocreacion,
						estatus: estatus,
					
					},
					success: function(resp){
						alert('Visita registrada exitosamente');
						$.ajax({
										type:'post',
										url:'OFVConsultasMaster/buscadorseguimientos.php',
										data: {valor: idProyecto},
										success: function(res){
											
											$("#detalleseguimiento tbody").empty();
											$("#detalleseguimiento tbody").append(res);
											
										}
					});
					}
					
				});
			});
			$("#btnActualizaPrecioVenta").on('click', function(){
				alert("Se actualizaron los precios de venta correctamente");
				var IdProyecto = $('#cdoc').val();
				var array_id = [];
				var array_precioventa = [];
				var array_idn = [];
				var array_precioventan = [];
				$('.id').each(function(){
								array_id.push($(this).text());
				});
				$('.precioventa').each(function(){ // este es el .16
								array_precioventa.push($(this).text());
				});
				$('.idn').each(function(){
								array_idn.push($(this).text());
				});
				$('.precioventan').each(function(){ // este es el .16
								array_precioventan.push($(this).text());
				});
				$.ajax({
								type:'post',
								url: "OFVConsultasMaster/masterDetalleActualizaPrecioVenta.php",
								data:{
									IdProyecto: IdProyecto,
									IdProyectoD: array_id,
									precioventa: array_precioventa,
									
								},
								success: function(resp){
								setTimeout('document.location.reload()',1000);
									
								}
				});
				$.ajax({
								type:'post',
								url: "OFVConsultasMaster/masterDetalleActualizaPrecioVenta.php",
								data:{
									IdProyecto: IdProyecto,
									IdProyectoD: array_idn,
									precioventa: array_precioventan,
								},
								success: function(resp){
									setTimeout('document.location.reload()',1000);
								}
							});
				
			})
			$("#btnLiberarPrecios").on('click', function(){
				
				var Id = $('#cdoc').val();
				var observacionescostos = $('#observacionescostos').val();
				var estatus = 'PRECIOSPARCIALES';
						
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterActualizaCostos.php",
					data:{
						
						Id: Id,
						observacionescostos: observacionescostos,
						estatus: estatus,
						
					},
					success: function(resp){
					
						alert('Se guardo correctamente el proyecto');
						
						
							var IdProyecto = $('#cdoc').val();
							var array_id = [];
							var array_costo = [];
							var array_monedacosto = [];
							var array_putilidad = [];
							var array_coeficiente = [];
							var array_tiempoentrega = [];
							var array_precioventa = [];
							var array_monedaventa = [];
							var array_liberado = [];
							var array_idn = [];
							var array_coston = [];
							var array_monedacoston = [];
							var array_putilidadn = [];
							var array_coeficienten = [];
							var array_tiempoentregan = [];
							var array_precioventan = [];
							var array_monedaventan = [];
							var array_liberadon = [];
						
							$('.id').each(function(){
								array_id.push($(this).text());
							});
							$('.costo').each(function(){
								array_costo.push($(this).text());
							});
							$('.monedacosto').each(function(){
								array_monedacosto.push($('option:selected',this).text());
							});
							$('.putilidad').each(function(){
								array_putilidad.push($(this).text());
							});
							$('.coeficiente').each(function(){
								array_coeficiente.push($(this).text());
							});
							$('.tiempoentrega').each(function(){
								array_tiempoentrega.push($(this).text());
							});
							$('.precioventa').each(function(){ // este es el .16
								array_precioventa.push($(this).text());
							});
							$('.monedaventa').each(function(){    // esto es el hide
								array_monedaventa.push($('option:selected',this).text());
							});
							$('.liberado').each(function(){
								array_liberado.push($(':checked', this).val());
							
							});
							
							
							
							$('.idn').each(function(){
								array_idn.push($(this).text());
							});
							$('.coston').each(function(){
								array_coston.push($(this).text());
							});
							$('.monedacoston').each(function(){
								array_monedacoston.push($('option:selected',this).text());
							});
							$('.putilidadn').each(function(){
								array_putilidadn.push($(this).text());
							});
							$('.coeficienten').each(function(){
								array_coeficienten.push($(this).text());
							});
							$('.tiempoentregan').each(function(){
								array_tiempoentregan.push($(this).text());
							});
							$('.precioventan').each(function(){ // este es el .16
								array_precioventan.push($(this).text());
							});
							$('.monedaventan').each(function(){    // esto es el hide
								array_monedaventan.push($('option:selected',this).text());
							});
							$('.liberadon').each(function(){
								array_liberadon.push($(':checked', this).val());
							});
							
							$.ajax({
								type:'post',
								url: "OFVConsultasMaster/masterDetalleActualizaCostos.php",
								data:{
									IdProyecto: IdProyecto,
									IdProyectoD: array_id,
									costo: array_costo,
									monedacosto: array_monedacosto,
									utilidad: array_putilidad,
									coeficiente: array_coeficiente,
									precioventa: array_precioventa,
									monedaventa: array_monedaventa,
									tiempoentrega: array_tiempoentrega,
									liberado: array_liberado,
									
								},
								success: function(resp){
								setTimeout('document.location.reload()',1000);
									
								}
							});
							$.ajax({
								type:'post',
								url: "OFVConsultasMaster/masterDetalleActualizaCostos.php",
								data:{
									IdProyecto: IdProyecto,
									IdProyectoD: array_idn,
									costo: array_coston,
									monedacosto: array_monedacoston,
									utilidad: array_putilidadn,
									coeficiente: array_coeficienten,
									precioventa: array_precioventan,
									monedaventa: array_monedaventan,
									tiempoentrega: array_tiempoentregan,
									liberado: array_liberadon,
								},
								success: function(resp){
									setTimeout('document.location.reload()',1000);
								}
							});
						
					}
				});
			});
			
			$("#btnAutorizar").on('click', function(){
				
				var Id = $('#cdoc').val();
				var observacionescostos = $('#observacionescostos').val();
				var estatus = 'TERMINADO';
				
				
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterActualizaCostos.php",
					data:{
						
						Id: Id,
						observacionescostos: observacionescostos,
						estatus: estatus,
						
					},
					success: function(resp){
					    
							alert('El proyecto se guardo correctamente y cambio a estatus TERMINADO');
						
						
						
						
					}
				});
			});
			$("#btnRechazar").on('click', function(){
				
				var Id = $('#cdoc').val();
				var observacionescostos = $('#observacionescostos').val();
				var estatus = 'RECHAZADOVENTAS';
				
				
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterActualizaCostos.php",
					data:{
						
						Id: Id,
						observacionescostos: observacionescostos,
						estatus: estatus,
						
					},
					success: function(resp){
					    
							alert('El proyecto ha sido RECHAZADO correctamente');
						
						
						
						
					}
				});
			});
			
			$("#btnConcluir").on('click', function(){
				
				var Id = $('#cdoc').val();
				var observacionescostos = $('#observacionescostos').val();
				var estatus = 'CONCLUIDO';								
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterActualizaCostos.php",
					data: {						
						Id: Id,
						observacionescostos: observacionescostos,
						estatus: estatus,						
					},
					success: function(resp){					    
						alert('El proyecto ha sido CONCLUIDO exitosamente');																		
					}
				});
			});
			
			$("#btnPerder").on('click', function(){
				
				var Id = $('#cdoc').val();
				var observacionescostos = $('#observacionesp').val();
				var estatus = 'PERDIDO';								
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterActualizaCostos.php",
					data: {						
						Id: Id,
						observacionescostos: observacionescostos,
						estatus: estatus,						
					},
					success: function(resp){
					    
							alert('El proyecto ha sido almacenado como PERDIDO');
						
						$("#MyModalPerdido").modal('hide');
					postForm('seguimiento.php');
						
						
					}
					
				});
			});
			
		
			
			$("#btnOferta").on('click', function(){
				
				var Id = $('#cdoc').val();
				var observacionescostos = $('#observacionescostos').val();
				var estatus = 'SAP';
				
				
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterActualizaCostos.php",
					data:{
						
						Id: Id,
						observacionescostos: observacionescostos,
						estatus: estatus,
						
					},
					success: function(resp){
					    
							alert('El proyecto ha sido enviado a sincronizacion con SAP');
						
						
						
						
					}
				});
			});
			
			$("#btnCotizacion").on('click', function(){
				
				var IdProyecto = $('#cdoc').val();
				var rev = $('#Revisiones').val();
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterinsertacotizacion.php",
					data:{
						
						IdProyecto: IdProyecto,
						Revision: rev,
						
					},
					success: function(resp){								
						alert('Se ha generado la cotización '+resp+' correctamente.');						
						postForm('seguimiento.php');												
					}
				});
			});

			$("#btnCodigoProtegido").on('click', function(){

				event.preventDefault();
				  
				  
				  var valorEscrito=$('#Siglas').val();
				  
				  postForm('opcioncodigos.php', {valor: valorEscrito});
			});
			function postForm(path, params, method) {
				method = method || 'post';

				var form = document.createElement('form');
				form.setAttribute('method', method);
				form.setAttribute('action', path);

				for (var key in params) {
					if (params.hasOwnProperty(key)) {
						var hiddenField = document.createElement('input');
						hiddenField.setAttribute('type', 'hidden');
						hiddenField.setAttribute('name', key);
						hiddenField.setAttribute('value', params[key]);

						form.appendChild(hiddenField);
					}
				}

				document.body.appendChild(form);
				form.submit();
			}
			function seleccionarMoneda(elemento) {
				var combo =document.getElementById("tmoneda");
				var cantidad = combo.length;
				for (i = 0; i < cantidad; i++) {
					if (combo[i].value == elemento) {
						combo[i].selected = true;
					}
				}
			}

			$("#buscadorProyectos").keyup(function(){
				var  fechaInicio = document.getElementById("FechaInicio").value;
				 var fechaFin = document.getElementById("FechaFin").value;
				var valorEscrito = $('#buscadorProyectos').val();
				
				if (valorEscrito) {
					
					$.ajax({
						url: 'ofvConsultasMaster/buscadorGeneralAnteProyectos.php',
						type: 'post',
						data: {valor:valorEscrito, fechainicio:fechaInicio, fechafin:fechaFin},
						success: function(response){
							
							$('#tblBuscarProyectos tbody').empty();
							$('#tblBuscarProyectos tbody').append(response);
							$("#tblBuscarProyectos tbody tr").on('click',function(){
								var codigo = $(this).find('td').eq(0).text();
								var usuario = $(this).find('td').eq(8).text().toUpperCase();
								var usuarioactual = '<?php echo $_SESSION['usuario']?>'.toUpperCase();
								
								if ("<?php echo $_SESSION['CodigoPosicion']?>" == '36'||"<?php echo $_SESSION['CodigoPosicion']?>" == '46' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'||"<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'||"<?php echo $_SESSION['CodigoPosicion']?>" == '50'){
									postForm('masterproyectosseguimiento.php', {valor: codigo, revision: -1});
								}
								else if (usuario==usuarioactual)
								{
									postForm('masterproyectosseguimiento.php', {valor: codigo, revision: -1});
								}
								else
								{
								}
								
								
							});
						},
					});

				}else {
					$('#tblBuscarProyectos tbody').empty();
				}
			});
		
$("#btnGuardar").on('click', function(){
				
				var Id = $('#cdoc').val();
				var cliente = $('#codcliente').val();
				var nombre = $('#NombreC').val();
				var personaContacto = $('#Contacto').val();
				var direccion = $('#Direccion').val();
				var telefono = $('#Telefono').val();
				var correo = $('#Correo').val();
				var tipopropuesta = $('#tpropuesta').val();
				var asesorventas = $('#AsesorV').val();
				var administradorventas = $('#AdminV').val();
				var prioridad = $('#Prioridad').val();
				var ventasdirectivas = $('#vDirectivas').val();	
				var observaciones =  $('#Observaciones').val();
				var observacionesvolumetria =  $('#observacionesvolumetria').val();
				var NombreProyecto = $('#NombreProyecto').val();
				var Ubicacion = $('#Ubicacion').val();
				var FechaCompromiso = $('#FechaCompromiso').val();
				var Proyectista = $('#Proyectista').val();
				var ProyectistaDom = $('#ProyectistaDom').val();
				var Coordinador = $('#Coordinador').val();
				var TipoProyecto = $('#TipoProyecto').val();
				var Siglas = $('#Siglas').val();
				var NoProyecto = $('#NoProyecto').val();
				var Revision = $('#Revision').val();
				var Fechaventa = $('#FechaVenta').val();
				var Fechaentrega = $('#FechaEntrega').val();
				var NombreProyecto = $('#NombreProyecto').val();
				var Ubicacion = $('#Ubicacion').val();
				var Techo = $('#TechoFinanciero').val();
				var Observacionesproyectos = $('#ObservacionesProyectos').val();
				var observacionescostos =  $('#observacionescostos').val();
			
				$.ajax({
					type:'post',
					url: "OFVConsultasMaster/masterasignaproyecto.php",
					data:{
						
						Id: Id,
						cliente: cliente,
						nombre: nombre,
						personaContacto: personaContacto,
						direccion: direccion,
						telefono: telefono,
						correo: correo,
						tipopropuesta: tipopropuesta,
						asesorventas: asesorventas,
						administradorventas: administradorventas,
						prioridad: prioridad,
						ventasdirectivas: ventasdirectivas,
						observaciones: observaciones,
						NombreProyecto: NombreProyecto,
						Ubicacion: Ubicacion,
						FechaCompromiso: FechaCompromiso,
						Proyectista: Proyectista,
						ProyectistaDom: ProyectistaDom,
						Coordinador: Coordinador,
						TipoProyecto: TipoProyecto,
						Siglas: Siglas,
						NoProyecto: NoProyecto,
						Estatus: 'VOLUMETRIA',
						Revision: Revision,
						observacionesvolumetria: observacionesvolumetria,
						Fechaventa: Fechaventa,
						Fechaentrega: Fechaentrega,
						Techo: Techo,
						Observacionesproyectos: Observacionesproyectos,
						Observacionescostos: observacionescostos,
						Accion: 'GUARDAR',
						
					},
					success: function(resp){
						
						alert('Se guardo correctamente el proyecto');
							
						
					}
				});
				 postForm('seguimiento.php');
			});
			function wait(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
			}
			$("#consulta1Atras").on('click', function(){
				var cdoc = $("#cdoc").val();
				
				var usuario = '';
				if ("<?php echo $_SESSION['CodigoPosicion']?>" == '36'||"<?php echo $_SESSION['CodigoPosicion']?>" == '46' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '51' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'||"<?php echo $_SESSION['CodigoPosicion']?>" == '50'){
					cdoc = cdoc -1;
					usuario = 'coordinador';
					var orden = "ASC"
				$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaPrimerUltimoRegistro.php', 
								dataType:'json',
								data:{ orden: orden, usuario:usuario },
								success: function(response){
									if (cdoc > 0)
									{
								postForm('masterproyectosseguimiento.php', {valor: cdoc, revision: -1});
									
									}
									
								},
				});
				}
				else
				{
					usuario = '<?php echo $_SESSION['usuario']?>';
					var adelanteatras = 'ATRAS';
					 //var usuario = '<?php echo $_SESSION['usuario']?>';
					 		$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaatrasadelanteusuario.php',
								dataType:'json',
								data:{ 
								id: cdoc,
								adelanteatras: adelanteatras,
								usuario: usuario,
								},
								success: function(response){
									if (response['IdProyecto'] != null)
									{
										if (cdoc >= response['IdProyecto']){
										postForm('masterproyectosseguimiento.php', {valor: response['IdProyecto'], revision: -1});
									}
									}
									
										
								},
							});
				}
				
			});

			$("#consulta1Adelante").on('click', function(){
				var cdoc = $("#cdoc").val();
				
				var usuario = '';
				if ("<?php echo $_SESSION['CodigoPosicion']?>" == '36'||"<?php echo $_SESSION['CodigoPosicion']?>" == '46' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '51' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'||"<?php echo $_SESSION['CodigoPosicion']?>" == '50'){
					cdoc = parseInt(cdoc) + 1;
					usuario = 'coordinador';
					var orden = "DESC"
				$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaPrimerUltimoRegistro.php', 
								dataType:'json',
								data:{ orden: orden, usuario:usuario },
								success: function(response){
								
									if (response['IdProyecto'] != null)
									{
									if (cdoc <= response['IdProyecto'])
									{
										postForm('masterproyectosseguimiento.php', {valor: cdoc, revision: -1});
									}
									}
								},
				});
				}
				else
				{
					
					usuario = '<?php echo $_SESSION['usuario']?>';
					 var adelanteatras = 'ADELANTE';
					 //var usuario = '<?php echo $_SESSION['usuario']?>';
					 		$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaatrasadelanteusuario.php',
								dataType:'json',
								data:{ 
								id: cdoc,
								adelanteatras: adelanteatras,
								usuario: usuario,
								},
								success: function(response){
									if (response['IdProyecto'] != null)
									{
									if (cdoc <= response['IdProyecto']){
										postForm('masterproyectosseguimiento.php', {valor: response['IdProyecto'], revision: -1});
									}
									}
										
								},
							});
				}
				
			});
			function consultaOFV(folio, rev) {
				var cdoc = folio;
				var rev = rev;	
												
								$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaAtrasAdelante.php', 
								dataType:'json',
								data:{ cdoc: cdoc },
								success: function(response){
									$("#codcliente").val(response['Cliente']).prop("disabled", true);
									$("#NombreC").val(response['NombreCliente']).prop("disabled", true);
									$("#Contacto").val(response['Contacto']);
									$("#Prioridad").val(response['Prioridad']);
									$("#Direccion").val(response['Direccion']);
									$("#Telefono").val(response['Telefono']);
									$("#Correo").val(response['Email']);
									$("#AsesorV").val(response['AsesorVentas']);
									$("#AdminV").val(response['AdminVentas']);
									$("#cdoc").val(response['IdProyecto']).prop("disabled", true);
									$("#idproyecto").val(response['IdProyecto']).prop("disabled", true);
									$("#vDirectivas").val(response['VentasDirectivas']);
									$("#tpropuesta").val(response['TipoPropuesta']);
									$("#Observaciones").val(response['Observaciones']);
									$("#NombreProyecto").val(response['NombreProyecto']);
									$("#FechaVenta").val(response['FechaVenta']);
									$("#FechaEntrega").val(response['FechaEntrega']);
									$("#NombreProyecto").val(response['NombreProyecto']);
									$("#Ubicacion").val(response['Ubicacion']);
									$("#TechoFinanciero").val(response['TechoFinanciero']);
									$("#ObservacionesProyectos").val(response['ObservacionesProyectos']);
									$("#observacionesvolumetria").val(response['observacionesvolumetria']);
									$("#Estatus").val(response['Estatus']).prop("disabled", true);
									$("#FechaCompromiso").val(response['FechaCompromiso']);
									$("#Proyectista").val(response['Proyectista']);
									$("#ProyectistaDom").val(response['ProyectistaDom']);
									$("#Coordinador").val(response['Coordinador']);
									$("#Siglas").val(response['Siglas']);
									$("#TipoProyecto").val(response['TipoProyecto']);
									$("#FechaCompromiso").val(response['FechaCompromiso']);
									$("#NoProyecto").val(response['NoProyecto']).prop("disabled", true);
									$("#Complejidad").val(response['Complejidad']).prop("disabled", true);
									$("#Revision").val(response['Revision']).prop("disabled", true);
												
									$("#observacionescostos").val(response['observacionescostos']);
									var Estatus = response['Estatus'];
									
									
									
									if (Estatus == 'TERMINADO')
										{
											$("#btnOferta").prop("disabled", false);
											$("#btnCotizacion").prop("disabled", false);
											$("#btnConcluir").prop("disabled", false);
											$("#btnActualizaPrecioVenta").prop("disabled", false);
											
										}
										else
										{
											$("#btnOferta").prop("disabled", true);
											$("#btnCotizacion").prop("disabled", true);
											$("#btnConcluir").prop("disabled", true);
											$("#btnActualizaPrecioVenta").prop("disabled", true);
										}
									
									if (rev == -1)
									{
										if (response['Revision'] == '')
										{
											rev = '0';
										}
										else
										{
										rev = response['Revision'];
										}
										
										$("#Revisiones").val(rev);
									}
									else
									{
										$("#Revisiones").val(rev);
									
										$("#Revisiones").val(rev.toString());
									
									}
									var revisionletra = rev;
									
									if (rev == ''){
								 $("#Revision").val(0).prop("disabled", true);
								 revisionletra = '0';
								 }
									else
									{
										var revisionentera = parseInt(rev)+0;
										$("#Revision").val(revisionentera).prop("disabled", true);
										// $("#NoProyecto").val(response['NoProyecto']).prop("disabled", true);
										revisionletra = rev;
										$("#Revisiones").val(rev);
										//alert(revisionletra);
									}
									
									
									var valor = $('#cdoc').val();
										
										$.ajax({
											url: 'OFVConsultasMaster/buscadorArchivosAdjuntossineliminar.php',
											type: 'post',
											data: {valor: valor},
											success: function(response){
												$("#Adjuntos tbody").empty();
												$("#Adjuntos tbody").append(response);
											 }
										});
										
										var code = $('#Siglas').val();
										var TipoPropuesta = response['TipoPropuesta'];
										
										$.ajax({
											url: 'ofvConsultasMaster/buscacodigosiglas.php',
											type: 'post',
											data: {TipoPropuesta: TipoPropuesta},
											success: function(response){
												
												 var siglas = response;
												 
												 var revision = 'R' + revisionletra;
												
												 $("#NoProyecto").val(code+'-'+siglas+'-'+revision).prop("disabled", true);
												 //$("#NoProyecto").val(code.'-'.siglas.'-'.revision);
											}
										});
											$.ajax({
											url: 'ofvConsultasMaster/buscatipodecambio.php',
											type: 'post',
											
											success: function(response){
												
																						
												 $("#tipocambio").val(response).prop("disabled", true);
												 
											}
										});
									$.ajax({
										type:'post',
										url:'ofvConsultasMaster/consultaAtrasAdelante5.php',
										data: { IdProyecto: cdoc, tipocodigo:'PROTEGIDO', revision:'<?php echo $_SESSION['revision'];?>'},
										success: function(res){
												
											if (res.length == 12)
											{
												
											}
												else
												{
											$("#detalleprotegidos tbody").empty();
											$("#detalleprotegidos tbody").append(res);
											
											calculartotal();
											
											
											}
											
										}
									});
							
									
										
									$.ajax({
										type:'post',
										url:'ofvConsultasMaster/consultaAtrasAdelante5.php',
										data: { IdProyecto: cdoc, tipocodigo:'EDITABLE', revision:'<?php echo $_SESSION['revision'];?>'},
										success: function(res){
											if (res.length==12)
											{
												
											}
											else
											{
											 $("#detallenoprotegidos tbody").empty();
											$("#detallenoprotegidos tbody").append(res);
											calculartotal();
										
											}
										}
									});
									$.ajax({
										type:'post',
										url:'OFVConsultasMaster/buscadorseguimientos.php',
										data: {valor: cdoc},
										success: function(res){
											
											$("#detalleseguimiento tbody").empty();
											$("#detalleseguimiento tbody").append(res);
											
										}
									});
									
									$.ajax({
										type:'post',
										url:'OFVConsultasMaster/buscadorlimiteautorizacion.php',
										dataType:'json',
										success: function(res){
												$("#limiteautorizacionc").val(res['limiteautorizacion'].toFixed(2));
												$("#limiteautorizacion").val('$' +res['limiteautorizacion'].toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')).prop("disabled", true);
											}
										
									});
									
									
									
										
									$("#modalBuscarProyectos").modal('hide');
								},
								});
							
						

			}
		
			$("#consultaPrimerRegistro").on('click', function(){
				var usuario = '';
				if ("<?php echo $_SESSION['CodigoPosicion']?>" == '36'||"<?php echo $_SESSION['CodigoPosicion']?>" == '46' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '51' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'||"<?php echo $_SESSION['CodigoPosicion']?>" == '50'){
					usuario = 'coordinador';
				}
				else
				{
					usuario = '<?php echo $_SESSION['usuario']?>';
				}
				var orden = "ASC"
				
				$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaPrimerUltimoRegistro.php',
								dataType:'json',
								data:{ orden: orden, usuario:usuario },
								success: function(response){
										postForm('masterproyectosseguimiento.php', {valor: response['IdProyecto'], revision: -1});
										//consultaOFV(response['IdProyecto']);
								},
				});
				
				
			});
			$("#consultaUltimoRegistro").on('click', function consultaUltimoRegistro(){
				var usuario = '';
				if ("<?php echo $_SESSION['CodigoPosicion']?>" == '36'||"<?php echo $_SESSION['CodigoPosicion']?>" == '46' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '51' ||"<?php echo $_SESSION['CodigoPosicion']?>" == '52'||"<?php echo $_SESSION['CodigoPosicion']?>" == '53'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'||"<?php echo $_SESSION['CodigoPosicion']?>" == '50'){
					usuario = 'coordinador';
				}
				else
				{
					usuario = '<?php echo $_SESSION['usuario']?>';
				}
				var orden = "DESC"
				
				$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaPrimerUltimoRegistro.php', 
								dataType:'json',
								data:{ orden: orden, usuario:usuario },
								success: function(response){
									postForm('masterproyectosseguimiento.php', {valor: response['IdProyecto'], revision: -1});
										
								},
				});
			});
			

			

			

			$(document).on('click', '#eliminarFila', function (event) {
				
				  event.preventDefault();
				  var currentRow=$(this).closest("tr"); 
				  var valorEscrito=currentRow.find("td:eq(1)").text();
				  $(this).closest('tr').remove();
				  
				    respuestas(valorEscrito);
					
				
			});
			function respuestas(Id){
								 
				   $.ajax({
						type: "post",
						url: "OFVConsultasMaster/eliminaseguimiento.php?valor="+Id,

						success  : function(data){
							 //window.location.href="nomina.php";
						},
						error    : function(){
							
						}

					});
			}

		
			$("#btnPdf").on('click', function(){
				var cdoc = $("#cdoc").val();
				window.open('reportes/pdfOfv.php?folio='+cdoc, "nombre de la ventana", "width=1024, height=325");
			});

		</script>
	</html>
