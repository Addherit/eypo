	<?php

	include "db/Utils.php";
	session_start();
	$_SESSION['usuario'];





	?>

	<!DOCTYPE html>
	<html>
		<head>
			<meta charset="UTF-8">
			<meta http-equiv="X-UA-Compatible" content="IE=Edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>EYPO</title>
			<script src="Chart.js/Chart.bundle.js"></script>
			<script src="Chart.js/samples/utils.js"></script>
			<style>
		canvas {
			-moz-user-select: none;
			-webkit-user-select: none;
			-ms-user-select: none;
		}
	</style>
		</head>


		<body>

		<?php include "header.php"; ?>

		<?php include "modales.php"; ?>

		<div class="container" id="estadisticas">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Estadisticas:</h3>
				</div>

			</div>
			<div class="row datosEnc" style="font-size: .7rem">
				<div class="col-md-12">


<div class="row" id="base" style="margin-bottom: 30px">

						 <div class="col-md-4">
									<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Proyectista:</label>
									<select name="" id="Proyectista" style="width: 100%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<?php
											$sql ="SELECT iv.NombreEmpleadoVC+ ' ' + isnull(firstName,'') + ' ' + isnull(middleName,'') + ' ' + isnull(lastName,'') as Nombre, iv.* FROM IV_EY_PV_EmpleadosVentasCompras iv
left outer join OHEM e on iv.CodigoEmpleadoVC   = e.salesPrson where iv.NombreEmpleadoVC like 'P%'";
											$consulta = sqlsrv_query($conn, $sql);
											while ($row = sqlsrv_fetch_array($consulta)) { ?>
												<option value="<?php echo utf8_encode($row['NombreEmpleadoVC']); ?>"> <?php echo utf8_encode($row['Nombre']); ?> </option>
												<?php } ?>

									</select>
						</div>
						 <div class="col-md-4">
						 <label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Mes:</label>
									<select name="" id="mes" style="width: 100%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<option value="1">Enero</option>
										<option value="2">Febrero</option>
										<option value="3">Marzo</option>
										<option value="4">Abril</option>
										<option value="5">Mayo</option>
										<option value="6">Junio</option>
										<option value="7">Julio</option>
										<option value="8">Agosto</option>
										<option value="9">Septiembre</option>
										<option value="10">Octubre</option>
										<option value="11">Noviembre</option>
										<option value="12">Diciembre</option>


									</select>
									</div>
								<div class="col-md-4">
								<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Año:</label>
									<select name="" id="año" style="width: 100%; height:23px;">
										<option value="" disabled selected>Seleccionar</option>

										<option value="2019">2019</option>
										<option value="2020">2020</option>
										<option value="2021">2021</option>
										<option value="2022">2022</option>
										<option value="2023">2023</option>
										<option value="2024">2024</option>
										<option value="2025">2025</option>
										<option value="2026">2026</option>
										<option value="2027">2027</option>
										<option value="2028">2028</option>
										<option value="2029">2029</option>
										<option value="2030">2030</option>


									</select>
						   </div>

			</div>


						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnProyectistas" title="Crear">Consultar Graficas Proyectista Seleccionado</button>
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnRegresar" title="Crear">Regresar a Estadisticas Generales</button>
			<div class="container" id="contenedorgraficas">
			<br>
			<div class="row" id="graficas1" width="100%" >

						 <div class="col-md-4">
									<label for="" class="col-sm-9 col-form-label">Metas de Proyectos en el mes:</label>
									<div class="col-sm-3">
									<input type="text" class="" name="Meta" id="Meta" style="width: 70%">
									</div>
									<label for="" class="col-sm-9 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Proyectos Realizados en el mes:</label>
									<div class="col-sm-3">
									<input type="text" class="" name="proyectostotal" id="proyectostotal" style="width: 70%">

									</div>
									<br>

									<label for="" class="col-sm-9 col-form-label" style="padding-right: 0px; padding-bottom:  0px">ProyectosA (+ de 3 días):</label>
									<div class="col-sm-3">
									<input type="text" class="" name="proyectosa" id="proyectosa" style="width: 70%">

									</div>
									<label for="" class="col-sm-9 col-form-label" style="padding-right: 0px; padding-bottom:  0px">ProyectosB (2-3 días)::</label>
									<div class="col-sm-3">
									<input type="text" class="" name="proyectosb" id="proyectosb" style="width: 70%">

									</div>
									<label for="" class="col-sm-9 col-form-label" style="padding-right: 0px; padding-bottom:  0px">ProyectosC (1 día)::</label>
									<div class="col-sm-3">
									<input type="text" class="" name="proyectosc" id="proyectosc" style="width: 70%">

									</div>
						</div>
						 <div class="col-md-4">
						 <label for="" class="col-sm-6 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Meta de Proyectos:</label>
						<div id="canvas-holder">
									<canvas id="chart-area"></canvas>
						</div>

						</div>
								<div class="col-md-4">
								<label for="" class="col-sm-6 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Desgloce Proy. Realizado:</label>
									<div id="canvas-holder">
									<canvas id="chart-area2"></canvas>
						</div>

						</div>
			</div>

			<div class="row" id="graficas2" width="100%" >

						 <div class="col-md-4">
									<label for="" class="col-sm-9 col-form-label" style="padding-right: 0px; padding-bottom:  0px"># Proy. Entregados en TYF:</label>
									<div class="col-sm-3">
									<input type="text" class="" name="proyectostf" id="proyectostf" style="width: 70%">

									</div>
									<label for="" class="col-sm-9 col-form-label" style="padding-right: 0px; padding-bottom:  0px"># Proy. Retrasados:</label>
									<div class="col-sm-3">
									<input type="text" class="" name="proyectosr" id="proyectosr" style="width: 70%">

									</div>
									<br>
									<label for="" class="col-sm-9 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Proy. Interior:</label>
									<div class="col-sm-3">
									<input type="text" class="" name="proyectosin" id="proyectosin" style="width: 70%">

									</div>
									<label for="" class="col-sm-9 col-form-label" style="padding-right: 0px; padding-bottom:  0px">c/control:</label>
									<div class="col-sm-3">
									<input type="text" class="" name="proyectoscon" id="proyectoscon" style="width: 70%">

									</div>
						</div>
						 <div class="col-md-4">
						 <label for="" class="col-sm-6 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Entrega TYF:</label>
						<div id="canvas-holder">
									<canvas id="chart-area3"></canvas>
						</div>

						</div>
								<div class="col-md-4">
								<label for="" class="col-sm-7 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Proyectos de Interior c/ Control:</label>
									<div id="canvas-holder">
									<canvas id="chart-area4"></canvas>
						</div>

						</div>
			</div>

			<div class="row" id="graficas3" width="100%" >

						 <div class="col-md-4">
									<label for="" class="col-sm-9 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Meta Visitas x Mes:</label>
									<div class="col-sm-3">
									<input type="text" class="" name="metavisitas" id="metavisitas" style="width: 70%">

									</div>
									<label for="" class="col-sm-9 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Visitas Realizadas:</label>
									<div class="col-sm-3">
									<input type="text" class="" name="visitasrealizadas" id="visitasrealizadas" style="width: 70%">

									</div>
									<br>
									<label for="" class="col-sm-9 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Calificacion:</label>
									<div class="col-sm-3">
									<input type="text" class="" name="calificacion" id="calificacion" style="width: 70%">

									</div>

						</div>
						 <div class="col-md-4">
						 <label for="" class="col-sm-6 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Visitas:</label>
						<div id="canvas-holder">
									<canvas id="chart-area5"></canvas>
						</div>

						</div>
								<div class="col-md-4">
								<label for="" class="col-sm-7 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Proyectos de Interior c/ Control:</label>
									<div>
										<canvas id="canvas"></canvas>
									</div>

						</div>
			</div>





			</div>
			</div>
			</div>
			</div>

		<?php include "footer.php"; ?>
	</body>
		<script>
		var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};

		var config = {
			type: 'pie',
			data: {
				datasets: [{
					data: [
						$('#Meta').val(),
						$('#proyectostotal').val(),

					],
					backgroundColor: [
						window.chartColors.red,
						window.chartColors.green,

					],
					label: 'Dataset 1'
				}],
				labels: [
					'Meta',
					'Proyectos Realizados',

				]
			},
			options: {
				responsive: true
			}
		};

		var config2 = {
			type: 'pie',
			data: {
				datasets: [{
					data: [
						$('#proyectosa').val(),
						$('#proyectosb').val(),
						$('#proyectosc').val(),

					],
					backgroundColor: [
						window.chartColors.blue,
						window.chartColors.red,
						window.chartColors.green,
					],
					label: 'Dataset 1'
				}],
				labels: [
					'Proy. A',
					'Proy. B',
					'Proy. C',
				]
			},
			options: {
				responsive: true
			}
		};
		var config3 = {
			type: 'pie',
			data: {
				datasets: [{
					data: [
						$('#proyectostf').val(),
						$('#proyectosr').val(),

					],
					backgroundColor: [
						window.chartColors.green,
						window.chartColors.red,

					],
					label: 'Dataset 1'
				}],
				labels: [
					'Proyectos TYF',
					'Proyectos Retrasados',
				]
			},
			options: {
				responsive: true
			}
		};
		var config4 = {
			type: 'pie',
			data: {
				datasets: [{
					data: [
						$('#proyectosin').val(),
						$('#proyectoscon').val(),
					],
					backgroundColor: [
						window.chartColors.red,
						window.chartColors.green,
					],
					label: 'Dataset 1'
				}],
				labels: [
					'Proy. Interior',
					'Proy. Interior realizados c/control',
				]
			},
			options: {
				responsive: true
			}
		};
				var config5 = {
			type: 'pie',
			data: {
				datasets: [{
					data: [
						$('#metavisitas').val(),
						$('#visitasrealizadas').val(),
					],
					backgroundColor: [
						window.chartColors.red,
						window.chartColors.green,
					],
					label: 'Dataset 1'
				}],
				labels: [
					'Meta Visitas x mes',
					'Visitas Realizadas',

				]
			},
			options: {
				responsive: true
			}
		};
		var color = Chart.helpers.color;
		var config6 = {
			type: 'radar',
			data: {
				labels: [['Meta Proyectos'], ['Entrega', 'TYF'],  ['Ilum & Control'], 'Visitas'],
				datasets: [{
					label: 'Resultado',
					backgroundColor: color(window.chartColors.red).alpha(0.2).rgbString(),
					borderColor: window.chartColors.red,
					pointBackgroundColor: window.chartColors.red,
					data: [
						$('#Meta').val(),
						$('#proyectostf').val(),
						$("#proyectostf").val(),
						$('#visitasrealizadas').val(),

					]
				}//, {
					// label: 'My Second dataset',
					// backgroundColor: color(window.chartColors.blue).alpha(0.2).rgbString(),
					// borderColor: window.chartColors.blue,
					// pointBackgroundColor: window.chartColors.blue,
					// data: [
						// randomScalingFactor(),
						// randomScalingFactor(),
						// randomScalingFactor(),
						// randomScalingFactor()

					// ]
				//}

				]
			},
			// options: {
				// legend: {
					// position: 'top',
				// },
				// title: {
					// display: true,
					// text: 'Radar'
				// },
				// scale: {
					// ticks: {
						// beginAtZero: true
					// }
				// }
			// }
		};

var colorNames = Object.keys(window.chartColors);
		window.onload = function() {
			window.myRadar = new Chart(document.getElementById('canvas'), config6);
			var ctx = document.getElementById('chart-area').getContext('2d');
			window.myPie = new Chart(ctx, config);
			var ctx2 = document.getElementById('chart-area2').getContext('2d');
			window.myPie2 = new Chart(ctx2, config2);
			var ctx3 = document.getElementById('chart-area3').getContext('2d');
			window.myPie3 = new Chart(ctx3, config3);
			var ctx4 = document.getElementById('chart-area4').getContext('2d');
			window.myPie4 = new Chart(ctx4, config4);
			var ctx5 = document.getElementById('chart-area5').getContext('2d');
			window.myPie5 = new Chart(ctx5, config5);

		};



		// document.getElementById('randomizeData').addEventListener('click', function() {
			// config.data.datasets.forEach(function(dataset) {
				// dataset.data = dataset.data.map(function() {
					// return randomScalingFactor();
				// });
			// });
			// window.myPie.update();
		// });
			// document.getElementById('randomizeData2').addEventListener('click', function() {
			// config2.data.datasets.forEach(function(dataset) {
				// dataset.data = dataset.data.map(function() {
					// return randomScalingFactor();
				// });
			// });

			// window.myPie.update();
		// });


		// document.getElementById('addDataset').addEventListener('click', function() {
			// var newDataset = {
				// backgroundColor: [],
				// data: [],
				// label: 'New dataset ' + config.data.datasets.length,
			// };

			// for (var index = 0; index < config.data.labels.length; ++index) {
				// newDataset.data.push(randomScalingFactor());

				// var colorName = colorNames[index % colorNames.length];
				// var newColor = window.chartColors[colorName];
				// newDataset.backgroundColor.push(newColor);
			// }

			// config.data.datasets.push(newDataset);
			// window.myPie.update();
		// });

		// document.getElementById('addDataset2').addEventListener('click', function() {
			// var newDataset = {
				// backgroundColor: [],
				// data: [],
				// label: 'New dataset ' + config2.data.datasets.length,
			// };

			// for (var index = 0; index < config2.data.labels.length; ++index) {
				// newDataset.data.push(randomScalingFactor());

				// var colorName = colorNames[index % colorNames.length];
				// var newColor = window.chartColors[colorName];
				// newDataset.backgroundColor.push(newColor);
			// }

			// config2.data.datasets.push(newDataset);
			// window.myPie.update();
		// });

		// document.getElementById('removeDataset').addEventListener('click', function() {
			// config.data.datasets.splice(0, 1);
			// window.myPie.update();
		// });
		// document.getElementById('removeDataset2').addEventListener('click', function() {
			// config2.data.datasets.splice(0, 1);
			// window.myPie.update();
		// });

function postForm(path, params, method) {
    method = method || 'post';

    var form = document.createElement('form');
    form.setAttribute('method', method);
    form.setAttribute('action', path);

    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var hiddenField = document.createElement('input');
            hiddenField.setAttribute('type', 'hidden');
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}
		$("#btnRegresar").on('click', function(){
			postForm('estadisticas.php');
		});
			$("#btnProyectistas").on('click', function(){

				var Proyectista = $('#Proyectista').val();
				var Mes = $('#mes').val();
				var Año = $('#año').val();

				$.ajax({
					type:'post',
					dataType:'json',
					url: "ofvConsultasMaster/consultaEstadisticasProyectista.php",
					data:{
						Proyectista: Proyectista,
						Mes: Mes,
						Año: Año,
					},
					success: function(response){


									$("#Meta").val(response['Meta']).prop("disabled", true);
									$("#proyectostotal").val(response['ProyectosTotales']).prop("disabled", true);
									$("#proyectosa").val(response['ProyectosA']).prop("disabled", true);
									$("#proyectosb").val(response['ProyectosB']).prop("disabled", true);
									$("#proyectosc").val(response['ProyectosC']).prop("disabled", true);
									$("#proyectostf").val(response['ProyectosTF']).prop("disabled", true);
									$("#proyectosr").val(response['ProyectosR']).prop("disabled", true);
									$("#proyectosin").val(response['ProyectosIn']).prop("disabled", true);
									$("#proyectoscon").val(response['ProyectosCon']).prop("disabled", true);
									$("#metavisitas").val(response['MetaVisitas']).prop("disabled", true);
									$("#visitasrealizadas").val(response['VisitasRealizadas']).prop("disabled", true);
									$("#calificacion").val(response['Calificacion']).prop("disabled", true);
									config.data.datasets.splice(0, 1);
									var newDataset = {
											backgroundColor: [window.chartColors.red,window.chartColors.green],
											data: [$("#Meta").val(), $("#proyectostotal").val()],

									};
									config.data.datasets.push(newDataset);
									window.myPie.update();

									config2.data.datasets.splice(0, 1);
									var newDataset2 = {
											backgroundColor: [window.chartColors.blue,window.chartColors.red,window.chartColors.green],
											data: [$("#proyectosa").val(),$("#proyectosb").val(),$("#proyectosc").val()],

									};
									config2.data.datasets.push(newDataset2);
									window.myPie2.update();

									config3.data.datasets.splice(0, 1);
									var newDataset3 = {
											backgroundColor: [window.chartColors.green,window.chartColors.red],
											data: [$("#proyectostf").val(),$("#proyectosr").val()],

									};
									config3.data.datasets.push(newDataset3);
									window.myPie3.update();

									config4.data.datasets.splice(0, 1);
									var newDataset4 = {
											backgroundColor: [window.chartColors.red,window.chartColors.green],
											data: [$("#proyectosin").val(),$("#proyectoscon").val()],

									};
									config4.data.datasets.push(newDataset4);
									window.myPie4.update();

									config5.data.datasets.splice(0, 1);
									var newDataset5 = {
											backgroundColor: [window.chartColors.red,window.chartColors.green],
											data: [$("#metavisitas").val(),$("#visitasrealizadas").val()],

									};
									config5.data.datasets.push(newDataset5);
									window.myPie5.update();

									config6.data.datasets.splice(0, 1);
									var newDataset6 = {
										label: 'Resultado',
										borderColor: window.chartColors.red,
										//backgroundColor: color(newColor).alpha(0.2).rgbString(),
										//pointBorderColor: newColor,
										data: [$("#Meta").val(), $("#proyectostf").val(),response['ProyectosIlumControl'], $("#visitasrealizadas").val()],
									};
									config6.data.datasets.push(newDataset6);
									window.myRadar.update();

					}
				});



			});


$("#buscadorProyectos").keyup(function(){
				var valorEscrito = $('#buscadorProyectos').val();

				if (valorEscrito) {
					$.ajax({
						url: 'ofvConsultasMaster/buscadorGeneralAnteProyectos.php',
						type: 'post',
						data: {valor: valorEscrito, estatus: '%'},
						success: function(response){
							$('#tblBuscarProyectos tbody').empty();
							$('#tblBuscarProyectos tbody').append(response);
							$("#tblBuscarProyectos tbody tr").on('click',function(){
								var codigo = $(this).find('td').eq(0).text();

								$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaAtrasAdelante.php',
								dataType:'json',
								data:{ cdoc: codigo },
								success: function(response){

									$("#siglas").val(response['Siglas']);
									$("#tpropuesta").val(response['TipoPropuesta']);

									$("#modalBuscarProyectos").modal('hide');
								},
								});
							});
						},
					});

				}else {
					$('#tblBuscarProyectos tbody').empty();
				}
			});







		</script>
	</html>
