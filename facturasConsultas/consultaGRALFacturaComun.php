<?php  
    include "../conexion.php";
    $valor = $_POST['valorEscrito'];
    $tipo =$_POST['tipo'];
    $cont = 0;
    $sql="SELECT TOP 50 fac.FolioSAP, fac.FechaContabilizacion, fac.Estatus, soc.Nombre, fac.TipoFactura FROM EYPO.dbo.IV_EY_PV_FacturasClientesCab fac
        INNER JOIN EYPO.dbo.IV_EY_PV_SociosNegocios soc 
        ON fac.CodigoSN = soc.CodigoSN 
        WHERE (FolioSAP LIKE '%$valor%' OR Nombre LIKE '%$valor%' OR fac.CodigoSN LIKE '%$valor%') AND (TipoFactura = '$tipo') AND (SerieNombre != 'COLONIAS')
        ORDER BY FechaContabilizacion DESC";
    $consulta = sqlsrv_query($conn, $sql);

    WHILE ($row = sqlsrv_fetch_array($consulta)){
        $cont++;
        echo '
            <tr>
                <td width="70px">'.$cont.'</td>
                <td width="130px">'.utf8_encode($row['FolioSAP']).'</td>
                <td width="310px">'.utf8_encode($row['Nombre']).'</td>
                <td width="150px">'.$row['FechaContabilizacion']->format('Y-m-d').'</td>                
                <td width="110px">'.utf8_encode($row['Estatus']).'</td>
            </tr>
        ';               
    }
?>