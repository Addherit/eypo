<?php 
	include "db/Utils.php";	

	if(!empty($_GET['folio'])){
		$folioGet = $_GET['folio'];
	}
?>
<!DOCTYPE html>
	<html lang="en">
	<?php include "header.php"; ?>
	<body>	
		<?php include "nav.php"; ?>
		<?php include "modales.php"; ?>
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-sm-9">
					<h1 style="color: #2fa4e7">HISTORIAL DE ORDENES DE FABRICACIÓN</h1>
				</div>
				<div class="col-sm-3 text-right">
					<form action="arcosConsultas/crear_excel.php" method="post" target="_blank" id="FormularioExportacion">						
						<button class="btn btn-sm btn-primary" id="btn_exportar_excel">Exportar a excel</button>
						<input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
					</form>
				</div>								
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<span class="col-sm-2 col-form-label">Filtro por:</span>	
					<div class="col-sm-4">			
						<input type="text" id="buscador_historial" placeholder="Buscar"><br>										
					</div>
								
					<label class="col-sm-1 col-form-label" >Desde: </label>
					<div class="col-sm-2">
						<input type="date" id="buscador_historial_fecha">
					</div>	

					<label class="col-sm-1 col-form-label" >Hasta: </label>
					<div class="col-sm-2">
						<input type="date" id="buscador_historial_fecha_fin" value="<?php echo date('Y-m-d')?>"> <br>				
					</div>		
				</div>	
				
				<div class="col-md-12">
					<span style="font-size: .8rem; color grey">Busca por <b>Folio</b>, <b>Nombre</b>, <b>estatus</b>, <b>empresa</b> y para <b>Fecha</b>, utiliza el campo de fecha</span>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<table id="tbl_historia_ordenes" class="table table-sm table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th>N°</th>
								<th>Folio</th>
								<th>Nombre del solicitante</th>								
								<th>Numero de orden</th>
								<th>Empresa</th>
								<th>Pord. Fabricar</th>
								<th>Estatus</th>
								<th>Fecha de solicitud</th>							
							</tr>
						</thead>
						<tbody></tbody>
					</table>					
				</div>
			</div>

		</div>		
		<?php include "footer.php"; ?>
	</body>

	<script>
	var codigoCoordinador = <?php echo $CodigoPosicion ?>;
	var empresa = ''
	switch (codigoCoordinador) {
		case 48:
			empresa = 'eypo'
			break;
		case 38:
			empresa = 'novalux'
			break;
		case 39:
			empresa = 'pec'
			break;
		case 41:
			empresa = 'proton'
			break;	
		case 40:
			empresa = 'tj'
			break;	
	}

		
		function consultarTodasLasOdenesFab(){
			$.ajax({
				url:'arcosConsultas/historial_ordenes_fabricacion.php',
				type:'POST',
				data:{
					empresa: empresa
				}
				
			}).done (function (response) {
				$("#tbl_historia_ordenes tbody").empty();
				$("#tbl_historia_ordenes tbody").append(response);
			})
		}
		consultarTodasLasOdenesFab();

		$(document).ready(function() {
			$("#btn_exportar_excel").click(function(event) {
				$("#datos_a_enviar").val( $("<div>").append( $("#tbl_historia_ordenes").eq(0).clone()).html());										
				$("#FormularioExportacion").submit();					
			});
		});


		$("#buscador_historial").keyup(function () {
			var valorEscrito = $(this).val();
			if (valorEscrito) {
				$.ajax({
					url:'arcosConsultas/buscar_ordenes_historial.php',
					type:'POST',
					data:{valorEscrito: valorEscrito},
				}).done(function (response) {
					$("#tbl_historia_ordenes tbody").empty();
					$("#tbl_historia_ordenes tbody").append(response);
				})				
			} else {
				consultarTodasLasOdenesFab();
			}									
		})

		$("#buscador_historial_fecha").change(function () {
			var valorEscrito = $(this).val();
			var fechaFin = $("#buscador_historial_fecha_fin").val();

			if (valorEscrito) {
				$.ajax({
					url:'arcosConsultas/buscar_ordenes_historial_fecha.php',
					type:'POST',
					data:{valorEscrito: valorEscrito, fechaFin: fechaFin},
				}).done(function (response) {
					$("#tbl_historia_ordenes tbody").empty();
					$("#tbl_historia_ordenes tbody").append(response);
				})
			} else {
				consultarTodasLasOdenesFab();
			}				
		})

		$("#buscador_historial_fecha_fin").change(function () {
			var fechaFin = $(this).val();
			var valorEscrito = $("#buscador_historial_fecha").val();

			if (valorEscrito) {
				$.ajax({
					url:'arcosConsultas/buscar_ordenes_historial_fecha.php',
					type:'POST',
					data:{valorEscrito: valorEscrito, fechaFin: fechaFin},
				}).done(function (response) {
					$("#tbl_historia_ordenes tbody").empty();
					$("#tbl_historia_ordenes tbody").append(response);
				})
			} else {
				consultarTodasLasOdenesFab();
			}				
		})

	</script>

</html>
