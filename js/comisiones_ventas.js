var fechaS = $("#fechaS").val() ;
var fechaM = $("#fechaM").val() ;
$.ajax({
    url:"querys/comisiones.php",
    type:"post",
    data:{ 
        fechaS : fechaS,
        fechaM : fechaM,
    },
    dataType: 'JSON',
}).done((comisiones) => {

    $("#tblComisiones tbody").empty()
    var tr_order = "";

    if(comisiones.length > 0) {
        comisiones.forEach((element, index) => {
            index++
            tr_order += `<tr>
            <td>${index}</td>
            <td>${element.DocEntryPago}</td>
            <td>${element.DocEntryFac}</td>
            <td>${element.CardName}</td>
            <td>${parseFloat(element.Subtotal).toFixed(2)}</td>
            <td>${parseFloat(element.IVA).toFixed(2)}</td>
            <td>${parseFloat(element.Total).toFixed(2)}</td>
            <td>${parseFloat(element["Dias Vencimiento"]).toFixed(2)}</td>
            <td>${parseFloat(element.SubtotalPago).toFixed(2)}</td>
            <td>${parseFloat(element["IVA Pago"]).toFixed(2)}</td>
            <td>${parseFloat(element.Montodepago).toFixed(2)}</td>
            <td>${parseFloat(element["% del pago"]).toFixed(2)}</td>
            <td>${parseFloat(element["Saldo Pendiente"]).toFixed(2)}</td>
            <td>${parseFloat(element["Pagado al dia"]).toFixed(2)}</td>
            <td>${parseInt(element["%pagadoaldia"])}</td>
            <td>${parseFloat(element.L1).toFixed(2)}</td>
            <td>${parseFloat(element.L2).toFixed(2)}</td>
            <td>${parseFloat(element.L3).toFixed(2)}</td>
            <td>${parseFloat(element.L4).toFixed(2)}</td>
            <td>${parseFloat(element.L5).toFixed(2)}</td>
            <td>${parseFloat(element.L6).toFixed(2)}</td>
            <td>${parseFloat(element.L7).toFixed(2)}</td>
            <td>${parseFloat(element.L8).toFixed(2)}</td>
            <td>${parseFloat(element.L9).toFixed(2)}</td>
            <td>${parseFloat(element.L10).toFixed(2)}</td>
            <td>${element["Vendedro Principal"]}</td>
            <td>${element["Comision Principal"]}</td>
            <td>${element["Vendedor Adicional"]}</td>
            <td>${element["Comision Adicional"]}</td>
            <td>${element["Promotor de ventas"]}</td>
            <td>${element["Comision Promotor"]}</td>
            <td>${element["Coordinador de Ventas"]}</td>
            <td>${element["Comision Coordinador de Ventas"]}</td>
            <td>${element.Proyecto}</td>
            <td>${element["Proyectista 1"]}</td>
            <td>${element["Comision Proytectista 1"]}</td>
            <td>${element["Proyectista 2"]}</td>
            <td>${element["Comision Proytectista 2"]}</td>
            <td>${element["Proyectista 3"]}</td>
            <td>${element["Comision Proytectista 3"]}</td>
            <td>${element["Proyectista 4"]}</td>
            <td>${element["Comision Proytectista 4"]}</td>
            <td>${element["Proyectista 5"]}</td>
            <td>${element["Comision Proytectista 5"]}</td>
            <td>${element["Coordinador de proyecto"]}</td>
            <td>${element["Comision coordinador de Proyecto"]}</td>         
            </tr>`
        })
    } else {
        tr_order += '<tr><td colspan="17">No existe registro alguno con los datos proporcionados</td></tr>' 
    }    
    $("#tblComisiones tbody").append(tr_order);			
});	
