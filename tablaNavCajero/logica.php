<?php

$month = date('m');
$day = date('d');
$year = date('Y');

$today = $year . '-' . $month . '-' . $day;

$FechaInicio = date( "Y-m-d", strtotime( "$today -1 month" ) );
?>
<br>
						<div class="row">
						    <div class="col-md-6">
						        <div class="row">
								<div class="col-sm-4">
						        	<label>Fecha Inicio:</label>

										<input type="date" onchange="buscarMovimientosFecha();" id="FechaIC" value="<?php echo $FechaInicio; ?>">
									</div>
									<div class="col-sm-4">
									<label>Fecha Fin:</label>

										<input type="date" onchange="buscarMovimientosFecha();" id="FechaFC" value="<?php echo $today; ?>">
									</div>
									<div class="col-sm-4" >
									<label>Tipo:</label>

												<select id="tipo" onchange="buscarMovimientosFecha();">
													<option value="todos">Todos</option>
														<option value="sale_order_payment">Pago de Orden de Venta</option>
														<option value="cancelled_sale_order_payment">Pago de Orden de Venta Cancelado</option>
														<option value="sale_order_payment_cancelled_for_inactivity">Pago de Orden de Venta Cancelado por Inactividad</option>
														<option value="incomplete_sale_order_payment">Pago de Orden de Venta Incompleto</option>
														<option value="payment">Pago de Servicio</option>
														<option value="cancelled_payment">Pago de Servicio Cancelado</option>
														<option value="payment_cancelled_for_inactivity">Pago de Servicio Cancelado Por Inactividad</option>
														<option value="incomplete_payment">Pago de Servicio Incompleto</option>
														<option value="withdrawal">Retiro de Cashbox</option>
														<option value="salary_withdrawal">Retiro de Nomina</option>
														<option value="incomplete_salary_withdrawal">Retiro de Nomina Incompleto</option>
														<option value="full_withdrawal">Retiro Total</option>
														<option value="supplyment">Suministro</option>
														<option value="cobro_por_deuda">Cobro por deuda</option>
														<option value="pago_por_deuda">Pago por deuda</option>
												</select>
									 </div>
						        </div>
						    </div>

						</div>
						<br>

						<br>
						<button onclick="exportTableToExcel('detallemovimientos')">Exportar a Excel</button>
						<div class="row">
					  		<div class="col-md-12">
					  			<table class="table-bordered table-editable table table-hover table-striped table-responsive" width="100%" id="detallemovimientos" >
					        		<thead>
					        			<tr class="encabezado" >
											<th>Numero</th>
					        				<th>Id</th>
											<th>Tipo</th>
					        				<th>Cantidad</th>
											<th>FechaCreacion</th>
					        				<th>Usuarios</th>
											<th>Detalles</th>
											<th>Cajero</th>
					        			</tr>
					        		</thead>
					        		<tbody>
							        	<tr>
											<td class="Numero"></td>
								            <td class="Id"></td>
											<td class="Type"></td>
								            <td class="Cantidad"></td>
											<td class="CreatedAt"></td>
								            <td class="Users"></td>
											<td class="Details"></td>
											<td class="Name"></td>
										</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>
<br>
