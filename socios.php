<?php include "conexion.php"; ?>

<!DOCTYPE html>
<html>
	<?php include "header.php"?>
<body>

	<?php include "nav.php"?>
	<?php include "modales.php"?>
	<br>

	<div class="container formato-font-design" id="contenedorDePagina">
		<div class="row">
			<div class="col-10">
				<h1 style="color: #2fa4e7">Datos maestros socio negocios</h1>
			</div>
			<div id="btnEnca" class="col-2 text" style="font-size: 2rem">
				<a href="#" class="btn-default btn-sm" id="btnbusquedaGeneralSocio" data-toggle="modal" data-target="#myModal"><i class="fa fa-binoculars fa-2x" aria-hidden="true" style="color: #6E736D;" title="Búsqueda"></i></a>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-6">
				<div class="row">
					<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Codigo:</label>
					<div class="col-6">
						<input type="text" class="" id="codcliente"  disabled>						
					</div>
				</div>
				<div class="row">
					<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Nombre:</label><br>
					<div class="col-6">
					    <input type="text" id="NombreC" disabled>					    	
					</div>
				</div>
				<div class="row">
					<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Grupo:</label>
					<div class="col-6">
					    </select>
						<input type="text" id="grupo" disabled>
					</div>
				</div>
				<div class="row">
					<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Moneda:</label>
					<div class="col-6">
						<input type="text"id="moneda"  disabled>
					</div>
				</div>
				<div class="row">
					<label for="" class="col-sm-3 col-form-label">RFC</label>
					<div class="col-6">
						<input type="text" id="rfc" disabled>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Saldo de cuenta:</label>
					<div class="col-6">
						<input type="text" class="text-right" value="0.00" id="saldoCuenta" disabled>
					</div>
				</div>  
				<div class="row">
					<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Entregas:</label>
					<div class="col-6">
						<input type="text" class="text-right" value="0.00" id="saldoEntrega" disabled>
					</div>
				</div>
				<div class="row">
					<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Pedidos de cliente:</label>
					<div class="col-6">
						<input type="text" class="text-right" value="0.00" id="saldoPedidos" disabled>
					</div>
				</div>					
				<div class="row">
					<div class="col-md-7 offset-md-4" style="margin-top: 10px">
						<br>
						<label style="font-size: 1.1rem; color: #2fa4e9">Campos definidos por el usuario</label>		
					</div>
				</div>
				<div class="row">
					<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Ref depositos MXP:</label>
					<div class="col-6">
						<input type="text" id="referenciaMxp" disabled>
					</div>
				</div>
				<div class="row">
					<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Ref depositos USD:</label>
					<div class="col-6">
						<input type="text" id="referenciaUSD" disabled>
					</div>
				</div>					
			</div>
		</div>
		<br>



	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">General</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="profile-tab" data-toggle="tab" href="#logica" role="tab" aria-controls="logica" aria-selected="false">Personas de contacto</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="contact-tab" data-toggle="tab" href="#finanzas" role="tab" aria-controls="finanzas" aria-selected="false">Direcciones</a>
				</li>
			</ul>
			<div class="tab-content" id="nav-tabContent">
				<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
					<br>
					<div class="row datosEnc">
						<div class="col-md-6">
							<div class="row">
								<label for="" class="col-sm-3 col-form-label" >Teléfono 1  :</label>
								<div class="col-sm-6">
									<input type="text" id="telefono1" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Teléfono 2  :</label>
								<div class="col-sm-6">
									<input type="text" id="telefono2" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Correo electrónico:</label>
								<div class="col-sm-6">
									<input type="text" id="correoElectronico" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Tipo socio negocios:</label>
								<div class="col-sm-6">
									<input type="text" id="tipo" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Condiciones de pago:</label>
								<div class="col-sm-6">
									<input type="text" id="condicionesPago" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Lista de precios:</label>
								<div class="col-sm-6">
									<input type="text" id="listaPrecios" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Limite de credito:</label>
								<div class="col-sm-6">
									<input type="text" id="limiteCredito" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Limite comprometido:</label>
								<div class="col-sm-6">
									<input type="text" id="limiteComprometido" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Comentarios:</label>
								<div class="col-sm-4">
									<textarea id="comentarios" cols="70" rows="3" style="background-color: #ffff002e;"></textarea>
								</div>
							</div>
						</div>
						<div class="col-md-6	">
							<div class="row">
								<label class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Persona de contacto:</label>
								<div class="col-6">
									<input type="text" id="personaContacto" style="width: 100%" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Curp:</label>
								<div class="col-6">
									<input type="text" id="curp" style="width: 100%" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Vendedor asignado:</label>
								<div class="col-6">
									<input type="text" id="Vendedor" style="width: 100%" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Federal unificado:</label>
								<div class="col-6">
									<input type="text" id="idFiscalFederalUnificado" style="width: 100%" disabled>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane fade text-left" id="logica">
					<br>
					<div class="row datosEnc">
						<div class="col-md-6">
			
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">ID de contacto:</label>
								<div class="col-sm-6">
									<input type="text" id="idConta" disabled>
									<a href="#" onclick="consultaPersonaContacto()"  data-toggle="modal" data-target="#modalPersonasContacto">
										<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cliente"></i>
									</a>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Nombre:</label>
								<div class="col-sm-6">
									<input type="text" id="NomC"disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Cargo:</label>
								<div class="col-sm-6">
									<input type="text" id="cargos" disabled>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="tab-pane fade" id="finanzas">
					<br>
					<div class="row datosEnc">
						<div class="col-md-6">
							<div class="row">
								<label for="" class="col-sm-3 col-form-label"><u>Destino:</u></label>
								<div class="col-sm-7">
									<a class="button noticias" id="urlMapaDireccion" target="_blank">Mostrar ubicacion en explorador web</a>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Código:</label>
								<div class="col-sm-6">
									<input type="text" id="idDireccion" disabled>									
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Id dirección:</label>
								<div class="col-sm-6">
									<input type="text" id="idd" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Calle/Numero:</label>
								<div class="col-sm-6">
									<input type="text" id="calle" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Ciudad:</label>
								<div class="col-sm-6">
									<input type="text" id="ciudad" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label">Codigo postal:</label>
								<div class="col-sm-6">
									<input type="text" id="cpostal" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Colonia:</label>
								<div class="col-sm-6">
									<input type="text" id="colonia" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Estado:</label>
								<div class="col-sm-6">
									<input type="text" id="estado" disabled>
								</div>
							</div>
							<div class="row">
								<label for="" class="col-sm-3 col-form-label" style="padding-right: 0px; padding-bottom:  0px">Pais:</label>
								<div class="col-sm-6">
									<input type="text" id="pais" disabled>
								</div>
							</div>
						</div>
						<div class="col-md-6">																		
				
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



<!-- Modal Cotizaciones-->
 <div class="col"></div>
</div>
<div class="contenedor-modal">
</div>

<div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3>Resumen de mensajes/alertas</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

      </div>
      <div class="modal-body">
        <div class="tab">
  <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">Carpeta Entrada</button>
  <button class="tablinks" onclick="openCity(event, 'Paris')">Carpeta Salida</button>
  <button class="tablinks" onclick="openCity(event, 'Tokyo')">Mensajes Enviados</button>
</div>
<div id="London" class="tabcontent">


<table class="table-bordered table-editable table-sm table-hover overflow-y: auto" width="100%" id="tblAsuntoOferta" style="font-size: .7rem">
	<thead>
		<tr>
	    <th scope="col">!</th>
	    <th scope="col">Asunto</th>
	    <th scope="col">Fecha y hora</th>
	    <th scope="col">Desde</th>
	  </tr>
	</thead>
	<tbody>
		<?php
		      $cont=0;
		      $sql = "SELECT CONVERT(varchar(50), RecDate) as RecDate, Subject
					FROM OAIB
					INNER JOIN OALR ON OAIB.AlertCode = OALR.Code
					WHERE OAIB.UserSign = '36'";
		      $consultasql = sqlsrv_query($conn, $sql) ;
					while ($Row = sqlsrv_fetch_array($consultasql)) {						

						?>
		<tr>
				<td></td>
		 		<td><?php echo $algo= utf8_encode($Row['Subject']);?></td>
				<td> <?php echo $Row['RecDate']; ?> </td>
				<td></td>


		</tr>
		 <?php }?>
	</tbody>
</table>
</div>

<table class="table table-bordered table-striped table-sm table-hover overflow-y: auto" id="tblAsuntoOfertaCodigo">
	<thead>
		<th>#</th>
		<th>N°</th>
		<th>Doducumento preliminar</th>
		<th>Clase de documento</th>
	</thead>
	<tbody>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tbody>
</table>

<div id="Paris" class="tabcontent">
  <span onclick="this.parentElement.style.display='none'" class="topright">&times</span>
  <h3>Paris</h3>
  <p>Paris is the capital of France.</p>
</div><div class="modal-footer">
   <a href="#"  class="btn btn-default btn-sm"><i class="  fa fa-trash fa-2x" aria-hidden="true"></i></a>
    <a href="#"  class="btn btn-default btn-sm">Responder<i  aria-hidden="true"></i></a>
     <a href="#"  class="btn btn-default btn-sm">Transmitir<i aria-hidden="true"></i></a>
     <br><br><br><br>
      <a href="#"  class="btn btn-default btn-sm">Ausente<i aria-hidden="true"></i></a>
      <a href="#"  class="btn btn-default btn-sm">Cerrado<i aria-hidden="true"></i></a> 

      </div>
    </div>
  </div>
</div>


	    <?php include "footer.php"; ?>
	</body>
	<script type="text/javascript">		


		$("#buscadorCliente").keyup(function() {			
			if ($("#buscadorCliente").val()) {
				$.ajax({
					url: 'socios/buscadorConsultaClientesYN.php',
					type: 'POST',
					data: { valor: $("#buscadorCliente").val() },
				}).done((resp) => {
					
					$("#tblcliente tbody").empty();
					$("#tblcliente tbody").append(resp);

					$("#tblcliente tr").on('click',function(){
						var codigo = $(this).find('td').eq(1).text();
						var name = $(this).find('td').eq(2).text();
						agregar(codigo, name);
						$("#tblcliente tbody").empty().val("");	
					});   											
				});
			} else {
				$("#tblcliente tbody").empty();
			}
		})
		

		$("#myModal").on('click',function() {
			$("#tblcliente tbody").empty().val("");
		});

		function agregar($code, $name) {
			var code = $code;
			var name = $name;
			$("#codcliente").val(code);
			$("#NombreC").val(name);
			$('#myModal').modal('hide');
			agregarConta(code);
			agregarDire(code);
			$.ajax({ 
				url: 'socios/sociosSelect.php',
				type: 'POST',    
				dataType: 'JSON',
				data: { CodigoSN: code },
			}).done((response) => {
									
				$("#grupo").val(response['grupo']);
				$("#moneda").val(response['moneda']);
				$("#rfc").val(response['rfc']);
				$("#saldoCuenta").val(response['saldoCuenta']);
				$("#saldoEntrega").val(response['saldoEntrega']);
				$("#saldoPedidos").val(response['saldoPedidos']);

				$("#referenciaMxp").val(response['referenciaMXN']);
				$("#referenciaUSD").val(response['referenciaUSD']);

				$("#telefono1").val(response['telefono1']);
				$("#telefono2").val(response['telefono2']);
				$("#correoElectronico").val(response['correoElectronico']);
				$("#tipo").val(response['tipo']);
				$("#condicionesPago").val(response['condicionesPago']);
				$("#listaPrecios").val(response['listaPrecios']);
				$("#limiteCredito").val(response['limiteCredito']);
				$("#limiteComprometido").val(response['limiteComprometido']);
				$("#personaContacto").val(response['personaContacto']);
				$("#curp").val(response['curp']);
				$("#idFiscalFederalUnificado").val(response['idFiscalFederalUnificado']);
				$("#Vendedor").val(response['vendedor']);				
			});
		}		
		
		function consultaPersonaContacto() {
			
			$.ajax({
				url: 'buscadorConsultaContacto.php',
				type: 'POST',
				data: { valor: $("#codcliente").val() },
			}).done((resp) => {				
				$("#tblPersonaContacto tbody").empty().append(resp);

				$("#tblPersonaContacto tr").on('click',function() {
					var codigo = $(this).find('td').eq(1).text();
					agregarConta(codigo);
					
					$("#tblPersonaContacto tbody").empty();
					$("#buscadorPersonaContacto").val("");
				});   				
			});

		}

		function agregarConta (code){
			var code = code;
			$('#modalPersonasContacto').modal('hide');
			$.ajax({
				url: 'socios/sociosSelect2.php',
				type: 'post',    
				dataType: 'json',
				data: {CodigoSN: code},
			}).done((response) => {				
				$("#idConta").val(response['codigo']);
				$("#NomC").val(response['nombre']);
				$("#cargos").val(response['cargos']);				
			});
		}

		function typea_y_busca_cliente() {
			
			if ($("#buscadorDireccion").val()) {
				$.ajax({
					url: 'buscadorConsultaDireccion.php',
					type: 'post',
					data: {valor: $("#buscadorDireccion").val()},
				}).done((resp) => {				

					$("#tblDireccion tbody").empty().append(resp);
					$("#tblDireccion tr").on('click',function(){
						var codigo = $(this).find('td').eq(1).text();
						agregarDire(codigo);
						$("#tblDireccion tbody").empty();
					});   					
				});
			} else {
				$("#tblDireccion tbody").empty();
			}
		}

		function agregarDire (code){

			$("#idDireccion").val(code);			
			$('#myModalDireccion').modal('hide');
			$.ajax({
				url: 'socios/sociosSelect3.php',
				type: 'post',    
				dataType: 'json',
				data: {CodigoSN: code},
			}).done((response) => {
										
				var calle = response['calle']
				nuevaCalle = calle.replace("#", "")
				$("#calle").val(response['calle']);
				$("#ciudad").val(response['ciudad']);
				$("#cpostal").val(response['cpostal']);
				$("#colonia").val(response['colonia']);
				$("#estado").val(response['estado']);
				$("#pais").val(response['pais']);
				$("#idd").val(response['idd']);
				$("#urlMapaDireccion").attr('href',  'http://maps.google.com/?q=' + nuevaCalle)	
			})
		}
	</script>
</html>
