<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>EYPO</title>
	</head>
	<?php include "header.php" ?>
	<body>
		<div class="row">
			<div class="col-12">			
				<div style="overflow:auto; height: 520px;">
                <table class="table table-striped table-hover table-bordered table-sm" id="tblPedidoDia">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Num Doc</th>
                                <th scope="col">Hora</th>
                                <th scope="col">Op/Cl</th>
                                <th scope="col">Codigo</th>
                                <th scope="col">CardName</th>
                                <th scope="col">Mon.</th>
                                <th scope="col">T.Fac</th>
                                <th scope="col">DocTotal</th>
                                <th scope="col">AsVts</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">FechaEntrega</th>
                                <th scope="col">Ref</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
				</div>
				
			</div>
			<div class="col-5 offset-7">

			<a href="ofertaDeVenta.php">
				<button class="btn btn-primary btn-block">Regresar a OFV</button>
			</a>
			</div>
		</div>
		<?php include "footer.php" ?>		
	</body>                     
	<script>
        
        var fechaconta = '<?php echo $_GET["fechaconta"] ?>';
        var statusDocumento = '<?php echo $_GET["statusDocumento"] ?>';
        $.ajax({
				url:"querys/pedidosDia.php",
				type:"post",
				data:{ 
					fechaconta : fechaconta,
					statusDocumento : statusDocumento,
				},
				
			}).done(function (response) {			
				$("#tblPedidoDia tbody").append(response);				
			});	
	</script>
</html>