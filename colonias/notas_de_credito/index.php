<!DOCTYPE html>
<?php include "../header.html" ?>
<html lang="en">
	<body onload="cargar_funciones_principales()">
		<?php include "../nav.php" ?> 		
		<?php include "../../modales.php" ?>
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h1 style="color: #2fa4e7">Notas de crédito</h1>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">
					<?php include "../../botonesDeControl.php" ?>
					<input type="hidden" id="primer-registro">
					<input type="hidden" id="ultimo-registro">
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<label class="col-3 col-md-4 col-lg-3 col-form-label" >Cliente:</label>
						<div class="col-6">
							<input type="text" id="cliente">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Nombre:</label><br>
						<div class="col-6">
							<input type="text" id="nombreDelCliente">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Persona de contacto:</label>
						<div class="col-6">
							<input type="text" id="personaContacto">								
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Referencia:</label>
						<div class="col-6">
							<input type="text" class="" id="referencia">
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-4 col-lg-3 col-form-label">Tipo moneda:</label>
						<div class="col-6">
							<input type="text" id="tipoMoneda">								
						</div>
					</div>	
					<div class="row">								
						<div class="col-3 col-md-4 col-lg-3 col-form-label">
							<input type="text" id="rutaArchivos" style="display: none">	
						</div>
					</div>									
				</div>
				<div class="col-md-6"> 
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">N° Folio:</label>
						<div class="col-6">													
							<input type="text" id="cdoc" class="text-right" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Estado:</label>
						<div class="col-6">
							<input type="text" id="estado" class="text-right" value="Abierto" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha de contabilización:</label>
						<div class="col-6">
							<input type="date" value="<?php echo $hoy ?>" id="fconta" disabled>
						</div>
					</div>						
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha de vencimiento:</label>
						<div class="col-6">
							<input type="date" value="<?php echo $quincena ?>" id="fven" disabled>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Fecha del documento:</label>
						<div class="col-6">
							<input type="date" value="<?php echo $hoy ?>" id="fdoc" disabled>
						</div>
					</div>		
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Método de pago:</label>
						<div class="col-6">
							<input type="text" id="metodoPago" disabled>
						</div>
					</div>		
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Forma de pago:</label>
						<div class="col-6">
							<input type="text" id="formaPago" disabled>
						</div>
					</div>		
					<div class="row">
						<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Uso principal:</label>
						<div class="col-6">
							<input type="text" id="usoPrincipal" disabled>
						</div>
					</div>						
				</div>
			</div>				
			<div class="row datosEnc">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Contenido</a>
						</li>						
					</ul>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
						<br>
						<div class="row">
							<div class="col-md-12">
								<section class="table-responsive">
									<table class=" table table-sm table-bordered table-striped text-center" id="tblNotasDeCredito">
										<thead>
											<tr class="encabezado" >											
												<th>#</th>
												<th>Código articulo</th>
												<th style="width: 100%">Descripción de artíuclo</th>					        				
												<th>Cantidad</th>
												<th>PrecioUnitario</th>
												<th>Almacén</th>	
												<th>Comentarios de partida 1</th>
												<th>Comentarios de partida 2</th>	
												<th>Stock</th>
												<th>Comprometido</th>
												<th>Solicitado</th>																																													
											</tr>
										</thead>
										<tbody> 
											<tr>
												<!-- <th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th> -->
												<td class="cont"></td>
												<td class="narticulo"></td>
												<td class="darticulo"></td>
												<td contenteditable="true" class="cantidad"></td>
												<td class="PrecioUnitario"></td>
												<td class="almacen"></td>		
												<td contenteditable="true" class="comentario1"></td>
												<td contenteditable="true" class="comentario2"></td>
												<td class="stock"></td>
												<td class="comprometido"></td>
												<td class="solicitado"></td>																									
											</tr>
										</tbody>
									</table>
								</section>
							</div>
						</div>
						<br>
						<div class="row datosEnc">
							<div class="col-md-6">
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Empleado de ventas:</label>
									<div class="col-sm-6">
										<input type="text" id="empleado">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-sm-3 col-form-label">Propietario:</label>
									<div class="col-sm-6">
										<input type="text" id="propietario">
									</div>
								</div>
																						
								<div class="row">
									<label for="" class="col-sm-3 col-form-label" >Comentarios:</label>
									<div class="col-sm-4">
										<textarea name="" id="comentarios" cols="60" rows="4" style="background-color: #ffff002e;"></textarea>
									</div>
								</div>
							</div>

							<div class="col-md-6">								
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Subtotal:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="totalAntesDescuento">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label">Descuento:</label>
									<div class="col-6">
										<input class="text-center" value="0" type="text" id="descNum" style="width: 16%;">
										<span whidth="6%">%</span>
										<input class="text-right" value="0.00" type="text" id="descuento" style="width: 68% ">
									</div>
								</div>	
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >Anticipo total:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="anticipoTotal">
									</div>
								</div>		
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >Redondeo:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="redondeo">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >Impuesto:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="impuestoTotal">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >WImpte.retención:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="retencion">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >Total del documento:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="totalDocumento" style="width: 74%">
										<input class="text-right" type="text" id="monedaVisor" style="width: 15%">
									</div>
								</div>
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >Importe Aplicado:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="importeAplicado">
									</div>
								</div>			
								<div class="row">
									<label for="" class="col-3 col-md-5 col-lg-3 offset-md-1 offset-lg-3 col-form-label" >Saldo pendiente:</label>
									<div class="col-6">
										<input class="text-right" value="0.00" type="text" id="saldoPendiente">
									</div>
								</div>																		
							</div>								
						</div>		 				
					</div>
				</div>
				<div class="row" id= btnFoot style="margin-bottom: 30px">
					<div class="col-md-6">				                            
						<a href=""><button class="btn btn-sm" style="background-color: orange" title="Cancelar">Cancelar</button></a>					
					</div>	
					
				</div>	
			</div>						
		</div> 		
	</body>
	<?php include "../footer.html"; ?>
	<script src="js/notas_de_credito.js"></script>
</html>
