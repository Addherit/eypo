	<?php
	include "conexioncajero.php";

	session_start();
	if (!isset($_SESSION["usuario"]))
   {
      header("location: login.php");
   }
	$_SESSION['usuario'];
	$empleadoVentas = $_SESSION['usuario'];
	$hoy = date("Y-m-d");
	$mesDespues = date( "Y-m-d", strtotime( "$hoy +1 month" ) );
	$quincena = date( "Y-m-d", strtotime( "$hoy +15 days" ) );

	$sql = "SELECT lastName, firstName, middleName FROM OHEM WHERE U_IV_EY_PV_User = '$empleadoVentas'";
	$consulta = sqlsrv_query($conn2, $sql);
	while ($Row = sqlsrv_fetch_array($consulta)) {
		$nombre = $Row['firstName'];
		$sNombre = $Row['middleName'];
		$apellido = $Row['lastName'];
	}
	$nombreCompleto = $nombre . " " . $sNombre . " " . $apellido;



	?>

	<!DOCTYPE html>
	<html>
	<?php include "header.php"; ?>
	<!--<body onload="cargarEmpleados()">-->
	<body >

		<?php include "nav.php"; ?>
		<?php include "modalQuerys.php"; ?>
		<?php include "modales.php"; ?>

		<div class="container" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-6">
					<h3 style="color: #2fa4e7">Administración de Usuarios Cajero</h3>
				</div>
				<div id="btnEnca" class="col-md-6" style="font-size: 2rem">

				</div>
			</div>
			<div class="row datosEnc" style="font-size: .7rem">

			<br>
				<div class="col-md-6">
					<div class="row">
						<label class="col-sm-3 col-form-label" >Id Cajero:</label>
						<div class="col-sm-4">
							<input type="text" class="" name="codcajero" id="codcajero" style="width: 70%">
							<a id="btnCajero" href="#" data-toggle="modal" data-target="#myModalCajero">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Cajero"></i>
							</a>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Nombre Cajero:</label><br>
						<div class="col-sm-4">
							<input type="text" class="" name="NombreC" id="NombreC" style="width: 70%">
							<a href="#" data-toggle="modal" data-target="#myModalCajero" >
								<i class="fas fa-search" style="color: #57b4ea" title="Búsqueda Nombre"></i>
							</a>
						</div>
					</div>
				</div>
			<br>

			</div>
			<br>

			<div class="row" style="font-size: .7rem">
				<div class="col-md-12">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#contenido" role="tab" aria-controls="contenido" aria-selected="true">Empleados Cajero</a>
						</li>


					</ul>
					<div id="auto_content">

							</div>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="contenido" role="tabpanel" aria-labelledby="home-tab">
							<?php include "tablaNavNomina/generalusuarios.php"; ?>

						</div>
					</div>
				</div>

		</div>
	</div>

		<?php include "footer.php"; ?>
</body>

<script>
	if ( window.history.replaceState ) {
		window.history.replaceState( null, null, window.location.href );
	}

	function cambiar(){
		var pdrs = document.getElementById('file-upload').files[0].name;
		document.getElementById('info').innerHTML = pdrs;
	}

	function cargarEmpleados(){
		var id = $("#codcajero").val();
		console.log(id);
		$.ajax({
			url: 'buscadorempleados.php',
			type: 'post',
			data: {code: id},
			success: function(response){
				$("#detallenuevo tbody").empty();
				$("#detallenuevo tbody").append(response);
				}
		});
	}

	$(document).on('click', '#eliminarFila', function (event) {

			event.preventDefault();
			var currentRow=$(this).closest("tr");
			var valorEscrito=currentRow.find("td:eq(1)").text();
			$(this).closest('tr').remove();
			respuestas(valorEscrito);


	});

	function respuestas(Id){

			$.ajax({
				type: "post",
				url: "eliminaempleado.php?valor="+Id,

				success  : function(data){
						//window.location.href="nomina.php";
				},
				error    : function(){

				}

			});
	}

	$(document).on('click', '#convertiradmin', function (event) {

			event.preventDefault();
			var currentRow=$(this).closest("tr");
			var valorEscrito=currentRow.find("td:eq(1)").text();
			var admin=currentRow.find("td:eq(4)").text();

			respuestasadmin(valorEscrito, admin);


	});

	function respuestasadmin(Id, admin){


			$.ajax({
				type: "post",
				url: "convertiradmin.php?valor="+Id+"&admin="+admin,

				success  : function(data){
						window.location.href="empleados.php";
				},
				error    : function(){

				}

			});
	}

	//codigo del cajero
	$("#buscadorCajero").keyup(function(){
		var valorEscrito = $("#buscadorCajero").val();
		if (valorEscrito) {
			$.ajax({
				url: 'buscadorConsultaCajero.php',
				type: 'post',
				data: {valor: valorEscrito},
				success: function(resp){


					$("#tblcajero tbody").empty();
					$("#tblcajero tbody").append(resp);

					$("#tblcajero tr").on('click',function(){


						if (!$('#BackOrderVentas').is(':visible')) {
							var codigo = $(this).find('td').eq(1).text();
							var name = $(this).find('td').eq(2).text();


							agregarCajero(codigo, name);
							$("#tblcajero tbody").empty();
							$("#buscadorcajero").val("");

							$("#nombreCajero").val(name);
							$("#codigoCajero").val(codigo);
							$("#myModalCajero").hide();

						} else {
							var codigo = $(this).find('td').eq(1).text();
							var name = $(this).find('td').eq(2).text();
							$("#nombreCajero").val(name);
							$("#codigoCajero").val(codigo);
							$("#myModalCajero").hide();


							}
						});
					},
				});
		} else {
			$("#tblcajero tbody").empty();
		}
	});

	$("#myModalCajero").on('click',function() {
		$("#buscadorCajero").val("");
	});



	function agregar ($code, $name){
		var code = $code;
		var name = $name;
		$("#codcajero").val(code);
		$("#NombreC").val(name);
		$('#myModalCajero').modal('hide');
		$.ajax({
			url: 'consultaPersonaContacto.php',
			type: 'post',
			data: {code: code},
			success: function(response){
				$("#listcontactos").append(response);
			}
		});
	}

	function agregarCajero ($code, $name){
		var code = $code;
		var name = $name;
		$("#codcajero").val(code);
		$("#NombreC").val(name);
		$('#myModalCajero').modal('hide');
		cargarEmpleados();
	}

</script>
</html>
