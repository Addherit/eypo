<?php
    include "../conexion.php";
    $nuevoDocEntry = $_POST['codigo'];

    $cont = 0;
    $infoDetalle = [];
    $sqlDetalle = "SELECT * FROM EYPO.dbo.IV_EY_PV_BorradoresDet WHERE FolioInterno = '$nuevoDocEntry'";
    $consulta = sqlsrv_query($conn, $sqlDetalle);
    while ($row = sqlsrv_fetch_array($consulta)) {
        $cont++;  
        $linea = '<tr>
            <th><a href="#" style="color: red" id="eliminarFila"><i class="fas fa-trash-alt"></i></a></th>
            <td>'. $cont .'</td>
            <td data-toggle="modal" data-target="#myModalArticulos" class="narticulo">'.$row['CodigoArticulo'].'</td>   
            <td data-toggle="modal" data-target="#myModalArticulos" class="darticulo">'.utf8_encode($row['NombreArticulo']).'</td>   
            <td class="cantidad" contenteditable="true">'.number_format($row['Quantity'], 0, '.', '').'</td>
            <td class="precio" contenteditable="true">'.number_format($row['PrecioUnitario'], 2, '.', '').'</td>                        
            <td class="total">'.number_format($row['TotalLinea'], 2, '.', '').'</td>
            <td class="almacen">'.$row['Almacen'].'</td>
            <td class="pendiente">'.number_format($row['CantidadAbierta'], 0, '.', '').'</td>
            <td class="codigoPlanificacionSAP">'.$row['CodigoPlanificacionSat'].'</td>
            <td class="unidadMedidaSAP">'.$row['UnidadMedida'].'</td>
            <td class="comentario1" contenteditable="true">'.utf8_encode($Row['ComentarioPartida1']).'</td>
            <td class="comentario2" contenteditable="true">'.utf8_encode($Row['ComentarioPartida2']).'</td>
            <td >'.number_format($Row['EnStock'], 2, '.', ',').'</td>
            <td >'.number_format($Row['Comprometido'], 2, '.', ',').'</td>
            <td >'.number_format($Row['Solicitado'], 2, '.', ',').'</td>';

            array_push($infoDetalle, $linea);
    }   





    $sql = "SELECT TOP 1 orv.*, soc.Nombre, soc.PersonaContacto FROM EYPO.dbo.IV_EY_PV_BorradoresCab orv
        LEFT JOIN EYPO.dbo.IV_EY_PV_SociosNegocios soc ON orv.CodigoSN = soc.CodigoSN
        WHERE orv.FolioInterno = '$nuevoDocEntry'";
        $consulta = sqlsrv_query($conn, $sql);
        $Row = sqlsrv_fetch_array($consulta);

    $consultaunoatras = [
        "CodCliente" => $Row['CodigoSN'],
        "NombreC" => $Row['Nombre'],
        "ListContactos" => $Row['PersonaContacto'],
        "TipoMoneda" => $Row['Moneda'],
        "usoPrincipal" => $Row['UsoCFDi'],
        "metodoPago" => $Row['MetodoPago'],
        "formaPago" => $Row['IdFormaPago'],

        // "NDoc" => $Row['NDoc'],
        "DocNum" => $Row['FolioSAP'],
        "Status" => $Row['Estatus'],
        "FConta" => $Row['FechaContabilizacion']->format('Y-m-d'),
        "FEntrega" => $Row['FechaVencimiento']->format('Y-m-d'),
        "FDoc" => $Row['FechaDocumento']->format('Y-m-d'),
        "Fvencimiento" => $Row['FechaVencimiento']->format('Y-m-d'),

        "numLetra" => $Row['NumeroLetra'],
        "TipoFactura" => $Row['TipoFactura'],
        "Lab" => $Row['U_LAB'],
        "CondicionPago" => $Row['DescCondicionesPago'],
        "RFC" => $Row['RFC'],

        "comentarios" => $Row['Comentarios'],
        "totalDescuento" => $Row['SubTotalDocumento'],
        // "descNum" => $Row['DescNum'],
        // "desAplicado" => $Row['DesAplicado'],	
        "impuesto" => $Row['SumaImpuestos'],
        "totalDelDocumento" => $Row['TotalDocumento'],
        "detalle" => $infoDetalle,
    ];
    echo json_encode($consultaunoatras);
    ?>
