<?php
include "conexionCajero.php";
$tok = '';
$fechaInicio = $_POST['fechaInicio'];
$fechaFin = $_POST['fechaFin'];
$tipo = $_POST['tipo'];
$valor = $_POST['valor'];
$cont=0;
if($tipo == 'todos')
{
	$tipo = '%[a-z]%';
}

$sql = "Select m.Id, case m.Type when 'supplyment' then 'Suministro'
 when 'full_withdrawal' then 'Retiro Total'
 when 'sale_order_payment' then 'Pago de Orden de Venta'
 when 'payment' then 'Pago de Servicio'
 when 'incomplete_payment' then 'Pago de Servicio Incompleto'
 when 'cancelled_payment' then 'Pago de Servicio Cancelado'
 when 'payment_cancelled_for_inactivity' then 'Pago de Servicio Cancelado Por Inactividad'
 when 'withdrawal' then 'Retiro de Cashbox'
 when 'cancelled_sale_order_payment' then 'Pago de Orden de Venta Cancelado'
 when 'incomplete_sale_order_payment' then 'Pago de Orden de Venta Incompleto'
 when 'sale_order_payment_cancelled_for_inactivity' then 'Pago de Orden de Venta Cancelado por Inactividad'
 when 'salary_withdrawal' then 'Retiro de Nomina'
 when 'deposit' then 'Pago por Deuda'
 when 'incomplete_salary_withdrawal' then 'Retiro de Nomina Incompleto'
else m.Type End as Type, isnull(sum (case when d.Endpoint = 'out' then (d.Value * -1) else d.value end * d.Count),0) [Cantidad], m.CreatedAt, m.Users, Replace(Replace(m.Detalles2, CHAR(13), ''), CHAR(10), '') as Details, s.Name
from money_transaction m left outer join document d on m.IdMoneyTransaction = d.IdMoneyTransaction
inner join solution s on s.Id = m.Solution
where m.CreatedAt between '$fechaInicio' and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111) and m.Type like '$tipo' and s.Uid like '%$valor%'
and m.Type in ('sale_order_payment', 'payment', 'incomplete_payment')
group by m.Id, m.Type, m.Users, m.Detalles2, s.Name, s.Uid, m.CreatedAt
union all
Select m.Id, case m.Type when 'supplyment' then 'Suministro'
 when 'full_withdrawal' then 'Retiro Total'
 when 'sale_order_payment' then 'Pago de Orden de Venta'
 when 'payment' then 'Pago de Servicio'
 when 'incomplete_payment' then 'Pago de Servicio Incompleto'
 when 'cancelled_payment' then 'Pago de Servicio Cancelado'
 when 'payment_cancelled_for_inactivity' then 'Pago de Servicio Cancelado Por Inactividad'
 when 'withdrawal' then 'Retiro de Cashbox'
 when 'cancelled_sale_order_payment' then 'Pago de Orden de Venta Cancelado'
 when 'incomplete_sale_order_payment' then 'Pago de Orden de Venta Incompleto'
 when 'sale_order_payment_cancelled_for_inactivity' then 'Pago de Orden de Venta Cancelado por Inactividad'
 when 'salary_withdrawal' then 'Retiro de Nomina'
 when 'deposit' then 'Pago por Deuda'
 when 'incomplete_salary_withdrawal' then 'Retiro de Nomina Incompleto'
else m.Type End as Type, isnull(sum (d.Value * d.Count),0) [Cantidad], m.CreatedAt, m.Users, Replace(Replace(m.Detalles2, CHAR(13), ''), CHAR(10), '') as Details, s.Name
from money_transaction m left outer join document d on m.IdMoneyTransaction = d.IdMoneyTransaction
inner join solution s on s.Id = m.Solution
where
m.CreatedAt between '$fechaInicio' and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111) and
m.Type like '$tipo' and s.Uid like '%$valor%'
and m.Type not in ('sale_order_payment', 'payment', 'incomplete_payment', 'withdrawal', 'full_withdrawal')
group by m.Id, m.Type, m.Users, m.Detalles2, s.Name, s.Uid, m.CreatedAt
union all
Select m.Id, case m.Type when 'supplyment' then 'Suministro'
 when 'full_withdrawal' then 'Retiro Total'
 when 'sale_order_payment' then 'Pago de Orden de Venta'
 when 'payment' then 'Pago de Servicio'
 when 'incomplete_payment' then 'Pago de Servicio Incompleto'
 when 'cancelled_payment' then 'Pago de Servicio Cancelado'
 when 'payment_cancelled_for_inactivity' then 'Pago de Servicio Cancelado Por Inactividad'
 when 'withdrawal' then 'Retiro de Cashbox'
 when 'cancelled_sale_order_payment' then 'Pago de Orden de Venta Cancelado'
 when 'incomplete_sale_order_payment' then 'Pago de Orden de Venta Incompleto'
 when 'sale_order_payment_cancelled_for_inactivity' then 'Pago de Orden de Venta Cancelado por Inactividad'
 when 'salary_withdrawal' then 'Retiro de Nomina'
 when 'deposit' then 'Pago por Deuda'
 when 'incomplete_salary_withdrawal' then 'Retiro de Nomina Incompleto'
else m.Type End as Type, REPLACE(REPLACE(SUBSTRING(Detalles2,CHARINDEX(':',Detalles2)+1,LEN(Detalles2)), ' ', ''), '}', '')[Cantidad] , m.CreatedAt, m.Users, Replace(Replace(m.Detalles2, CHAR(13), ''), CHAR(10), '') as Details, s.Name
from money_transaction m left outer join document d on m.IdMoneyTransaction = d.IdMoneyTransaction
inner join solution s on s.Id = m.Solution
where m.CreatedAt between '$fechaInicio' and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111) and m.Type like '$tipo' and s.Uid like '%$valor%'
and m.Type in ('withdrawal', 'full_withdrawal')
group by m.Id, m.Type, m.Users, m.Detalles2, s.Name, s.Uid, m.CreatedAt
order by m.createdAt desc";

$sqldeuda = "select m.Id,case m.Type when 'supplyment' then 'Suministro'
 when 'full_withdrawal' then 'Retiro Total'
 when 'sale_order_payment' then 'Pago de Orden de Venta'
 when 'payment' then 'Pago de Servicio'
 when 'incomplete_payment' then 'Pago de Servicio Incompleto'
 when 'cancelled_payment' then 'Pago de Servicio Cancelado'
 when 'payment_cancelled_for_inactivity' then 'Pago de Servicio Cancelado Por Inactividad'
 when 'withdrawal' then 'Retiro de Cashbox'
 when 'cancelled_sale_order_payment' then 'Pago de Orden de Venta Cancelado'
 when 'incomplete_sale_order_payment' then 'Pago de Orden de Venta Incompleto'
 when 'sale_order_payment_cancelled_for_inactivity' then 'Pago de Orden de Venta Cancelado por Inactividad'
 when 'salary_withdrawal' then 'Retiro de Nomina'
 when 'incomplete_salary_withdrawal' then 'Retiro de Nomina Incompleto'
else m.Type End as Type,tc.value as Cantidad,tc.CreatedAt,tc.Users,
Replace(Replace(tc.Detalles2, CHAR(13), ''), CHAR(10), '') as Details,s.Name,tc.Token
 from transaction_change tc inner join solution s on s.Id = tc.Solution
 inner join money_transaction m on m.IdMoneyTransaction = tc.id
  where tc.CreatedAt between '$fechaInicio' 
and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111) 
 and s.Uid like '%$valor%'and tc.status = 'activo'
order by m.createdAt desc";

$sqlcobrados = "select m.Id,'Pago por Deuda' as Type,tc.value as Cantidad,tc.PaidAt as 'CreatedAt',tc.Users,
Replace(Replace(tc.Detalles2, CHAR(13), ''), CHAR(10), '') as Details,s.Name
 from transaction_change tc inner join solution s on s.Id = tc.Solution
 inner join money_transaction m on m.IdMoneyTransaction = tc.id
  where tc.PaidAt between '$fechaInicio' 
and CONVERT(DATETIME, CONVERT(varchar(11),'$fechaFin', 111 ) + ' 23:59:59', 111) 
 and s.Uid like '%$valor%'and tc.status <> 'activo'
order by tc.PaidAt desc";

switch ($tipo) {
  case "cobro_por_deuda":
    $consultasql = sqlsrv_query($conn,$sqldeuda);
    $tok = 'ok';
    break;
  case "pago_por_deuda":
    $consultasql = sqlsrv_query($conn,$sqlcobrados);
    break;
  default:
    $consultasql = sqlsrv_query($conn,$sql);
}

while ($Row = sqlsrv_fetch_array($consultasql)) {
  $cont++;
?>
<tr>
  <td><?php echo $cont;?></td>
  <td><?php echo $Row['Id'];?></td>
  <td><?php echo utf8_encode($Row['Type']);?></td>
  <td><?php echo number_format($Row['Cantidad'],2,'.','');?></td>
  <td><?php echo $Row['CreatedAt']->format('Y-m-d H:i:s')?></td>
  <td><?php echo utf8_encode($Row['Users']);?></td>
  <td><?php echo utf8_encode($Row['Details']);?></td>
  <td><?php echo utf8_encode($Row['Name']);?></td>
  <?php if($tok == 'ok'){
   // echo '<td><button type="button" onclick="alert('.$Row['Token'].')">QR</button></td>';
    echo '<td><button type="button" onclick="getqr('.$Row['Token'].')">QR</button></td>';
  } ?>
 </tr>
<?php } ?>
