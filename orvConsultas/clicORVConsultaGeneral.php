<?php
include "../conexion.php";
$cdoc = $_POST['codigo'];  //ORDEN DE VENTA

if (isset($_POST['ofertaVenta'])) {
  $ofertaVenta = $_POST['ofertaVenta'];
} else {

  $sql ="SELECT TOP 1 CodOferta FROM EYPO.dbo.IV_EY_PV_BorradoresCab brCab
  LEFT JOIN dbEypo.dbo.ordenes ord ON brCab.FolioInterno = ord.NuevoDocEntry
  WHERE brCab.FolioSAP = '$cdoc'"; 
  $consulta = sqlsrv_query($conn, $sql);
  $row = sqlsrv_fetch_array($consulta);
  $ofertaVenta = $row['CodOferta'];

}




  $sql = "SELECT TOP 1 orv.*, soc.Nombre, soc.PersonaContacto, ord.UsoCfdi as ucfdi, ord.MetodPago as mp, ord.FormaPago as fp
  FROM EYPO.dbo.IV_EY_PV_OrdenesVentaCab orv
  LEFT JOIN EYPO.dbo.IV_EY_PV_SociosNegocios soc ON orv.CodigoSN = soc.CodigoSN
  LEFT JOIN EYPO.dbo.IV_EY_PV_BorradoresCab brCab ON orv.FolioSAP = brCab.FolioSAP
  LEFT JOIN dbEypo.dbo.ordenes ord ON brCab.FolioInterno = ord.NuevoDocEntry
    WHERE orv.FolioSAP = '$cdoc'";

  $consulta = sqlsrv_query($conn, $sql );
  $Row = sqlsrv_fetch_array($consulta);

$consultaunoatras = [
    "CodCliente" => $Row['CodigoSN'],
    "NombreC" => $Row['Nombre'],
    "ListContactos" => $Row['PersonaContacto'],

    "OrdCompra" => $ofertaVenta,
    "TipoMoneda" => $Row['Moneda'],
    "usoPrincipal" => $Row['ucfdi'],
    "metodoPago" => $Row['mp'],
    "formaPago" => $Row['fp'],
	// "NDoc" => $Row['NDoc'],
    "DocNum" => $cdoc,
    "Status" => $Row['Estatus'],
    "FConta" => $Row['FechaContabilizacion']->format('Y-m-d'),
	// "FEntrega" => $feDate,
    "FDoc" => $Row['FechaDocumento']->format('Y-m-d'),
    "Fvencimiento" => $Row['FechaVencimiento']->format('Y-m-d'),
    "numLetra" => $Row['NumeroLetra'],
    "TipoFactura" => $Row['TipoFactura'],
    "Lab" => $Row['LAB'],
    "CondicionPago" => $Row['DescCondicionesPago'],
    "RFC" => $Row['RFC'],

    "comentarios" => $Row['Comentarios'],
    "totalDescuento" => $Row['SubTotalDocumento'],
	// "descNum" => $Row['DescNum'],
	// "desAplicado" => $Row['DesAplicado'],
	// "redondeo" => $Row['Redondeo'],
    "impuesto" => $Row['SumaImpuestos'],
    "totalDelDocumento" => $Row['TotalDocumento'],
  ];
  echo json_encode($consultaunoatras);
  ?>
