<?php
    include "../db/Utils.php";

    $empresa = $_POST['empresa'];
    $folio = $_POST['folio'];
    $fechaSolicitud = $_POST['fechaInicio'];
    $fechaEntrega = $_POST['dateFinal']; 
    $nombreSolicitante = $_POST['nombreSolicitante'];
    $nombreProyecto = $_POST['nombreProyecto'];
    $numeroOrden = $_POST['numeroDeOrden'];
    $nombreCliente = $_POST['nombreCliente'];
    $cantidadFabricar = $_POST['cantidadFabricar'];
    $entregasParciales = $_POST['entregasParciales'];
    $codigoProducto = $_POST['codigoProducto'];
    $descripcionProducto = $_POST['descripcionProducto'];
    $productosFabricarNombre = $_POST['productosFabricarNombre'];
    $terminadoFinal = $_POST['terminadoFinal'];
    $observaciones = $_POST['observaciones'];
    $updated_at = getdate();
    $d = $updated_at['mday'];
    $m = $updated_at['mon'];
    $y = $updated_at['year'];
    $hoy = "$y-$m-$d";
    $response = [];

    $SePM = $_POST['SePM'];
    $SeFt = $_POST['SeFt'];
    $SeDib = $_POST['SeDib'];
    $SeDia = $_POST['SeDia'];
    $SeOtr = $_POST['SeOtr'];

    $sql ="UPDATE dbEypo.dbo.arcos SET

    fechaSolicitud = '$fechaSolicitud',
    fechaEntrega = '$fechaEntrega',
    nombreProyecto = '$nombreProyecto',
    numeroOrden = '$numeroOrden',
    nombreCliente = '$nombreCliente',
    cantidadFabricar = '$cantidadFabricar',
    entregasParciales = '$entregasParciales',
    codigoProducto = '$codigoProducto',
    descripcionProducto = '$descripcionProducto',
    productosFabricarNombre = '$productosFabricarNombre',
    terminadoFinal = '$terminadoFinal',
    observaciones = '$observaciones',
    updated_at = '$hoy',
    SePM = '$SePM',
    SeFt = '$SeFt',
    SeDib = '$SeDib',
    SeDia = '$SeDia',
    SeOtr = '$SeOtr'

    WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'";

    $consulta = sqlsrv_query($conn, $sql);
    if ($consulta) {
        $response = ['resp' => 'ok', 'msj' => 'Actualizado correctamente'];                
    } else {
        $response = ['resp' => 'err', 'msj' => 'No se pudo actualizar, verifique su información'];        
    }

    echo json_encode($response);
?>
