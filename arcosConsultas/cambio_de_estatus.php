<?php 
    include "../db/Utils.php";
    $folio = $_POST['folio'];
    $empresa = $_POST['empresa'];
    $estatus = $_POST['estatus'];
    $response = []; 

    switch ($estatus) {
        case 'AUTORIZADA':
            $consultaSelect = sqlsrv_query($conn, "SELECT * FROM dbEypo.dbo.arcos WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'");
            $row = sqlsrv_fetch_array($consultaSelect);

            if (!$row){                
                $response = ['resp'=>'err', 'mnj'=>'Folio de orden de fabricación no encontrado'];   
            } else {            
                if (!$row['AutorizacionCoordinador']) {
                    $consulta = sqlsrv_query($conn, "UPDATE dbEypo.dbo.arcos SET AutorizacionCoordinador = 2, estatus = '$estatus' WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'");
                    if ($consulta === false) {
                        $response = ['resp'=>'err', 'mnj'=>'No se pudo autorizar la orden, favor de verificar la información'];
                    } else {                        
                        $response = ['resp'=>'ok', 'mnj'=>'AUTORIZADA correctamente'];
                    }
                } else {                    
                    $response = ['resp'=>'err', 'mnj'=>'Esta orden ya a sido AUTORIZADA anteriormente']; 
                }        
            }
            break;
        case 'SOLICITUD EN PLANTA':
                $consultaSelect = sqlsrv_query($conn, "SELECT * FROM dbEypo.dbo.arcos WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'");
                $row = sqlsrv_fetch_array($consultaSelect);
                
                if (!$row) {
                    $response = ['resp' => 'err', 'mnj' => 'Folio de orden de fabricación no encontrado'];
                } else {              
                    if ($row['AutorizacionCoordinador'] && !$row['SolicitudEnPlanta']) {
                        $consulta = sqlsrv_query($conn, "UPDATE dbEypo.dbo.arcos SET SolicitudEnPlanta = 2, estatus = '$estatus' WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'");
                        if ($consulta === false) {
                            $response = ['resp'=>'err', 'mnj'=>'No se pudo autorizar la orden, favor de verificar la información'];
                        } else {                                
                            $response = ['resp'=>'ok', 'mnj'=>'SOLICITUD EN PLANTA correctamente'];
                        }
                    }else {
                        $response = ['resp'=>'err', 'mnj'=>'Esta orden aun no está AUTORIZADA o ya está en SOLICITUD EN PLANTA'];                        
                    }
                }
            break;
        case 'PROCESO DE FABRICACION':
            $consultaSelect = sqlsrv_query($conn, "SELECT * FROM dbEypo.dbo.arcos WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'");
            $row = sqlsrv_fetch_array($consultaSelect);
            if (!$row) {                
                $response = ['resp'=>'err', 'mnj'=>'Folio de orden de fabricación no encontrado'];                        
            } else {
                if ($row['SolicitudEnPlanta'] && !$row['ProcesoDeFabricacion']) {
                    $sql = "UPDATE dbEypo.dbo.arcos SET ProcesoDeFabricacion = 2, estatus = '$estatus' WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'";
                    $consulta = sqlsrv_query($conn, $sql);

                    if ($consulta === false) {
                        $response = ['resp'=>'err', 'mnj'=>'No se pudo PROCESAR la orden, favor de verificar la información'];
                    } else {                        
                        $response = ['resp'=>'ok', 'mnj'=>'PROCESO DE FABRICACION correctamente'];
                    }              
                }else {                    
                    $response = ['resp'=>'err', 'mnj'=>'Esta orden aun no está en proceso de SOLICITUD EN PLANTA o ya está en SOLICITUD EN PLANTA'];                        
                }
            }
            break;
        case 'ENTREGA PARCIAL':

            $entregas_parciales = $_POST['entregas_parciales'];
            $observaciones_planta = $_POST['obParcial'];
            $consultaSelect = sqlsrv_query($conn, "SELECT * FROM dbEypo.dbo.arcos WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'");
            $row = sqlsrv_fetch_array($consultaSelect);
            if (!$row) {
                $response = ['resp'=>'err', 'mnj'=>'Folio de orden de fabricación no encontrado'];
            } else {
                if ($row['ProcesoDeFabricacion']) {                    
                    $consulta = sqlsrv_query($conn, "UPDATE dbEypo.dbo.arcos SET EntregaParcial = 2, estatus = 'ENTREGA PARCIAL', observacionesPlanta = '$observaciones_planta' WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'");
                    if ($consulta) {                        
                        foreach ($entregas_parciales as $tr) {
                            $string = "";
                            foreach ($tr as $td) {
                                $string .="'".str_replace("'", "''", $td)."',";
                            }
                            $string .= "'".$folio."',"."'".$empresa."'";
                            $consultaSelect = sqlsrv_query($conn, "INSERT INTO dbEypo.dbo.arcosEntregaParcial (Piezas, Fecha, DocNum, Empresa) VALUES ($string)" );
                        }
                        $response = ['resp'=>'ok', 'mnj'=>'En ENTREGA PARCIAL correctamente'];                                                
                    } else {      
                        $response = ['resp'=>'err', 'mnj'=>'No se pudo PROCESAR la orden, favor de verificar la información'];                  
                    }
                } else {                    
                    $response = ['resp'=>'err', 'mnj'=>'Esta orden aun no está en PROCESO DE FABRICACIÓN'];
                }            
            }
            break;    
        case 'ENTREGA TOTAL':
            $checado = $_POST['checado'];            
            $fecha = $_POST['fechaEntregaTotal'];                                                
            $consultaSelect = sqlsrv_query($conn, "SELECT * FROM dbEypo.dbo.arcos WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'");
            $row = sqlsrv_fetch_array($consultaSelect);
            if (!$row) {
                $response = ['resp'=>'err', 'mnj'=>'Folio de orden de fabricación no encontrado'];
            } else {
                if ($row['estatus'] === 'ENTREGA PARCIAL' && !$row['EntregaTotal'] || $row['entregasParciales'] === 'no') {                    
                    $sql = "UPDATE dbEypo.dbo.arcos SET EntregaTotal = '$checado', estatus = 'ENTREGA TOTAL', FechaEntregaTotal = '$fecha' WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'";
                    $consulta = sqlsrv_query($conn, $sql);
        
                    if ($consulta === false) {
                        $response = ['resp'=>'err', 'mnj'=>'No se pudo PROCESAR la orden, favor de verificar la información'];
                    } else {
                        echo "";
                        $response = ['resp'=>'ok', 'mnj'=>'En ENTREGA TOTAL correctamente'];
                    }                                            
                } else {                    
                    $response = ['resp'=>'err', 'mnj'=>'Esta orden aun no está en ENTREGA PARCIAL o ya está en ENTREGA TOTAL'];
                }            
            }
            break;
        case 'CERRADO':
                $logistica = $_POST['logistica'];
                $comentario_extra = $_POST['comExt'];

                $sql = "UPDATE dbEypo.dbo.arcos SET estatus = '$estatus', Logistica = '$logistica', ComentarioLogistica = '$comentario_extra'
                WHERE empresa = '$empresa' AND FolioIndependiente = '$folio'";
                $consulta = sqlsrv_query($conn, $sql);
                if ($consulta) {                    
                    $response = ['resp' => 'ok', 'mnj' => 'CERRADO correctamente'];     
                } else {                                           
                    $response = ['resp' => 'err', 'mnj' => 'No se pudo CERRAR, verifique su información']; 
                }
            break;
        
    }
    echo json_encode( $response );
?>