<?php
include "../../../db/Utils.php";
$folio = $_POST['cdoc']; //recibo el nuevo docentry
$usoPrincipal = $_POST['usoPrincipal'];
$metodoPago = str_replace("'", "''", $_POST['metodoPago']);
$formaPago = $_POST['formaPago'];
$condicionPago = $_POST['condicionPago'];
$numLetra = $_POST['numLetra'];
$tipoFactura = $_POST['tipoFactura'];
$lab = $_POST['lab'];
$rfc = $_POST['rfc'];
$hoy = date('Y-m-d');
$comentarios = $_POST['comentarios'];

$subtotal = $_POST['subtotal'];
$descuentoPorcentaje = $_POST['descuentoPorcentaje'];
$descuentoCantidad = $_POST['descuentoCantidad'];
$redondeo = $_POST['redondeo'];
$impuesto = $_POST['impuesto'];
$totalDocumento = $_POST['totalDocumento'];
            
$datoDet = $_POST['detalle'];
$updateOferta = 0;
$actualizar = 'S';
$response = [];

$sql_cab = "UPDATE dbEypo.dbo.ofertas SET UsoPrincipal = '$usoPrincipal', MetodoPago = '$metodoPago', FormaPago = '$formaPago',
numLetra = '$numLetra', TipoFactura = '$tipoFactura', Lab = '$lab', CondicionPago = '$condicionPago', RFC = '$rfc',
TotalDescuento = '$subtotal', DescNum = '$descuentoPorcentaje', DesAplicado = '$descuentoCantidad', Redondeo = '$redondeo', Impuesto = '$impuesto',  TotalDelDocumento = '$totalDocumento',
updated_at = '$hoy', 
Comentarios = '$comentarios', Actualizar = '$actualizar', EstatusActualizar = '$updateOferta' WHERE NuevoDocEntry = '$folio'";

    $consultasql = sqlsrv_query($conn, $sql_cab);
    
    if( $consultasql === false ) {        
      $response = ['resp' => 0 , 'mnj' => 'Algo falló al insertar los datos de cabecera', 'query_cab' => $sql_cab];
    } else {
      $sql1 ="SELECT DocNum FROM [dbEypo].[dbo].[ofertas] WHERE NuevoDocEntry = '$folio'";
      $consulta = sqlsrv_query( $conn, $sql1);
      $row = $Row = sqlsrv_fetch_array($consulta);
      $folio_docnum = $row['DocNum'];


      $sqlDelete ="DELETE FROM dbEypo.dbo.ofertaDet WHERE DocNum = '$folio_docnum'";
      $consultasql = sqlsrv_query($conn, $sqlDelete);

      foreach ($datoDet as $tr) {
        $string = "";
        foreach ($tr as $td) {
          $string .= "'".str_replace("'", "''", $td)."',";
        }
        $string .= "'".$folio_docnum."'";
        $sql_det = "INSERT INTO dbEypo.dbo.ofertaDet (articulo, descripcion, cantidad, precioUnidad, total, almacen, 
        cantidadPendiente, CodigoPlanificacionSAP, unidadMedidaSAP, comentarioPartida1, comentarioPartida2, stock, comprometido, solicitado,
        TreeType, lineNum, Hijo, DocNum)
        VALUES ($string)";
        $consulta2 = sqlsrv_query($conn, $sql_det);  
      }

      if( $consulta2 === false ) {
        $response = ['resp' => 0 , 'mnj' => 'Algo falló al ACTUALIZAR los datos de detalle', 'query_det' => $sql_det];        
      } else { 
        $response = ['resp' => 2 , 'mnj' => 'Se ACTUALIZÓ correctamente la oferta de venta', 'query_det' => $sql_det];
      }
    }

    echo json_encode($response);

?>
