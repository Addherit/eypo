	<?php
	
	include "conexion.php";
	session_start();
	$_SESSION['usuario'];
	

	$month = date('m');
	$day = date('d');
	$year = date('Y');
	$today = $year . '-' . $month . '-' . $day;
	$FechaInicio = date( "Y-m-d", strtotime( "$today -6 month" ) );
	
	
	?>

	<!DOCTYPE html>
	<html>
		<?php include "header.php"; ?>
		
		<body onload="cargarProyectosNR()">		
		<?php include "nav.php"; ?>		
		<?php include "modales.php"; ?>
		
		<div class="container formato-font-design" id="contenedorDePagina">
			<br>
			<div class="row">
				<div class="col-md-12">
					<h3 style="color: #2fa4e7">Revisión:</h3>
				</div>				
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<label class="col-sm-3 col-form-label" >Siglas:</label>
						<div class="col-sm-9">
							<input type="text" class="" name="siglas" id="siglas" style="width: 70%">
							<a id="btnbuscarProyecto" href="#" data-toggle="modal" data-target="#modalBuscarProyectos">
								<i class="fas fa-search" style="color: #57b4ea" aria-hidden="true" title="Búsqueda Proyecto"></i>
							</a>
						</div>
					</div>
					<br>
					<div class="row">
						<label for="" class="col-sm-3 col-form-label">Tipo de Propuesta:</label>
						<div class="col-sm-9">
							<select name="" id="tpropuesta" style="height: 23px; width: 70%">
								<option value="" disabled selected>Seleccione</option>
									<?php
								$sql = "SELECT * FROM MasterEypo.dbo.TiposPropuestas where Estatus = 'ALTA'";
								$consulta = sqlsrv_query($conn, $sql);
								while ($row = sqlsrv_fetch_array($consulta)) { ?>
											<option value="<?php echo ($row['TipoPropuesta']); ?>"><?php echo ($row['TipoPropuesta']); ?></option>
									<?php 
							} ?>
							</select>
						</div>
					</div>					
			<br>
			
			
<div class="row" id= btnFoot style="margin-bottom: 30px">
				<div class="col-md-12" align="center">
						<button class="btn btn-sm" style="background-color: #005580; color:white;" id="btnCrear" title="Crear">REGISTRAR</button>
							
					</div>
				
			</div>
			</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<h3 style="color: #2fa4e7">Solicitudes de Nuevas Revisiones</h3>
				</div>
				
			</div>
			<div class="row" style="font-size: .7rem">
				<div class="col-md-12">
					<div class="row">
							
					  		<div class="col-md-12">
					  			<table class="table-bordered table-editable table-hover table-striped table-responsive table" width="100%" id="listaespera" >
					        		<thead>
					        			<tr class="encabezado" style="background-color: #005580; color:white;" >
											<th>Ver</th>
											<th>No.</th>
											<th>Prioridad</th>
											<th>Nombre del Proyecto</th>
											<th>Fecha de Ingreso</th>
					        				<th>Tipo de propuesta</th>
											<th>Cliente</th>
					        				<th>Telefono</th>
											<th>Correo</th>
											<th>Proy</th>
											<th>ProyDom</th>
					        				<th>Asesor Ventas</th>
											<th>Estatus</th>
											
					        			</tr>
					        		</thead>
					        		<tbody> 
							        	<tr>
											<th>
											
											
											<a href="#" style="color: green" id="eliminarFila"  data-toggle="modal" data-target="#myModalCajero"><i class="fas fa-folder-open"></i></a></th>
											<td class="Id"></td> 
											<td class="Prioridad"></td> 
											<td class="NombreProyecto"></td>
								            <td class="FechaIngreso"></td>
											<td class="TipoPropuesta"></td>
								            <td class="Cliente"></td>
											<td class="Telefono"></td>
											<td class="Correo"></td>
											<td class="Proyectista"></td>
											<td class="ProyectistaDom"></td>
											<td class="AsesorVentas"></td>
											<td class="Estatus"></td>
										</tr>
				            		</tbody>
				        		</table>
					  		</div>
					  	</div>
				</div>
			
		</div>
		
		<?php include "footer.php"; ?>
	</body>
		<script>
		function cargarProyectosNR (){
			if ("<?php echo $_SESSION['CodigoPosicion']?>" == '50'||"<?php echo $_SESSION['CodigoPosicion']?>" == '51'){
				var tipopropuesta = 'dom';
			}
			else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '46'||"<?php echo $_SESSION['CodigoPosicion']?>" == '49'){
				var tipopropuesta = 'nodom';
			}
			else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '52'){
				var tipopropuesta = 'crossn';
			}
			else if ("<?php echo $_SESSION['CodigoPosicion']?>" == '53'){
				var tipopropuesta = 'crossd';
			}
			else {
				var tipopropuesta = 'normal';
			}
				
			$.ajax({
				url: 'ofvConsultasMaster/buscadorlistaespera.php',
				type: 'post',
				data: {valor:'7', tipopropuesta:tipopropuesta},
				success: function(response) {
					$("#listaespera tbody").empty();
					$("#listaespera tbody").append(response);
				}
			});
		}
		
		function postForm(path, params, method) {
    		method = method || 'post';

			var form = document.createElement('form');
			form.setAttribute('method', method);
			form.setAttribute('action', path);
			for (var key in params) {
				if (params.hasOwnProperty(key)) {
					var hiddenField = document.createElement('input');
					hiddenField.setAttribute('type', 'hidden');
					hiddenField.setAttribute('name', key);
					hiddenField.setAttribute('value', params[key]);

					form.appendChild(hiddenField);
				}
			}
			document.body.appendChild(form);
			form.submit();
		}
		
			$("#btnCrear").on('click', function(){
				
				var Siglas = $('#siglas').val();
				var TipoPropuesta = $('#tpropuesta').val();				
				if (Siglas != '')
				{
			
				$.ajax({
					type:'post',
					url: "ofvConsultasMaster/buscarevision.php",
					data:{
						TipoPropuesta: TipoPropuesta,
						Siglas: Siglas,
					},
					success: function(resp){
						console.log(resp);
						
						if (resp != '') {
							//event.preventDefault();
							postForm('masterproyectosvolumetria.php', {valor: resp, revision: -1});
						} else {
							alert("No se pudo localizar ese proyecto")
						}
					}
				});
				}
				else
				{
					alert("Debes ingresar un proyecto válido")
				}
			});
			$(document).on('click', '#eliminarFila', function (event) {
				
				 // event.preventDefault();
				  
				  var currentRow=$(this).closest("tr"); 
				  var valorEscrito=currentRow.find("td:eq(0)").text();
				  
				  postForm('masterproyectosvolumetria.php', {valor: valorEscrito, revision: -1});
				
			});

					function cargarProyectos(){
				var  fechaInicio = document.getElementById("FechaInicio").value;
				 var fechaFin = document.getElementById("FechaFin").value;
				 var valorescrito = $("#buscadorProyectos").val();
				 
				 if (valorescrito) {
				$.ajax({
					url: 'ofvConsultasMaster/buscadorGeneralAnteProyectos.php',
					type: 'post',
					data: {valor:valorescrito, fechainicio:fechaInicio, fechafin:fechaFin},
					success: function(response){
							
							$('#tblBuscarProyectos tbody').empty();
							$('#tblBuscarProyectos tbody').append(response);
							$("#tblBuscarProyectos tbody tr").on('click',function(){
								var codigo = $(this).find('td').eq(0).text();								
								$.ajax({
								type: 'post',
								url: 'ofvConsultasMaster/consultaAtrasAdelante.php', 
								dataType:'json',
								data:{ cdoc: codigo },
								success: function(response){
									$("#siglas").val(response['Siglas']);
									$("#tpropuesta").val(response['TipoPropuesta']);
									
									$("#modalBuscarProyectos").modal('hide');
								},
								});
							});
                     }
					
				});
				 }else {
					$('#tblBuscarProyectos tbody').empty();
				}
				};
$("#buscadorProyectos").keyup(function(){
				cargarProyectos();
			});
			

			

			
		
			
		</script>
	</html>
